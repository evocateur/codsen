# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.3.0 (2018-01-20)

- ✨ Various documentation and setup tweaks after we migrated to monorepo
- ✨ Setup refresh: updated dependencies and all config files using automated tools

## 1.1.0 (2018-12-14)

- ✨ Restore ava linting
- ✨ General setup refresh

## 1.0.0 (2018-10-11)

- ✨ Initial release
