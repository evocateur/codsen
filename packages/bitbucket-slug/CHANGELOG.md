# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.9.0 (2019-01-20)

- ✨ Various documentation and setup tweaks after we migrated to monorepo
- ✨ Setup refresh: updated dependencies and all config files using automated tools

## 1.3.0 (2018-12-26)

- ✨ recognise consecutive dashes ([b44ff9d](https://gitlab.com/codsen/codsen/tree/master/packages/bitbucket-slug/commits/b44ff9d))

## 1.2.0 (2018-10-14)

- ✨ Updated all dependencies and restored unit test coverage tracking: reporting in terminal and coveralls.io

## 1.1.0 (2018-06-13)

- ✨ Feature - recognise consecutive dashes surrounded by hyphens (`## Title -- is the best`)

## 1.0.0 (2018-06-13)

- ✨ First public release
