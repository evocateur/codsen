# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.4.0 (2019-06-01)

### Features

- Migrate to monorepo and some rebasing ([2015845](https://gitlab.com/codsen/codsen/commit/2015845))

## 1.4.0 (2019-04-14)

- ✨ Migrating this package to our main monorepo on GitLab
