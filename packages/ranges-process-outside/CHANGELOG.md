# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 2.1.0 (2019-06-01)

### Features

- Change of the API - ranges are inverted and each character called via cb() ([cd2e2be](https://gitlab.com/codsen/codsen/commit/cd2e2be))
- Offsets ([7ae4a8c](https://gitlab.com/codsen/codsen/commit/7ae4a8c))

## 1.0.0 (2018-01-23)

- ✨ Initial release
