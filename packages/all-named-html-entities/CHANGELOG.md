# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.0 (2019-06-01)

### Features

- Add &poud; to the list of recognised broken HTML entities ([759aa9f](https://gitlab.com/codsen/codsen/commit/759aa9f))
- Add a list of entities which are dangerous to add missing semicolon (uncertain.json) ([c783a6e](https://gitlab.com/codsen/codsen/commit/c783a6e))
- Add a list of named HTML entities not commonly supported among email consumption software ([2bc2c25](https://gitlab.com/codsen/codsen/commit/2bc2c25))
- Add more broken entities, sort the list and improve unit tests ([18e0b54](https://gitlab.com/codsen/codsen/commit/18e0b54))
- entStartsWithCaseInsensitive and entEndsWithCaseInsensitive ([b1e657b](https://gitlab.com/codsen/codsen/commit/b1e657b))
- Export all entities ([34b480a](https://gitlab.com/codsen/codsen/commit/34b480a))
- Export brokenNamedEntities ([e6a986b](https://gitlab.com/codsen/codsen/commit/e6a986b))
- minLength and maxLength of all known named HTML entities ([ec4d154](https://gitlab.com/codsen/codsen/commit/ec4d154))
- Remove broken nbsp's from the known broken entities list and start a new list ([2b532fd](https://gitlab.com/codsen/codsen/commit/2b532fd))
- Uncertain entities and other tweaks ([c13a254](https://gitlab.com/codsen/codsen/commit/c13a254))

## 1.0.0 (2019-04-02)

- ✨ First public release
