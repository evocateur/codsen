/**
 * all-named-html-entities
 * Array of all named HTML entities
 * Version: 1.1.2
 * Author: Roy Revelt, Codsen Ltd
 * License: MIT
 * Homepage: https://gitlab.com/codsen/codsen/tree/master/packages/all-named-html-entities
 */

var Aacute = "Á";
var aacute = "á";
var Abreve = "Ă";
var abreve = "ă";
var ac = "∾";
var acd = "∿";
var acE = "∾̳";
var Acirc = "Â";
var acirc = "â";
var acute = "´";
var Acy = "А";
var acy = "а";
var AElig = "Æ";
var aelig = "æ";
var af = "⁡";
var Afr = "𝔄";
var afr = "𝔞";
var Agrave = "À";
var agrave = "à";
var alefsym = "ℵ";
var aleph = "ℵ";
var Alpha = "Α";
var alpha = "α";
var Amacr = "Ā";
var amacr = "ā";
var amalg = "⨿";
var AMP = "&";
var amp = "&";
var And = "⩓";
var and = "∧";
var andand = "⩕";
var andd = "⩜";
var andslope = "⩘";
var andv = "⩚";
var ang = "∠";
var ange = "⦤";
var angle = "∠";
var angmsd = "∡";
var angmsdaa = "⦨";
var angmsdab = "⦩";
var angmsdac = "⦪";
var angmsdad = "⦫";
var angmsdae = "⦬";
var angmsdaf = "⦭";
var angmsdag = "⦮";
var angmsdah = "⦯";
var angrt = "∟";
var angrtvb = "⊾";
var angrtvbd = "⦝";
var angsph = "∢";
var angst = "Å";
var angzarr = "⍼";
var Aogon = "Ą";
var aogon = "ą";
var Aopf = "𝔸";
var aopf = "𝕒";
var ap = "≈";
var apacir = "⩯";
var apE = "⩰";
var ape = "≊";
var apid = "≋";
var apos = "'";
var ApplyFunction = "⁡";
var approx = "≈";
var approxeq = "≊";
var Aring = "Å";
var aring = "å";
var Ascr = "𝒜";
var ascr = "𝒶";
var Assign = "≔";
var ast = "*";
var asymp = "≈";
var asympeq = "≍";
var Atilde = "Ã";
var atilde = "ã";
var Auml = "Ä";
var auml = "ä";
var awconint = "∳";
var awint = "⨑";
var backcong = "≌";
var backepsilon = "϶";
var backprime = "‵";
var backsim = "∽";
var backsimeq = "⋍";
var Backslash = "∖";
var Barv = "⫧";
var barvee = "⊽";
var Barwed = "⌆";
var barwed = "⌅";
var barwedge = "⌅";
var bbrk = "⎵";
var bbrktbrk = "⎶";
var bcong = "≌";
var Bcy = "Б";
var bcy = "б";
var bdquo = "„";
var becaus = "∵";
var Because = "∵";
var because = "∵";
var bemptyv = "⦰";
var bepsi = "϶";
var bernou = "ℬ";
var Bernoullis = "ℬ";
var Beta = "Β";
var beta = "β";
var beth = "ℶ";
var between = "≬";
var Bfr = "𝔅";
var bfr = "𝔟";
var bigcap = "⋂";
var bigcirc = "◯";
var bigcup = "⋃";
var bigodot = "⨀";
var bigoplus = "⨁";
var bigotimes = "⨂";
var bigsqcup = "⨆";
var bigstar = "★";
var bigtriangledown = "▽";
var bigtriangleup = "△";
var biguplus = "⨄";
var bigvee = "⋁";
var bigwedge = "⋀";
var bkarow = "⤍";
var blacklozenge = "⧫";
var blacksquare = "▪";
var blacktriangle = "▴";
var blacktriangledown = "▾";
var blacktriangleleft = "◂";
var blacktriangleright = "▸";
var blank = "␣";
var blk12 = "▒";
var blk14 = "░";
var blk34 = "▓";
var block = "█";
var bne = "=⃥";
var bnequiv = "≡⃥";
var bNot = "⫭";
var bnot = "⌐";
var Bopf = "𝔹";
var bopf = "𝕓";
var bot = "⊥";
var bottom = "⊥";
var bowtie = "⋈";
var boxbox = "⧉";
var boxDL = "╗";
var boxDl = "╖";
var boxdL = "╕";
var boxdl = "┐";
var boxDR = "╔";
var boxDr = "╓";
var boxdR = "╒";
var boxdr = "┌";
var boxH = "═";
var boxh = "─";
var boxHD = "╦";
var boxHd = "╤";
var boxhD = "╥";
var boxhd = "┬";
var boxHU = "╩";
var boxHu = "╧";
var boxhU = "╨";
var boxhu = "┴";
var boxminus = "⊟";
var boxplus = "⊞";
var boxtimes = "⊠";
var boxUL = "╝";
var boxUl = "╜";
var boxuL = "╛";
var boxul = "┘";
var boxUR = "╚";
var boxUr = "╙";
var boxuR = "╘";
var boxur = "└";
var boxV = "║";
var boxv = "│";
var boxVH = "╬";
var boxVh = "╫";
var boxvH = "╪";
var boxvh = "┼";
var boxVL = "╣";
var boxVl = "╢";
var boxvL = "╡";
var boxvl = "┤";
var boxVR = "╠";
var boxVr = "╟";
var boxvR = "╞";
var boxvr = "├";
var bprime = "‵";
var Breve = "˘";
var breve = "˘";
var brvbar = "¦";
var Bscr = "ℬ";
var bscr = "𝒷";
var bsemi = "⁏";
var bsim = "∽";
var bsime = "⋍";
var bsol = "\\";
var bsolb = "⧅";
var bsolhsub = "⟈";
var bull = "•";
var bullet = "•";
var bump = "≎";
var bumpE = "⪮";
var bumpe = "≏";
var Bumpeq = "≎";
var bumpeq = "≏";
var Cacute = "Ć";
var cacute = "ć";
var Cap = "⋒";
var cap = "∩";
var capand = "⩄";
var capbrcup = "⩉";
var capcap = "⩋";
var capcup = "⩇";
var capdot = "⩀";
var CapitalDifferentialD = "ⅅ";
var caps = "∩︀";
var caret = "⁁";
var caron = "ˇ";
var Cayleys = "ℭ";
var ccaps = "⩍";
var Ccaron = "Č";
var ccaron = "č";
var Ccedil = "Ç";
var ccedil = "ç";
var Ccirc = "Ĉ";
var ccirc = "ĉ";
var Cconint = "∰";
var ccups = "⩌";
var ccupssm = "⩐";
var Cdot = "Ċ";
var cdot = "ċ";
var cedil = "¸";
var Cedilla = "¸";
var cemptyv = "⦲";
var cent = "¢";
var CenterDot = "·";
var centerdot = "·";
var Cfr = "ℭ";
var cfr = "𝔠";
var CHcy = "Ч";
var chcy = "ч";
var check = "✓";
var checkmark = "✓";
var Chi = "Χ";
var chi = "χ";
var cir = "○";
var circ = "ˆ";
var circeq = "≗";
var circlearrowleft = "↺";
var circlearrowright = "↻";
var circledast = "⊛";
var circledcirc = "⊚";
var circleddash = "⊝";
var CircleDot = "⊙";
var circledR = "®";
var circledS = "Ⓢ";
var CircleMinus = "⊖";
var CirclePlus = "⊕";
var CircleTimes = "⊗";
var cirE = "⧃";
var cire = "≗";
var cirfnint = "⨐";
var cirmid = "⫯";
var cirscir = "⧂";
var ClockwiseContourIntegral = "∲";
var CloseCurlyDoubleQuote = "”";
var CloseCurlyQuote = "’";
var clubs = "♣";
var clubsuit = "♣";
var Colon = "∷";
var colon = ":";
var Colone = "⩴";
var colone = "≔";
var coloneq = "≔";
var comma = ",";
var commat = "@";
var comp = "∁";
var compfn = "∘";
var complement = "∁";
var complexes = "ℂ";
var cong = "≅";
var congdot = "⩭";
var Congruent = "≡";
var Conint = "∯";
var conint = "∮";
var ContourIntegral = "∮";
var Copf = "ℂ";
var copf = "𝕔";
var coprod = "∐";
var Coproduct = "∐";
var COPY = "©";
var copy = "©";
var copysr = "℗";
var CounterClockwiseContourIntegral = "∳";
var crarr = "↵";
var Cross = "⨯";
var cross = "✗";
var Cscr = "𝒞";
var cscr = "𝒸";
var csub = "⫏";
var csube = "⫑";
var csup = "⫐";
var csupe = "⫒";
var ctdot = "⋯";
var cudarrl = "⤸";
var cudarrr = "⤵";
var cuepr = "⋞";
var cuesc = "⋟";
var cularr = "↶";
var cularrp = "⤽";
var Cup = "⋓";
var cup = "∪";
var cupbrcap = "⩈";
var CupCap = "≍";
var cupcap = "⩆";
var cupcup = "⩊";
var cupdot = "⊍";
var cupor = "⩅";
var cups = "∪︀";
var curarr = "↷";
var curarrm = "⤼";
var curlyeqprec = "⋞";
var curlyeqsucc = "⋟";
var curlyvee = "⋎";
var curlywedge = "⋏";
var curren = "¤";
var curvearrowleft = "↶";
var curvearrowright = "↷";
var cuvee = "⋎";
var cuwed = "⋏";
var cwconint = "∲";
var cwint = "∱";
var cylcty = "⌭";
var Dagger = "‡";
var dagger = "†";
var daleth = "ℸ";
var Darr = "↡";
var dArr = "⇓";
var darr = "↓";
var dash = "‐";
var Dashv = "⫤";
var dashv = "⊣";
var dbkarow = "⤏";
var dblac = "˝";
var Dcaron = "Ď";
var dcaron = "ď";
var Dcy = "Д";
var dcy = "д";
var DD = "ⅅ";
var dd = "ⅆ";
var ddagger = "‡";
var ddarr = "⇊";
var DDotrahd = "⤑";
var ddotseq = "⩷";
var deg = "°";
var Del = "∇";
var Delta = "Δ";
var delta = "δ";
var demptyv = "⦱";
var dfisht = "⥿";
var Dfr = "𝔇";
var dfr = "𝔡";
var dHar = "⥥";
var dharl = "⇃";
var dharr = "⇂";
var DiacriticalAcute = "´";
var DiacriticalDot = "˙";
var DiacriticalDoubleAcute = "˝";
var DiacriticalGrave = "`";
var DiacriticalTilde = "˜";
var diam = "⋄";
var Diamond = "⋄";
var diamond = "⋄";
var diamondsuit = "♦";
var diams = "♦";
var die = "¨";
var DifferentialD = "ⅆ";
var digamma = "ϝ";
var disin = "⋲";
var div = "÷";
var divide = "÷";
var divideontimes = "⋇";
var divonx = "⋇";
var DJcy = "Ђ";
var djcy = "ђ";
var dlcorn = "⌞";
var dlcrop = "⌍";
var dollar = "$";
var Dopf = "𝔻";
var dopf = "𝕕";
var Dot = "¨";
var dot = "˙";
var DotDot = "⃜";
var doteq = "≐";
var doteqdot = "≑";
var DotEqual = "≐";
var dotminus = "∸";
var dotplus = "∔";
var dotsquare = "⊡";
var doublebarwedge = "⌆";
var DoubleContourIntegral = "∯";
var DoubleDot = "¨";
var DoubleDownArrow = "⇓";
var DoubleLeftArrow = "⇐";
var DoubleLeftRightArrow = "⇔";
var DoubleLeftTee = "⫤";
var DoubleLongLeftArrow = "⟸";
var DoubleLongLeftRightArrow = "⟺";
var DoubleLongRightArrow = "⟹";
var DoubleRightArrow = "⇒";
var DoubleRightTee = "⊨";
var DoubleUpArrow = "⇑";
var DoubleUpDownArrow = "⇕";
var DoubleVerticalBar = "∥";
var DownArrow = "↓";
var Downarrow = "⇓";
var downarrow = "↓";
var DownArrowBar = "⤓";
var DownArrowUpArrow = "⇵";
var DownBreve = "̑";
var downdownarrows = "⇊";
var downharpoonleft = "⇃";
var downharpoonright = "⇂";
var DownLeftRightVector = "⥐";
var DownLeftTeeVector = "⥞";
var DownLeftVector = "↽";
var DownLeftVectorBar = "⥖";
var DownRightTeeVector = "⥟";
var DownRightVector = "⇁";
var DownRightVectorBar = "⥗";
var DownTee = "⊤";
var DownTeeArrow = "↧";
var drbkarow = "⤐";
var drcorn = "⌟";
var drcrop = "⌌";
var Dscr = "𝒟";
var dscr = "𝒹";
var DScy = "Ѕ";
var dscy = "ѕ";
var dsol = "⧶";
var Dstrok = "Đ";
var dstrok = "đ";
var dtdot = "⋱";
var dtri = "▿";
var dtrif = "▾";
var duarr = "⇵";
var duhar = "⥯";
var dwangle = "⦦";
var DZcy = "Џ";
var dzcy = "џ";
var dzigrarr = "⟿";
var Eacute = "É";
var eacute = "é";
var easter = "⩮";
var Ecaron = "Ě";
var ecaron = "ě";
var ecir = "≖";
var Ecirc = "Ê";
var ecirc = "ê";
var ecolon = "≕";
var Ecy = "Э";
var ecy = "э";
var eDDot = "⩷";
var Edot = "Ė";
var eDot = "≑";
var edot = "ė";
var ee = "ⅇ";
var efDot = "≒";
var Efr = "𝔈";
var efr = "𝔢";
var eg = "⪚";
var Egrave = "È";
var egrave = "è";
var egs = "⪖";
var egsdot = "⪘";
var el = "⪙";
var Element = "∈";
var elinters = "⏧";
var ell = "ℓ";
var els = "⪕";
var elsdot = "⪗";
var Emacr = "Ē";
var emacr = "ē";
var empty = "∅";
var emptyset = "∅";
var EmptySmallSquare = "◻";
var emptyv = "∅";
var EmptyVerySmallSquare = "▫";
var emsp = " ";
var emsp13 = " ";
var emsp14 = " ";
var ENG = "Ŋ";
var eng = "ŋ";
var ensp = " ";
var Eogon = "Ę";
var eogon = "ę";
var Eopf = "𝔼";
var eopf = "𝕖";
var epar = "⋕";
var eparsl = "⧣";
var eplus = "⩱";
var epsi = "ε";
var Epsilon = "Ε";
var epsilon = "ε";
var epsiv = "ϵ";
var eqcirc = "≖";
var eqcolon = "≕";
var eqsim = "≂";
var eqslantgtr = "⪖";
var eqslantless = "⪕";
var Equal = "⩵";
var equals = "=";
var EqualTilde = "≂";
var equest = "≟";
var Equilibrium = "⇌";
var equiv = "≡";
var equivDD = "⩸";
var eqvparsl = "⧥";
var erarr = "⥱";
var erDot = "≓";
var Escr = "ℰ";
var escr = "ℯ";
var esdot = "≐";
var Esim = "⩳";
var esim = "≂";
var Eta = "Η";
var eta = "η";
var ETH = "Ð";
var eth = "ð";
var Euml = "Ë";
var euml = "ë";
var euro = "€";
var excl = "!";
var exist = "∃";
var Exists = "∃";
var expectation = "ℰ";
var ExponentialE = "ⅇ";
var exponentiale = "ⅇ";
var fallingdotseq = "≒";
var Fcy = "Ф";
var fcy = "ф";
var female = "♀";
var ffilig = "ﬃ";
var fflig = "ﬀ";
var ffllig = "ﬄ";
var Ffr = "𝔉";
var ffr = "𝔣";
var filig = "ﬁ";
var FilledSmallSquare = "◼";
var FilledVerySmallSquare = "▪";
var fjlig = "fj";
var flat = "♭";
var fllig = "ﬂ";
var fltns = "▱";
var fnof = "ƒ";
var Fopf = "𝔽";
var fopf = "𝕗";
var ForAll = "∀";
var forall = "∀";
var fork = "⋔";
var forkv = "⫙";
var Fouriertrf = "ℱ";
var fpartint = "⨍";
var frac12 = "½";
var frac13 = "⅓";
var frac14 = "¼";
var frac15 = "⅕";
var frac16 = "⅙";
var frac18 = "⅛";
var frac23 = "⅔";
var frac25 = "⅖";
var frac34 = "¾";
var frac35 = "⅗";
var frac38 = "⅜";
var frac45 = "⅘";
var frac56 = "⅚";
var frac58 = "⅝";
var frac78 = "⅞";
var frasl = "⁄";
var frown = "⌢";
var Fscr = "ℱ";
var fscr = "𝒻";
var gacute = "ǵ";
var Gamma = "Γ";
var gamma = "γ";
var Gammad = "Ϝ";
var gammad = "ϝ";
var gap = "⪆";
var Gbreve = "Ğ";
var gbreve = "ğ";
var Gcedil = "Ģ";
var Gcirc = "Ĝ";
var gcirc = "ĝ";
var Gcy = "Г";
var gcy = "г";
var Gdot = "Ġ";
var gdot = "ġ";
var gE = "≧";
var ge = "≥";
var gEl = "⪌";
var gel = "⋛";
var geq = "≥";
var geqq = "≧";
var geqslant = "⩾";
var ges = "⩾";
var gescc = "⪩";
var gesdot = "⪀";
var gesdoto = "⪂";
var gesdotol = "⪄";
var gesl = "⋛︀";
var gesles = "⪔";
var Gfr = "𝔊";
var gfr = "𝔤";
var Gg = "⋙";
var gg = "≫";
var ggg = "⋙";
var gimel = "ℷ";
var GJcy = "Ѓ";
var gjcy = "ѓ";
var gl = "≷";
var gla = "⪥";
var glE = "⪒";
var glj = "⪤";
var gnap = "⪊";
var gnapprox = "⪊";
var gnE = "≩";
var gne = "⪈";
var gneq = "⪈";
var gneqq = "≩";
var gnsim = "⋧";
var Gopf = "𝔾";
var gopf = "𝕘";
var grave = "`";
var GreaterEqual = "≥";
var GreaterEqualLess = "⋛";
var GreaterFullEqual = "≧";
var GreaterGreater = "⪢";
var GreaterLess = "≷";
var GreaterSlantEqual = "⩾";
var GreaterTilde = "≳";
var Gscr = "𝒢";
var gscr = "ℊ";
var gsim = "≳";
var gsime = "⪎";
var gsiml = "⪐";
var GT = ">";
var Gt = "≫";
var gt = ">";
var gtcc = "⪧";
var gtcir = "⩺";
var gtdot = "⋗";
var gtlPar = "⦕";
var gtquest = "⩼";
var gtrapprox = "⪆";
var gtrarr = "⥸";
var gtrdot = "⋗";
var gtreqless = "⋛";
var gtreqqless = "⪌";
var gtrless = "≷";
var gtrsim = "≳";
var gvertneqq = "≩︀";
var gvnE = "≩︀";
var Hacek = "ˇ";
var hairsp = " ";
var half = "½";
var hamilt = "ℋ";
var HARDcy = "Ъ";
var hardcy = "ъ";
var hArr = "⇔";
var harr = "↔";
var harrcir = "⥈";
var harrw = "↭";
var Hat = "^";
var hbar = "ℏ";
var Hcirc = "Ĥ";
var hcirc = "ĥ";
var hearts = "♥";
var heartsuit = "♥";
var hellip = "…";
var hercon = "⊹";
var Hfr = "ℌ";
var hfr = "𝔥";
var HilbertSpace = "ℋ";
var hksearow = "⤥";
var hkswarow = "⤦";
var hoarr = "⇿";
var homtht = "∻";
var hookleftarrow = "↩";
var hookrightarrow = "↪";
var Hopf = "ℍ";
var hopf = "𝕙";
var horbar = "―";
var HorizontalLine = "─";
var Hscr = "ℋ";
var hscr = "𝒽";
var hslash = "ℏ";
var Hstrok = "Ħ";
var hstrok = "ħ";
var HumpDownHump = "≎";
var HumpEqual = "≏";
var hybull = "⁃";
var hyphen = "‐";
var Iacute = "Í";
var iacute = "í";
var ic = "⁣";
var Icirc = "Î";
var icirc = "î";
var Icy = "И";
var icy = "и";
var Idot = "İ";
var IEcy = "Е";
var iecy = "е";
var iexcl = "¡";
var iff = "⇔";
var Ifr = "ℑ";
var ifr = "𝔦";
var Igrave = "Ì";
var igrave = "ì";
var ii = "ⅈ";
var iiiint = "⨌";
var iiint = "∭";
var iinfin = "⧜";
var iiota = "℩";
var IJlig = "Ĳ";
var ijlig = "ĳ";
var Im = "ℑ";
var Imacr = "Ī";
var imacr = "ī";
var image = "ℑ";
var ImaginaryI = "ⅈ";
var imagline = "ℐ";
var imagpart = "ℑ";
var imath = "ı";
var imof = "⊷";
var imped = "Ƶ";
var Implies = "⇒";
var incare = "℅";
var infin = "∞";
var infintie = "⧝";
var inodot = "ı";
var Int = "∬";
var int = "∫";
var intcal = "⊺";
var integers = "ℤ";
var Integral = "∫";
var intercal = "⊺";
var Intersection = "⋂";
var intlarhk = "⨗";
var intprod = "⨼";
var InvisibleComma = "⁣";
var InvisibleTimes = "⁢";
var IOcy = "Ё";
var iocy = "ё";
var Iogon = "Į";
var iogon = "į";
var Iopf = "𝕀";
var iopf = "𝕚";
var Iota = "Ι";
var iota = "ι";
var iprod = "⨼";
var iquest = "¿";
var Iscr = "ℐ";
var iscr = "𝒾";
var isin = "∈";
var isindot = "⋵";
var isinE = "⋹";
var isins = "⋴";
var isinsv = "⋳";
var isinv = "∈";
var it = "⁢";
var Itilde = "Ĩ";
var itilde = "ĩ";
var Iukcy = "І";
var iukcy = "і";
var Iuml = "Ï";
var iuml = "ï";
var Jcirc = "Ĵ";
var jcirc = "ĵ";
var Jcy = "Й";
var jcy = "й";
var Jfr = "𝔍";
var jfr = "𝔧";
var jmath = "ȷ";
var Jopf = "𝕁";
var jopf = "𝕛";
var Jscr = "𝒥";
var jscr = "𝒿";
var Jsercy = "Ј";
var jsercy = "ј";
var Jukcy = "Є";
var jukcy = "є";
var Kappa = "Κ";
var kappa = "κ";
var kappav = "ϰ";
var Kcedil = "Ķ";
var kcedil = "ķ";
var Kcy = "К";
var kcy = "к";
var Kfr = "𝔎";
var kfr = "𝔨";
var kgreen = "ĸ";
var KHcy = "Х";
var khcy = "х";
var KJcy = "Ќ";
var kjcy = "ќ";
var Kopf = "𝕂";
var kopf = "𝕜";
var Kscr = "𝒦";
var kscr = "𝓀";
var lAarr = "⇚";
var Lacute = "Ĺ";
var lacute = "ĺ";
var laemptyv = "⦴";
var lagran = "ℒ";
var Lambda = "Λ";
var lambda = "λ";
var Lang = "⟪";
var lang = "⟨";
var langd = "⦑";
var langle = "⟨";
var lap = "⪅";
var Laplacetrf = "ℒ";
var laquo = "«";
var Larr = "↞";
var lArr = "⇐";
var larr = "←";
var larrb = "⇤";
var larrbfs = "⤟";
var larrfs = "⤝";
var larrhk = "↩";
var larrlp = "↫";
var larrpl = "⤹";
var larrsim = "⥳";
var larrtl = "↢";
var lat = "⪫";
var lAtail = "⤛";
var latail = "⤙";
var late = "⪭";
var lates = "⪭︀";
var lBarr = "⤎";
var lbarr = "⤌";
var lbbrk = "❲";
var lbrace = "{";
var lbrack = "[";
var lbrke = "⦋";
var lbrksld = "⦏";
var lbrkslu = "⦍";
var Lcaron = "Ľ";
var lcaron = "ľ";
var Lcedil = "Ļ";
var lcedil = "ļ";
var lceil = "⌈";
var lcub = "{";
var Lcy = "Л";
var lcy = "л";
var ldca = "⤶";
var ldquo = "“";
var ldquor = "„";
var ldrdhar = "⥧";
var ldrushar = "⥋";
var ldsh = "↲";
var lE = "≦";
var le = "≤";
var LeftAngleBracket = "⟨";
var LeftArrow = "←";
var Leftarrow = "⇐";
var leftarrow = "←";
var LeftArrowBar = "⇤";
var LeftArrowRightArrow = "⇆";
var leftarrowtail = "↢";
var LeftCeiling = "⌈";
var LeftDoubleBracket = "⟦";
var LeftDownTeeVector = "⥡";
var LeftDownVector = "⇃";
var LeftDownVectorBar = "⥙";
var LeftFloor = "⌊";
var leftharpoondown = "↽";
var leftharpoonup = "↼";
var leftleftarrows = "⇇";
var LeftRightArrow = "↔";
var Leftrightarrow = "⇔";
var leftrightarrow = "↔";
var leftrightarrows = "⇆";
var leftrightharpoons = "⇋";
var leftrightsquigarrow = "↭";
var LeftRightVector = "⥎";
var LeftTee = "⊣";
var LeftTeeArrow = "↤";
var LeftTeeVector = "⥚";
var leftthreetimes = "⋋";
var LeftTriangle = "⊲";
var LeftTriangleBar = "⧏";
var LeftTriangleEqual = "⊴";
var LeftUpDownVector = "⥑";
var LeftUpTeeVector = "⥠";
var LeftUpVector = "↿";
var LeftUpVectorBar = "⥘";
var LeftVector = "↼";
var LeftVectorBar = "⥒";
var lEg = "⪋";
var leg = "⋚";
var leq = "≤";
var leqq = "≦";
var leqslant = "⩽";
var les = "⩽";
var lescc = "⪨";
var lesdot = "⩿";
var lesdoto = "⪁";
var lesdotor = "⪃";
var lesg = "⋚︀";
var lesges = "⪓";
var lessapprox = "⪅";
var lessdot = "⋖";
var lesseqgtr = "⋚";
var lesseqqgtr = "⪋";
var LessEqualGreater = "⋚";
var LessFullEqual = "≦";
var LessGreater = "≶";
var lessgtr = "≶";
var LessLess = "⪡";
var lesssim = "≲";
var LessSlantEqual = "⩽";
var LessTilde = "≲";
var lfisht = "⥼";
var lfloor = "⌊";
var Lfr = "𝔏";
var lfr = "𝔩";
var lg = "≶";
var lgE = "⪑";
var lHar = "⥢";
var lhard = "↽";
var lharu = "↼";
var lharul = "⥪";
var lhblk = "▄";
var LJcy = "Љ";
var ljcy = "љ";
var Ll = "⋘";
var ll = "≪";
var llarr = "⇇";
var llcorner = "⌞";
var Lleftarrow = "⇚";
var llhard = "⥫";
var lltri = "◺";
var Lmidot = "Ŀ";
var lmidot = "ŀ";
var lmoust = "⎰";
var lmoustache = "⎰";
var lnap = "⪉";
var lnapprox = "⪉";
var lnE = "≨";
var lne = "⪇";
var lneq = "⪇";
var lneqq = "≨";
var lnsim = "⋦";
var loang = "⟬";
var loarr = "⇽";
var lobrk = "⟦";
var LongLeftArrow = "⟵";
var Longleftarrow = "⟸";
var longleftarrow = "⟵";
var LongLeftRightArrow = "⟷";
var Longleftrightarrow = "⟺";
var longleftrightarrow = "⟷";
var longmapsto = "⟼";
var LongRightArrow = "⟶";
var Longrightarrow = "⟹";
var longrightarrow = "⟶";
var looparrowleft = "↫";
var looparrowright = "↬";
var lopar = "⦅";
var Lopf = "𝕃";
var lopf = "𝕝";
var loplus = "⨭";
var lotimes = "⨴";
var lowast = "∗";
var lowbar = "_";
var LowerLeftArrow = "↙";
var LowerRightArrow = "↘";
var loz = "◊";
var lozenge = "◊";
var lozf = "⧫";
var lpar = "(";
var lparlt = "⦓";
var lrarr = "⇆";
var lrcorner = "⌟";
var lrhar = "⇋";
var lrhard = "⥭";
var lrm = "‎";
var lrtri = "⊿";
var lsaquo = "‹";
var Lscr = "ℒ";
var lscr = "𝓁";
var Lsh = "↰";
var lsh = "↰";
var lsim = "≲";
var lsime = "⪍";
var lsimg = "⪏";
var lsqb = "[";
var lsquo = "‘";
var lsquor = "‚";
var Lstrok = "Ł";
var lstrok = "ł";
var LT = "<";
var Lt = "≪";
var lt = "<";
var ltcc = "⪦";
var ltcir = "⩹";
var ltdot = "⋖";
var lthree = "⋋";
var ltimes = "⋉";
var ltlarr = "⥶";
var ltquest = "⩻";
var ltri = "◃";
var ltrie = "⊴";
var ltrif = "◂";
var ltrPar = "⦖";
var lurdshar = "⥊";
var luruhar = "⥦";
var lvertneqq = "≨︀";
var lvnE = "≨︀";
var macr = "¯";
var male = "♂";
var malt = "✠";
var maltese = "✠";
var map = "↦";
var mapsto = "↦";
var mapstodown = "↧";
var mapstoleft = "↤";
var mapstoup = "↥";
var marker = "▮";
var mcomma = "⨩";
var Mcy = "М";
var mcy = "м";
var mdash = "—";
var mDDot = "∺";
var measuredangle = "∡";
var MediumSpace = " ";
var Mellintrf = "ℳ";
var Mfr = "𝔐";
var mfr = "𝔪";
var mho = "℧";
var micro = "µ";
var mid = "∣";
var midast = "*";
var midcir = "⫰";
var middot = "·";
var minus = "−";
var minusb = "⊟";
var minusd = "∸";
var minusdu = "⨪";
var MinusPlus = "∓";
var mlcp = "⫛";
var mldr = "…";
var mnplus = "∓";
var models = "⊧";
var Mopf = "𝕄";
var mopf = "𝕞";
var mp = "∓";
var Mscr = "ℳ";
var mscr = "𝓂";
var mstpos = "∾";
var Mu = "Μ";
var mu = "μ";
var multimap = "⊸";
var mumap = "⊸";
var nabla = "∇";
var Nacute = "Ń";
var nacute = "ń";
var nang = "∠⃒";
var nap = "≉";
var napE = "⩰̸";
var napid = "≋̸";
var napos = "ŉ";
var napprox = "≉";
var natur = "♮";
var natural = "♮";
var naturals = "ℕ";
var nbsp = " ";
var nbump = "≎̸";
var nbumpe = "≏̸";
var ncap = "⩃";
var Ncaron = "Ň";
var ncaron = "ň";
var Ncedil = "Ņ";
var ncedil = "ņ";
var ncong = "≇";
var ncongdot = "⩭̸";
var ncup = "⩂";
var Ncy = "Н";
var ncy = "н";
var ndash = "–";
var ne = "≠";
var nearhk = "⤤";
var neArr = "⇗";
var nearr = "↗";
var nearrow = "↗";
var nedot = "≐̸";
var NegativeMediumSpace = "​";
var NegativeThickSpace = "​";
var NegativeThinSpace = "​";
var NegativeVeryThinSpace = "​";
var nequiv = "≢";
var nesear = "⤨";
var nesim = "≂̸";
var NestedGreaterGreater = "≫";
var NestedLessLess = "≪";
var NewLine = "\n";
var nexist = "∄";
var nexists = "∄";
var Nfr = "𝔑";
var nfr = "𝔫";
var ngE = "≧̸";
var nge = "≱";
var ngeq = "≱";
var ngeqq = "≧̸";
var ngeqslant = "⩾̸";
var nges = "⩾̸";
var nGg = "⋙̸";
var ngsim = "≵";
var nGt = "≫⃒";
var ngt = "≯";
var ngtr = "≯";
var nGtv = "≫̸";
var nhArr = "⇎";
var nharr = "↮";
var nhpar = "⫲";
var ni = "∋";
var nis = "⋼";
var nisd = "⋺";
var niv = "∋";
var NJcy = "Њ";
var njcy = "њ";
var nlArr = "⇍";
var nlarr = "↚";
var nldr = "‥";
var nlE = "≦̸";
var nle = "≰";
var nLeftarrow = "⇍";
var nleftarrow = "↚";
var nLeftrightarrow = "⇎";
var nleftrightarrow = "↮";
var nleq = "≰";
var nleqq = "≦̸";
var nleqslant = "⩽̸";
var nles = "⩽̸";
var nless = "≮";
var nLl = "⋘̸";
var nlsim = "≴";
var nLt = "≪⃒";
var nlt = "≮";
var nltri = "⋪";
var nltrie = "⋬";
var nLtv = "≪̸";
var nmid = "∤";
var NoBreak = "⁠";
var NonBreakingSpace = " ";
var Nopf = "ℕ";
var nopf = "𝕟";
var Not = "⫬";
var not = "¬";
var NotCongruent = "≢";
var NotCupCap = "≭";
var NotDoubleVerticalBar = "∦";
var NotElement = "∉";
var NotEqual = "≠";
var NotEqualTilde = "≂̸";
var NotExists = "∄";
var NotGreater = "≯";
var NotGreaterEqual = "≱";
var NotGreaterFullEqual = "≧̸";
var NotGreaterGreater = "≫̸";
var NotGreaterLess = "≹";
var NotGreaterSlantEqual = "⩾̸";
var NotGreaterTilde = "≵";
var NotHumpDownHump = "≎̸";
var NotHumpEqual = "≏̸";
var notin = "∉";
var notindot = "⋵̸";
var notinE = "⋹̸";
var notinva = "∉";
var notinvb = "⋷";
var notinvc = "⋶";
var NotLeftTriangle = "⋪";
var NotLeftTriangleBar = "⧏̸";
var NotLeftTriangleEqual = "⋬";
var NotLess = "≮";
var NotLessEqual = "≰";
var NotLessGreater = "≸";
var NotLessLess = "≪̸";
var NotLessSlantEqual = "⩽̸";
var NotLessTilde = "≴";
var NotNestedGreaterGreater = "⪢̸";
var NotNestedLessLess = "⪡̸";
var notni = "∌";
var notniva = "∌";
var notnivb = "⋾";
var notnivc = "⋽";
var NotPrecedes = "⊀";
var NotPrecedesEqual = "⪯̸";
var NotPrecedesSlantEqual = "⋠";
var NotReverseElement = "∌";
var NotRightTriangle = "⋫";
var NotRightTriangleBar = "⧐̸";
var NotRightTriangleEqual = "⋭";
var NotSquareSubset = "⊏̸";
var NotSquareSubsetEqual = "⋢";
var NotSquareSuperset = "⊐̸";
var NotSquareSupersetEqual = "⋣";
var NotSubset = "⊂⃒";
var NotSubsetEqual = "⊈";
var NotSucceeds = "⊁";
var NotSucceedsEqual = "⪰̸";
var NotSucceedsSlantEqual = "⋡";
var NotSucceedsTilde = "≿̸";
var NotSuperset = "⊃⃒";
var NotSupersetEqual = "⊉";
var NotTilde = "≁";
var NotTildeEqual = "≄";
var NotTildeFullEqual = "≇";
var NotTildeTilde = "≉";
var NotVerticalBar = "∤";
var npar = "∦";
var nparallel = "∦";
var nparsl = "⫽⃥";
var npart = "∂̸";
var npolint = "⨔";
var npr = "⊀";
var nprcue = "⋠";
var npre = "⪯̸";
var nprec = "⊀";
var npreceq = "⪯̸";
var nrArr = "⇏";
var nrarr = "↛";
var nrarrc = "⤳̸";
var nrarrw = "↝̸";
var nRightarrow = "⇏";
var nrightarrow = "↛";
var nrtri = "⋫";
var nrtrie = "⋭";
var nsc = "⊁";
var nsccue = "⋡";
var nsce = "⪰̸";
var Nscr = "𝒩";
var nscr = "𝓃";
var nshortmid = "∤";
var nshortparallel = "∦";
var nsim = "≁";
var nsime = "≄";
var nsimeq = "≄";
var nsmid = "∤";
var nspar = "∦";
var nsqsube = "⋢";
var nsqsupe = "⋣";
var nsub = "⊄";
var nsubE = "⫅̸";
var nsube = "⊈";
var nsubset = "⊂⃒";
var nsubseteq = "⊈";
var nsubseteqq = "⫅̸";
var nsucc = "⊁";
var nsucceq = "⪰̸";
var nsup = "⊅";
var nsupE = "⫆̸";
var nsupe = "⊉";
var nsupset = "⊃⃒";
var nsupseteq = "⊉";
var nsupseteqq = "⫆̸";
var ntgl = "≹";
var Ntilde = "Ñ";
var ntilde = "ñ";
var ntlg = "≸";
var ntriangleleft = "⋪";
var ntrianglelefteq = "⋬";
var ntriangleright = "⋫";
var ntrianglerighteq = "⋭";
var Nu = "Ν";
var nu = "ν";
var num = "#";
var numero = "№";
var numsp = " ";
var nvap = "≍⃒";
var nVDash = "⊯";
var nVdash = "⊮";
var nvDash = "⊭";
var nvdash = "⊬";
var nvge = "≥⃒";
var nvgt = ">⃒";
var nvHarr = "⤄";
var nvinfin = "⧞";
var nvlArr = "⤂";
var nvle = "≤⃒";
var nvlt = "<⃒";
var nvltrie = "⊴⃒";
var nvrArr = "⤃";
var nvrtrie = "⊵⃒";
var nvsim = "∼⃒";
var nwarhk = "⤣";
var nwArr = "⇖";
var nwarr = "↖";
var nwarrow = "↖";
var nwnear = "⤧";
var Oacute = "Ó";
var oacute = "ó";
var oast = "⊛";
var ocir = "⊚";
var Ocirc = "Ô";
var ocirc = "ô";
var Ocy = "О";
var ocy = "о";
var odash = "⊝";
var Odblac = "Ő";
var odblac = "ő";
var odiv = "⨸";
var odot = "⊙";
var odsold = "⦼";
var OElig = "Œ";
var oelig = "œ";
var ofcir = "⦿";
var Ofr = "𝔒";
var ofr = "𝔬";
var ogon = "˛";
var Ograve = "Ò";
var ograve = "ò";
var ogt = "⧁";
var ohbar = "⦵";
var ohm = "Ω";
var oint = "∮";
var olarr = "↺";
var olcir = "⦾";
var olcross = "⦻";
var oline = "‾";
var olt = "⧀";
var Omacr = "Ō";
var omacr = "ō";
var Omega = "Ω";
var omega = "ω";
var Omicron = "Ο";
var omicron = "ο";
var omid = "⦶";
var ominus = "⊖";
var Oopf = "𝕆";
var oopf = "𝕠";
var opar = "⦷";
var OpenCurlyDoubleQuote = "“";
var OpenCurlyQuote = "‘";
var operp = "⦹";
var oplus = "⊕";
var Or = "⩔";
var or = "∨";
var orarr = "↻";
var ord = "⩝";
var order = "ℴ";
var orderof = "ℴ";
var ordf = "ª";
var ordm = "º";
var origof = "⊶";
var oror = "⩖";
var orslope = "⩗";
var orv = "⩛";
var oS = "Ⓢ";
var Oscr = "𝒪";
var oscr = "ℴ";
var Oslash = "Ø";
var oslash = "ø";
var osol = "⊘";
var Otilde = "Õ";
var otilde = "õ";
var Otimes = "⨷";
var otimes = "⊗";
var otimesas = "⨶";
var Ouml = "Ö";
var ouml = "ö";
var ovbar = "⌽";
var OverBar = "‾";
var OverBrace = "⏞";
var OverBracket = "⎴";
var OverParenthesis = "⏜";
var par = "∥";
var para = "¶";
var parallel = "∥";
var parsim = "⫳";
var parsl = "⫽";
var part = "∂";
var PartialD = "∂";
var Pcy = "П";
var pcy = "п";
var percnt = "%";
var period = ".";
var permil = "‰";
var perp = "⊥";
var pertenk = "‱";
var Pfr = "𝔓";
var pfr = "𝔭";
var Phi = "Φ";
var phi = "φ";
var phiv = "ϕ";
var phmmat = "ℳ";
var phone = "☎";
var Pi = "Π";
var pi = "π";
var pitchfork = "⋔";
var piv = "ϖ";
var planck = "ℏ";
var planckh = "ℎ";
var plankv = "ℏ";
var plus = "+";
var plusacir = "⨣";
var plusb = "⊞";
var pluscir = "⨢";
var plusdo = "∔";
var plusdu = "⨥";
var pluse = "⩲";
var PlusMinus = "±";
var plusmn = "±";
var plussim = "⨦";
var plustwo = "⨧";
var pm = "±";
var Poincareplane = "ℌ";
var pointint = "⨕";
var Popf = "ℙ";
var popf = "𝕡";
var pound = "£";
var Pr = "⪻";
var pr = "≺";
var prap = "⪷";
var prcue = "≼";
var prE = "⪳";
var pre = "⪯";
var prec = "≺";
var precapprox = "⪷";
var preccurlyeq = "≼";
var Precedes = "≺";
var PrecedesEqual = "⪯";
var PrecedesSlantEqual = "≼";
var PrecedesTilde = "≾";
var preceq = "⪯";
var precnapprox = "⪹";
var precneqq = "⪵";
var precnsim = "⋨";
var precsim = "≾";
var Prime = "″";
var prime = "′";
var primes = "ℙ";
var prnap = "⪹";
var prnE = "⪵";
var prnsim = "⋨";
var prod = "∏";
var Product = "∏";
var profalar = "⌮";
var profline = "⌒";
var profsurf = "⌓";
var prop = "∝";
var Proportion = "∷";
var Proportional = "∝";
var propto = "∝";
var prsim = "≾";
var prurel = "⊰";
var Pscr = "𝒫";
var pscr = "𝓅";
var Psi = "Ψ";
var psi = "ψ";
var puncsp = " ";
var Qfr = "𝔔";
var qfr = "𝔮";
var qint = "⨌";
var Qopf = "ℚ";
var qopf = "𝕢";
var qprime = "⁗";
var Qscr = "𝒬";
var qscr = "𝓆";
var quaternions = "ℍ";
var quatint = "⨖";
var quest = "?";
var questeq = "≟";
var QUOT = "\"";
var quot = "\"";
var rAarr = "⇛";
var race = "∽̱";
var Racute = "Ŕ";
var racute = "ŕ";
var radic = "√";
var raemptyv = "⦳";
var Rang = "⟫";
var rang = "⟩";
var rangd = "⦒";
var range = "⦥";
var rangle = "⟩";
var raquo = "»";
var Rarr = "↠";
var rArr = "⇒";
var rarr = "→";
var rarrap = "⥵";
var rarrb = "⇥";
var rarrbfs = "⤠";
var rarrc = "⤳";
var rarrfs = "⤞";
var rarrhk = "↪";
var rarrlp = "↬";
var rarrpl = "⥅";
var rarrsim = "⥴";
var Rarrtl = "⤖";
var rarrtl = "↣";
var rarrw = "↝";
var rAtail = "⤜";
var ratail = "⤚";
var ratio = "∶";
var rationals = "ℚ";
var RBarr = "⤐";
var rBarr = "⤏";
var rbarr = "⤍";
var rbbrk = "❳";
var rbrace = "}";
var rbrack = "]";
var rbrke = "⦌";
var rbrksld = "⦎";
var rbrkslu = "⦐";
var Rcaron = "Ř";
var rcaron = "ř";
var Rcedil = "Ŗ";
var rcedil = "ŗ";
var rceil = "⌉";
var rcub = "}";
var Rcy = "Р";
var rcy = "р";
var rdca = "⤷";
var rdldhar = "⥩";
var rdquo = "”";
var rdquor = "”";
var rdsh = "↳";
var Re = "ℜ";
var real = "ℜ";
var realine = "ℛ";
var realpart = "ℜ";
var reals = "ℝ";
var rect = "▭";
var REG = "®";
var reg = "®";
var ReverseElement = "∋";
var ReverseEquilibrium = "⇋";
var ReverseUpEquilibrium = "⥯";
var rfisht = "⥽";
var rfloor = "⌋";
var Rfr = "ℜ";
var rfr = "𝔯";
var rHar = "⥤";
var rhard = "⇁";
var rharu = "⇀";
var rharul = "⥬";
var Rho = "Ρ";
var rho = "ρ";
var rhov = "ϱ";
var RightAngleBracket = "⟩";
var RightArrow = "→";
var Rightarrow = "⇒";
var rightarrow = "→";
var RightArrowBar = "⇥";
var RightArrowLeftArrow = "⇄";
var rightarrowtail = "↣";
var RightCeiling = "⌉";
var RightDoubleBracket = "⟧";
var RightDownTeeVector = "⥝";
var RightDownVector = "⇂";
var RightDownVectorBar = "⥕";
var RightFloor = "⌋";
var rightharpoondown = "⇁";
var rightharpoonup = "⇀";
var rightleftarrows = "⇄";
var rightleftharpoons = "⇌";
var rightrightarrows = "⇉";
var rightsquigarrow = "↝";
var RightTee = "⊢";
var RightTeeArrow = "↦";
var RightTeeVector = "⥛";
var rightthreetimes = "⋌";
var RightTriangle = "⊳";
var RightTriangleBar = "⧐";
var RightTriangleEqual = "⊵";
var RightUpDownVector = "⥏";
var RightUpTeeVector = "⥜";
var RightUpVector = "↾";
var RightUpVectorBar = "⥔";
var RightVector = "⇀";
var RightVectorBar = "⥓";
var ring = "˚";
var risingdotseq = "≓";
var rlarr = "⇄";
var rlhar = "⇌";
var rlm = "‏";
var rmoust = "⎱";
var rmoustache = "⎱";
var rnmid = "⫮";
var roang = "⟭";
var roarr = "⇾";
var robrk = "⟧";
var ropar = "⦆";
var Ropf = "ℝ";
var ropf = "𝕣";
var roplus = "⨮";
var rotimes = "⨵";
var RoundImplies = "⥰";
var rpar = ")";
var rpargt = "⦔";
var rppolint = "⨒";
var rrarr = "⇉";
var Rrightarrow = "⇛";
var rsaquo = "›";
var Rscr = "ℛ";
var rscr = "𝓇";
var Rsh = "↱";
var rsh = "↱";
var rsqb = "]";
var rsquo = "’";
var rsquor = "’";
var rthree = "⋌";
var rtimes = "⋊";
var rtri = "▹";
var rtrie = "⊵";
var rtrif = "▸";
var rtriltri = "⧎";
var RuleDelayed = "⧴";
var ruluhar = "⥨";
var rx = "℞";
var Sacute = "Ś";
var sacute = "ś";
var sbquo = "‚";
var Sc = "⪼";
var sc = "≻";
var scap = "⪸";
var Scaron = "Š";
var scaron = "š";
var sccue = "≽";
var scE = "⪴";
var sce = "⪰";
var Scedil = "Ş";
var scedil = "ş";
var Scirc = "Ŝ";
var scirc = "ŝ";
var scnap = "⪺";
var scnE = "⪶";
var scnsim = "⋩";
var scpolint = "⨓";
var scsim = "≿";
var Scy = "С";
var scy = "с";
var sdot = "⋅";
var sdotb = "⊡";
var sdote = "⩦";
var searhk = "⤥";
var seArr = "⇘";
var searr = "↘";
var searrow = "↘";
var sect = "§";
var semi = ";";
var seswar = "⤩";
var setminus = "∖";
var setmn = "∖";
var sext = "✶";
var Sfr = "𝔖";
var sfr = "𝔰";
var sfrown = "⌢";
var sharp = "♯";
var SHCHcy = "Щ";
var shchcy = "щ";
var SHcy = "Ш";
var shcy = "ш";
var ShortDownArrow = "↓";
var ShortLeftArrow = "←";
var shortmid = "∣";
var shortparallel = "∥";
var ShortRightArrow = "→";
var ShortUpArrow = "↑";
var shy = "­";
var Sigma = "Σ";
var sigma = "σ";
var sigmaf = "ς";
var sigmav = "ς";
var sim = "∼";
var simdot = "⩪";
var sime = "≃";
var simeq = "≃";
var simg = "⪞";
var simgE = "⪠";
var siml = "⪝";
var simlE = "⪟";
var simne = "≆";
var simplus = "⨤";
var simrarr = "⥲";
var slarr = "←";
var SmallCircle = "∘";
var smallsetminus = "∖";
var smashp = "⨳";
var smeparsl = "⧤";
var smid = "∣";
var smile = "⌣";
var smt = "⪪";
var smte = "⪬";
var smtes = "⪬︀";
var SOFTcy = "Ь";
var softcy = "ь";
var sol = "/";
var solb = "⧄";
var solbar = "⌿";
var Sopf = "𝕊";
var sopf = "𝕤";
var spades = "♠";
var spadesuit = "♠";
var spar = "∥";
var sqcap = "⊓";
var sqcaps = "⊓︀";
var sqcup = "⊔";
var sqcups = "⊔︀";
var Sqrt = "√";
var sqsub = "⊏";
var sqsube = "⊑";
var sqsubset = "⊏";
var sqsubseteq = "⊑";
var sqsup = "⊐";
var sqsupe = "⊒";
var sqsupset = "⊐";
var sqsupseteq = "⊒";
var squ = "□";
var Square = "□";
var square = "□";
var SquareIntersection = "⊓";
var SquareSubset = "⊏";
var SquareSubsetEqual = "⊑";
var SquareSuperset = "⊐";
var SquareSupersetEqual = "⊒";
var SquareUnion = "⊔";
var squarf = "▪";
var squf = "▪";
var srarr = "→";
var Sscr = "𝒮";
var sscr = "𝓈";
var ssetmn = "∖";
var ssmile = "⌣";
var sstarf = "⋆";
var Star = "⋆";
var star = "☆";
var starf = "★";
var straightepsilon = "ϵ";
var straightphi = "ϕ";
var strns = "¯";
var Sub = "⋐";
var sub = "⊂";
var subdot = "⪽";
var subE = "⫅";
var sube = "⊆";
var subedot = "⫃";
var submult = "⫁";
var subnE = "⫋";
var subne = "⊊";
var subplus = "⪿";
var subrarr = "⥹";
var Subset = "⋐";
var subset = "⊂";
var subseteq = "⊆";
var subseteqq = "⫅";
var SubsetEqual = "⊆";
var subsetneq = "⊊";
var subsetneqq = "⫋";
var subsim = "⫇";
var subsub = "⫕";
var subsup = "⫓";
var succ = "≻";
var succapprox = "⪸";
var succcurlyeq = "≽";
var Succeeds = "≻";
var SucceedsEqual = "⪰";
var SucceedsSlantEqual = "≽";
var SucceedsTilde = "≿";
var succeq = "⪰";
var succnapprox = "⪺";
var succneqq = "⪶";
var succnsim = "⋩";
var succsim = "≿";
var SuchThat = "∋";
var Sum = "∑";
var sum = "∑";
var sung = "♪";
var Sup = "⋑";
var sup = "⊃";
var sup1 = "¹";
var sup2 = "²";
var sup3 = "³";
var supdot = "⪾";
var supdsub = "⫘";
var supE = "⫆";
var supe = "⊇";
var supedot = "⫄";
var Superset = "⊃";
var SupersetEqual = "⊇";
var suphsol = "⟉";
var suphsub = "⫗";
var suplarr = "⥻";
var supmult = "⫂";
var supnE = "⫌";
var supne = "⊋";
var supplus = "⫀";
var Supset = "⋑";
var supset = "⊃";
var supseteq = "⊇";
var supseteqq = "⫆";
var supsetneq = "⊋";
var supsetneqq = "⫌";
var supsim = "⫈";
var supsub = "⫔";
var supsup = "⫖";
var swarhk = "⤦";
var swArr = "⇙";
var swarr = "↙";
var swarrow = "↙";
var swnwar = "⤪";
var szlig = "ß";
var Tab = "\t";
var target = "⌖";
var Tau = "Τ";
var tau = "τ";
var tbrk = "⎴";
var Tcaron = "Ť";
var tcaron = "ť";
var Tcedil = "Ţ";
var tcedil = "ţ";
var Tcy = "Т";
var tcy = "т";
var tdot = "⃛";
var telrec = "⌕";
var Tfr = "𝔗";
var tfr = "𝔱";
var there4 = "∴";
var Therefore = "∴";
var therefore = "∴";
var Theta = "Θ";
var theta = "θ";
var thetasym = "ϑ";
var thetav = "ϑ";
var thickapprox = "≈";
var thicksim = "∼";
var ThickSpace = "  ";
var thinsp = " ";
var ThinSpace = " ";
var thkap = "≈";
var thksim = "∼";
var THORN = "Þ";
var thorn = "þ";
var Tilde = "∼";
var tilde = "˜";
var TildeEqual = "≃";
var TildeFullEqual = "≅";
var TildeTilde = "≈";
var times = "×";
var timesb = "⊠";
var timesbar = "⨱";
var timesd = "⨰";
var tint = "∭";
var toea = "⤨";
var top = "⊤";
var topbot = "⌶";
var topcir = "⫱";
var Topf = "𝕋";
var topf = "𝕥";
var topfork = "⫚";
var tosa = "⤩";
var tprime = "‴";
var TRADE = "™";
var trade = "™";
var triangle = "▵";
var triangledown = "▿";
var triangleleft = "◃";
var trianglelefteq = "⊴";
var triangleq = "≜";
var triangleright = "▹";
var trianglerighteq = "⊵";
var tridot = "◬";
var trie = "≜";
var triminus = "⨺";
var TripleDot = "⃛";
var triplus = "⨹";
var trisb = "⧍";
var tritime = "⨻";
var trpezium = "⏢";
var Tscr = "𝒯";
var tscr = "𝓉";
var TScy = "Ц";
var tscy = "ц";
var TSHcy = "Ћ";
var tshcy = "ћ";
var Tstrok = "Ŧ";
var tstrok = "ŧ";
var twixt = "≬";
var twoheadleftarrow = "↞";
var twoheadrightarrow = "↠";
var Uacute = "Ú";
var uacute = "ú";
var Uarr = "↟";
var uArr = "⇑";
var uarr = "↑";
var Uarrocir = "⥉";
var Ubrcy = "Ў";
var ubrcy = "ў";
var Ubreve = "Ŭ";
var ubreve = "ŭ";
var Ucirc = "Û";
var ucirc = "û";
var Ucy = "У";
var ucy = "у";
var udarr = "⇅";
var Udblac = "Ű";
var udblac = "ű";
var udhar = "⥮";
var ufisht = "⥾";
var Ufr = "𝔘";
var ufr = "𝔲";
var Ugrave = "Ù";
var ugrave = "ù";
var uHar = "⥣";
var uharl = "↿";
var uharr = "↾";
var uhblk = "▀";
var ulcorn = "⌜";
var ulcorner = "⌜";
var ulcrop = "⌏";
var ultri = "◸";
var Umacr = "Ū";
var umacr = "ū";
var uml = "¨";
var UnderBar = "_";
var UnderBrace = "⏟";
var UnderBracket = "⎵";
var UnderParenthesis = "⏝";
var Union = "⋃";
var UnionPlus = "⊎";
var Uogon = "Ų";
var uogon = "ų";
var Uopf = "𝕌";
var uopf = "𝕦";
var UpArrow = "↑";
var Uparrow = "⇑";
var uparrow = "↑";
var UpArrowBar = "⤒";
var UpArrowDownArrow = "⇅";
var UpDownArrow = "↕";
var Updownarrow = "⇕";
var updownarrow = "↕";
var UpEquilibrium = "⥮";
var upharpoonleft = "↿";
var upharpoonright = "↾";
var uplus = "⊎";
var UpperLeftArrow = "↖";
var UpperRightArrow = "↗";
var Upsi = "ϒ";
var upsi = "υ";
var upsih = "ϒ";
var Upsilon = "Υ";
var upsilon = "υ";
var UpTee = "⊥";
var UpTeeArrow = "↥";
var upuparrows = "⇈";
var urcorn = "⌝";
var urcorner = "⌝";
var urcrop = "⌎";
var Uring = "Ů";
var uring = "ů";
var urtri = "◹";
var Uscr = "𝒰";
var uscr = "𝓊";
var utdot = "⋰";
var Utilde = "Ũ";
var utilde = "ũ";
var utri = "▵";
var utrif = "▴";
var uuarr = "⇈";
var Uuml = "Ü";
var uuml = "ü";
var uwangle = "⦧";
var vangrt = "⦜";
var varepsilon = "ϵ";
var varkappa = "ϰ";
var varnothing = "∅";
var varphi = "ϕ";
var varpi = "ϖ";
var varpropto = "∝";
var vArr = "⇕";
var varr = "↕";
var varrho = "ϱ";
var varsigma = "ς";
var varsubsetneq = "⊊︀";
var varsubsetneqq = "⫋︀";
var varsupsetneq = "⊋︀";
var varsupsetneqq = "⫌︀";
var vartheta = "ϑ";
var vartriangleleft = "⊲";
var vartriangleright = "⊳";
var Vbar = "⫫";
var vBar = "⫨";
var vBarv = "⫩";
var Vcy = "В";
var vcy = "в";
var VDash = "⊫";
var Vdash = "⊩";
var vDash = "⊨";
var vdash = "⊢";
var Vdashl = "⫦";
var Vee = "⋁";
var vee = "∨";
var veebar = "⊻";
var veeeq = "≚";
var vellip = "⋮";
var Verbar = "‖";
var verbar = "|";
var Vert = "‖";
var vert = "|";
var VerticalBar = "∣";
var VerticalLine = "|";
var VerticalSeparator = "❘";
var VerticalTilde = "≀";
var VeryThinSpace = " ";
var Vfr = "𝔙";
var vfr = "𝔳";
var vltri = "⊲";
var vnsub = "⊂⃒";
var vnsup = "⊃⃒";
var Vopf = "𝕍";
var vopf = "𝕧";
var vprop = "∝";
var vrtri = "⊳";
var Vscr = "𝒱";
var vscr = "𝓋";
var vsubnE = "⫋︀";
var vsubne = "⊊︀";
var vsupnE = "⫌︀";
var vsupne = "⊋︀";
var Vvdash = "⊪";
var vzigzag = "⦚";
var Wcirc = "Ŵ";
var wcirc = "ŵ";
var wedbar = "⩟";
var Wedge = "⋀";
var wedge = "∧";
var wedgeq = "≙";
var weierp = "℘";
var Wfr = "𝔚";
var wfr = "𝔴";
var Wopf = "𝕎";
var wopf = "𝕨";
var wp = "℘";
var wr = "≀";
var wreath = "≀";
var Wscr = "𝒲";
var wscr = "𝓌";
var xcap = "⋂";
var xcirc = "◯";
var xcup = "⋃";
var xdtri = "▽";
var Xfr = "𝔛";
var xfr = "𝔵";
var xhArr = "⟺";
var xharr = "⟷";
var Xi = "Ξ";
var xi = "ξ";
var xlArr = "⟸";
var xlarr = "⟵";
var xmap = "⟼";
var xnis = "⋻";
var xodot = "⨀";
var Xopf = "𝕏";
var xopf = "𝕩";
var xoplus = "⨁";
var xotime = "⨂";
var xrArr = "⟹";
var xrarr = "⟶";
var Xscr = "𝒳";
var xscr = "𝓍";
var xsqcup = "⨆";
var xuplus = "⨄";
var xutri = "△";
var xvee = "⋁";
var xwedge = "⋀";
var Yacute = "Ý";
var yacute = "ý";
var YAcy = "Я";
var yacy = "я";
var Ycirc = "Ŷ";
var ycirc = "ŷ";
var Ycy = "Ы";
var ycy = "ы";
var yen = "¥";
var Yfr = "𝔜";
var yfr = "𝔶";
var YIcy = "Ї";
var yicy = "ї";
var Yopf = "𝕐";
var yopf = "𝕪";
var Yscr = "𝒴";
var yscr = "𝓎";
var YUcy = "Ю";
var yucy = "ю";
var Yuml = "Ÿ";
var yuml = "ÿ";
var Zacute = "Ź";
var zacute = "ź";
var Zcaron = "Ž";
var zcaron = "ž";
var Zcy = "З";
var zcy = "з";
var Zdot = "Ż";
var zdot = "ż";
var zeetrf = "ℨ";
var ZeroWidthSpace = "​";
var Zeta = "Ζ";
var zeta = "ζ";
var Zfr = "ℨ";
var zfr = "𝔷";
var ZHcy = "Ж";
var zhcy = "ж";
var zigrarr = "⇝";
var Zopf = "ℤ";
var zopf = "𝕫";
var Zscr = "𝒵";
var zscr = "𝓏";
var zwj = "‍";
var zwnj = "‌";
var allNamedEntities = {
	Aacute: Aacute,
	aacute: aacute,
	Abreve: Abreve,
	abreve: abreve,
	ac: ac,
	acd: acd,
	acE: acE,
	Acirc: Acirc,
	acirc: acirc,
	acute: acute,
	Acy: Acy,
	acy: acy,
	AElig: AElig,
	aelig: aelig,
	af: af,
	Afr: Afr,
	afr: afr,
	Agrave: Agrave,
	agrave: agrave,
	alefsym: alefsym,
	aleph: aleph,
	Alpha: Alpha,
	alpha: alpha,
	Amacr: Amacr,
	amacr: amacr,
	amalg: amalg,
	AMP: AMP,
	amp: amp,
	And: And,
	and: and,
	andand: andand,
	andd: andd,
	andslope: andslope,
	andv: andv,
	ang: ang,
	ange: ange,
	angle: angle,
	angmsd: angmsd,
	angmsdaa: angmsdaa,
	angmsdab: angmsdab,
	angmsdac: angmsdac,
	angmsdad: angmsdad,
	angmsdae: angmsdae,
	angmsdaf: angmsdaf,
	angmsdag: angmsdag,
	angmsdah: angmsdah,
	angrt: angrt,
	angrtvb: angrtvb,
	angrtvbd: angrtvbd,
	angsph: angsph,
	angst: angst,
	angzarr: angzarr,
	Aogon: Aogon,
	aogon: aogon,
	Aopf: Aopf,
	aopf: aopf,
	ap: ap,
	apacir: apacir,
	apE: apE,
	ape: ape,
	apid: apid,
	apos: apos,
	ApplyFunction: ApplyFunction,
	approx: approx,
	approxeq: approxeq,
	Aring: Aring,
	aring: aring,
	Ascr: Ascr,
	ascr: ascr,
	Assign: Assign,
	ast: ast,
	asymp: asymp,
	asympeq: asympeq,
	Atilde: Atilde,
	atilde: atilde,
	Auml: Auml,
	auml: auml,
	awconint: awconint,
	awint: awint,
	backcong: backcong,
	backepsilon: backepsilon,
	backprime: backprime,
	backsim: backsim,
	backsimeq: backsimeq,
	Backslash: Backslash,
	Barv: Barv,
	barvee: barvee,
	Barwed: Barwed,
	barwed: barwed,
	barwedge: barwedge,
	bbrk: bbrk,
	bbrktbrk: bbrktbrk,
	bcong: bcong,
	Bcy: Bcy,
	bcy: bcy,
	bdquo: bdquo,
	becaus: becaus,
	Because: Because,
	because: because,
	bemptyv: bemptyv,
	bepsi: bepsi,
	bernou: bernou,
	Bernoullis: Bernoullis,
	Beta: Beta,
	beta: beta,
	beth: beth,
	between: between,
	Bfr: Bfr,
	bfr: bfr,
	bigcap: bigcap,
	bigcirc: bigcirc,
	bigcup: bigcup,
	bigodot: bigodot,
	bigoplus: bigoplus,
	bigotimes: bigotimes,
	bigsqcup: bigsqcup,
	bigstar: bigstar,
	bigtriangledown: bigtriangledown,
	bigtriangleup: bigtriangleup,
	biguplus: biguplus,
	bigvee: bigvee,
	bigwedge: bigwedge,
	bkarow: bkarow,
	blacklozenge: blacklozenge,
	blacksquare: blacksquare,
	blacktriangle: blacktriangle,
	blacktriangledown: blacktriangledown,
	blacktriangleleft: blacktriangleleft,
	blacktriangleright: blacktriangleright,
	blank: blank,
	blk12: blk12,
	blk14: blk14,
	blk34: blk34,
	block: block,
	bne: bne,
	bnequiv: bnequiv,
	bNot: bNot,
	bnot: bnot,
	Bopf: Bopf,
	bopf: bopf,
	bot: bot,
	bottom: bottom,
	bowtie: bowtie,
	boxbox: boxbox,
	boxDL: boxDL,
	boxDl: boxDl,
	boxdL: boxdL,
	boxdl: boxdl,
	boxDR: boxDR,
	boxDr: boxDr,
	boxdR: boxdR,
	boxdr: boxdr,
	boxH: boxH,
	boxh: boxh,
	boxHD: boxHD,
	boxHd: boxHd,
	boxhD: boxhD,
	boxhd: boxhd,
	boxHU: boxHU,
	boxHu: boxHu,
	boxhU: boxhU,
	boxhu: boxhu,
	boxminus: boxminus,
	boxplus: boxplus,
	boxtimes: boxtimes,
	boxUL: boxUL,
	boxUl: boxUl,
	boxuL: boxuL,
	boxul: boxul,
	boxUR: boxUR,
	boxUr: boxUr,
	boxuR: boxuR,
	boxur: boxur,
	boxV: boxV,
	boxv: boxv,
	boxVH: boxVH,
	boxVh: boxVh,
	boxvH: boxvH,
	boxvh: boxvh,
	boxVL: boxVL,
	boxVl: boxVl,
	boxvL: boxvL,
	boxvl: boxvl,
	boxVR: boxVR,
	boxVr: boxVr,
	boxvR: boxvR,
	boxvr: boxvr,
	bprime: bprime,
	Breve: Breve,
	breve: breve,
	brvbar: brvbar,
	Bscr: Bscr,
	bscr: bscr,
	bsemi: bsemi,
	bsim: bsim,
	bsime: bsime,
	bsol: bsol,
	bsolb: bsolb,
	bsolhsub: bsolhsub,
	bull: bull,
	bullet: bullet,
	bump: bump,
	bumpE: bumpE,
	bumpe: bumpe,
	Bumpeq: Bumpeq,
	bumpeq: bumpeq,
	Cacute: Cacute,
	cacute: cacute,
	Cap: Cap,
	cap: cap,
	capand: capand,
	capbrcup: capbrcup,
	capcap: capcap,
	capcup: capcup,
	capdot: capdot,
	CapitalDifferentialD: CapitalDifferentialD,
	caps: caps,
	caret: caret,
	caron: caron,
	Cayleys: Cayleys,
	ccaps: ccaps,
	Ccaron: Ccaron,
	ccaron: ccaron,
	Ccedil: Ccedil,
	ccedil: ccedil,
	Ccirc: Ccirc,
	ccirc: ccirc,
	Cconint: Cconint,
	ccups: ccups,
	ccupssm: ccupssm,
	Cdot: Cdot,
	cdot: cdot,
	cedil: cedil,
	Cedilla: Cedilla,
	cemptyv: cemptyv,
	cent: cent,
	CenterDot: CenterDot,
	centerdot: centerdot,
	Cfr: Cfr,
	cfr: cfr,
	CHcy: CHcy,
	chcy: chcy,
	check: check,
	checkmark: checkmark,
	Chi: Chi,
	chi: chi,
	cir: cir,
	circ: circ,
	circeq: circeq,
	circlearrowleft: circlearrowleft,
	circlearrowright: circlearrowright,
	circledast: circledast,
	circledcirc: circledcirc,
	circleddash: circleddash,
	CircleDot: CircleDot,
	circledR: circledR,
	circledS: circledS,
	CircleMinus: CircleMinus,
	CirclePlus: CirclePlus,
	CircleTimes: CircleTimes,
	cirE: cirE,
	cire: cire,
	cirfnint: cirfnint,
	cirmid: cirmid,
	cirscir: cirscir,
	ClockwiseContourIntegral: ClockwiseContourIntegral,
	CloseCurlyDoubleQuote: CloseCurlyDoubleQuote,
	CloseCurlyQuote: CloseCurlyQuote,
	clubs: clubs,
	clubsuit: clubsuit,
	Colon: Colon,
	colon: colon,
	Colone: Colone,
	colone: colone,
	coloneq: coloneq,
	comma: comma,
	commat: commat,
	comp: comp,
	compfn: compfn,
	complement: complement,
	complexes: complexes,
	cong: cong,
	congdot: congdot,
	Congruent: Congruent,
	Conint: Conint,
	conint: conint,
	ContourIntegral: ContourIntegral,
	Copf: Copf,
	copf: copf,
	coprod: coprod,
	Coproduct: Coproduct,
	COPY: COPY,
	copy: copy,
	copysr: copysr,
	CounterClockwiseContourIntegral: CounterClockwiseContourIntegral,
	crarr: crarr,
	Cross: Cross,
	cross: cross,
	Cscr: Cscr,
	cscr: cscr,
	csub: csub,
	csube: csube,
	csup: csup,
	csupe: csupe,
	ctdot: ctdot,
	cudarrl: cudarrl,
	cudarrr: cudarrr,
	cuepr: cuepr,
	cuesc: cuesc,
	cularr: cularr,
	cularrp: cularrp,
	Cup: Cup,
	cup: cup,
	cupbrcap: cupbrcap,
	CupCap: CupCap,
	cupcap: cupcap,
	cupcup: cupcup,
	cupdot: cupdot,
	cupor: cupor,
	cups: cups,
	curarr: curarr,
	curarrm: curarrm,
	curlyeqprec: curlyeqprec,
	curlyeqsucc: curlyeqsucc,
	curlyvee: curlyvee,
	curlywedge: curlywedge,
	curren: curren,
	curvearrowleft: curvearrowleft,
	curvearrowright: curvearrowright,
	cuvee: cuvee,
	cuwed: cuwed,
	cwconint: cwconint,
	cwint: cwint,
	cylcty: cylcty,
	Dagger: Dagger,
	dagger: dagger,
	daleth: daleth,
	Darr: Darr,
	dArr: dArr,
	darr: darr,
	dash: dash,
	Dashv: Dashv,
	dashv: dashv,
	dbkarow: dbkarow,
	dblac: dblac,
	Dcaron: Dcaron,
	dcaron: dcaron,
	Dcy: Dcy,
	dcy: dcy,
	DD: DD,
	dd: dd,
	ddagger: ddagger,
	ddarr: ddarr,
	DDotrahd: DDotrahd,
	ddotseq: ddotseq,
	deg: deg,
	Del: Del,
	Delta: Delta,
	delta: delta,
	demptyv: demptyv,
	dfisht: dfisht,
	Dfr: Dfr,
	dfr: dfr,
	dHar: dHar,
	dharl: dharl,
	dharr: dharr,
	DiacriticalAcute: DiacriticalAcute,
	DiacriticalDot: DiacriticalDot,
	DiacriticalDoubleAcute: DiacriticalDoubleAcute,
	DiacriticalGrave: DiacriticalGrave,
	DiacriticalTilde: DiacriticalTilde,
	diam: diam,
	Diamond: Diamond,
	diamond: diamond,
	diamondsuit: diamondsuit,
	diams: diams,
	die: die,
	DifferentialD: DifferentialD,
	digamma: digamma,
	disin: disin,
	div: div,
	divide: divide,
	divideontimes: divideontimes,
	divonx: divonx,
	DJcy: DJcy,
	djcy: djcy,
	dlcorn: dlcorn,
	dlcrop: dlcrop,
	dollar: dollar,
	Dopf: Dopf,
	dopf: dopf,
	Dot: Dot,
	dot: dot,
	DotDot: DotDot,
	doteq: doteq,
	doteqdot: doteqdot,
	DotEqual: DotEqual,
	dotminus: dotminus,
	dotplus: dotplus,
	dotsquare: dotsquare,
	doublebarwedge: doublebarwedge,
	DoubleContourIntegral: DoubleContourIntegral,
	DoubleDot: DoubleDot,
	DoubleDownArrow: DoubleDownArrow,
	DoubleLeftArrow: DoubleLeftArrow,
	DoubleLeftRightArrow: DoubleLeftRightArrow,
	DoubleLeftTee: DoubleLeftTee,
	DoubleLongLeftArrow: DoubleLongLeftArrow,
	DoubleLongLeftRightArrow: DoubleLongLeftRightArrow,
	DoubleLongRightArrow: DoubleLongRightArrow,
	DoubleRightArrow: DoubleRightArrow,
	DoubleRightTee: DoubleRightTee,
	DoubleUpArrow: DoubleUpArrow,
	DoubleUpDownArrow: DoubleUpDownArrow,
	DoubleVerticalBar: DoubleVerticalBar,
	DownArrow: DownArrow,
	Downarrow: Downarrow,
	downarrow: downarrow,
	DownArrowBar: DownArrowBar,
	DownArrowUpArrow: DownArrowUpArrow,
	DownBreve: DownBreve,
	downdownarrows: downdownarrows,
	downharpoonleft: downharpoonleft,
	downharpoonright: downharpoonright,
	DownLeftRightVector: DownLeftRightVector,
	DownLeftTeeVector: DownLeftTeeVector,
	DownLeftVector: DownLeftVector,
	DownLeftVectorBar: DownLeftVectorBar,
	DownRightTeeVector: DownRightTeeVector,
	DownRightVector: DownRightVector,
	DownRightVectorBar: DownRightVectorBar,
	DownTee: DownTee,
	DownTeeArrow: DownTeeArrow,
	drbkarow: drbkarow,
	drcorn: drcorn,
	drcrop: drcrop,
	Dscr: Dscr,
	dscr: dscr,
	DScy: DScy,
	dscy: dscy,
	dsol: dsol,
	Dstrok: Dstrok,
	dstrok: dstrok,
	dtdot: dtdot,
	dtri: dtri,
	dtrif: dtrif,
	duarr: duarr,
	duhar: duhar,
	dwangle: dwangle,
	DZcy: DZcy,
	dzcy: dzcy,
	dzigrarr: dzigrarr,
	Eacute: Eacute,
	eacute: eacute,
	easter: easter,
	Ecaron: Ecaron,
	ecaron: ecaron,
	ecir: ecir,
	Ecirc: Ecirc,
	ecirc: ecirc,
	ecolon: ecolon,
	Ecy: Ecy,
	ecy: ecy,
	eDDot: eDDot,
	Edot: Edot,
	eDot: eDot,
	edot: edot,
	ee: ee,
	efDot: efDot,
	Efr: Efr,
	efr: efr,
	eg: eg,
	Egrave: Egrave,
	egrave: egrave,
	egs: egs,
	egsdot: egsdot,
	el: el,
	Element: Element,
	elinters: elinters,
	ell: ell,
	els: els,
	elsdot: elsdot,
	Emacr: Emacr,
	emacr: emacr,
	empty: empty,
	emptyset: emptyset,
	EmptySmallSquare: EmptySmallSquare,
	emptyv: emptyv,
	EmptyVerySmallSquare: EmptyVerySmallSquare,
	emsp: emsp,
	emsp13: emsp13,
	emsp14: emsp14,
	ENG: ENG,
	eng: eng,
	ensp: ensp,
	Eogon: Eogon,
	eogon: eogon,
	Eopf: Eopf,
	eopf: eopf,
	epar: epar,
	eparsl: eparsl,
	eplus: eplus,
	epsi: epsi,
	Epsilon: Epsilon,
	epsilon: epsilon,
	epsiv: epsiv,
	eqcirc: eqcirc,
	eqcolon: eqcolon,
	eqsim: eqsim,
	eqslantgtr: eqslantgtr,
	eqslantless: eqslantless,
	Equal: Equal,
	equals: equals,
	EqualTilde: EqualTilde,
	equest: equest,
	Equilibrium: Equilibrium,
	equiv: equiv,
	equivDD: equivDD,
	eqvparsl: eqvparsl,
	erarr: erarr,
	erDot: erDot,
	Escr: Escr,
	escr: escr,
	esdot: esdot,
	Esim: Esim,
	esim: esim,
	Eta: Eta,
	eta: eta,
	ETH: ETH,
	eth: eth,
	Euml: Euml,
	euml: euml,
	euro: euro,
	excl: excl,
	exist: exist,
	Exists: Exists,
	expectation: expectation,
	ExponentialE: ExponentialE,
	exponentiale: exponentiale,
	fallingdotseq: fallingdotseq,
	Fcy: Fcy,
	fcy: fcy,
	female: female,
	ffilig: ffilig,
	fflig: fflig,
	ffllig: ffllig,
	Ffr: Ffr,
	ffr: ffr,
	filig: filig,
	FilledSmallSquare: FilledSmallSquare,
	FilledVerySmallSquare: FilledVerySmallSquare,
	fjlig: fjlig,
	flat: flat,
	fllig: fllig,
	fltns: fltns,
	fnof: fnof,
	Fopf: Fopf,
	fopf: fopf,
	ForAll: ForAll,
	forall: forall,
	fork: fork,
	forkv: forkv,
	Fouriertrf: Fouriertrf,
	fpartint: fpartint,
	frac12: frac12,
	frac13: frac13,
	frac14: frac14,
	frac15: frac15,
	frac16: frac16,
	frac18: frac18,
	frac23: frac23,
	frac25: frac25,
	frac34: frac34,
	frac35: frac35,
	frac38: frac38,
	frac45: frac45,
	frac56: frac56,
	frac58: frac58,
	frac78: frac78,
	frasl: frasl,
	frown: frown,
	Fscr: Fscr,
	fscr: fscr,
	gacute: gacute,
	Gamma: Gamma,
	gamma: gamma,
	Gammad: Gammad,
	gammad: gammad,
	gap: gap,
	Gbreve: Gbreve,
	gbreve: gbreve,
	Gcedil: Gcedil,
	Gcirc: Gcirc,
	gcirc: gcirc,
	Gcy: Gcy,
	gcy: gcy,
	Gdot: Gdot,
	gdot: gdot,
	gE: gE,
	ge: ge,
	gEl: gEl,
	gel: gel,
	geq: geq,
	geqq: geqq,
	geqslant: geqslant,
	ges: ges,
	gescc: gescc,
	gesdot: gesdot,
	gesdoto: gesdoto,
	gesdotol: gesdotol,
	gesl: gesl,
	gesles: gesles,
	Gfr: Gfr,
	gfr: gfr,
	Gg: Gg,
	gg: gg,
	ggg: ggg,
	gimel: gimel,
	GJcy: GJcy,
	gjcy: gjcy,
	gl: gl,
	gla: gla,
	glE: glE,
	glj: glj,
	gnap: gnap,
	gnapprox: gnapprox,
	gnE: gnE,
	gne: gne,
	gneq: gneq,
	gneqq: gneqq,
	gnsim: gnsim,
	Gopf: Gopf,
	gopf: gopf,
	grave: grave,
	GreaterEqual: GreaterEqual,
	GreaterEqualLess: GreaterEqualLess,
	GreaterFullEqual: GreaterFullEqual,
	GreaterGreater: GreaterGreater,
	GreaterLess: GreaterLess,
	GreaterSlantEqual: GreaterSlantEqual,
	GreaterTilde: GreaterTilde,
	Gscr: Gscr,
	gscr: gscr,
	gsim: gsim,
	gsime: gsime,
	gsiml: gsiml,
	GT: GT,
	Gt: Gt,
	gt: gt,
	gtcc: gtcc,
	gtcir: gtcir,
	gtdot: gtdot,
	gtlPar: gtlPar,
	gtquest: gtquest,
	gtrapprox: gtrapprox,
	gtrarr: gtrarr,
	gtrdot: gtrdot,
	gtreqless: gtreqless,
	gtreqqless: gtreqqless,
	gtrless: gtrless,
	gtrsim: gtrsim,
	gvertneqq: gvertneqq,
	gvnE: gvnE,
	Hacek: Hacek,
	hairsp: hairsp,
	half: half,
	hamilt: hamilt,
	HARDcy: HARDcy,
	hardcy: hardcy,
	hArr: hArr,
	harr: harr,
	harrcir: harrcir,
	harrw: harrw,
	Hat: Hat,
	hbar: hbar,
	Hcirc: Hcirc,
	hcirc: hcirc,
	hearts: hearts,
	heartsuit: heartsuit,
	hellip: hellip,
	hercon: hercon,
	Hfr: Hfr,
	hfr: hfr,
	HilbertSpace: HilbertSpace,
	hksearow: hksearow,
	hkswarow: hkswarow,
	hoarr: hoarr,
	homtht: homtht,
	hookleftarrow: hookleftarrow,
	hookrightarrow: hookrightarrow,
	Hopf: Hopf,
	hopf: hopf,
	horbar: horbar,
	HorizontalLine: HorizontalLine,
	Hscr: Hscr,
	hscr: hscr,
	hslash: hslash,
	Hstrok: Hstrok,
	hstrok: hstrok,
	HumpDownHump: HumpDownHump,
	HumpEqual: HumpEqual,
	hybull: hybull,
	hyphen: hyphen,
	Iacute: Iacute,
	iacute: iacute,
	ic: ic,
	Icirc: Icirc,
	icirc: icirc,
	Icy: Icy,
	icy: icy,
	Idot: Idot,
	IEcy: IEcy,
	iecy: iecy,
	iexcl: iexcl,
	iff: iff,
	Ifr: Ifr,
	ifr: ifr,
	Igrave: Igrave,
	igrave: igrave,
	ii: ii,
	iiiint: iiiint,
	iiint: iiint,
	iinfin: iinfin,
	iiota: iiota,
	IJlig: IJlig,
	ijlig: ijlig,
	Im: Im,
	Imacr: Imacr,
	imacr: imacr,
	image: image,
	ImaginaryI: ImaginaryI,
	imagline: imagline,
	imagpart: imagpart,
	imath: imath,
	imof: imof,
	imped: imped,
	Implies: Implies,
	"in": "∈",
	incare: incare,
	infin: infin,
	infintie: infintie,
	inodot: inodot,
	Int: Int,
	int: int,
	intcal: intcal,
	integers: integers,
	Integral: Integral,
	intercal: intercal,
	Intersection: Intersection,
	intlarhk: intlarhk,
	intprod: intprod,
	InvisibleComma: InvisibleComma,
	InvisibleTimes: InvisibleTimes,
	IOcy: IOcy,
	iocy: iocy,
	Iogon: Iogon,
	iogon: iogon,
	Iopf: Iopf,
	iopf: iopf,
	Iota: Iota,
	iota: iota,
	iprod: iprod,
	iquest: iquest,
	Iscr: Iscr,
	iscr: iscr,
	isin: isin,
	isindot: isindot,
	isinE: isinE,
	isins: isins,
	isinsv: isinsv,
	isinv: isinv,
	it: it,
	Itilde: Itilde,
	itilde: itilde,
	Iukcy: Iukcy,
	iukcy: iukcy,
	Iuml: Iuml,
	iuml: iuml,
	Jcirc: Jcirc,
	jcirc: jcirc,
	Jcy: Jcy,
	jcy: jcy,
	Jfr: Jfr,
	jfr: jfr,
	jmath: jmath,
	Jopf: Jopf,
	jopf: jopf,
	Jscr: Jscr,
	jscr: jscr,
	Jsercy: Jsercy,
	jsercy: jsercy,
	Jukcy: Jukcy,
	jukcy: jukcy,
	Kappa: Kappa,
	kappa: kappa,
	kappav: kappav,
	Kcedil: Kcedil,
	kcedil: kcedil,
	Kcy: Kcy,
	kcy: kcy,
	Kfr: Kfr,
	kfr: kfr,
	kgreen: kgreen,
	KHcy: KHcy,
	khcy: khcy,
	KJcy: KJcy,
	kjcy: kjcy,
	Kopf: Kopf,
	kopf: kopf,
	Kscr: Kscr,
	kscr: kscr,
	lAarr: lAarr,
	Lacute: Lacute,
	lacute: lacute,
	laemptyv: laemptyv,
	lagran: lagran,
	Lambda: Lambda,
	lambda: lambda,
	Lang: Lang,
	lang: lang,
	langd: langd,
	langle: langle,
	lap: lap,
	Laplacetrf: Laplacetrf,
	laquo: laquo,
	Larr: Larr,
	lArr: lArr,
	larr: larr,
	larrb: larrb,
	larrbfs: larrbfs,
	larrfs: larrfs,
	larrhk: larrhk,
	larrlp: larrlp,
	larrpl: larrpl,
	larrsim: larrsim,
	larrtl: larrtl,
	lat: lat,
	lAtail: lAtail,
	latail: latail,
	late: late,
	lates: lates,
	lBarr: lBarr,
	lbarr: lbarr,
	lbbrk: lbbrk,
	lbrace: lbrace,
	lbrack: lbrack,
	lbrke: lbrke,
	lbrksld: lbrksld,
	lbrkslu: lbrkslu,
	Lcaron: Lcaron,
	lcaron: lcaron,
	Lcedil: Lcedil,
	lcedil: lcedil,
	lceil: lceil,
	lcub: lcub,
	Lcy: Lcy,
	lcy: lcy,
	ldca: ldca,
	ldquo: ldquo,
	ldquor: ldquor,
	ldrdhar: ldrdhar,
	ldrushar: ldrushar,
	ldsh: ldsh,
	lE: lE,
	le: le,
	LeftAngleBracket: LeftAngleBracket,
	LeftArrow: LeftArrow,
	Leftarrow: Leftarrow,
	leftarrow: leftarrow,
	LeftArrowBar: LeftArrowBar,
	LeftArrowRightArrow: LeftArrowRightArrow,
	leftarrowtail: leftarrowtail,
	LeftCeiling: LeftCeiling,
	LeftDoubleBracket: LeftDoubleBracket,
	LeftDownTeeVector: LeftDownTeeVector,
	LeftDownVector: LeftDownVector,
	LeftDownVectorBar: LeftDownVectorBar,
	LeftFloor: LeftFloor,
	leftharpoondown: leftharpoondown,
	leftharpoonup: leftharpoonup,
	leftleftarrows: leftleftarrows,
	LeftRightArrow: LeftRightArrow,
	Leftrightarrow: Leftrightarrow,
	leftrightarrow: leftrightarrow,
	leftrightarrows: leftrightarrows,
	leftrightharpoons: leftrightharpoons,
	leftrightsquigarrow: leftrightsquigarrow,
	LeftRightVector: LeftRightVector,
	LeftTee: LeftTee,
	LeftTeeArrow: LeftTeeArrow,
	LeftTeeVector: LeftTeeVector,
	leftthreetimes: leftthreetimes,
	LeftTriangle: LeftTriangle,
	LeftTriangleBar: LeftTriangleBar,
	LeftTriangleEqual: LeftTriangleEqual,
	LeftUpDownVector: LeftUpDownVector,
	LeftUpTeeVector: LeftUpTeeVector,
	LeftUpVector: LeftUpVector,
	LeftUpVectorBar: LeftUpVectorBar,
	LeftVector: LeftVector,
	LeftVectorBar: LeftVectorBar,
	lEg: lEg,
	leg: leg,
	leq: leq,
	leqq: leqq,
	leqslant: leqslant,
	les: les,
	lescc: lescc,
	lesdot: lesdot,
	lesdoto: lesdoto,
	lesdotor: lesdotor,
	lesg: lesg,
	lesges: lesges,
	lessapprox: lessapprox,
	lessdot: lessdot,
	lesseqgtr: lesseqgtr,
	lesseqqgtr: lesseqqgtr,
	LessEqualGreater: LessEqualGreater,
	LessFullEqual: LessFullEqual,
	LessGreater: LessGreater,
	lessgtr: lessgtr,
	LessLess: LessLess,
	lesssim: lesssim,
	LessSlantEqual: LessSlantEqual,
	LessTilde: LessTilde,
	lfisht: lfisht,
	lfloor: lfloor,
	Lfr: Lfr,
	lfr: lfr,
	lg: lg,
	lgE: lgE,
	lHar: lHar,
	lhard: lhard,
	lharu: lharu,
	lharul: lharul,
	lhblk: lhblk,
	LJcy: LJcy,
	ljcy: ljcy,
	Ll: Ll,
	ll: ll,
	llarr: llarr,
	llcorner: llcorner,
	Lleftarrow: Lleftarrow,
	llhard: llhard,
	lltri: lltri,
	Lmidot: Lmidot,
	lmidot: lmidot,
	lmoust: lmoust,
	lmoustache: lmoustache,
	lnap: lnap,
	lnapprox: lnapprox,
	lnE: lnE,
	lne: lne,
	lneq: lneq,
	lneqq: lneqq,
	lnsim: lnsim,
	loang: loang,
	loarr: loarr,
	lobrk: lobrk,
	LongLeftArrow: LongLeftArrow,
	Longleftarrow: Longleftarrow,
	longleftarrow: longleftarrow,
	LongLeftRightArrow: LongLeftRightArrow,
	Longleftrightarrow: Longleftrightarrow,
	longleftrightarrow: longleftrightarrow,
	longmapsto: longmapsto,
	LongRightArrow: LongRightArrow,
	Longrightarrow: Longrightarrow,
	longrightarrow: longrightarrow,
	looparrowleft: looparrowleft,
	looparrowright: looparrowright,
	lopar: lopar,
	Lopf: Lopf,
	lopf: lopf,
	loplus: loplus,
	lotimes: lotimes,
	lowast: lowast,
	lowbar: lowbar,
	LowerLeftArrow: LowerLeftArrow,
	LowerRightArrow: LowerRightArrow,
	loz: loz,
	lozenge: lozenge,
	lozf: lozf,
	lpar: lpar,
	lparlt: lparlt,
	lrarr: lrarr,
	lrcorner: lrcorner,
	lrhar: lrhar,
	lrhard: lrhard,
	lrm: lrm,
	lrtri: lrtri,
	lsaquo: lsaquo,
	Lscr: Lscr,
	lscr: lscr,
	Lsh: Lsh,
	lsh: lsh,
	lsim: lsim,
	lsime: lsime,
	lsimg: lsimg,
	lsqb: lsqb,
	lsquo: lsquo,
	lsquor: lsquor,
	Lstrok: Lstrok,
	lstrok: lstrok,
	LT: LT,
	Lt: Lt,
	lt: lt,
	ltcc: ltcc,
	ltcir: ltcir,
	ltdot: ltdot,
	lthree: lthree,
	ltimes: ltimes,
	ltlarr: ltlarr,
	ltquest: ltquest,
	ltri: ltri,
	ltrie: ltrie,
	ltrif: ltrif,
	ltrPar: ltrPar,
	lurdshar: lurdshar,
	luruhar: luruhar,
	lvertneqq: lvertneqq,
	lvnE: lvnE,
	macr: macr,
	male: male,
	malt: malt,
	maltese: maltese,
	"Map": "⤅",
	map: map,
	mapsto: mapsto,
	mapstodown: mapstodown,
	mapstoleft: mapstoleft,
	mapstoup: mapstoup,
	marker: marker,
	mcomma: mcomma,
	Mcy: Mcy,
	mcy: mcy,
	mdash: mdash,
	mDDot: mDDot,
	measuredangle: measuredangle,
	MediumSpace: MediumSpace,
	Mellintrf: Mellintrf,
	Mfr: Mfr,
	mfr: mfr,
	mho: mho,
	micro: micro,
	mid: mid,
	midast: midast,
	midcir: midcir,
	middot: middot,
	minus: minus,
	minusb: minusb,
	minusd: minusd,
	minusdu: minusdu,
	MinusPlus: MinusPlus,
	mlcp: mlcp,
	mldr: mldr,
	mnplus: mnplus,
	models: models,
	Mopf: Mopf,
	mopf: mopf,
	mp: mp,
	Mscr: Mscr,
	mscr: mscr,
	mstpos: mstpos,
	Mu: Mu,
	mu: mu,
	multimap: multimap,
	mumap: mumap,
	nabla: nabla,
	Nacute: Nacute,
	nacute: nacute,
	nang: nang,
	nap: nap,
	napE: napE,
	napid: napid,
	napos: napos,
	napprox: napprox,
	natur: natur,
	natural: natural,
	naturals: naturals,
	nbsp: nbsp,
	nbump: nbump,
	nbumpe: nbumpe,
	ncap: ncap,
	Ncaron: Ncaron,
	ncaron: ncaron,
	Ncedil: Ncedil,
	ncedil: ncedil,
	ncong: ncong,
	ncongdot: ncongdot,
	ncup: ncup,
	Ncy: Ncy,
	ncy: ncy,
	ndash: ndash,
	ne: ne,
	nearhk: nearhk,
	neArr: neArr,
	nearr: nearr,
	nearrow: nearrow,
	nedot: nedot,
	NegativeMediumSpace: NegativeMediumSpace,
	NegativeThickSpace: NegativeThickSpace,
	NegativeThinSpace: NegativeThinSpace,
	NegativeVeryThinSpace: NegativeVeryThinSpace,
	nequiv: nequiv,
	nesear: nesear,
	nesim: nesim,
	NestedGreaterGreater: NestedGreaterGreater,
	NestedLessLess: NestedLessLess,
	NewLine: NewLine,
	nexist: nexist,
	nexists: nexists,
	Nfr: Nfr,
	nfr: nfr,
	ngE: ngE,
	nge: nge,
	ngeq: ngeq,
	ngeqq: ngeqq,
	ngeqslant: ngeqslant,
	nges: nges,
	nGg: nGg,
	ngsim: ngsim,
	nGt: nGt,
	ngt: ngt,
	ngtr: ngtr,
	nGtv: nGtv,
	nhArr: nhArr,
	nharr: nharr,
	nhpar: nhpar,
	ni: ni,
	nis: nis,
	nisd: nisd,
	niv: niv,
	NJcy: NJcy,
	njcy: njcy,
	nlArr: nlArr,
	nlarr: nlarr,
	nldr: nldr,
	nlE: nlE,
	nle: nle,
	nLeftarrow: nLeftarrow,
	nleftarrow: nleftarrow,
	nLeftrightarrow: nLeftrightarrow,
	nleftrightarrow: nleftrightarrow,
	nleq: nleq,
	nleqq: nleqq,
	nleqslant: nleqslant,
	nles: nles,
	nless: nless,
	nLl: nLl,
	nlsim: nlsim,
	nLt: nLt,
	nlt: nlt,
	nltri: nltri,
	nltrie: nltrie,
	nLtv: nLtv,
	nmid: nmid,
	NoBreak: NoBreak,
	NonBreakingSpace: NonBreakingSpace,
	Nopf: Nopf,
	nopf: nopf,
	Not: Not,
	not: not,
	NotCongruent: NotCongruent,
	NotCupCap: NotCupCap,
	NotDoubleVerticalBar: NotDoubleVerticalBar,
	NotElement: NotElement,
	NotEqual: NotEqual,
	NotEqualTilde: NotEqualTilde,
	NotExists: NotExists,
	NotGreater: NotGreater,
	NotGreaterEqual: NotGreaterEqual,
	NotGreaterFullEqual: NotGreaterFullEqual,
	NotGreaterGreater: NotGreaterGreater,
	NotGreaterLess: NotGreaterLess,
	NotGreaterSlantEqual: NotGreaterSlantEqual,
	NotGreaterTilde: NotGreaterTilde,
	NotHumpDownHump: NotHumpDownHump,
	NotHumpEqual: NotHumpEqual,
	notin: notin,
	notindot: notindot,
	notinE: notinE,
	notinva: notinva,
	notinvb: notinvb,
	notinvc: notinvc,
	NotLeftTriangle: NotLeftTriangle,
	NotLeftTriangleBar: NotLeftTriangleBar,
	NotLeftTriangleEqual: NotLeftTriangleEqual,
	NotLess: NotLess,
	NotLessEqual: NotLessEqual,
	NotLessGreater: NotLessGreater,
	NotLessLess: NotLessLess,
	NotLessSlantEqual: NotLessSlantEqual,
	NotLessTilde: NotLessTilde,
	NotNestedGreaterGreater: NotNestedGreaterGreater,
	NotNestedLessLess: NotNestedLessLess,
	notni: notni,
	notniva: notniva,
	notnivb: notnivb,
	notnivc: notnivc,
	NotPrecedes: NotPrecedes,
	NotPrecedesEqual: NotPrecedesEqual,
	NotPrecedesSlantEqual: NotPrecedesSlantEqual,
	NotReverseElement: NotReverseElement,
	NotRightTriangle: NotRightTriangle,
	NotRightTriangleBar: NotRightTriangleBar,
	NotRightTriangleEqual: NotRightTriangleEqual,
	NotSquareSubset: NotSquareSubset,
	NotSquareSubsetEqual: NotSquareSubsetEqual,
	NotSquareSuperset: NotSquareSuperset,
	NotSquareSupersetEqual: NotSquareSupersetEqual,
	NotSubset: NotSubset,
	NotSubsetEqual: NotSubsetEqual,
	NotSucceeds: NotSucceeds,
	NotSucceedsEqual: NotSucceedsEqual,
	NotSucceedsSlantEqual: NotSucceedsSlantEqual,
	NotSucceedsTilde: NotSucceedsTilde,
	NotSuperset: NotSuperset,
	NotSupersetEqual: NotSupersetEqual,
	NotTilde: NotTilde,
	NotTildeEqual: NotTildeEqual,
	NotTildeFullEqual: NotTildeFullEqual,
	NotTildeTilde: NotTildeTilde,
	NotVerticalBar: NotVerticalBar,
	npar: npar,
	nparallel: nparallel,
	nparsl: nparsl,
	npart: npart,
	npolint: npolint,
	npr: npr,
	nprcue: nprcue,
	npre: npre,
	nprec: nprec,
	npreceq: npreceq,
	nrArr: nrArr,
	nrarr: nrarr,
	nrarrc: nrarrc,
	nrarrw: nrarrw,
	nRightarrow: nRightarrow,
	nrightarrow: nrightarrow,
	nrtri: nrtri,
	nrtrie: nrtrie,
	nsc: nsc,
	nsccue: nsccue,
	nsce: nsce,
	Nscr: Nscr,
	nscr: nscr,
	nshortmid: nshortmid,
	nshortparallel: nshortparallel,
	nsim: nsim,
	nsime: nsime,
	nsimeq: nsimeq,
	nsmid: nsmid,
	nspar: nspar,
	nsqsube: nsqsube,
	nsqsupe: nsqsupe,
	nsub: nsub,
	nsubE: nsubE,
	nsube: nsube,
	nsubset: nsubset,
	nsubseteq: nsubseteq,
	nsubseteqq: nsubseteqq,
	nsucc: nsucc,
	nsucceq: nsucceq,
	nsup: nsup,
	nsupE: nsupE,
	nsupe: nsupe,
	nsupset: nsupset,
	nsupseteq: nsupseteq,
	nsupseteqq: nsupseteqq,
	ntgl: ntgl,
	Ntilde: Ntilde,
	ntilde: ntilde,
	ntlg: ntlg,
	ntriangleleft: ntriangleleft,
	ntrianglelefteq: ntrianglelefteq,
	ntriangleright: ntriangleright,
	ntrianglerighteq: ntrianglerighteq,
	Nu: Nu,
	nu: nu,
	num: num,
	numero: numero,
	numsp: numsp,
	nvap: nvap,
	nVDash: nVDash,
	nVdash: nVdash,
	nvDash: nvDash,
	nvdash: nvdash,
	nvge: nvge,
	nvgt: nvgt,
	nvHarr: nvHarr,
	nvinfin: nvinfin,
	nvlArr: nvlArr,
	nvle: nvle,
	nvlt: nvlt,
	nvltrie: nvltrie,
	nvrArr: nvrArr,
	nvrtrie: nvrtrie,
	nvsim: nvsim,
	nwarhk: nwarhk,
	nwArr: nwArr,
	nwarr: nwarr,
	nwarrow: nwarrow,
	nwnear: nwnear,
	Oacute: Oacute,
	oacute: oacute,
	oast: oast,
	ocir: ocir,
	Ocirc: Ocirc,
	ocirc: ocirc,
	Ocy: Ocy,
	ocy: ocy,
	odash: odash,
	Odblac: Odblac,
	odblac: odblac,
	odiv: odiv,
	odot: odot,
	odsold: odsold,
	OElig: OElig,
	oelig: oelig,
	ofcir: ofcir,
	Ofr: Ofr,
	ofr: ofr,
	ogon: ogon,
	Ograve: Ograve,
	ograve: ograve,
	ogt: ogt,
	ohbar: ohbar,
	ohm: ohm,
	oint: oint,
	olarr: olarr,
	olcir: olcir,
	olcross: olcross,
	oline: oline,
	olt: olt,
	Omacr: Omacr,
	omacr: omacr,
	Omega: Omega,
	omega: omega,
	Omicron: Omicron,
	omicron: omicron,
	omid: omid,
	ominus: ominus,
	Oopf: Oopf,
	oopf: oopf,
	opar: opar,
	OpenCurlyDoubleQuote: OpenCurlyDoubleQuote,
	OpenCurlyQuote: OpenCurlyQuote,
	operp: operp,
	oplus: oplus,
	Or: Or,
	or: or,
	orarr: orarr,
	ord: ord,
	order: order,
	orderof: orderof,
	ordf: ordf,
	ordm: ordm,
	origof: origof,
	oror: oror,
	orslope: orslope,
	orv: orv,
	oS: oS,
	Oscr: Oscr,
	oscr: oscr,
	Oslash: Oslash,
	oslash: oslash,
	osol: osol,
	Otilde: Otilde,
	otilde: otilde,
	Otimes: Otimes,
	otimes: otimes,
	otimesas: otimesas,
	Ouml: Ouml,
	ouml: ouml,
	ovbar: ovbar,
	OverBar: OverBar,
	OverBrace: OverBrace,
	OverBracket: OverBracket,
	OverParenthesis: OverParenthesis,
	par: par,
	para: para,
	parallel: parallel,
	parsim: parsim,
	parsl: parsl,
	part: part,
	PartialD: PartialD,
	Pcy: Pcy,
	pcy: pcy,
	percnt: percnt,
	period: period,
	permil: permil,
	perp: perp,
	pertenk: pertenk,
	Pfr: Pfr,
	pfr: pfr,
	Phi: Phi,
	phi: phi,
	phiv: phiv,
	phmmat: phmmat,
	phone: phone,
	Pi: Pi,
	pi: pi,
	pitchfork: pitchfork,
	piv: piv,
	planck: planck,
	planckh: planckh,
	plankv: plankv,
	plus: plus,
	plusacir: plusacir,
	plusb: plusb,
	pluscir: pluscir,
	plusdo: plusdo,
	plusdu: plusdu,
	pluse: pluse,
	PlusMinus: PlusMinus,
	plusmn: plusmn,
	plussim: plussim,
	plustwo: plustwo,
	pm: pm,
	Poincareplane: Poincareplane,
	pointint: pointint,
	Popf: Popf,
	popf: popf,
	pound: pound,
	Pr: Pr,
	pr: pr,
	prap: prap,
	prcue: prcue,
	prE: prE,
	pre: pre,
	prec: prec,
	precapprox: precapprox,
	preccurlyeq: preccurlyeq,
	Precedes: Precedes,
	PrecedesEqual: PrecedesEqual,
	PrecedesSlantEqual: PrecedesSlantEqual,
	PrecedesTilde: PrecedesTilde,
	preceq: preceq,
	precnapprox: precnapprox,
	precneqq: precneqq,
	precnsim: precnsim,
	precsim: precsim,
	Prime: Prime,
	prime: prime,
	primes: primes,
	prnap: prnap,
	prnE: prnE,
	prnsim: prnsim,
	prod: prod,
	Product: Product,
	profalar: profalar,
	profline: profline,
	profsurf: profsurf,
	prop: prop,
	Proportion: Proportion,
	Proportional: Proportional,
	propto: propto,
	prsim: prsim,
	prurel: prurel,
	Pscr: Pscr,
	pscr: pscr,
	Psi: Psi,
	psi: psi,
	puncsp: puncsp,
	Qfr: Qfr,
	qfr: qfr,
	qint: qint,
	Qopf: Qopf,
	qopf: qopf,
	qprime: qprime,
	Qscr: Qscr,
	qscr: qscr,
	quaternions: quaternions,
	quatint: quatint,
	quest: quest,
	questeq: questeq,
	QUOT: QUOT,
	quot: quot,
	rAarr: rAarr,
	race: race,
	Racute: Racute,
	racute: racute,
	radic: radic,
	raemptyv: raemptyv,
	Rang: Rang,
	rang: rang,
	rangd: rangd,
	range: range,
	rangle: rangle,
	raquo: raquo,
	Rarr: Rarr,
	rArr: rArr,
	rarr: rarr,
	rarrap: rarrap,
	rarrb: rarrb,
	rarrbfs: rarrbfs,
	rarrc: rarrc,
	rarrfs: rarrfs,
	rarrhk: rarrhk,
	rarrlp: rarrlp,
	rarrpl: rarrpl,
	rarrsim: rarrsim,
	Rarrtl: Rarrtl,
	rarrtl: rarrtl,
	rarrw: rarrw,
	rAtail: rAtail,
	ratail: ratail,
	ratio: ratio,
	rationals: rationals,
	RBarr: RBarr,
	rBarr: rBarr,
	rbarr: rbarr,
	rbbrk: rbbrk,
	rbrace: rbrace,
	rbrack: rbrack,
	rbrke: rbrke,
	rbrksld: rbrksld,
	rbrkslu: rbrkslu,
	Rcaron: Rcaron,
	rcaron: rcaron,
	Rcedil: Rcedil,
	rcedil: rcedil,
	rceil: rceil,
	rcub: rcub,
	Rcy: Rcy,
	rcy: rcy,
	rdca: rdca,
	rdldhar: rdldhar,
	rdquo: rdquo,
	rdquor: rdquor,
	rdsh: rdsh,
	Re: Re,
	real: real,
	realine: realine,
	realpart: realpart,
	reals: reals,
	rect: rect,
	REG: REG,
	reg: reg,
	ReverseElement: ReverseElement,
	ReverseEquilibrium: ReverseEquilibrium,
	ReverseUpEquilibrium: ReverseUpEquilibrium,
	rfisht: rfisht,
	rfloor: rfloor,
	Rfr: Rfr,
	rfr: rfr,
	rHar: rHar,
	rhard: rhard,
	rharu: rharu,
	rharul: rharul,
	Rho: Rho,
	rho: rho,
	rhov: rhov,
	RightAngleBracket: RightAngleBracket,
	RightArrow: RightArrow,
	Rightarrow: Rightarrow,
	rightarrow: rightarrow,
	RightArrowBar: RightArrowBar,
	RightArrowLeftArrow: RightArrowLeftArrow,
	rightarrowtail: rightarrowtail,
	RightCeiling: RightCeiling,
	RightDoubleBracket: RightDoubleBracket,
	RightDownTeeVector: RightDownTeeVector,
	RightDownVector: RightDownVector,
	RightDownVectorBar: RightDownVectorBar,
	RightFloor: RightFloor,
	rightharpoondown: rightharpoondown,
	rightharpoonup: rightharpoonup,
	rightleftarrows: rightleftarrows,
	rightleftharpoons: rightleftharpoons,
	rightrightarrows: rightrightarrows,
	rightsquigarrow: rightsquigarrow,
	RightTee: RightTee,
	RightTeeArrow: RightTeeArrow,
	RightTeeVector: RightTeeVector,
	rightthreetimes: rightthreetimes,
	RightTriangle: RightTriangle,
	RightTriangleBar: RightTriangleBar,
	RightTriangleEqual: RightTriangleEqual,
	RightUpDownVector: RightUpDownVector,
	RightUpTeeVector: RightUpTeeVector,
	RightUpVector: RightUpVector,
	RightUpVectorBar: RightUpVectorBar,
	RightVector: RightVector,
	RightVectorBar: RightVectorBar,
	ring: ring,
	risingdotseq: risingdotseq,
	rlarr: rlarr,
	rlhar: rlhar,
	rlm: rlm,
	rmoust: rmoust,
	rmoustache: rmoustache,
	rnmid: rnmid,
	roang: roang,
	roarr: roarr,
	robrk: robrk,
	ropar: ropar,
	Ropf: Ropf,
	ropf: ropf,
	roplus: roplus,
	rotimes: rotimes,
	RoundImplies: RoundImplies,
	rpar: rpar,
	rpargt: rpargt,
	rppolint: rppolint,
	rrarr: rrarr,
	Rrightarrow: Rrightarrow,
	rsaquo: rsaquo,
	Rscr: Rscr,
	rscr: rscr,
	Rsh: Rsh,
	rsh: rsh,
	rsqb: rsqb,
	rsquo: rsquo,
	rsquor: rsquor,
	rthree: rthree,
	rtimes: rtimes,
	rtri: rtri,
	rtrie: rtrie,
	rtrif: rtrif,
	rtriltri: rtriltri,
	RuleDelayed: RuleDelayed,
	ruluhar: ruluhar,
	rx: rx,
	Sacute: Sacute,
	sacute: sacute,
	sbquo: sbquo,
	Sc: Sc,
	sc: sc,
	scap: scap,
	Scaron: Scaron,
	scaron: scaron,
	sccue: sccue,
	scE: scE,
	sce: sce,
	Scedil: Scedil,
	scedil: scedil,
	Scirc: Scirc,
	scirc: scirc,
	scnap: scnap,
	scnE: scnE,
	scnsim: scnsim,
	scpolint: scpolint,
	scsim: scsim,
	Scy: Scy,
	scy: scy,
	sdot: sdot,
	sdotb: sdotb,
	sdote: sdote,
	searhk: searhk,
	seArr: seArr,
	searr: searr,
	searrow: searrow,
	sect: sect,
	semi: semi,
	seswar: seswar,
	setminus: setminus,
	setmn: setmn,
	sext: sext,
	Sfr: Sfr,
	sfr: sfr,
	sfrown: sfrown,
	sharp: sharp,
	SHCHcy: SHCHcy,
	shchcy: shchcy,
	SHcy: SHcy,
	shcy: shcy,
	ShortDownArrow: ShortDownArrow,
	ShortLeftArrow: ShortLeftArrow,
	shortmid: shortmid,
	shortparallel: shortparallel,
	ShortRightArrow: ShortRightArrow,
	ShortUpArrow: ShortUpArrow,
	shy: shy,
	Sigma: Sigma,
	sigma: sigma,
	sigmaf: sigmaf,
	sigmav: sigmav,
	sim: sim,
	simdot: simdot,
	sime: sime,
	simeq: simeq,
	simg: simg,
	simgE: simgE,
	siml: siml,
	simlE: simlE,
	simne: simne,
	simplus: simplus,
	simrarr: simrarr,
	slarr: slarr,
	SmallCircle: SmallCircle,
	smallsetminus: smallsetminus,
	smashp: smashp,
	smeparsl: smeparsl,
	smid: smid,
	smile: smile,
	smt: smt,
	smte: smte,
	smtes: smtes,
	SOFTcy: SOFTcy,
	softcy: softcy,
	sol: sol,
	solb: solb,
	solbar: solbar,
	Sopf: Sopf,
	sopf: sopf,
	spades: spades,
	spadesuit: spadesuit,
	spar: spar,
	sqcap: sqcap,
	sqcaps: sqcaps,
	sqcup: sqcup,
	sqcups: sqcups,
	Sqrt: Sqrt,
	sqsub: sqsub,
	sqsube: sqsube,
	sqsubset: sqsubset,
	sqsubseteq: sqsubseteq,
	sqsup: sqsup,
	sqsupe: sqsupe,
	sqsupset: sqsupset,
	sqsupseteq: sqsupseteq,
	squ: squ,
	Square: Square,
	square: square,
	SquareIntersection: SquareIntersection,
	SquareSubset: SquareSubset,
	SquareSubsetEqual: SquareSubsetEqual,
	SquareSuperset: SquareSuperset,
	SquareSupersetEqual: SquareSupersetEqual,
	SquareUnion: SquareUnion,
	squarf: squarf,
	squf: squf,
	srarr: srarr,
	Sscr: Sscr,
	sscr: sscr,
	ssetmn: ssetmn,
	ssmile: ssmile,
	sstarf: sstarf,
	Star: Star,
	star: star,
	starf: starf,
	straightepsilon: straightepsilon,
	straightphi: straightphi,
	strns: strns,
	Sub: Sub,
	sub: sub,
	subdot: subdot,
	subE: subE,
	sube: sube,
	subedot: subedot,
	submult: submult,
	subnE: subnE,
	subne: subne,
	subplus: subplus,
	subrarr: subrarr,
	Subset: Subset,
	subset: subset,
	subseteq: subseteq,
	subseteqq: subseteqq,
	SubsetEqual: SubsetEqual,
	subsetneq: subsetneq,
	subsetneqq: subsetneqq,
	subsim: subsim,
	subsub: subsub,
	subsup: subsup,
	succ: succ,
	succapprox: succapprox,
	succcurlyeq: succcurlyeq,
	Succeeds: Succeeds,
	SucceedsEqual: SucceedsEqual,
	SucceedsSlantEqual: SucceedsSlantEqual,
	SucceedsTilde: SucceedsTilde,
	succeq: succeq,
	succnapprox: succnapprox,
	succneqq: succneqq,
	succnsim: succnsim,
	succsim: succsim,
	SuchThat: SuchThat,
	Sum: Sum,
	sum: sum,
	sung: sung,
	Sup: Sup,
	sup: sup,
	sup1: sup1,
	sup2: sup2,
	sup3: sup3,
	supdot: supdot,
	supdsub: supdsub,
	supE: supE,
	supe: supe,
	supedot: supedot,
	Superset: Superset,
	SupersetEqual: SupersetEqual,
	suphsol: suphsol,
	suphsub: suphsub,
	suplarr: suplarr,
	supmult: supmult,
	supnE: supnE,
	supne: supne,
	supplus: supplus,
	Supset: Supset,
	supset: supset,
	supseteq: supseteq,
	supseteqq: supseteqq,
	supsetneq: supsetneq,
	supsetneqq: supsetneqq,
	supsim: supsim,
	supsub: supsub,
	supsup: supsup,
	swarhk: swarhk,
	swArr: swArr,
	swarr: swarr,
	swarrow: swarrow,
	swnwar: swnwar,
	szlig: szlig,
	Tab: Tab,
	target: target,
	Tau: Tau,
	tau: tau,
	tbrk: tbrk,
	Tcaron: Tcaron,
	tcaron: tcaron,
	Tcedil: Tcedil,
	tcedil: tcedil,
	Tcy: Tcy,
	tcy: tcy,
	tdot: tdot,
	telrec: telrec,
	Tfr: Tfr,
	tfr: tfr,
	there4: there4,
	Therefore: Therefore,
	therefore: therefore,
	Theta: Theta,
	theta: theta,
	thetasym: thetasym,
	thetav: thetav,
	thickapprox: thickapprox,
	thicksim: thicksim,
	ThickSpace: ThickSpace,
	thinsp: thinsp,
	ThinSpace: ThinSpace,
	thkap: thkap,
	thksim: thksim,
	THORN: THORN,
	thorn: thorn,
	Tilde: Tilde,
	tilde: tilde,
	TildeEqual: TildeEqual,
	TildeFullEqual: TildeFullEqual,
	TildeTilde: TildeTilde,
	times: times,
	timesb: timesb,
	timesbar: timesbar,
	timesd: timesd,
	tint: tint,
	toea: toea,
	top: top,
	topbot: topbot,
	topcir: topcir,
	Topf: Topf,
	topf: topf,
	topfork: topfork,
	tosa: tosa,
	tprime: tprime,
	TRADE: TRADE,
	trade: trade,
	triangle: triangle,
	triangledown: triangledown,
	triangleleft: triangleleft,
	trianglelefteq: trianglelefteq,
	triangleq: triangleq,
	triangleright: triangleright,
	trianglerighteq: trianglerighteq,
	tridot: tridot,
	trie: trie,
	triminus: triminus,
	TripleDot: TripleDot,
	triplus: triplus,
	trisb: trisb,
	tritime: tritime,
	trpezium: trpezium,
	Tscr: Tscr,
	tscr: tscr,
	TScy: TScy,
	tscy: tscy,
	TSHcy: TSHcy,
	tshcy: tshcy,
	Tstrok: Tstrok,
	tstrok: tstrok,
	twixt: twixt,
	twoheadleftarrow: twoheadleftarrow,
	twoheadrightarrow: twoheadrightarrow,
	Uacute: Uacute,
	uacute: uacute,
	Uarr: Uarr,
	uArr: uArr,
	uarr: uarr,
	Uarrocir: Uarrocir,
	Ubrcy: Ubrcy,
	ubrcy: ubrcy,
	Ubreve: Ubreve,
	ubreve: ubreve,
	Ucirc: Ucirc,
	ucirc: ucirc,
	Ucy: Ucy,
	ucy: ucy,
	udarr: udarr,
	Udblac: Udblac,
	udblac: udblac,
	udhar: udhar,
	ufisht: ufisht,
	Ufr: Ufr,
	ufr: ufr,
	Ugrave: Ugrave,
	ugrave: ugrave,
	uHar: uHar,
	uharl: uharl,
	uharr: uharr,
	uhblk: uhblk,
	ulcorn: ulcorn,
	ulcorner: ulcorner,
	ulcrop: ulcrop,
	ultri: ultri,
	Umacr: Umacr,
	umacr: umacr,
	uml: uml,
	UnderBar: UnderBar,
	UnderBrace: UnderBrace,
	UnderBracket: UnderBracket,
	UnderParenthesis: UnderParenthesis,
	Union: Union,
	UnionPlus: UnionPlus,
	Uogon: Uogon,
	uogon: uogon,
	Uopf: Uopf,
	uopf: uopf,
	UpArrow: UpArrow,
	Uparrow: Uparrow,
	uparrow: uparrow,
	UpArrowBar: UpArrowBar,
	UpArrowDownArrow: UpArrowDownArrow,
	UpDownArrow: UpDownArrow,
	Updownarrow: Updownarrow,
	updownarrow: updownarrow,
	UpEquilibrium: UpEquilibrium,
	upharpoonleft: upharpoonleft,
	upharpoonright: upharpoonright,
	uplus: uplus,
	UpperLeftArrow: UpperLeftArrow,
	UpperRightArrow: UpperRightArrow,
	Upsi: Upsi,
	upsi: upsi,
	upsih: upsih,
	Upsilon: Upsilon,
	upsilon: upsilon,
	UpTee: UpTee,
	UpTeeArrow: UpTeeArrow,
	upuparrows: upuparrows,
	urcorn: urcorn,
	urcorner: urcorner,
	urcrop: urcrop,
	Uring: Uring,
	uring: uring,
	urtri: urtri,
	Uscr: Uscr,
	uscr: uscr,
	utdot: utdot,
	Utilde: Utilde,
	utilde: utilde,
	utri: utri,
	utrif: utrif,
	uuarr: uuarr,
	Uuml: Uuml,
	uuml: uuml,
	uwangle: uwangle,
	vangrt: vangrt,
	varepsilon: varepsilon,
	varkappa: varkappa,
	varnothing: varnothing,
	varphi: varphi,
	varpi: varpi,
	varpropto: varpropto,
	vArr: vArr,
	varr: varr,
	varrho: varrho,
	varsigma: varsigma,
	varsubsetneq: varsubsetneq,
	varsubsetneqq: varsubsetneqq,
	varsupsetneq: varsupsetneq,
	varsupsetneqq: varsupsetneqq,
	vartheta: vartheta,
	vartriangleleft: vartriangleleft,
	vartriangleright: vartriangleright,
	Vbar: Vbar,
	vBar: vBar,
	vBarv: vBarv,
	Vcy: Vcy,
	vcy: vcy,
	VDash: VDash,
	Vdash: Vdash,
	vDash: vDash,
	vdash: vdash,
	Vdashl: Vdashl,
	Vee: Vee,
	vee: vee,
	veebar: veebar,
	veeeq: veeeq,
	vellip: vellip,
	Verbar: Verbar,
	verbar: verbar,
	Vert: Vert,
	vert: vert,
	VerticalBar: VerticalBar,
	VerticalLine: VerticalLine,
	VerticalSeparator: VerticalSeparator,
	VerticalTilde: VerticalTilde,
	VeryThinSpace: VeryThinSpace,
	Vfr: Vfr,
	vfr: vfr,
	vltri: vltri,
	vnsub: vnsub,
	vnsup: vnsup,
	Vopf: Vopf,
	vopf: vopf,
	vprop: vprop,
	vrtri: vrtri,
	Vscr: Vscr,
	vscr: vscr,
	vsubnE: vsubnE,
	vsubne: vsubne,
	vsupnE: vsupnE,
	vsupne: vsupne,
	Vvdash: Vvdash,
	vzigzag: vzigzag,
	Wcirc: Wcirc,
	wcirc: wcirc,
	wedbar: wedbar,
	Wedge: Wedge,
	wedge: wedge,
	wedgeq: wedgeq,
	weierp: weierp,
	Wfr: Wfr,
	wfr: wfr,
	Wopf: Wopf,
	wopf: wopf,
	wp: wp,
	wr: wr,
	wreath: wreath,
	Wscr: Wscr,
	wscr: wscr,
	xcap: xcap,
	xcirc: xcirc,
	xcup: xcup,
	xdtri: xdtri,
	Xfr: Xfr,
	xfr: xfr,
	xhArr: xhArr,
	xharr: xharr,
	Xi: Xi,
	xi: xi,
	xlArr: xlArr,
	xlarr: xlarr,
	xmap: xmap,
	xnis: xnis,
	xodot: xodot,
	Xopf: Xopf,
	xopf: xopf,
	xoplus: xoplus,
	xotime: xotime,
	xrArr: xrArr,
	xrarr: xrarr,
	Xscr: Xscr,
	xscr: xscr,
	xsqcup: xsqcup,
	xuplus: xuplus,
	xutri: xutri,
	xvee: xvee,
	xwedge: xwedge,
	Yacute: Yacute,
	yacute: yacute,
	YAcy: YAcy,
	yacy: yacy,
	Ycirc: Ycirc,
	ycirc: ycirc,
	Ycy: Ycy,
	ycy: ycy,
	yen: yen,
	Yfr: Yfr,
	yfr: yfr,
	YIcy: YIcy,
	yicy: yicy,
	Yopf: Yopf,
	yopf: yopf,
	Yscr: Yscr,
	yscr: yscr,
	YUcy: YUcy,
	yucy: yucy,
	Yuml: Yuml,
	yuml: yuml,
	Zacute: Zacute,
	zacute: zacute,
	Zcaron: Zcaron,
	zcaron: zcaron,
	Zcy: Zcy,
	zcy: zcy,
	Zdot: Zdot,
	zdot: zdot,
	zeetrf: zeetrf,
	ZeroWidthSpace: ZeroWidthSpace,
	Zeta: Zeta,
	zeta: zeta,
	Zfr: Zfr,
	zfr: zfr,
	ZHcy: ZHcy,
	zhcy: zhcy,
	zigrarr: zigrarr,
	Zopf: Zopf,
	zopf: zopf,
	Zscr: Zscr,
	zscr: zscr,
	zwj: zwj,
	zwnj: zwnj
};

var ound = "pound";
var pond = "pound";
var poubd = "pound";
var poud = "pound";
var poumd = "pound";
var poun = "pound";
var pund = "pound";
var zvbj = "zwnj";
var zvhj = "zwnj";
var zvjb = "zwnj";
var zvjh = "zwnj";
var zvjm = "zwnj";
var zvjn = "zwnj";
var zvmj = "zwnj";
var zvng = "zwnj";
var zvnh = "zwnj";
var zvnj = "zwnj";
var zvnk = "zwnj";
var zvnm = "zwnj";
var zwbj = "zwnj";
var zwhj = "zwnj";
var zwjb = "zwnj";
var zwjh = "zwnj";
var zwjm = "zwnj";
var zwjn = "zwnj";
var zwmj = "zwnj";
var zwng = "zwnj";
var zwnh = "zwnj";
var zwnk = "zwnj";
var zwnm = "zwnj";
var brokenNamedEntities = {
	ound: ound,
	pond: pond,
	poubd: poubd,
	poud: poud,
	poumd: poumd,
	poun: poun,
	pund: pund,
	zvbj: zvbj,
	zvhj: zvhj,
	zvjb: zvjb,
	zvjh: zvjh,
	zvjm: zvjm,
	zvjn: zvjn,
	zvmj: zvmj,
	zvng: zvng,
	zvnh: zvnh,
	zvnj: zvnj,
	zvnk: zvnk,
	zvnm: zvnm,
	zwbj: zwbj,
	zwhj: zwhj,
	zwjb: zwjb,
	zwjh: zwjh,
	zwjm: zwjm,
	zwjn: zwjn,
	zwmj: zwmj,
	zwng: zwng,
	zwnh: zwnh,
	zwnk: zwnk,
	zwnm: zwnm
};

var A = {
	a: [
		"Aacute"
	],
	b: [
		"Abreve"
	],
	c: [
		"Acirc",
		"Acy"
	],
	E: [
		"AElig"
	],
	f: [
		"Afr"
	],
	g: [
		"Agrave"
	],
	l: [
		"Alpha"
	],
	m: [
		"Amacr"
	],
	M: [
		"AMP"
	],
	n: [
		"And"
	],
	o: [
		"Aogon",
		"Aopf"
	],
	p: [
		"ApplyFunction"
	],
	r: [
		"Aring"
	],
	s: [
		"Ascr",
		"Assign"
	],
	t: [
		"Atilde"
	],
	u: [
		"Auml"
	]
};
var a = {
	a: [
		"aacute"
	],
	b: [
		"abreve"
	],
	c: [
		"ac",
		"acd",
		"acE",
		"acirc",
		"acute",
		"acy"
	],
	e: [
		"aelig"
	],
	f: [
		"af",
		"afr"
	],
	g: [
		"agrave"
	],
	l: [
		"alefsym",
		"aleph",
		"alpha"
	],
	m: [
		"amacr",
		"amalg",
		"amp"
	],
	n: [
		"and",
		"andand",
		"andd",
		"andslope",
		"andv",
		"ang",
		"ange",
		"angle",
		"angmsd",
		"angmsdaa",
		"angmsdab",
		"angmsdac",
		"angmsdad",
		"angmsdae",
		"angmsdaf",
		"angmsdag",
		"angmsdah",
		"angrt",
		"angrtvb",
		"angrtvbd",
		"angsph",
		"angst",
		"angzarr"
	],
	o: [
		"aogon",
		"aopf"
	],
	p: [
		"ap",
		"apacir",
		"apE",
		"ape",
		"apid",
		"apos",
		"approx",
		"approxeq"
	],
	r: [
		"aring"
	],
	s: [
		"ascr",
		"ast",
		"asymp",
		"asympeq"
	],
	t: [
		"atilde"
	],
	u: [
		"auml"
	],
	w: [
		"awconint",
		"awint"
	]
};
var b = {
	a: [
		"backcong",
		"backepsilon",
		"backprime",
		"backsim",
		"backsimeq",
		"barvee",
		"barwed",
		"barwedge"
	],
	b: [
		"bbrk",
		"bbrktbrk"
	],
	c: [
		"bcong",
		"bcy"
	],
	d: [
		"bdquo"
	],
	e: [
		"becaus",
		"because",
		"bemptyv",
		"bepsi",
		"bernou",
		"beta",
		"beth",
		"between"
	],
	f: [
		"bfr"
	],
	i: [
		"bigcap",
		"bigcirc",
		"bigcup",
		"bigodot",
		"bigoplus",
		"bigotimes",
		"bigsqcup",
		"bigstar",
		"bigtriangledown",
		"bigtriangleup",
		"biguplus",
		"bigvee",
		"bigwedge"
	],
	k: [
		"bkarow"
	],
	l: [
		"blacklozenge",
		"blacksquare",
		"blacktriangle",
		"blacktriangledown",
		"blacktriangleleft",
		"blacktriangleright",
		"blank",
		"blk12",
		"blk14",
		"blk34",
		"block"
	],
	n: [
		"bne",
		"bnequiv",
		"bnot"
	],
	N: [
		"bNot"
	],
	o: [
		"bopf",
		"bot",
		"bottom",
		"bowtie",
		"boxbox",
		"boxDL",
		"boxDl",
		"boxdL",
		"boxdl",
		"boxDR",
		"boxDr",
		"boxdR",
		"boxdr",
		"boxH",
		"boxh",
		"boxHD",
		"boxHd",
		"boxhD",
		"boxhd",
		"boxHU",
		"boxHu",
		"boxhU",
		"boxhu",
		"boxminus",
		"boxplus",
		"boxtimes",
		"boxUL",
		"boxUl",
		"boxuL",
		"boxul",
		"boxUR",
		"boxUr",
		"boxuR",
		"boxur",
		"boxV",
		"boxv",
		"boxVH",
		"boxVh",
		"boxvH",
		"boxvh",
		"boxVL",
		"boxVl",
		"boxvL",
		"boxvl",
		"boxVR",
		"boxVr",
		"boxvR",
		"boxvr"
	],
	p: [
		"bprime"
	],
	r: [
		"breve",
		"brvbar"
	],
	s: [
		"bscr",
		"bsemi",
		"bsim",
		"bsime",
		"bsol",
		"bsolb",
		"bsolhsub"
	],
	u: [
		"bull",
		"bullet",
		"bump",
		"bumpE",
		"bumpe",
		"bumpeq"
	]
};
var B = {
	a: [
		"Backslash",
		"Barv",
		"Barwed"
	],
	c: [
		"Bcy"
	],
	e: [
		"Because",
		"Bernoullis",
		"Beta"
	],
	f: [
		"Bfr"
	],
	o: [
		"Bopf"
	],
	r: [
		"Breve"
	],
	s: [
		"Bscr"
	],
	u: [
		"Bumpeq"
	]
};
var C = {
	a: [
		"Cacute",
		"Cap",
		"CapitalDifferentialD",
		"Cayleys"
	],
	c: [
		"Ccaron",
		"Ccedil",
		"Ccirc",
		"Cconint"
	],
	d: [
		"Cdot"
	],
	e: [
		"Cedilla",
		"CenterDot"
	],
	f: [
		"Cfr"
	],
	H: [
		"CHcy"
	],
	h: [
		"Chi"
	],
	i: [
		"CircleDot",
		"CircleMinus",
		"CirclePlus",
		"CircleTimes"
	],
	l: [
		"ClockwiseContourIntegral",
		"CloseCurlyDoubleQuote",
		"CloseCurlyQuote"
	],
	o: [
		"Colon",
		"Colone",
		"Congruent",
		"Conint",
		"ContourIntegral",
		"Copf",
		"Coproduct",
		"CounterClockwiseContourIntegral"
	],
	O: [
		"COPY"
	],
	r: [
		"Cross"
	],
	s: [
		"Cscr"
	],
	u: [
		"Cup",
		"CupCap"
	]
};
var c = {
	a: [
		"cacute",
		"cap",
		"capand",
		"capbrcup",
		"capcap",
		"capcup",
		"capdot",
		"caps",
		"caret",
		"caron"
	],
	c: [
		"ccaps",
		"ccaron",
		"ccedil",
		"ccirc",
		"ccups",
		"ccupssm"
	],
	d: [
		"cdot"
	],
	e: [
		"cedil",
		"cemptyv",
		"cent",
		"centerdot"
	],
	f: [
		"cfr"
	],
	h: [
		"chcy",
		"check",
		"checkmark",
		"chi"
	],
	i: [
		"cir",
		"circ",
		"circeq",
		"circlearrowleft",
		"circlearrowright",
		"circledast",
		"circledcirc",
		"circleddash",
		"circledR",
		"circledS",
		"cirE",
		"cire",
		"cirfnint",
		"cirmid",
		"cirscir"
	],
	l: [
		"clubs",
		"clubsuit"
	],
	o: [
		"colon",
		"colone",
		"coloneq",
		"comma",
		"commat",
		"comp",
		"compfn",
		"complement",
		"complexes",
		"cong",
		"congdot",
		"conint",
		"copf",
		"coprod",
		"copy",
		"copysr"
	],
	r: [
		"crarr",
		"cross"
	],
	s: [
		"cscr",
		"csub",
		"csube",
		"csup",
		"csupe"
	],
	t: [
		"ctdot"
	],
	u: [
		"cudarrl",
		"cudarrr",
		"cuepr",
		"cuesc",
		"cularr",
		"cularrp",
		"cup",
		"cupbrcap",
		"cupcap",
		"cupcup",
		"cupdot",
		"cupor",
		"cups",
		"curarr",
		"curarrm",
		"curlyeqprec",
		"curlyeqsucc",
		"curlyvee",
		"curlywedge",
		"curren",
		"curvearrowleft",
		"curvearrowright",
		"cuvee",
		"cuwed"
	],
	w: [
		"cwconint",
		"cwint"
	],
	y: [
		"cylcty"
	]
};
var D = {
	a: [
		"Dagger",
		"Darr",
		"Dashv"
	],
	c: [
		"Dcaron",
		"Dcy"
	],
	D: [
		"DD",
		"DDotrahd"
	],
	e: [
		"Del",
		"Delta"
	],
	f: [
		"Dfr"
	],
	i: [
		"DiacriticalAcute",
		"DiacriticalDot",
		"DiacriticalDoubleAcute",
		"DiacriticalGrave",
		"DiacriticalTilde",
		"Diamond",
		"DifferentialD"
	],
	J: [
		"DJcy"
	],
	o: [
		"Dopf",
		"Dot",
		"DotDot",
		"DotEqual",
		"DoubleContourIntegral",
		"DoubleDot",
		"DoubleDownArrow",
		"DoubleLeftArrow",
		"DoubleLeftRightArrow",
		"DoubleLeftTee",
		"DoubleLongLeftArrow",
		"DoubleLongLeftRightArrow",
		"DoubleLongRightArrow",
		"DoubleRightArrow",
		"DoubleRightTee",
		"DoubleUpArrow",
		"DoubleUpDownArrow",
		"DoubleVerticalBar",
		"DownArrow",
		"Downarrow",
		"DownArrowBar",
		"DownArrowUpArrow",
		"DownBreve",
		"DownLeftRightVector",
		"DownLeftTeeVector",
		"DownLeftVector",
		"DownLeftVectorBar",
		"DownRightTeeVector",
		"DownRightVector",
		"DownRightVectorBar",
		"DownTee",
		"DownTeeArrow"
	],
	s: [
		"Dscr",
		"Dstrok"
	],
	S: [
		"DScy"
	],
	Z: [
		"DZcy"
	]
};
var d = {
	a: [
		"dagger",
		"daleth",
		"darr",
		"dash",
		"dashv"
	],
	A: [
		"dArr"
	],
	b: [
		"dbkarow",
		"dblac"
	],
	c: [
		"dcaron",
		"dcy"
	],
	d: [
		"dd",
		"ddagger",
		"ddarr",
		"ddotseq"
	],
	e: [
		"deg",
		"delta",
		"demptyv"
	],
	f: [
		"dfisht",
		"dfr"
	],
	H: [
		"dHar"
	],
	h: [
		"dharl",
		"dharr"
	],
	i: [
		"diam",
		"diamond",
		"diamondsuit",
		"diams",
		"die",
		"digamma",
		"disin",
		"div",
		"divide",
		"divideontimes",
		"divonx"
	],
	j: [
		"djcy"
	],
	l: [
		"dlcorn",
		"dlcrop"
	],
	o: [
		"dollar",
		"dopf",
		"dot",
		"doteq",
		"doteqdot",
		"dotminus",
		"dotplus",
		"dotsquare",
		"doublebarwedge",
		"downarrow",
		"downdownarrows",
		"downharpoonleft",
		"downharpoonright"
	],
	r: [
		"drbkarow",
		"drcorn",
		"drcrop"
	],
	s: [
		"dscr",
		"dscy",
		"dsol",
		"dstrok"
	],
	t: [
		"dtdot",
		"dtri",
		"dtrif"
	],
	u: [
		"duarr",
		"duhar"
	],
	w: [
		"dwangle"
	],
	z: [
		"dzcy",
		"dzigrarr"
	]
};
var E = {
	a: [
		"Eacute"
	],
	c: [
		"Ecaron",
		"Ecirc",
		"Ecy"
	],
	d: [
		"Edot"
	],
	f: [
		"Efr"
	],
	g: [
		"Egrave"
	],
	l: [
		"Element"
	],
	m: [
		"Emacr",
		"EmptySmallSquare",
		"EmptyVerySmallSquare"
	],
	N: [
		"ENG"
	],
	o: [
		"Eogon",
		"Eopf"
	],
	p: [
		"Epsilon"
	],
	q: [
		"Equal",
		"EqualTilde",
		"Equilibrium"
	],
	s: [
		"Escr",
		"Esim"
	],
	t: [
		"Eta"
	],
	T: [
		"ETH"
	],
	u: [
		"Euml"
	],
	x: [
		"Exists",
		"ExponentialE"
	]
};
var e = {
	a: [
		"eacute",
		"easter"
	],
	c: [
		"ecaron",
		"ecir",
		"ecirc",
		"ecolon",
		"ecy"
	],
	D: [
		"eDDot",
		"eDot"
	],
	d: [
		"edot"
	],
	e: [
		"ee"
	],
	f: [
		"efDot",
		"efr"
	],
	g: [
		"eg",
		"egrave",
		"egs",
		"egsdot"
	],
	l: [
		"el",
		"elinters",
		"ell",
		"els",
		"elsdot"
	],
	m: [
		"emacr",
		"empty",
		"emptyset",
		"emptyv",
		"emsp",
		"emsp13",
		"emsp14"
	],
	n: [
		"eng",
		"ensp"
	],
	o: [
		"eogon",
		"eopf"
	],
	p: [
		"epar",
		"eparsl",
		"eplus",
		"epsi",
		"epsilon",
		"epsiv"
	],
	q: [
		"eqcirc",
		"eqcolon",
		"eqsim",
		"eqslantgtr",
		"eqslantless",
		"equals",
		"equest",
		"equiv",
		"equivDD",
		"eqvparsl"
	],
	r: [
		"erarr",
		"erDot"
	],
	s: [
		"escr",
		"esdot",
		"esim"
	],
	t: [
		"eta",
		"eth"
	],
	u: [
		"euml",
		"euro"
	],
	x: [
		"excl",
		"exist",
		"expectation",
		"exponentiale"
	]
};
var f = {
	a: [
		"fallingdotseq"
	],
	c: [
		"fcy"
	],
	e: [
		"female"
	],
	f: [
		"ffilig",
		"fflig",
		"ffllig",
		"ffr"
	],
	i: [
		"filig"
	],
	j: [
		"fjlig"
	],
	l: [
		"flat",
		"fllig",
		"fltns"
	],
	n: [
		"fnof"
	],
	o: [
		"fopf",
		"forall",
		"fork",
		"forkv"
	],
	p: [
		"fpartint"
	],
	r: [
		"frac12",
		"frac13",
		"frac14",
		"frac15",
		"frac16",
		"frac18",
		"frac23",
		"frac25",
		"frac34",
		"frac35",
		"frac38",
		"frac45",
		"frac56",
		"frac58",
		"frac78",
		"frasl",
		"frown"
	],
	s: [
		"fscr"
	]
};
var F = {
	c: [
		"Fcy"
	],
	f: [
		"Ffr"
	],
	i: [
		"FilledSmallSquare",
		"FilledVerySmallSquare"
	],
	o: [
		"Fopf",
		"ForAll",
		"Fouriertrf"
	],
	s: [
		"Fscr"
	]
};
var g = {
	a: [
		"gacute",
		"gamma",
		"gammad",
		"gap"
	],
	b: [
		"gbreve"
	],
	c: [
		"gcirc",
		"gcy"
	],
	d: [
		"gdot"
	],
	E: [
		"gE",
		"gEl"
	],
	e: [
		"ge",
		"gel",
		"geq",
		"geqq",
		"geqslant",
		"ges",
		"gescc",
		"gesdot",
		"gesdoto",
		"gesdotol",
		"gesl",
		"gesles"
	],
	f: [
		"gfr"
	],
	g: [
		"gg",
		"ggg"
	],
	i: [
		"gimel"
	],
	j: [
		"gjcy"
	],
	l: [
		"gl",
		"gla",
		"glE",
		"glj"
	],
	n: [
		"gnap",
		"gnapprox",
		"gnE",
		"gne",
		"gneq",
		"gneqq",
		"gnsim"
	],
	o: [
		"gopf"
	],
	r: [
		"grave"
	],
	s: [
		"gscr",
		"gsim",
		"gsime",
		"gsiml"
	],
	t: [
		"gt",
		"gtcc",
		"gtcir",
		"gtdot",
		"gtlPar",
		"gtquest",
		"gtrapprox",
		"gtrarr",
		"gtrdot",
		"gtreqless",
		"gtreqqless",
		"gtrless",
		"gtrsim"
	],
	v: [
		"gvertneqq",
		"gvnE"
	]
};
var G = {
	a: [
		"Gamma",
		"Gammad"
	],
	b: [
		"Gbreve"
	],
	c: [
		"Gcedil",
		"Gcirc",
		"Gcy"
	],
	d: [
		"Gdot"
	],
	f: [
		"Gfr"
	],
	g: [
		"Gg"
	],
	J: [
		"GJcy"
	],
	o: [
		"Gopf"
	],
	r: [
		"GreaterEqual",
		"GreaterEqualLess",
		"GreaterFullEqual",
		"GreaterGreater",
		"GreaterLess",
		"GreaterSlantEqual",
		"GreaterTilde"
	],
	s: [
		"Gscr"
	],
	T: [
		"GT"
	],
	t: [
		"Gt"
	]
};
var H = {
	a: [
		"Hacek",
		"Hat"
	],
	A: [
		"HARDcy"
	],
	c: [
		"Hcirc"
	],
	f: [
		"Hfr"
	],
	i: [
		"HilbertSpace"
	],
	o: [
		"Hopf",
		"HorizontalLine"
	],
	s: [
		"Hscr",
		"Hstrok"
	],
	u: [
		"HumpDownHump",
		"HumpEqual"
	]
};
var h = {
	a: [
		"hairsp",
		"half",
		"hamilt",
		"hardcy",
		"harr",
		"harrcir",
		"harrw"
	],
	A: [
		"hArr"
	],
	b: [
		"hbar"
	],
	c: [
		"hcirc"
	],
	e: [
		"hearts",
		"heartsuit",
		"hellip",
		"hercon"
	],
	f: [
		"hfr"
	],
	k: [
		"hksearow",
		"hkswarow"
	],
	o: [
		"hoarr",
		"homtht",
		"hookleftarrow",
		"hookrightarrow",
		"hopf",
		"horbar"
	],
	s: [
		"hscr",
		"hslash",
		"hstrok"
	],
	y: [
		"hybull",
		"hyphen"
	]
};
var I = {
	a: [
		"Iacute"
	],
	c: [
		"Icirc",
		"Icy"
	],
	d: [
		"Idot"
	],
	E: [
		"IEcy"
	],
	f: [
		"Ifr"
	],
	g: [
		"Igrave"
	],
	J: [
		"IJlig"
	],
	m: [
		"Im",
		"Imacr",
		"ImaginaryI",
		"Implies"
	],
	n: [
		"Int",
		"Integral",
		"Intersection",
		"InvisibleComma",
		"InvisibleTimes"
	],
	O: [
		"IOcy"
	],
	o: [
		"Iogon",
		"Iopf",
		"Iota"
	],
	s: [
		"Iscr"
	],
	t: [
		"Itilde"
	],
	u: [
		"Iukcy",
		"Iuml"
	]
};
var i = {
	a: [
		"iacute"
	],
	c: [
		"ic",
		"icirc",
		"icy"
	],
	e: [
		"iecy",
		"iexcl"
	],
	f: [
		"iff",
		"ifr"
	],
	g: [
		"igrave"
	],
	i: [
		"ii",
		"iiiint",
		"iiint",
		"iinfin",
		"iiota"
	],
	j: [
		"ijlig"
	],
	m: [
		"imacr",
		"image",
		"imagline",
		"imagpart",
		"imath",
		"imof",
		"imped"
	],
	n: [
		"in",
		"incare",
		"infin",
		"infintie",
		"inodot",
		"int",
		"intcal",
		"integers",
		"intercal",
		"intlarhk",
		"intprod"
	],
	o: [
		"iocy",
		"iogon",
		"iopf",
		"iota"
	],
	p: [
		"iprod"
	],
	q: [
		"iquest"
	],
	s: [
		"iscr",
		"isin",
		"isindot",
		"isinE",
		"isins",
		"isinsv",
		"isinv"
	],
	t: [
		"it",
		"itilde"
	],
	u: [
		"iukcy",
		"iuml"
	]
};
var J = {
	c: [
		"Jcirc",
		"Jcy"
	],
	f: [
		"Jfr"
	],
	o: [
		"Jopf"
	],
	s: [
		"Jscr",
		"Jsercy"
	],
	u: [
		"Jukcy"
	]
};
var j = {
	c: [
		"jcirc",
		"jcy"
	],
	f: [
		"jfr"
	],
	m: [
		"jmath"
	],
	o: [
		"jopf"
	],
	s: [
		"jscr",
		"jsercy"
	],
	u: [
		"jukcy"
	]
};
var K = {
	a: [
		"Kappa"
	],
	c: [
		"Kcedil",
		"Kcy"
	],
	f: [
		"Kfr"
	],
	H: [
		"KHcy"
	],
	J: [
		"KJcy"
	],
	o: [
		"Kopf"
	],
	s: [
		"Kscr"
	]
};
var k = {
	a: [
		"kappa",
		"kappav"
	],
	c: [
		"kcedil",
		"kcy"
	],
	f: [
		"kfr"
	],
	g: [
		"kgreen"
	],
	h: [
		"khcy"
	],
	j: [
		"kjcy"
	],
	o: [
		"kopf"
	],
	s: [
		"kscr"
	]
};
var l = {
	A: [
		"lAarr",
		"lArr",
		"lAtail"
	],
	a: [
		"lacute",
		"laemptyv",
		"lagran",
		"lambda",
		"lang",
		"langd",
		"langle",
		"lap",
		"laquo",
		"larr",
		"larrb",
		"larrbfs",
		"larrfs",
		"larrhk",
		"larrlp",
		"larrpl",
		"larrsim",
		"larrtl",
		"lat",
		"latail",
		"late",
		"lates"
	],
	B: [
		"lBarr"
	],
	b: [
		"lbarr",
		"lbbrk",
		"lbrace",
		"lbrack",
		"lbrke",
		"lbrksld",
		"lbrkslu"
	],
	c: [
		"lcaron",
		"lcedil",
		"lceil",
		"lcub",
		"lcy"
	],
	d: [
		"ldca",
		"ldquo",
		"ldquor",
		"ldrdhar",
		"ldrushar",
		"ldsh"
	],
	E: [
		"lE",
		"lEg"
	],
	e: [
		"le",
		"leftarrow",
		"leftarrowtail",
		"leftharpoondown",
		"leftharpoonup",
		"leftleftarrows",
		"leftrightarrow",
		"leftrightarrows",
		"leftrightharpoons",
		"leftrightsquigarrow",
		"leftthreetimes",
		"leg",
		"leq",
		"leqq",
		"leqslant",
		"les",
		"lescc",
		"lesdot",
		"lesdoto",
		"lesdotor",
		"lesg",
		"lesges",
		"lessapprox",
		"lessdot",
		"lesseqgtr",
		"lesseqqgtr",
		"lessgtr",
		"lesssim"
	],
	f: [
		"lfisht",
		"lfloor",
		"lfr"
	],
	g: [
		"lg",
		"lgE"
	],
	H: [
		"lHar"
	],
	h: [
		"lhard",
		"lharu",
		"lharul",
		"lhblk"
	],
	j: [
		"ljcy"
	],
	l: [
		"ll",
		"llarr",
		"llcorner",
		"llhard",
		"lltri"
	],
	m: [
		"lmidot",
		"lmoust",
		"lmoustache"
	],
	n: [
		"lnap",
		"lnapprox",
		"lnE",
		"lne",
		"lneq",
		"lneqq",
		"lnsim"
	],
	o: [
		"loang",
		"loarr",
		"lobrk",
		"longleftarrow",
		"longleftrightarrow",
		"longmapsto",
		"longrightarrow",
		"looparrowleft",
		"looparrowright",
		"lopar",
		"lopf",
		"loplus",
		"lotimes",
		"lowast",
		"lowbar",
		"loz",
		"lozenge",
		"lozf"
	],
	p: [
		"lpar",
		"lparlt"
	],
	r: [
		"lrarr",
		"lrcorner",
		"lrhar",
		"lrhard",
		"lrm",
		"lrtri"
	],
	s: [
		"lsaquo",
		"lscr",
		"lsh",
		"lsim",
		"lsime",
		"lsimg",
		"lsqb",
		"lsquo",
		"lsquor",
		"lstrok"
	],
	t: [
		"lt",
		"ltcc",
		"ltcir",
		"ltdot",
		"lthree",
		"ltimes",
		"ltlarr",
		"ltquest",
		"ltri",
		"ltrie",
		"ltrif",
		"ltrPar"
	],
	u: [
		"lurdshar",
		"luruhar"
	],
	v: [
		"lvertneqq",
		"lvnE"
	]
};
var L = {
	a: [
		"Lacute",
		"Lambda",
		"Lang",
		"Laplacetrf",
		"Larr"
	],
	c: [
		"Lcaron",
		"Lcedil",
		"Lcy"
	],
	e: [
		"LeftAngleBracket",
		"LeftArrow",
		"Leftarrow",
		"LeftArrowBar",
		"LeftArrowRightArrow",
		"LeftCeiling",
		"LeftDoubleBracket",
		"LeftDownTeeVector",
		"LeftDownVector",
		"LeftDownVectorBar",
		"LeftFloor",
		"LeftRightArrow",
		"Leftrightarrow",
		"LeftRightVector",
		"LeftTee",
		"LeftTeeArrow",
		"LeftTeeVector",
		"LeftTriangle",
		"LeftTriangleBar",
		"LeftTriangleEqual",
		"LeftUpDownVector",
		"LeftUpTeeVector",
		"LeftUpVector",
		"LeftUpVectorBar",
		"LeftVector",
		"LeftVectorBar",
		"LessEqualGreater",
		"LessFullEqual",
		"LessGreater",
		"LessLess",
		"LessSlantEqual",
		"LessTilde"
	],
	f: [
		"Lfr"
	],
	J: [
		"LJcy"
	],
	l: [
		"Ll",
		"Lleftarrow"
	],
	m: [
		"Lmidot"
	],
	o: [
		"LongLeftArrow",
		"Longleftarrow",
		"LongLeftRightArrow",
		"Longleftrightarrow",
		"LongRightArrow",
		"Longrightarrow",
		"Lopf",
		"LowerLeftArrow",
		"LowerRightArrow"
	],
	s: [
		"Lscr",
		"Lsh",
		"Lstrok"
	],
	T: [
		"LT"
	],
	t: [
		"Lt"
	]
};
var m = {
	a: [
		"macr",
		"male",
		"malt",
		"maltese",
		"map",
		"mapsto",
		"mapstodown",
		"mapstoleft",
		"mapstoup",
		"marker"
	],
	c: [
		"mcomma",
		"mcy"
	],
	d: [
		"mdash"
	],
	D: [
		"mDDot"
	],
	e: [
		"measuredangle"
	],
	f: [
		"mfr"
	],
	h: [
		"mho"
	],
	i: [
		"micro",
		"mid",
		"midast",
		"midcir",
		"middot",
		"minus",
		"minusb",
		"minusd",
		"minusdu"
	],
	l: [
		"mlcp",
		"mldr"
	],
	n: [
		"mnplus"
	],
	o: [
		"models",
		"mopf"
	],
	p: [
		"mp"
	],
	s: [
		"mscr",
		"mstpos"
	],
	u: [
		"mu",
		"multimap",
		"mumap"
	]
};
var M = {
	a: [
		"Map"
	],
	c: [
		"Mcy"
	],
	e: [
		"MediumSpace",
		"Mellintrf"
	],
	f: [
		"Mfr"
	],
	i: [
		"MinusPlus"
	],
	o: [
		"Mopf"
	],
	s: [
		"Mscr"
	],
	u: [
		"Mu"
	]
};
var n = {
	a: [
		"nabla",
		"nacute",
		"nang",
		"nap",
		"napE",
		"napid",
		"napos",
		"napprox",
		"natur",
		"natural",
		"naturals"
	],
	b: [
		"nbsp",
		"nbump",
		"nbumpe"
	],
	c: [
		"ncap",
		"ncaron",
		"ncedil",
		"ncong",
		"ncongdot",
		"ncup",
		"ncy"
	],
	d: [
		"ndash"
	],
	e: [
		"ne",
		"nearhk",
		"neArr",
		"nearr",
		"nearrow",
		"nedot",
		"nequiv",
		"nesear",
		"nesim",
		"nexist",
		"nexists"
	],
	f: [
		"nfr"
	],
	g: [
		"ngE",
		"nge",
		"ngeq",
		"ngeqq",
		"ngeqslant",
		"nges",
		"ngsim",
		"ngt",
		"ngtr"
	],
	G: [
		"nGg",
		"nGt",
		"nGtv"
	],
	h: [
		"nhArr",
		"nharr",
		"nhpar"
	],
	i: [
		"ni",
		"nis",
		"nisd",
		"niv"
	],
	j: [
		"njcy"
	],
	l: [
		"nlArr",
		"nlarr",
		"nldr",
		"nlE",
		"nle",
		"nleftarrow",
		"nleftrightarrow",
		"nleq",
		"nleqq",
		"nleqslant",
		"nles",
		"nless",
		"nlsim",
		"nlt",
		"nltri",
		"nltrie"
	],
	L: [
		"nLeftarrow",
		"nLeftrightarrow",
		"nLl",
		"nLt",
		"nLtv"
	],
	m: [
		"nmid"
	],
	o: [
		"nopf",
		"not",
		"notin",
		"notindot",
		"notinE",
		"notinva",
		"notinvb",
		"notinvc",
		"notni",
		"notniva",
		"notnivb",
		"notnivc"
	],
	p: [
		"npar",
		"nparallel",
		"nparsl",
		"npart",
		"npolint",
		"npr",
		"nprcue",
		"npre",
		"nprec",
		"npreceq"
	],
	r: [
		"nrArr",
		"nrarr",
		"nrarrc",
		"nrarrw",
		"nrightarrow",
		"nrtri",
		"nrtrie"
	],
	R: [
		"nRightarrow"
	],
	s: [
		"nsc",
		"nsccue",
		"nsce",
		"nscr",
		"nshortmid",
		"nshortparallel",
		"nsim",
		"nsime",
		"nsimeq",
		"nsmid",
		"nspar",
		"nsqsube",
		"nsqsupe",
		"nsub",
		"nsubE",
		"nsube",
		"nsubset",
		"nsubseteq",
		"nsubseteqq",
		"nsucc",
		"nsucceq",
		"nsup",
		"nsupE",
		"nsupe",
		"nsupset",
		"nsupseteq",
		"nsupseteqq"
	],
	t: [
		"ntgl",
		"ntilde",
		"ntlg",
		"ntriangleleft",
		"ntrianglelefteq",
		"ntriangleright",
		"ntrianglerighteq"
	],
	u: [
		"nu",
		"num",
		"numero",
		"numsp"
	],
	v: [
		"nvap",
		"nvDash",
		"nvdash",
		"nvge",
		"nvgt",
		"nvHarr",
		"nvinfin",
		"nvlArr",
		"nvle",
		"nvlt",
		"nvltrie",
		"nvrArr",
		"nvrtrie",
		"nvsim"
	],
	V: [
		"nVDash",
		"nVdash"
	],
	w: [
		"nwarhk",
		"nwArr",
		"nwarr",
		"nwarrow",
		"nwnear"
	]
};
var N = {
	a: [
		"Nacute"
	],
	c: [
		"Ncaron",
		"Ncedil",
		"Ncy"
	],
	e: [
		"NegativeMediumSpace",
		"NegativeThickSpace",
		"NegativeThinSpace",
		"NegativeVeryThinSpace",
		"NestedGreaterGreater",
		"NestedLessLess",
		"NewLine"
	],
	f: [
		"Nfr"
	],
	J: [
		"NJcy"
	],
	o: [
		"NoBreak",
		"NonBreakingSpace",
		"Nopf",
		"Not",
		"NotCongruent",
		"NotCupCap",
		"NotDoubleVerticalBar",
		"NotElement",
		"NotEqual",
		"NotEqualTilde",
		"NotExists",
		"NotGreater",
		"NotGreaterEqual",
		"NotGreaterFullEqual",
		"NotGreaterGreater",
		"NotGreaterLess",
		"NotGreaterSlantEqual",
		"NotGreaterTilde",
		"NotHumpDownHump",
		"NotHumpEqual",
		"NotLeftTriangle",
		"NotLeftTriangleBar",
		"NotLeftTriangleEqual",
		"NotLess",
		"NotLessEqual",
		"NotLessGreater",
		"NotLessLess",
		"NotLessSlantEqual",
		"NotLessTilde",
		"NotNestedGreaterGreater",
		"NotNestedLessLess",
		"NotPrecedes",
		"NotPrecedesEqual",
		"NotPrecedesSlantEqual",
		"NotReverseElement",
		"NotRightTriangle",
		"NotRightTriangleBar",
		"NotRightTriangleEqual",
		"NotSquareSubset",
		"NotSquareSubsetEqual",
		"NotSquareSuperset",
		"NotSquareSupersetEqual",
		"NotSubset",
		"NotSubsetEqual",
		"NotSucceeds",
		"NotSucceedsEqual",
		"NotSucceedsSlantEqual",
		"NotSucceedsTilde",
		"NotSuperset",
		"NotSupersetEqual",
		"NotTilde",
		"NotTildeEqual",
		"NotTildeFullEqual",
		"NotTildeTilde",
		"NotVerticalBar"
	],
	s: [
		"Nscr"
	],
	t: [
		"Ntilde"
	],
	u: [
		"Nu"
	]
};
var O = {
	a: [
		"Oacute"
	],
	c: [
		"Ocirc",
		"Ocy"
	],
	d: [
		"Odblac"
	],
	E: [
		"OElig"
	],
	f: [
		"Ofr"
	],
	g: [
		"Ograve"
	],
	m: [
		"Omacr",
		"Omega",
		"Omicron"
	],
	o: [
		"Oopf"
	],
	p: [
		"OpenCurlyDoubleQuote",
		"OpenCurlyQuote"
	],
	r: [
		"Or"
	],
	s: [
		"Oscr",
		"Oslash"
	],
	t: [
		"Otilde",
		"Otimes"
	],
	u: [
		"Ouml"
	],
	v: [
		"OverBar",
		"OverBrace",
		"OverBracket",
		"OverParenthesis"
	]
};
var o = {
	a: [
		"oacute",
		"oast"
	],
	c: [
		"ocir",
		"ocirc",
		"ocy"
	],
	d: [
		"odash",
		"odblac",
		"odiv",
		"odot",
		"odsold"
	],
	e: [
		"oelig"
	],
	f: [
		"ofcir",
		"ofr"
	],
	g: [
		"ogon",
		"ograve",
		"ogt"
	],
	h: [
		"ohbar",
		"ohm"
	],
	i: [
		"oint"
	],
	l: [
		"olarr",
		"olcir",
		"olcross",
		"oline",
		"olt"
	],
	m: [
		"omacr",
		"omega",
		"omicron",
		"omid",
		"ominus"
	],
	o: [
		"oopf"
	],
	p: [
		"opar",
		"operp",
		"oplus"
	],
	r: [
		"or",
		"orarr",
		"ord",
		"order",
		"orderof",
		"ordf",
		"ordm",
		"origof",
		"oror",
		"orslope",
		"orv"
	],
	S: [
		"oS"
	],
	s: [
		"oscr",
		"oslash",
		"osol"
	],
	t: [
		"otilde",
		"otimes",
		"otimesas"
	],
	u: [
		"ouml"
	],
	v: [
		"ovbar"
	]
};
var p = {
	a: [
		"par",
		"para",
		"parallel",
		"parsim",
		"parsl",
		"part"
	],
	c: [
		"pcy"
	],
	e: [
		"percnt",
		"period",
		"permil",
		"perp",
		"pertenk"
	],
	f: [
		"pfr"
	],
	h: [
		"phi",
		"phiv",
		"phmmat",
		"phone"
	],
	i: [
		"pi",
		"pitchfork",
		"piv"
	],
	l: [
		"planck",
		"planckh",
		"plankv",
		"plus",
		"plusacir",
		"plusb",
		"pluscir",
		"plusdo",
		"plusdu",
		"pluse",
		"plusmn",
		"plussim",
		"plustwo"
	],
	m: [
		"pm"
	],
	o: [
		"pointint",
		"popf",
		"pound"
	],
	r: [
		"pr",
		"prap",
		"prcue",
		"prE",
		"pre",
		"prec",
		"precapprox",
		"preccurlyeq",
		"preceq",
		"precnapprox",
		"precneqq",
		"precnsim",
		"precsim",
		"prime",
		"primes",
		"prnap",
		"prnE",
		"prnsim",
		"prod",
		"profalar",
		"profline",
		"profsurf",
		"prop",
		"propto",
		"prsim",
		"prurel"
	],
	s: [
		"pscr",
		"psi"
	],
	u: [
		"puncsp"
	]
};
var P = {
	a: [
		"PartialD"
	],
	c: [
		"Pcy"
	],
	f: [
		"Pfr"
	],
	h: [
		"Phi"
	],
	i: [
		"Pi"
	],
	l: [
		"PlusMinus"
	],
	o: [
		"Poincareplane",
		"Popf"
	],
	r: [
		"Pr",
		"Precedes",
		"PrecedesEqual",
		"PrecedesSlantEqual",
		"PrecedesTilde",
		"Prime",
		"Product",
		"Proportion",
		"Proportional"
	],
	s: [
		"Pscr",
		"Psi"
	]
};
var Q = {
	f: [
		"Qfr"
	],
	o: [
		"Qopf"
	],
	s: [
		"Qscr"
	],
	U: [
		"QUOT"
	]
};
var q = {
	f: [
		"qfr"
	],
	i: [
		"qint"
	],
	o: [
		"qopf"
	],
	p: [
		"qprime"
	],
	s: [
		"qscr"
	],
	u: [
		"quaternions",
		"quatint",
		"quest",
		"questeq",
		"quot"
	]
};
var r = {
	A: [
		"rAarr",
		"rArr",
		"rAtail"
	],
	a: [
		"race",
		"racute",
		"radic",
		"raemptyv",
		"rang",
		"rangd",
		"range",
		"rangle",
		"raquo",
		"rarr",
		"rarrap",
		"rarrb",
		"rarrbfs",
		"rarrc",
		"rarrfs",
		"rarrhk",
		"rarrlp",
		"rarrpl",
		"rarrsim",
		"rarrtl",
		"rarrw",
		"ratail",
		"ratio",
		"rationals"
	],
	B: [
		"rBarr"
	],
	b: [
		"rbarr",
		"rbbrk",
		"rbrace",
		"rbrack",
		"rbrke",
		"rbrksld",
		"rbrkslu"
	],
	c: [
		"rcaron",
		"rcedil",
		"rceil",
		"rcub",
		"rcy"
	],
	d: [
		"rdca",
		"rdldhar",
		"rdquo",
		"rdquor",
		"rdsh"
	],
	e: [
		"real",
		"realine",
		"realpart",
		"reals",
		"rect",
		"reg"
	],
	f: [
		"rfisht",
		"rfloor",
		"rfr"
	],
	H: [
		"rHar"
	],
	h: [
		"rhard",
		"rharu",
		"rharul",
		"rho",
		"rhov"
	],
	i: [
		"rightarrow",
		"rightarrowtail",
		"rightharpoondown",
		"rightharpoonup",
		"rightleftarrows",
		"rightleftharpoons",
		"rightrightarrows",
		"rightsquigarrow",
		"rightthreetimes",
		"ring",
		"risingdotseq"
	],
	l: [
		"rlarr",
		"rlhar",
		"rlm"
	],
	m: [
		"rmoust",
		"rmoustache"
	],
	n: [
		"rnmid"
	],
	o: [
		"roang",
		"roarr",
		"robrk",
		"ropar",
		"ropf",
		"roplus",
		"rotimes"
	],
	p: [
		"rpar",
		"rpargt",
		"rppolint"
	],
	r: [
		"rrarr"
	],
	s: [
		"rsaquo",
		"rscr",
		"rsh",
		"rsqb",
		"rsquo",
		"rsquor"
	],
	t: [
		"rthree",
		"rtimes",
		"rtri",
		"rtrie",
		"rtrif",
		"rtriltri"
	],
	u: [
		"ruluhar"
	],
	x: [
		"rx"
	]
};
var R = {
	a: [
		"Racute",
		"Rang",
		"Rarr",
		"Rarrtl"
	],
	B: [
		"RBarr"
	],
	c: [
		"Rcaron",
		"Rcedil",
		"Rcy"
	],
	e: [
		"Re",
		"ReverseElement",
		"ReverseEquilibrium",
		"ReverseUpEquilibrium"
	],
	E: [
		"REG"
	],
	f: [
		"Rfr"
	],
	h: [
		"Rho"
	],
	i: [
		"RightAngleBracket",
		"RightArrow",
		"Rightarrow",
		"RightArrowBar",
		"RightArrowLeftArrow",
		"RightCeiling",
		"RightDoubleBracket",
		"RightDownTeeVector",
		"RightDownVector",
		"RightDownVectorBar",
		"RightFloor",
		"RightTee",
		"RightTeeArrow",
		"RightTeeVector",
		"RightTriangle",
		"RightTriangleBar",
		"RightTriangleEqual",
		"RightUpDownVector",
		"RightUpTeeVector",
		"RightUpVector",
		"RightUpVectorBar",
		"RightVector",
		"RightVectorBar"
	],
	o: [
		"Ropf",
		"RoundImplies"
	],
	r: [
		"Rrightarrow"
	],
	s: [
		"Rscr",
		"Rsh"
	],
	u: [
		"RuleDelayed"
	]
};
var S = {
	a: [
		"Sacute"
	],
	c: [
		"Sc",
		"Scaron",
		"Scedil",
		"Scirc",
		"Scy"
	],
	f: [
		"Sfr"
	],
	H: [
		"SHCHcy",
		"SHcy"
	],
	h: [
		"ShortDownArrow",
		"ShortLeftArrow",
		"ShortRightArrow",
		"ShortUpArrow"
	],
	i: [
		"Sigma"
	],
	m: [
		"SmallCircle"
	],
	O: [
		"SOFTcy"
	],
	o: [
		"Sopf"
	],
	q: [
		"Sqrt",
		"Square",
		"SquareIntersection",
		"SquareSubset",
		"SquareSubsetEqual",
		"SquareSuperset",
		"SquareSupersetEqual",
		"SquareUnion"
	],
	s: [
		"Sscr"
	],
	t: [
		"Star"
	],
	u: [
		"Sub",
		"Subset",
		"SubsetEqual",
		"Succeeds",
		"SucceedsEqual",
		"SucceedsSlantEqual",
		"SucceedsTilde",
		"SuchThat",
		"Sum",
		"Sup",
		"Superset",
		"SupersetEqual",
		"Supset"
	]
};
var s = {
	a: [
		"sacute"
	],
	b: [
		"sbquo"
	],
	c: [
		"sc",
		"scap",
		"scaron",
		"sccue",
		"scE",
		"sce",
		"scedil",
		"scirc",
		"scnap",
		"scnE",
		"scnsim",
		"scpolint",
		"scsim",
		"scy"
	],
	d: [
		"sdot",
		"sdotb",
		"sdote"
	],
	e: [
		"searhk",
		"seArr",
		"searr",
		"searrow",
		"sect",
		"semi",
		"seswar",
		"setminus",
		"setmn",
		"sext"
	],
	f: [
		"sfr",
		"sfrown"
	],
	h: [
		"sharp",
		"shchcy",
		"shcy",
		"shortmid",
		"shortparallel",
		"shy"
	],
	i: [
		"sigma",
		"sigmaf",
		"sigmav",
		"sim",
		"simdot",
		"sime",
		"simeq",
		"simg",
		"simgE",
		"siml",
		"simlE",
		"simne",
		"simplus",
		"simrarr"
	],
	l: [
		"slarr"
	],
	m: [
		"smallsetminus",
		"smashp",
		"smeparsl",
		"smid",
		"smile",
		"smt",
		"smte",
		"smtes"
	],
	o: [
		"softcy",
		"sol",
		"solb",
		"solbar",
		"sopf"
	],
	p: [
		"spades",
		"spadesuit",
		"spar"
	],
	q: [
		"sqcap",
		"sqcaps",
		"sqcup",
		"sqcups",
		"sqsub",
		"sqsube",
		"sqsubset",
		"sqsubseteq",
		"sqsup",
		"sqsupe",
		"sqsupset",
		"sqsupseteq",
		"squ",
		"square",
		"squarf",
		"squf"
	],
	r: [
		"srarr"
	],
	s: [
		"sscr",
		"ssetmn",
		"ssmile",
		"sstarf"
	],
	t: [
		"star",
		"starf",
		"straightepsilon",
		"straightphi",
		"strns"
	],
	u: [
		"sub",
		"subdot",
		"subE",
		"sube",
		"subedot",
		"submult",
		"subnE",
		"subne",
		"subplus",
		"subrarr",
		"subset",
		"subseteq",
		"subseteqq",
		"subsetneq",
		"subsetneqq",
		"subsim",
		"subsub",
		"subsup",
		"succ",
		"succapprox",
		"succcurlyeq",
		"succeq",
		"succnapprox",
		"succneqq",
		"succnsim",
		"succsim",
		"sum",
		"sung",
		"sup",
		"sup1",
		"sup2",
		"sup3",
		"supdot",
		"supdsub",
		"supE",
		"supe",
		"supedot",
		"suphsol",
		"suphsub",
		"suplarr",
		"supmult",
		"supnE",
		"supne",
		"supplus",
		"supset",
		"supseteq",
		"supseteqq",
		"supsetneq",
		"supsetneqq",
		"supsim",
		"supsub",
		"supsup"
	],
	w: [
		"swarhk",
		"swArr",
		"swarr",
		"swarrow",
		"swnwar"
	],
	z: [
		"szlig"
	]
};
var T = {
	a: [
		"Tab",
		"Tau"
	],
	c: [
		"Tcaron",
		"Tcedil",
		"Tcy"
	],
	f: [
		"Tfr"
	],
	h: [
		"Therefore",
		"Theta",
		"ThickSpace",
		"ThinSpace"
	],
	H: [
		"THORN"
	],
	i: [
		"Tilde",
		"TildeEqual",
		"TildeFullEqual",
		"TildeTilde"
	],
	o: [
		"Topf"
	],
	R: [
		"TRADE"
	],
	r: [
		"TripleDot"
	],
	s: [
		"Tscr",
		"Tstrok"
	],
	S: [
		"TScy",
		"TSHcy"
	]
};
var t = {
	a: [
		"target",
		"tau"
	],
	b: [
		"tbrk"
	],
	c: [
		"tcaron",
		"tcedil",
		"tcy"
	],
	d: [
		"tdot"
	],
	e: [
		"telrec"
	],
	f: [
		"tfr"
	],
	h: [
		"there4",
		"therefore",
		"theta",
		"thetasym",
		"thetav",
		"thickapprox",
		"thicksim",
		"thinsp",
		"thkap",
		"thksim",
		"thorn"
	],
	i: [
		"tilde",
		"times",
		"timesb",
		"timesbar",
		"timesd",
		"tint"
	],
	o: [
		"toea",
		"top",
		"topbot",
		"topcir",
		"topf",
		"topfork",
		"tosa"
	],
	p: [
		"tprime"
	],
	r: [
		"trade",
		"triangle",
		"triangledown",
		"triangleleft",
		"trianglelefteq",
		"triangleq",
		"triangleright",
		"trianglerighteq",
		"tridot",
		"trie",
		"triminus",
		"triplus",
		"trisb",
		"tritime",
		"trpezium"
	],
	s: [
		"tscr",
		"tscy",
		"tshcy",
		"tstrok"
	],
	w: [
		"twixt",
		"twoheadleftarrow",
		"twoheadrightarrow"
	]
};
var U = {
	a: [
		"Uacute",
		"Uarr",
		"Uarrocir"
	],
	b: [
		"Ubrcy",
		"Ubreve"
	],
	c: [
		"Ucirc",
		"Ucy"
	],
	d: [
		"Udblac"
	],
	f: [
		"Ufr"
	],
	g: [
		"Ugrave"
	],
	m: [
		"Umacr"
	],
	n: [
		"UnderBar",
		"UnderBrace",
		"UnderBracket",
		"UnderParenthesis",
		"Union",
		"UnionPlus"
	],
	o: [
		"Uogon",
		"Uopf"
	],
	p: [
		"UpArrow",
		"Uparrow",
		"UpArrowBar",
		"UpArrowDownArrow",
		"UpDownArrow",
		"Updownarrow",
		"UpEquilibrium",
		"UpperLeftArrow",
		"UpperRightArrow",
		"Upsi",
		"Upsilon",
		"UpTee",
		"UpTeeArrow"
	],
	r: [
		"Uring"
	],
	s: [
		"Uscr"
	],
	t: [
		"Utilde"
	],
	u: [
		"Uuml"
	]
};
var u = {
	a: [
		"uacute",
		"uarr"
	],
	A: [
		"uArr"
	],
	b: [
		"ubrcy",
		"ubreve"
	],
	c: [
		"ucirc",
		"ucy"
	],
	d: [
		"udarr",
		"udblac",
		"udhar"
	],
	f: [
		"ufisht",
		"ufr"
	],
	g: [
		"ugrave"
	],
	H: [
		"uHar"
	],
	h: [
		"uharl",
		"uharr",
		"uhblk"
	],
	l: [
		"ulcorn",
		"ulcorner",
		"ulcrop",
		"ultri"
	],
	m: [
		"umacr",
		"uml"
	],
	o: [
		"uogon",
		"uopf"
	],
	p: [
		"uparrow",
		"updownarrow",
		"upharpoonleft",
		"upharpoonright",
		"uplus",
		"upsi",
		"upsih",
		"upsilon",
		"upuparrows"
	],
	r: [
		"urcorn",
		"urcorner",
		"urcrop",
		"uring",
		"urtri"
	],
	s: [
		"uscr"
	],
	t: [
		"utdot",
		"utilde",
		"utri",
		"utrif"
	],
	u: [
		"uuarr",
		"uuml"
	],
	w: [
		"uwangle"
	]
};
var v = {
	a: [
		"vangrt",
		"varepsilon",
		"varkappa",
		"varnothing",
		"varphi",
		"varpi",
		"varpropto",
		"varr",
		"varrho",
		"varsigma",
		"varsubsetneq",
		"varsubsetneqq",
		"varsupsetneq",
		"varsupsetneqq",
		"vartheta",
		"vartriangleleft",
		"vartriangleright"
	],
	A: [
		"vArr"
	],
	B: [
		"vBar",
		"vBarv"
	],
	c: [
		"vcy"
	],
	D: [
		"vDash"
	],
	d: [
		"vdash"
	],
	e: [
		"vee",
		"veebar",
		"veeeq",
		"vellip",
		"verbar",
		"vert"
	],
	f: [
		"vfr"
	],
	l: [
		"vltri"
	],
	n: [
		"vnsub",
		"vnsup"
	],
	o: [
		"vopf"
	],
	p: [
		"vprop"
	],
	r: [
		"vrtri"
	],
	s: [
		"vscr",
		"vsubnE",
		"vsubne",
		"vsupnE",
		"vsupne"
	],
	z: [
		"vzigzag"
	]
};
var V = {
	b: [
		"Vbar"
	],
	c: [
		"Vcy"
	],
	D: [
		"VDash"
	],
	d: [
		"Vdash",
		"Vdashl"
	],
	e: [
		"Vee",
		"Verbar",
		"Vert",
		"VerticalBar",
		"VerticalLine",
		"VerticalSeparator",
		"VerticalTilde",
		"VeryThinSpace"
	],
	f: [
		"Vfr"
	],
	o: [
		"Vopf"
	],
	s: [
		"Vscr"
	],
	v: [
		"Vvdash"
	]
};
var W = {
	c: [
		"Wcirc"
	],
	e: [
		"Wedge"
	],
	f: [
		"Wfr"
	],
	o: [
		"Wopf"
	],
	s: [
		"Wscr"
	]
};
var w = {
	c: [
		"wcirc"
	],
	e: [
		"wedbar",
		"wedge",
		"wedgeq",
		"weierp"
	],
	f: [
		"wfr"
	],
	o: [
		"wopf"
	],
	p: [
		"wp"
	],
	r: [
		"wr",
		"wreath"
	],
	s: [
		"wscr"
	]
};
var x = {
	c: [
		"xcap",
		"xcirc",
		"xcup"
	],
	d: [
		"xdtri"
	],
	f: [
		"xfr"
	],
	h: [
		"xhArr",
		"xharr"
	],
	i: [
		"xi"
	],
	l: [
		"xlArr",
		"xlarr"
	],
	m: [
		"xmap"
	],
	n: [
		"xnis"
	],
	o: [
		"xodot",
		"xopf",
		"xoplus",
		"xotime"
	],
	r: [
		"xrArr",
		"xrarr"
	],
	s: [
		"xscr",
		"xsqcup"
	],
	u: [
		"xuplus",
		"xutri"
	],
	v: [
		"xvee"
	],
	w: [
		"xwedge"
	]
};
var X = {
	f: [
		"Xfr"
	],
	i: [
		"Xi"
	],
	o: [
		"Xopf"
	],
	s: [
		"Xscr"
	]
};
var Y = {
	a: [
		"Yacute"
	],
	A: [
		"YAcy"
	],
	c: [
		"Ycirc",
		"Ycy"
	],
	f: [
		"Yfr"
	],
	I: [
		"YIcy"
	],
	o: [
		"Yopf"
	],
	s: [
		"Yscr"
	],
	U: [
		"YUcy"
	],
	u: [
		"Yuml"
	]
};
var y = {
	a: [
		"yacute",
		"yacy"
	],
	c: [
		"ycirc",
		"ycy"
	],
	e: [
		"yen"
	],
	f: [
		"yfr"
	],
	i: [
		"yicy"
	],
	o: [
		"yopf"
	],
	s: [
		"yscr"
	],
	u: [
		"yucy",
		"yuml"
	]
};
var Z = {
	a: [
		"Zacute"
	],
	c: [
		"Zcaron",
		"Zcy"
	],
	d: [
		"Zdot"
	],
	e: [
		"ZeroWidthSpace",
		"Zeta"
	],
	f: [
		"Zfr"
	],
	H: [
		"ZHcy"
	],
	o: [
		"Zopf"
	],
	s: [
		"Zscr"
	]
};
var z = {
	a: [
		"zacute"
	],
	c: [
		"zcaron",
		"zcy"
	],
	d: [
		"zdot"
	],
	e: [
		"zeetrf",
		"zeta"
	],
	f: [
		"zfr"
	],
	h: [
		"zhcy"
	],
	i: [
		"zigrarr"
	],
	o: [
		"zopf"
	],
	s: [
		"zscr"
	],
	w: [
		"zwj",
		"zwnj"
	]
};
var startsWith = {
	A: A,
	a: a,
	b: b,
	B: B,
	C: C,
	c: c,
	D: D,
	d: d,
	E: E,
	e: e,
	f: f,
	F: F,
	g: g,
	G: G,
	H: H,
	h: h,
	I: I,
	i: i,
	J: J,
	j: j,
	K: K,
	k: k,
	l: l,
	L: L,
	m: m,
	M: M,
	n: n,
	N: N,
	O: O,
	o: o,
	p: p,
	P: P,
	Q: Q,
	q: q,
	r: r,
	R: R,
	S: S,
	s: s,
	T: T,
	t: t,
	U: U,
	u: u,
	v: v,
	V: V,
	W: W,
	w: w,
	x: x,
	X: X,
	Y: Y,
	y: y,
	Z: Z,
	z: z
};

var e$1 = {
	t: [
		"Aacute",
		"aacute",
		"acute",
		"Cacute",
		"cacute",
		"CloseCurlyDoubleQuote",
		"CloseCurlyQuote",
		"DiacriticalAcute",
		"DiacriticalDoubleAcute",
		"Eacute",
		"eacute",
		"gacute",
		"Iacute",
		"iacute",
		"Lacute",
		"lacute",
		"late",
		"Nacute",
		"nacute",
		"Oacute",
		"oacute",
		"OpenCurlyDoubleQuote",
		"OpenCurlyQuote",
		"Racute",
		"racute",
		"Sacute",
		"sacute",
		"sdote",
		"smte",
		"Uacute",
		"uacute",
		"Yacute",
		"yacute",
		"Zacute",
		"zacute"
	],
	v: [
		"Abreve",
		"abreve",
		"Agrave",
		"agrave",
		"Breve",
		"breve",
		"DiacriticalGrave",
		"DownBreve",
		"Egrave",
		"egrave",
		"Gbreve",
		"gbreve",
		"grave",
		"Igrave",
		"igrave",
		"Ograve",
		"ograve",
		"Ubreve",
		"ubreve",
		"Ugrave",
		"ugrave"
	],
	p: [
		"andslope",
		"ape",
		"bumpe",
		"csupe",
		"nbumpe",
		"nsqsupe",
		"nsupe",
		"orslope",
		"sqsupe",
		"supe"
	],
	g: [
		"ange",
		"barwedge",
		"bigwedge",
		"blacklozenge",
		"curlywedge",
		"doublebarwedge",
		"ge",
		"image",
		"lozenge",
		"nge",
		"nvge",
		"range",
		"Wedge",
		"wedge",
		"xwedge"
	],
	l: [
		"angle",
		"blacktriangle",
		"dwangle",
		"exponentiale",
		"female",
		"langle",
		"le",
		"LeftTriangle",
		"male",
		"measuredangle",
		"nle",
		"NotLeftTriangle",
		"NotRightTriangle",
		"nvle",
		"rangle",
		"RightTriangle",
		"SmallCircle",
		"smile",
		"ssmile",
		"triangle",
		"uwangle"
	],
	a: [
		"angmsdae"
	],
	d: [
		"Atilde",
		"atilde",
		"DiacriticalTilde",
		"divide",
		"EqualTilde",
		"GreaterTilde",
		"Itilde",
		"itilde",
		"LessTilde",
		"NotEqualTilde",
		"NotGreaterTilde",
		"NotLessTilde",
		"NotSucceedsTilde",
		"NotTilde",
		"NotTildeTilde",
		"Ntilde",
		"ntilde",
		"Otilde",
		"otilde",
		"PrecedesTilde",
		"SucceedsTilde",
		"Tilde",
		"tilde",
		"TildeTilde",
		"trade",
		"Utilde",
		"utilde",
		"VerticalTilde"
	],
	m: [
		"backprime",
		"bprime",
		"bsime",
		"gsime",
		"lsime",
		"nsime",
		"Prime",
		"prime",
		"qprime",
		"sime",
		"tprime",
		"tritime",
		"xotime"
	],
	e: [
		"barvee",
		"bigvee",
		"curlyvee",
		"cuvee",
		"DoubleLeftTee",
		"DoubleRightTee",
		"DownTee",
		"ee",
		"LeftTee",
		"lthree",
		"RightTee",
		"rthree",
		"UpTee",
		"Vee",
		"vee",
		"xvee"
	],
	s: [
		"Because",
		"because",
		"maltese",
		"pluse"
	],
	r: [
		"blacksquare",
		"cire",
		"dotsquare",
		"EmptySmallSquare",
		"EmptyVerySmallSquare",
		"FilledSmallSquare",
		"FilledVerySmallSquare",
		"incare",
		"npre",
		"pre",
		"Square",
		"square",
		"Therefore",
		"therefore"
	],
	n: [
		"bne",
		"Colone",
		"colone",
		"gne",
		"HorizontalLine",
		"imagline",
		"lne",
		"ne",
		"NewLine",
		"oline",
		"phone",
		"Poincareplane",
		"profline",
		"realine",
		"simne",
		"subne",
		"supne",
		"VerticalLine",
		"vsubne",
		"vsupne"
	],
	i: [
		"bowtie",
		"die",
		"infintie",
		"ltrie",
		"nltrie",
		"nrtrie",
		"nvltrie",
		"nvrtrie",
		"rtrie",
		"trie"
	],
	b: [
		"csube",
		"nsqsube",
		"nsube",
		"sqsube",
		"sube"
	],
	c: [
		"HilbertSpace",
		"lbrace",
		"MediumSpace",
		"NegativeMediumSpace",
		"NegativeThickSpace",
		"NegativeThinSpace",
		"NegativeVeryThinSpace",
		"NonBreakingSpace",
		"nsce",
		"OverBrace",
		"race",
		"rbrace",
		"sce",
		"ThickSpace",
		"ThinSpace",
		"UnderBrace",
		"VeryThinSpace",
		"ZeroWidthSpace"
	],
	k: [
		"lbrke",
		"rbrke"
	],
	h: [
		"lmoustache",
		"rmoustache"
	],
	u: [
		"nprcue",
		"nsccue",
		"prcue",
		"sccue"
	],
	R: [
		"Re"
	]
};
var c$1 = {
	a: [
		"ac",
		"angmsdac",
		"dblac",
		"Odblac",
		"odblac",
		"Udblac",
		"udblac"
	],
	r: [
		"Acirc",
		"acirc",
		"bigcirc",
		"Ccirc",
		"ccirc",
		"circ",
		"circledcirc",
		"Ecirc",
		"ecirc",
		"eqcirc",
		"Gcirc",
		"gcirc",
		"Hcirc",
		"hcirc",
		"Icirc",
		"icirc",
		"Jcirc",
		"jcirc",
		"nrarrc",
		"Ocirc",
		"ocirc",
		"rarrc",
		"Scirc",
		"scirc",
		"Ucirc",
		"ucirc",
		"Wcirc",
		"wcirc",
		"xcirc",
		"Ycirc",
		"ycirc"
	],
	s: [
		"cuesc",
		"nsc",
		"sc"
	],
	e: [
		"curlyeqprec",
		"nprec",
		"prec",
		"telrec"
	],
	c: [
		"curlyeqsucc",
		"gescc",
		"gtcc",
		"lescc",
		"ltcc",
		"nsucc",
		"succ"
	],
	i: [
		"ic",
		"radic"
	],
	v: [
		"notinvc",
		"notnivc"
	],
	S: [
		"Sc"
	]
};
var d$1 = {
	c: [
		"acd"
	],
	n: [
		"And",
		"and",
		"andand",
		"capand",
		"Diamond",
		"diamond",
		"pound"
	],
	d: [
		"andd",
		"dd"
	],
	s: [
		"angmsd",
		"minusd",
		"nisd",
		"timesd"
	],
	a: [
		"angmsdad",
		"Gammad",
		"gammad"
	],
	b: [
		"angrtvbd"
	],
	i: [
		"apid",
		"cirmid",
		"mid",
		"napid",
		"nmid",
		"nshortmid",
		"nsmid",
		"omid",
		"rnmid",
		"shortmid",
		"smid"
	],
	e: [
		"Barwed",
		"barwed",
		"cuwed",
		"imped",
		"RuleDelayed"
	],
	H: [
		"boxHd"
	],
	h: [
		"boxhd",
		"DDotrahd"
	],
	o: [
		"coprod",
		"intprod",
		"iprod",
		"period",
		"prod"
	],
	g: [
		"langd",
		"rangd"
	],
	l: [
		"lbrksld",
		"odsold",
		"rbrksld"
	],
	r: [
		"lhard",
		"llhard",
		"lrhard",
		"ord",
		"rhard"
	]
};
var E$1 = {
	c: [
		"acE",
		"scE"
	],
	p: [
		"apE",
		"bumpE",
		"napE",
		"nsupE",
		"supE"
	],
	r: [
		"cirE",
		"prE"
	],
	l: [
		"ExponentialE",
		"glE",
		"lE",
		"nlE",
		"simlE"
	],
	g: [
		"gE",
		"lgE",
		"ngE",
		"simgE"
	],
	n: [
		"gnE",
		"gvnE",
		"isinE",
		"lnE",
		"lvnE",
		"notinE",
		"prnE",
		"scnE",
		"subnE",
		"supnE",
		"vsubnE",
		"vsupnE"
	],
	b: [
		"nsubE",
		"subE"
	],
	D: [
		"TRADE"
	]
};
var y$1 = {
	c: [
		"Acy",
		"acy",
		"Bcy",
		"bcy",
		"CHcy",
		"chcy",
		"Dcy",
		"dcy",
		"DJcy",
		"djcy",
		"DScy",
		"dscy",
		"DZcy",
		"dzcy",
		"Ecy",
		"ecy",
		"Fcy",
		"fcy",
		"Gcy",
		"gcy",
		"GJcy",
		"gjcy",
		"HARDcy",
		"hardcy",
		"Icy",
		"icy",
		"IEcy",
		"iecy",
		"IOcy",
		"iocy",
		"Iukcy",
		"iukcy",
		"Jcy",
		"jcy",
		"Jsercy",
		"jsercy",
		"Jukcy",
		"jukcy",
		"Kcy",
		"kcy",
		"KHcy",
		"khcy",
		"KJcy",
		"kjcy",
		"Lcy",
		"lcy",
		"LJcy",
		"ljcy",
		"Mcy",
		"mcy",
		"Ncy",
		"ncy",
		"NJcy",
		"njcy",
		"Ocy",
		"ocy",
		"Pcy",
		"pcy",
		"Rcy",
		"rcy",
		"Scy",
		"scy",
		"SHCHcy",
		"shchcy",
		"SHcy",
		"shcy",
		"SOFTcy",
		"softcy",
		"Tcy",
		"tcy",
		"TScy",
		"tscy",
		"TSHcy",
		"tshcy",
		"Ubrcy",
		"ubrcy",
		"Ucy",
		"ucy",
		"Vcy",
		"vcy",
		"YAcy",
		"yacy",
		"Ycy",
		"ycy",
		"YIcy",
		"yicy",
		"YUcy",
		"yucy",
		"Zcy",
		"zcy",
		"ZHcy",
		"zhcy"
	],
	p: [
		"copy"
	],
	t: [
		"cylcty",
		"empty"
	],
	h: [
		"shy"
	]
};
var g$1 = {
	i: [
		"AElig",
		"aelig",
		"ffilig",
		"fflig",
		"ffllig",
		"filig",
		"fjlig",
		"fllig",
		"IJlig",
		"ijlig",
		"OElig",
		"oelig",
		"szlig"
	],
	l: [
		"amalg",
		"lg",
		"ntlg"
	],
	n: [
		"ang",
		"Aring",
		"aring",
		"backcong",
		"bcong",
		"cong",
		"eng",
		"Lang",
		"lang",
		"LeftCeiling",
		"loang",
		"nang",
		"ncong",
		"Rang",
		"rang",
		"RightCeiling",
		"ring",
		"roang",
		"sung",
		"Uring",
		"uring",
		"varnothing"
	],
	a: [
		"angmsdag",
		"vzigzag"
	],
	e: [
		"deg",
		"eg",
		"leg",
		"reg"
	],
	G: [
		"Gg",
		"nGg"
	],
	g: [
		"gg",
		"ggg"
	],
	E: [
		"lEg"
	],
	s: [
		"lesg"
	],
	m: [
		"lsimg",
		"simg"
	]
};
var f$1 = {
	a: [
		"af",
		"angmsdaf",
		"sigmaf"
	],
	p: [
		"Aopf",
		"aopf",
		"Bopf",
		"bopf",
		"Copf",
		"copf",
		"Dopf",
		"dopf",
		"Eopf",
		"eopf",
		"Fopf",
		"fopf",
		"Gopf",
		"gopf",
		"Hopf",
		"hopf",
		"Iopf",
		"iopf",
		"Jopf",
		"jopf",
		"Kopf",
		"kopf",
		"Lopf",
		"lopf",
		"Mopf",
		"mopf",
		"Nopf",
		"nopf",
		"Oopf",
		"oopf",
		"Popf",
		"popf",
		"Qopf",
		"qopf",
		"Ropf",
		"ropf",
		"Sopf",
		"sopf",
		"Topf",
		"topf",
		"Uopf",
		"uopf",
		"Vopf",
		"vopf",
		"Wopf",
		"wopf",
		"Xopf",
		"xopf",
		"Yopf",
		"yopf",
		"Zopf",
		"zopf"
	],
	i: [
		"dtrif",
		"ltrif",
		"rtrif",
		"utrif"
	],
	o: [
		"fnof",
		"imof",
		"orderof",
		"origof"
	],
	r: [
		"Fouriertrf",
		"Laplacetrf",
		"Mellintrf",
		"profsurf",
		"squarf",
		"sstarf",
		"starf",
		"zeetrf"
	],
	l: [
		"half"
	],
	f: [
		"iff"
	],
	z: [
		"lozf"
	],
	d: [
		"ordf"
	],
	u: [
		"squf"
	]
};
var r$1 = {
	f: [
		"Afr",
		"afr",
		"Bfr",
		"bfr",
		"Cfr",
		"cfr",
		"Dfr",
		"dfr",
		"Efr",
		"efr",
		"Ffr",
		"ffr",
		"Gfr",
		"gfr",
		"Hfr",
		"hfr",
		"Ifr",
		"ifr",
		"Jfr",
		"jfr",
		"Kfr",
		"kfr",
		"Lfr",
		"lfr",
		"Mfr",
		"mfr",
		"Nfr",
		"nfr",
		"Ofr",
		"ofr",
		"Pfr",
		"pfr",
		"Qfr",
		"qfr",
		"Rfr",
		"rfr",
		"Sfr",
		"sfr",
		"Tfr",
		"tfr",
		"Ufr",
		"ufr",
		"Vfr",
		"vfr",
		"Wfr",
		"wfr",
		"Xfr",
		"xfr",
		"Yfr",
		"yfr",
		"Zfr",
		"zfr"
	],
	c: [
		"Amacr",
		"amacr",
		"Ascr",
		"ascr",
		"Bscr",
		"bscr",
		"Cscr",
		"cscr",
		"Dscr",
		"dscr",
		"Emacr",
		"emacr",
		"Escr",
		"escr",
		"Fscr",
		"fscr",
		"Gscr",
		"gscr",
		"Hscr",
		"hscr",
		"Imacr",
		"imacr",
		"Iscr",
		"iscr",
		"Jscr",
		"jscr",
		"Kscr",
		"kscr",
		"Lscr",
		"lscr",
		"macr",
		"Mscr",
		"mscr",
		"Nscr",
		"nscr",
		"Omacr",
		"omacr",
		"Oscr",
		"oscr",
		"Pscr",
		"pscr",
		"Qscr",
		"qscr",
		"Rscr",
		"rscr",
		"Sscr",
		"sscr",
		"Tscr",
		"tscr",
		"Umacr",
		"umacr",
		"Uscr",
		"uscr",
		"Vscr",
		"vscr",
		"Wscr",
		"wscr",
		"Xscr",
		"xscr",
		"Yscr",
		"yscr",
		"Zscr",
		"zscr"
	],
	r: [
		"angzarr",
		"crarr",
		"cudarrr",
		"cularr",
		"curarr",
		"Darr",
		"dArr",
		"darr",
		"ddarr",
		"dharr",
		"duarr",
		"dzigrarr",
		"erarr",
		"gtrarr",
		"hArr",
		"harr",
		"hoarr",
		"lAarr",
		"Larr",
		"lArr",
		"larr",
		"lBarr",
		"lbarr",
		"llarr",
		"loarr",
		"lrarr",
		"ltlarr",
		"neArr",
		"nearr",
		"nhArr",
		"nharr",
		"nlArr",
		"nlarr",
		"nrArr",
		"nrarr",
		"nvHarr",
		"nvlArr",
		"nvrArr",
		"nwArr",
		"nwarr",
		"olarr",
		"orarr",
		"rAarr",
		"Rarr",
		"rArr",
		"rarr",
		"RBarr",
		"rBarr",
		"rbarr",
		"rlarr",
		"roarr",
		"rrarr",
		"seArr",
		"searr",
		"simrarr",
		"slarr",
		"srarr",
		"subrarr",
		"suplarr",
		"swArr",
		"swarr",
		"Uarr",
		"uArr",
		"uarr",
		"udarr",
		"uharr",
		"uuarr",
		"vArr",
		"varr",
		"xhArr",
		"xharr",
		"xlArr",
		"xlarr",
		"xrArr",
		"xrarr",
		"zigrarr"
	],
	i: [
		"apacir",
		"cir",
		"cirscir",
		"ecir",
		"gtcir",
		"harrcir",
		"ltcir",
		"midcir",
		"ocir",
		"ofcir",
		"olcir",
		"plusacir",
		"pluscir",
		"topcir",
		"Uarrocir"
	],
	a: [
		"bigstar",
		"brvbar",
		"dHar",
		"dollar",
		"DoubleVerticalBar",
		"DownArrowBar",
		"DownLeftVectorBar",
		"DownRightVectorBar",
		"duhar",
		"epar",
		"gtlPar",
		"hbar",
		"horbar",
		"ldrdhar",
		"ldrushar",
		"LeftArrowBar",
		"LeftDownVectorBar",
		"LeftTriangleBar",
		"LeftUpVectorBar",
		"LeftVectorBar",
		"lHar",
		"lopar",
		"lowbar",
		"lpar",
		"lrhar",
		"ltrPar",
		"lurdshar",
		"luruhar",
		"nesear",
		"nhpar",
		"NotDoubleVerticalBar",
		"NotLeftTriangleBar",
		"NotRightTriangleBar",
		"NotVerticalBar",
		"npar",
		"nspar",
		"nwnear",
		"ohbar",
		"opar",
		"ovbar",
		"OverBar",
		"par",
		"profalar",
		"rdldhar",
		"rHar",
		"RightArrowBar",
		"RightDownVectorBar",
		"RightTriangleBar",
		"RightUpVectorBar",
		"RightVectorBar",
		"rlhar",
		"ropar",
		"rpar",
		"ruluhar",
		"seswar",
		"solbar",
		"spar",
		"Star",
		"star",
		"swnwar",
		"timesbar",
		"udhar",
		"uHar",
		"UnderBar",
		"UpArrowBar",
		"Vbar",
		"vBar",
		"veebar",
		"Verbar",
		"verbar",
		"VerticalBar",
		"wedbar"
	],
	D: [
		"boxDr"
	],
	d: [
		"boxdr",
		"mldr",
		"nldr"
	],
	U: [
		"boxUr"
	],
	u: [
		"boxur",
		"natur"
	],
	V: [
		"boxVr"
	],
	v: [
		"boxvr"
	],
	s: [
		"copysr"
	],
	p: [
		"cuepr",
		"npr",
		"pr"
	],
	o: [
		"cupor",
		"DownLeftRightVector",
		"DownLeftTeeVector",
		"DownLeftVector",
		"DownRightTeeVector",
		"DownRightVector",
		"ldquor",
		"LeftDownTeeVector",
		"LeftDownVector",
		"LeftFloor",
		"LeftRightVector",
		"LeftTeeVector",
		"LeftUpDownVector",
		"LeftUpTeeVector",
		"LeftUpVector",
		"LeftVector",
		"lesdotor",
		"lfloor",
		"lsquor",
		"or",
		"oror",
		"rdquor",
		"rfloor",
		"RightDownTeeVector",
		"RightDownVector",
		"RightFloor",
		"RightTeeVector",
		"RightUpDownVector",
		"RightUpTeeVector",
		"RightUpVector",
		"RightVector",
		"rsquor",
		"VerticalSeparator"
	],
	e: [
		"Dagger",
		"dagger",
		"ddagger",
		"easter",
		"GreaterGreater",
		"LessEqualGreater",
		"LessGreater",
		"llcorner",
		"lrcorner",
		"marker",
		"NestedGreaterGreater",
		"NotGreater",
		"NotGreaterGreater",
		"NotLessGreater",
		"NotNestedGreaterGreater",
		"order",
		"ulcorner",
		"urcorner"
	],
	t: [
		"eqslantgtr",
		"lesseqgtr",
		"lesseqqgtr",
		"lessgtr",
		"ngtr"
	],
	O: [
		"Or"
	],
	P: [
		"Pr"
	],
	w: [
		"wr"
	]
};
var m$1 = {
	y: [
		"alefsym",
		"thetasym"
	],
	i: [
		"backsim",
		"bsim",
		"eqsim",
		"Esim",
		"esim",
		"gnsim",
		"gsim",
		"gtrsim",
		"larrsim",
		"lesssim",
		"lnsim",
		"lsim",
		"nesim",
		"ngsim",
		"nlsim",
		"nsim",
		"nvsim",
		"parsim",
		"plussim",
		"precnsim",
		"precsim",
		"prnsim",
		"prsim",
		"rarrsim",
		"scnsim",
		"scsim",
		"sim",
		"subsim",
		"succnsim",
		"succsim",
		"supsim",
		"thicksim",
		"thksim"
	],
	o: [
		"bottom"
	],
	s: [
		"ccupssm"
	],
	r: [
		"curarrm",
		"lrm"
	],
	a: [
		"diam"
	],
	u: [
		"Equilibrium",
		"num",
		"ReverseEquilibrium",
		"ReverseUpEquilibrium",
		"Sum",
		"sum",
		"trpezium",
		"UpEquilibrium"
	],
	I: [
		"Im"
	],
	h: [
		"ohm"
	],
	d: [
		"ordm"
	],
	p: [
		"pm"
	],
	l: [
		"rlm"
	]
};
var h$1 = {
	p: [
		"aleph",
		"angsph"
	],
	a: [
		"angmsdah"
	],
	s: [
		"Backslash",
		"circleddash",
		"dash",
		"hslash",
		"ldsh",
		"Lsh",
		"lsh",
		"mdash",
		"ndash",
		"nVDash",
		"nVdash",
		"nvDash",
		"nvdash",
		"odash",
		"Oslash",
		"oslash",
		"rdsh",
		"Rsh",
		"rsh",
		"VDash",
		"Vdash",
		"vDash",
		"vdash",
		"Vvdash"
	],
	t: [
		"beth",
		"daleth",
		"eth",
		"imath",
		"jmath",
		"wreath"
	],
	x: [
		"boxh"
	],
	V: [
		"boxVh"
	],
	v: [
		"boxvh"
	],
	k: [
		"planckh"
	],
	i: [
		"upsih"
	]
};
var a$1 = {
	h: [
		"Alpha",
		"alpha"
	],
	a: [
		"angmsdaa"
	],
	t: [
		"Beta",
		"beta",
		"Delta",
		"delta",
		"Eta",
		"eta",
		"iiota",
		"Iota",
		"iota",
		"Theta",
		"theta",
		"vartheta",
		"Zeta",
		"zeta"
	],
	l: [
		"Cedilla",
		"gla",
		"nabla"
	],
	m: [
		"comma",
		"digamma",
		"Gamma",
		"gamma",
		"InvisibleComma",
		"mcomma",
		"Sigma",
		"sigma",
		"varsigma"
	],
	p: [
		"Kappa",
		"kappa",
		"varkappa"
	],
	d: [
		"Lambda",
		"lambda"
	],
	c: [
		"ldca",
		"rdca"
	],
	v: [
		"notinva",
		"notniva"
	],
	g: [
		"Omega",
		"omega"
	],
	r: [
		"para"
	],
	e: [
		"toea"
	],
	s: [
		"tosa"
	]
};
var P$1 = {
	M: [
		"AMP"
	]
};
var p$1 = {
	m: [
		"amp",
		"asymp",
		"bump",
		"comp",
		"HumpDownHump",
		"mp",
		"nbump",
		"NotHumpDownHump"
	],
	a: [
		"ap",
		"bigcap",
		"Cap",
		"cap",
		"capcap",
		"cupbrcap",
		"CupCap",
		"cupcap",
		"gap",
		"gnap",
		"lap",
		"lnap",
		"Map",
		"map",
		"multimap",
		"mumap",
		"nap",
		"ncap",
		"NotCupCap",
		"nvap",
		"prap",
		"prnap",
		"rarrap",
		"scap",
		"scnap",
		"sqcap",
		"thkap",
		"xcap",
		"xmap"
	],
	u: [
		"bigcup",
		"bigsqcup",
		"bigtriangleup",
		"capbrcup",
		"capcup",
		"csup",
		"Cup",
		"cup",
		"cupcup",
		"leftharpoonup",
		"mapstoup",
		"ncup",
		"nsup",
		"rightharpoonup",
		"sqcup",
		"sqsup",
		"subsup",
		"Sup",
		"sup",
		"supsup",
		"vnsup",
		"xcup",
		"xsqcup"
	],
	r: [
		"cularrp",
		"operp",
		"perp",
		"sharp",
		"weierp"
	],
	o: [
		"dlcrop",
		"drcrop",
		"prop",
		"top",
		"ulcrop",
		"urcrop",
		"vprop"
	],
	s: [
		"emsp",
		"ensp",
		"hairsp",
		"nbsp",
		"numsp",
		"puncsp",
		"thinsp"
	],
	i: [
		"hellip",
		"vellip"
	],
	l: [
		"larrlp",
		"rarrlp"
	],
	c: [
		"mlcp"
	],
	h: [
		"smashp"
	],
	w: [
		"wp"
	]
};
var v$1 = {
	d: [
		"andv"
	],
	r: [
		"Barv",
		"orv",
		"vBarv"
	],
	y: [
		"bemptyv",
		"cemptyv",
		"demptyv",
		"emptyv",
		"laemptyv",
		"raemptyv"
	],
	i: [
		"bnequiv",
		"div",
		"epsiv",
		"equiv",
		"nequiv",
		"niv",
		"odiv",
		"phiv",
		"piv"
	],
	x: [
		"boxv"
	],
	h: [
		"Dashv",
		"dashv"
	],
	k: [
		"forkv",
		"plankv"
	],
	s: [
		"isinsv"
	],
	n: [
		"isinv"
	],
	a: [
		"kappav",
		"sigmav",
		"thetav"
	],
	t: [
		"nGtv",
		"nLtv"
	],
	o: [
		"rhov"
	]
};
var b$1 = {
	a: [
		"angmsdab",
		"Tab"
	],
	v: [
		"angrtvb",
		"notinvb",
		"notnivb"
	],
	l: [
		"bsolb",
		"solb"
	],
	u: [
		"bsolhsub",
		"csub",
		"lcub",
		"nsub",
		"rcub",
		"sqsub",
		"Sub",
		"sub",
		"subsub",
		"supdsub",
		"suphsub",
		"supsub",
		"vnsub"
	],
	r: [
		"larrb",
		"rarrb"
	],
	q: [
		"lsqb",
		"rsqb"
	],
	s: [
		"minusb",
		"plusb",
		"timesb",
		"trisb"
	],
	t: [
		"sdotb"
	]
};
var t$1 = {
	r: [
		"angrt",
		"imagpart",
		"npart",
		"part",
		"realpart",
		"Sqrt",
		"vangrt",
		"Vert",
		"vert"
	],
	s: [
		"angst",
		"ast",
		"circledast",
		"equest",
		"exist",
		"gtquest",
		"iquest",
		"lmoust",
		"lowast",
		"ltquest",
		"midast",
		"nexist",
		"oast",
		"quest",
		"rmoust"
	],
	n: [
		"awconint",
		"awint",
		"Cconint",
		"cent",
		"cirfnint",
		"complement",
		"Congruent",
		"Conint",
		"conint",
		"cwconint",
		"cwint",
		"Element",
		"fpartint",
		"geqslant",
		"iiiint",
		"iiint",
		"Int",
		"int",
		"leqslant",
		"ngeqslant",
		"nleqslant",
		"NotCongruent",
		"NotElement",
		"NotReverseElement",
		"npolint",
		"oint",
		"percnt",
		"pointint",
		"qint",
		"quatint",
		"ReverseElement",
		"rppolint",
		"scpolint",
		"tint"
	],
	o: [
		"bigodot",
		"bNot",
		"bnot",
		"bot",
		"capdot",
		"Cdot",
		"cdot",
		"CenterDot",
		"centerdot",
		"CircleDot",
		"congdot",
		"ctdot",
		"cupdot",
		"DiacriticalDot",
		"Dot",
		"dot",
		"DotDot",
		"doteqdot",
		"DoubleDot",
		"dtdot",
		"eDDot",
		"Edot",
		"eDot",
		"edot",
		"efDot",
		"egsdot",
		"elsdot",
		"erDot",
		"esdot",
		"Gdot",
		"gdot",
		"gesdot",
		"gtdot",
		"gtrdot",
		"Idot",
		"inodot",
		"isindot",
		"lesdot",
		"lessdot",
		"Lmidot",
		"lmidot",
		"ltdot",
		"mDDot",
		"middot",
		"ncongdot",
		"nedot",
		"Not",
		"not",
		"notindot",
		"odot",
		"quot",
		"sdot",
		"simdot",
		"subdot",
		"subedot",
		"supdot",
		"supedot",
		"tdot",
		"topbot",
		"tridot",
		"TripleDot",
		"utdot",
		"xodot",
		"Zdot",
		"zdot"
	],
	f: [
		"blacktriangleleft",
		"circlearrowleft",
		"curvearrowleft",
		"downharpoonleft",
		"looparrowleft",
		"mapstoleft",
		"ntriangleleft",
		"triangleleft",
		"upharpoonleft",
		"vartriangleleft"
	],
	h: [
		"blacktriangleright",
		"circlearrowright",
		"curvearrowright",
		"dfisht",
		"downharpoonright",
		"homtht",
		"lfisht",
		"looparrowright",
		"ntriangleright",
		"rfisht",
		"triangleright",
		"ufisht",
		"upharpoonright",
		"vartriangleright"
	],
	e: [
		"bullet",
		"caret",
		"emptyset",
		"LeftAngleBracket",
		"LeftDoubleBracket",
		"NotSquareSubset",
		"NotSquareSuperset",
		"NotSubset",
		"NotSuperset",
		"nsubset",
		"nsupset",
		"OverBracket",
		"RightAngleBracket",
		"RightDoubleBracket",
		"sqsubset",
		"sqsupset",
		"SquareSubset",
		"SquareSuperset",
		"Subset",
		"subset",
		"Superset",
		"Supset",
		"supset",
		"target",
		"UnderBracket"
	],
	i: [
		"clubsuit",
		"diamondsuit",
		"heartsuit",
		"it",
		"spadesuit"
	],
	a: [
		"commat",
		"flat",
		"Hat",
		"lat",
		"phmmat",
		"SuchThat"
	],
	c: [
		"Coproduct",
		"Product",
		"rect",
		"sect"
	],
	G: [
		"Gt",
		"nGt"
	],
	g: [
		"gt",
		"ngt",
		"nvgt",
		"ogt",
		"rpargt"
	],
	l: [
		"hamilt",
		"lparlt",
		"lt",
		"malt",
		"nlt",
		"nvlt",
		"olt",
		"submult",
		"supmult"
	],
	L: [
		"Lt",
		"nLt"
	],
	x: [
		"sext",
		"twixt"
	],
	m: [
		"smt"
	]
};
var n$1 = {
	o: [
		"Aogon",
		"aogon",
		"ApplyFunction",
		"backepsilon",
		"caron",
		"Ccaron",
		"ccaron",
		"Colon",
		"colon",
		"Dcaron",
		"dcaron",
		"Ecaron",
		"ecaron",
		"ecolon",
		"Eogon",
		"eogon",
		"Epsilon",
		"epsilon",
		"eqcolon",
		"expectation",
		"hercon",
		"Intersection",
		"Iogon",
		"iogon",
		"Lcaron",
		"lcaron",
		"Ncaron",
		"ncaron",
		"ogon",
		"Omicron",
		"omicron",
		"Proportion",
		"Rcaron",
		"rcaron",
		"Scaron",
		"scaron",
		"SquareIntersection",
		"SquareUnion",
		"straightepsilon",
		"Tcaron",
		"tcaron",
		"Union",
		"Uogon",
		"uogon",
		"Upsilon",
		"upsilon",
		"varepsilon",
		"Zcaron",
		"zcaron"
	],
	g: [
		"Assign"
	],
	e: [
		"between",
		"curren",
		"hyphen",
		"kgreen",
		"yen"
	],
	w: [
		"bigtriangledown",
		"blacktriangledown",
		"frown",
		"leftharpoondown",
		"mapstodown",
		"rightharpoondown",
		"sfrown",
		"triangledown"
	],
	f: [
		"compfn"
	],
	i: [
		"disin",
		"iinfin",
		"in",
		"infin",
		"isin",
		"notin",
		"nvinfin"
	],
	r: [
		"dlcorn",
		"drcorn",
		"thorn",
		"ulcorn",
		"urcorn"
	],
	a: [
		"lagran"
	],
	m: [
		"plusmn",
		"setmn",
		"ssetmn"
	]
};
var s$1 = {
	o: [
		"apos",
		"mstpos",
		"napos"
	],
	u: [
		"becaus",
		"bigoplus",
		"biguplus",
		"boxminus",
		"boxplus",
		"CircleMinus",
		"CirclePlus",
		"dotminus",
		"dotplus",
		"eplus",
		"loplus",
		"minus",
		"MinusPlus",
		"mnplus",
		"ominus",
		"oplus",
		"plus",
		"PlusMinus",
		"roplus",
		"setminus",
		"simplus",
		"smallsetminus",
		"subplus",
		"supplus",
		"triminus",
		"triplus",
		"UnionPlus",
		"uplus",
		"xoplus",
		"xuplus"
	],
	i: [
		"Bernoullis",
		"nis",
		"OverParenthesis",
		"UnderParenthesis",
		"xnis"
	],
	e: [
		"bigotimes",
		"boxtimes",
		"CircleTimes",
		"complexes",
		"divideontimes",
		"ges",
		"gesles",
		"Implies",
		"InvisibleTimes",
		"lates",
		"leftthreetimes",
		"les",
		"lesges",
		"lotimes",
		"ltimes",
		"nges",
		"nles",
		"NotPrecedes",
		"Otimes",
		"otimes",
		"Precedes",
		"primes",
		"rightthreetimes",
		"rotimes",
		"RoundImplies",
		"rtimes",
		"smtes",
		"spades",
		"times"
	],
	p: [
		"caps",
		"ccaps",
		"ccups",
		"cups",
		"sqcaps",
		"sqcups"
	],
	y: [
		"Cayleys"
	],
	b: [
		"clubs"
	],
	s: [
		"Cross",
		"cross",
		"eqslantless",
		"GreaterEqualLess",
		"GreaterLess",
		"gtreqless",
		"gtreqqless",
		"gtrless",
		"LessLess",
		"NestedLessLess",
		"nless",
		"NotGreaterLess",
		"NotLess",
		"NotLessLess",
		"NotNestedLessLess",
		"olcross"
	],
	m: [
		"diams"
	],
	w: [
		"downdownarrows",
		"leftleftarrows",
		"leftrightarrows",
		"rightleftarrows",
		"rightrightarrows",
		"upuparrows"
	],
	g: [
		"egs"
	],
	r: [
		"elinters",
		"integers"
	],
	l: [
		"els",
		"equals",
		"models",
		"naturals",
		"rationals",
		"reals"
	],
	t: [
		"Exists",
		"hearts",
		"nexists",
		"NotExists"
	],
	n: [
		"fltns",
		"isins",
		"leftrightharpoons",
		"quaternions",
		"rightleftharpoons",
		"strns"
	],
	f: [
		"larrbfs",
		"larrfs",
		"rarrbfs",
		"rarrfs"
	],
	d: [
		"NotSucceeds",
		"Succeeds"
	],
	a: [
		"otimesas"
	]
};
var x$1 = {
	o: [
		"approx",
		"boxbox",
		"gnapprox",
		"gtrapprox",
		"lessapprox",
		"lnapprox",
		"napprox",
		"precapprox",
		"precnapprox",
		"succapprox",
		"succnapprox",
		"thickapprox"
	],
	n: [
		"divonx"
	],
	r: [
		"rx"
	]
};
var q$1 = {
	e: [
		"approxeq",
		"asympeq",
		"backsimeq",
		"Bumpeq",
		"bumpeq",
		"circeq",
		"coloneq",
		"ddotseq",
		"doteq",
		"fallingdotseq",
		"geq",
		"gneq",
		"leq",
		"lneq",
		"ngeq",
		"nleq",
		"npreceq",
		"nsimeq",
		"nsubseteq",
		"nsucceq",
		"nsupseteq",
		"ntrianglelefteq",
		"ntrianglerighteq",
		"preccurlyeq",
		"preceq",
		"questeq",
		"risingdotseq",
		"simeq",
		"sqsubseteq",
		"sqsupseteq",
		"subseteq",
		"subsetneq",
		"succcurlyeq",
		"succeq",
		"supseteq",
		"supsetneq",
		"trianglelefteq",
		"triangleq",
		"trianglerighteq",
		"varsubsetneq",
		"varsupsetneq",
		"veeeq",
		"wedgeq"
	],
	q: [
		"geqq",
		"gneqq",
		"gvertneqq",
		"leqq",
		"lneqq",
		"lvertneqq",
		"ngeqq",
		"nleqq",
		"nsubseteqq",
		"nsupseteqq",
		"precneqq",
		"subseteqq",
		"subsetneqq",
		"succneqq",
		"supseteqq",
		"supsetneqq",
		"varsubsetneqq",
		"varsupsetneqq"
	]
};
var l$1 = {
	m: [
		"Auml",
		"auml",
		"Euml",
		"euml",
		"gsiml",
		"Iuml",
		"iuml",
		"Ouml",
		"ouml",
		"siml",
		"uml",
		"Uuml",
		"uuml",
		"Yuml",
		"yuml"
	],
	D: [
		"boxDl"
	],
	d: [
		"boxdl"
	],
	U: [
		"boxUl"
	],
	u: [
		"boxul",
		"lharul",
		"rharul"
	],
	V: [
		"boxVl"
	],
	v: [
		"boxvl"
	],
	o: [
		"bsol",
		"dsol",
		"gesdotol",
		"osol",
		"sol",
		"suphsol"
	],
	l: [
		"bull",
		"ell",
		"ForAll",
		"forall",
		"hybull",
		"ll"
	],
	i: [
		"Ccedil",
		"ccedil",
		"cedil",
		"Gcedil",
		"Kcedil",
		"kcedil",
		"lAtail",
		"latail",
		"Lcedil",
		"lcedil",
		"lceil",
		"leftarrowtail",
		"Ncedil",
		"ncedil",
		"permil",
		"rAtail",
		"ratail",
		"Rcedil",
		"rcedil",
		"rceil",
		"rightarrowtail",
		"Scedil",
		"scedil",
		"Tcedil",
		"tcedil"
	],
	a: [
		"ClockwiseContourIntegral",
		"ContourIntegral",
		"CounterClockwiseContourIntegral",
		"DotEqual",
		"DoubleContourIntegral",
		"Equal",
		"GreaterEqual",
		"GreaterFullEqual",
		"GreaterSlantEqual",
		"HumpEqual",
		"intcal",
		"Integral",
		"intercal",
		"LeftTriangleEqual",
		"LessFullEqual",
		"LessSlantEqual",
		"natural",
		"NotEqual",
		"NotGreaterEqual",
		"NotGreaterFullEqual",
		"NotGreaterSlantEqual",
		"NotHumpEqual",
		"NotLeftTriangleEqual",
		"NotLessEqual",
		"NotLessSlantEqual",
		"NotPrecedesEqual",
		"NotPrecedesSlantEqual",
		"NotRightTriangleEqual",
		"NotSquareSubsetEqual",
		"NotSquareSupersetEqual",
		"NotSubsetEqual",
		"NotSucceedsEqual",
		"NotSucceedsSlantEqual",
		"NotSupersetEqual",
		"NotTildeEqual",
		"NotTildeFullEqual",
		"PrecedesEqual",
		"PrecedesSlantEqual",
		"Proportional",
		"real",
		"RightTriangleEqual",
		"SquareSubsetEqual",
		"SquareSupersetEqual",
		"SubsetEqual",
		"SucceedsEqual",
		"SucceedsSlantEqual",
		"SupersetEqual",
		"TildeEqual",
		"TildeFullEqual"
	],
	r: [
		"cudarrl",
		"dharl",
		"uharl"
	],
	e: [
		"Del",
		"el",
		"gel",
		"gimel",
		"nparallel",
		"nshortparallel",
		"parallel",
		"prurel",
		"shortparallel"
	],
	s: [
		"eparsl",
		"eqvparsl",
		"frasl",
		"gesl",
		"nparsl",
		"parsl",
		"smeparsl"
	],
	c: [
		"excl",
		"iexcl"
	],
	E: [
		"gEl"
	],
	g: [
		"gl",
		"ntgl"
	],
	p: [
		"larrpl",
		"rarrpl"
	],
	t: [
		"larrtl",
		"Rarrtl",
		"rarrtl"
	],
	L: [
		"Ll",
		"nLl"
	],
	h: [
		"Vdashl"
	]
};
var k$1 = {
	r: [
		"bbrk",
		"bbrktbrk",
		"checkmark",
		"fork",
		"lbbrk",
		"lobrk",
		"pitchfork",
		"rbbrk",
		"robrk",
		"tbrk",
		"topfork"
	],
	n: [
		"blank",
		"pertenk"
	],
	c: [
		"block",
		"check",
		"lbrack",
		"planck",
		"rbrack"
	],
	o: [
		"Dstrok",
		"dstrok",
		"Hstrok",
		"hstrok",
		"Lstrok",
		"lstrok",
		"Tstrok",
		"tstrok"
	],
	e: [
		"Hacek"
	],
	h: [
		"intlarhk",
		"larrhk",
		"nearhk",
		"nwarhk",
		"rarrhk",
		"searhk",
		"swarhk"
	],
	l: [
		"lhblk",
		"uhblk"
	],
	a: [
		"NoBreak"
	]
};
var o$1 = {
	u: [
		"bdquo",
		"laquo",
		"ldquo",
		"lsaquo",
		"lsquo",
		"raquo",
		"rdquo",
		"rsaquo",
		"rsquo",
		"sbquo"
	],
	r: [
		"euro",
		"micro",
		"numero"
	],
	t: [
		"gesdoto",
		"lesdoto",
		"longmapsto",
		"mapsto",
		"propto",
		"varpropto"
	],
	h: [
		"mho",
		"Rho",
		"rho",
		"varrho"
	],
	d: [
		"plusdo"
	],
	w: [
		"plustwo"
	],
	i: [
		"ratio"
	]
};
var i$1 = {
	s: [
		"bepsi",
		"epsi",
		"Psi",
		"psi",
		"Upsi",
		"upsi"
	],
	m: [
		"bsemi",
		"semi"
	],
	h: [
		"Chi",
		"chi",
		"Phi",
		"phi",
		"straightphi",
		"varphi"
	],
	r: [
		"dtri",
		"lltri",
		"lrtri",
		"ltri",
		"nltri",
		"nrtri",
		"rtri",
		"rtriltri",
		"ultri",
		"urtri",
		"utri",
		"vltri",
		"vrtri",
		"xdtri",
		"xutri"
	],
	i: [
		"ii"
	],
	n: [
		"ni",
		"notni"
	],
	P: [
		"Pi"
	],
	p: [
		"pi",
		"varpi"
	],
	X: [
		"Xi"
	],
	x: [
		"xi"
	]
};
var u$1 = {
	o: [
		"bernou"
	],
	H: [
		"boxHu"
	],
	h: [
		"boxhu"
	],
	l: [
		"lbrkslu",
		"rbrkslu"
	],
	r: [
		"lharu",
		"rharu"
	],
	d: [
		"minusdu",
		"plusdu"
	],
	M: [
		"Mu"
	],
	m: [
		"mu"
	],
	N: [
		"Nu"
	],
	n: [
		"nu"
	],
	q: [
		"squ"
	],
	a: [
		"Tau",
		"tau"
	]
};
var w$1 = {
	o: [
		"bkarow",
		"dbkarow",
		"DoubleDownArrow",
		"DoubleLeftArrow",
		"DoubleLeftRightArrow",
		"DoubleLongLeftArrow",
		"DoubleLongLeftRightArrow",
		"DoubleLongRightArrow",
		"DoubleRightArrow",
		"DoubleUpArrow",
		"DoubleUpDownArrow",
		"DownArrow",
		"Downarrow",
		"downarrow",
		"DownArrowUpArrow",
		"DownTeeArrow",
		"drbkarow",
		"hksearow",
		"hkswarow",
		"hookleftarrow",
		"hookrightarrow",
		"LeftArrow",
		"Leftarrow",
		"leftarrow",
		"LeftArrowRightArrow",
		"LeftRightArrow",
		"Leftrightarrow",
		"leftrightarrow",
		"leftrightsquigarrow",
		"LeftTeeArrow",
		"Lleftarrow",
		"LongLeftArrow",
		"Longleftarrow",
		"longleftarrow",
		"LongLeftRightArrow",
		"Longleftrightarrow",
		"longleftrightarrow",
		"LongRightArrow",
		"Longrightarrow",
		"longrightarrow",
		"LowerLeftArrow",
		"LowerRightArrow",
		"nearrow",
		"nLeftarrow",
		"nleftarrow",
		"nLeftrightarrow",
		"nleftrightarrow",
		"nRightarrow",
		"nrightarrow",
		"nwarrow",
		"RightArrow",
		"Rightarrow",
		"rightarrow",
		"RightArrowLeftArrow",
		"rightsquigarrow",
		"RightTeeArrow",
		"Rrightarrow",
		"searrow",
		"ShortDownArrow",
		"ShortLeftArrow",
		"ShortRightArrow",
		"ShortUpArrow",
		"swarrow",
		"twoheadleftarrow",
		"twoheadrightarrow",
		"UpArrow",
		"Uparrow",
		"uparrow",
		"UpArrowDownArrow",
		"UpDownArrow",
		"Updownarrow",
		"updownarrow",
		"UpperLeftArrow",
		"UpperRightArrow",
		"UpTeeArrow"
	],
	r: [
		"harrw",
		"nrarrw",
		"rarrw"
	]
};
var L$1 = {
	D: [
		"boxDL"
	],
	d: [
		"boxdL"
	],
	U: [
		"boxUL"
	],
	u: [
		"boxuL"
	],
	V: [
		"boxVL"
	],
	v: [
		"boxvL"
	]
};
var R$1 = {
	D: [
		"boxDR"
	],
	d: [
		"boxdR",
		"circledR"
	],
	U: [
		"boxUR"
	],
	u: [
		"boxuR"
	],
	V: [
		"boxVR"
	],
	v: [
		"boxvR"
	]
};
var H$1 = {
	x: [
		"boxH"
	],
	V: [
		"boxVH"
	],
	v: [
		"boxvH"
	],
	T: [
		"ETH"
	]
};
var D$1 = {
	H: [
		"boxHD"
	],
	h: [
		"boxhD"
	],
	l: [
		"CapitalDifferentialD",
		"DifferentialD",
		"PartialD"
	],
	D: [
		"DD",
		"equivDD"
	]
};
var U$1 = {
	H: [
		"boxHU"
	],
	h: [
		"boxhU"
	]
};
var V$1 = {
	x: [
		"boxV"
	]
};
var S$1 = {
	d: [
		"circledS"
	],
	o: [
		"oS"
	]
};
var Y$1 = {
	P: [
		"COPY"
	]
};
var G$1 = {
	N: [
		"ENG"
	],
	E: [
		"REG"
	]
};
var j$1 = {
	l: [
		"glj"
	],
	w: [
		"zwj"
	],
	n: [
		"zwnj"
	]
};
var T$1 = {
	G: [
		"GT"
	],
	L: [
		"LT"
	],
	O: [
		"QUOT"
	]
};
var I$1 = {
	y: [
		"ImaginaryI"
	]
};
var z$1 = {
	o: [
		"loz"
	]
};
var N$1 = {
	R: [
		"THORN"
	]
};
var endsWith = {
	"1": {
	p: [
		"sup1"
	]
},
	"2": {
	"1": [
		"blk12",
		"frac12"
	],
	p: [
		"sup2"
	]
},
	"3": {
	"1": [
		"emsp13",
		"frac13"
	],
	"2": [
		"frac23"
	],
	p: [
		"sup3"
	]
},
	"4": {
	"1": [
		"blk14",
		"emsp14",
		"frac14"
	],
	"3": [
		"blk34",
		"frac34"
	],
	e: [
		"there4"
	]
},
	"5": {
	"1": [
		"frac15"
	],
	"2": [
		"frac25"
	],
	"3": [
		"frac35"
	],
	"4": [
		"frac45"
	]
},
	"6": {
	"1": [
		"frac16"
	],
	"5": [
		"frac56"
	]
},
	"8": {
	"1": [
		"frac18"
	],
	"3": [
		"frac38"
	],
	"5": [
		"frac58"
	],
	"7": [
		"frac78"
	]
},
	e: e$1,
	c: c$1,
	d: d$1,
	E: E$1,
	y: y$1,
	g: g$1,
	f: f$1,
	r: r$1,
	m: m$1,
	h: h$1,
	a: a$1,
	P: P$1,
	p: p$1,
	v: v$1,
	b: b$1,
	t: t$1,
	n: n$1,
	s: s$1,
	x: x$1,
	q: q$1,
	l: l$1,
	k: k$1,
	o: o$1,
	i: i$1,
	u: u$1,
	w: w$1,
	L: L$1,
	R: R$1,
	H: H$1,
	D: D$1,
	U: U$1,
	V: V$1,
	S: S$1,
	Y: Y$1,
	G: G$1,
	j: j$1,
	T: T$1,
	I: I$1,
	z: z$1,
	N: N$1
};

var a$2 = {
	a: [
		"aacute"
	],
	b: [
		"abreve"
	],
	c: [
		"ac",
		"acd",
		"ace",
		"acirc",
		"acute",
		"acy"
	],
	e: [
		"aelig"
	],
	f: [
		"af",
		"afr"
	],
	g: [
		"agrave"
	],
	l: [
		"alefsym",
		"aleph",
		"alpha"
	],
	m: [
		"amacr",
		"amalg",
		"amp"
	],
	n: [
		"and",
		"andand",
		"andd",
		"andslope",
		"andv",
		"ang",
		"ange",
		"angle",
		"angmsd",
		"angmsdaa",
		"angmsdab",
		"angmsdac",
		"angmsdad",
		"angmsdae",
		"angmsdaf",
		"angmsdag",
		"angmsdah",
		"angrt",
		"angrtvb",
		"angrtvbd",
		"angsph",
		"angst",
		"angzarr"
	],
	o: [
		"aogon",
		"aopf"
	],
	p: [
		"ap",
		"apacir",
		"ape",
		"apid",
		"apos",
		"applyfunction",
		"approx",
		"approxeq"
	],
	r: [
		"aring"
	],
	s: [
		"ascr",
		"assign",
		"ast",
		"asymp",
		"asympeq"
	],
	t: [
		"atilde"
	],
	u: [
		"auml"
	],
	w: [
		"awconint",
		"awint"
	]
};
var b$2 = {
	a: [
		"backcong",
		"backepsilon",
		"backprime",
		"backsim",
		"backsimeq",
		"backslash",
		"barv",
		"barvee",
		"barwed",
		"barwedge"
	],
	b: [
		"bbrk",
		"bbrktbrk"
	],
	c: [
		"bcong",
		"bcy"
	],
	d: [
		"bdquo"
	],
	e: [
		"becaus",
		"because",
		"bemptyv",
		"bepsi",
		"bernou",
		"bernoullis",
		"beta",
		"beth",
		"between"
	],
	f: [
		"bfr"
	],
	i: [
		"bigcap",
		"bigcirc",
		"bigcup",
		"bigodot",
		"bigoplus",
		"bigotimes",
		"bigsqcup",
		"bigstar",
		"bigtriangledown",
		"bigtriangleup",
		"biguplus",
		"bigvee",
		"bigwedge"
	],
	k: [
		"bkarow"
	],
	l: [
		"blacklozenge",
		"blacksquare",
		"blacktriangle",
		"blacktriangledown",
		"blacktriangleleft",
		"blacktriangleright",
		"blank",
		"blk12",
		"blk14",
		"blk34",
		"block"
	],
	n: [
		"bne",
		"bnequiv",
		"bnot"
	],
	o: [
		"bopf",
		"bot",
		"bottom",
		"bowtie",
		"boxbox",
		"boxdl",
		"boxdr",
		"boxh",
		"boxhd",
		"boxhu",
		"boxminus",
		"boxplus",
		"boxtimes",
		"boxul",
		"boxur",
		"boxv",
		"boxvh",
		"boxvl",
		"boxvr"
	],
	p: [
		"bprime"
	],
	r: [
		"breve",
		"brvbar"
	],
	s: [
		"bscr",
		"bsemi",
		"bsim",
		"bsime",
		"bsol",
		"bsolb",
		"bsolhsub"
	],
	u: [
		"bull",
		"bullet",
		"bump",
		"bumpe",
		"bumpeq"
	]
};
var c$2 = {
	a: [
		"cacute",
		"cap",
		"capand",
		"capbrcup",
		"capcap",
		"capcup",
		"capdot",
		"capitaldifferentiald",
		"caps",
		"caret",
		"caron",
		"cayleys"
	],
	c: [
		"ccaps",
		"ccaron",
		"ccedil",
		"ccirc",
		"cconint",
		"ccups",
		"ccupssm"
	],
	d: [
		"cdot"
	],
	e: [
		"cedil",
		"cedilla",
		"cemptyv",
		"cent",
		"centerdot"
	],
	f: [
		"cfr"
	],
	h: [
		"chcy",
		"check",
		"checkmark",
		"chi"
	],
	i: [
		"cir",
		"circ",
		"circeq",
		"circlearrowleft",
		"circlearrowright",
		"circledast",
		"circledcirc",
		"circleddash",
		"circledot",
		"circledr",
		"circleds",
		"circleminus",
		"circleplus",
		"circletimes",
		"cire",
		"cirfnint",
		"cirmid",
		"cirscir"
	],
	l: [
		"clockwisecontourintegral",
		"closecurlydoublequote",
		"closecurlyquote",
		"clubs",
		"clubsuit"
	],
	o: [
		"colon",
		"colone",
		"coloneq",
		"comma",
		"commat",
		"comp",
		"compfn",
		"complement",
		"complexes",
		"cong",
		"congdot",
		"congruent",
		"conint",
		"contourintegral",
		"copf",
		"coprod",
		"coproduct",
		"copy",
		"copysr",
		"counterclockwisecontourintegral"
	],
	r: [
		"crarr",
		"cross"
	],
	s: [
		"cscr",
		"csub",
		"csube",
		"csup",
		"csupe"
	],
	t: [
		"ctdot"
	],
	u: [
		"cudarrl",
		"cudarrr",
		"cuepr",
		"cuesc",
		"cularr",
		"cularrp",
		"cup",
		"cupbrcap",
		"cupcap",
		"cupcup",
		"cupdot",
		"cupor",
		"cups",
		"curarr",
		"curarrm",
		"curlyeqprec",
		"curlyeqsucc",
		"curlyvee",
		"curlywedge",
		"curren",
		"curvearrowleft",
		"curvearrowright",
		"cuvee",
		"cuwed"
	],
	w: [
		"cwconint",
		"cwint"
	],
	y: [
		"cylcty"
	]
};
var d$2 = {
	a: [
		"dagger",
		"daleth",
		"darr",
		"dash",
		"dashv"
	],
	b: [
		"dbkarow",
		"dblac"
	],
	c: [
		"dcaron",
		"dcy"
	],
	d: [
		"dd",
		"ddagger",
		"ddarr",
		"ddotrahd",
		"ddotseq"
	],
	e: [
		"deg",
		"del",
		"delta",
		"demptyv"
	],
	f: [
		"dfisht",
		"dfr"
	],
	h: [
		"dhar",
		"dharl",
		"dharr"
	],
	i: [
		"diacriticalacute",
		"diacriticaldot",
		"diacriticaldoubleacute",
		"diacriticalgrave",
		"diacriticaltilde",
		"diam",
		"diamond",
		"diamondsuit",
		"diams",
		"die",
		"differentiald",
		"digamma",
		"disin",
		"div",
		"divide",
		"divideontimes",
		"divonx"
	],
	j: [
		"djcy"
	],
	l: [
		"dlcorn",
		"dlcrop"
	],
	o: [
		"dollar",
		"dopf",
		"dot",
		"dotdot",
		"doteq",
		"doteqdot",
		"dotequal",
		"dotminus",
		"dotplus",
		"dotsquare",
		"doublebarwedge",
		"doublecontourintegral",
		"doubledot",
		"doubledownarrow",
		"doubleleftarrow",
		"doubleleftrightarrow",
		"doublelefttee",
		"doublelongleftarrow",
		"doublelongleftrightarrow",
		"doublelongrightarrow",
		"doublerightarrow",
		"doublerighttee",
		"doubleuparrow",
		"doubleupdownarrow",
		"doubleverticalbar",
		"downarrow",
		"downarrowbar",
		"downarrowuparrow",
		"downbreve",
		"downdownarrows",
		"downharpoonleft",
		"downharpoonright",
		"downleftrightvector",
		"downleftteevector",
		"downleftvector",
		"downleftvectorbar",
		"downrightteevector",
		"downrightvector",
		"downrightvectorbar",
		"downtee",
		"downteearrow"
	],
	r: [
		"drbkarow",
		"drcorn",
		"drcrop"
	],
	s: [
		"dscr",
		"dscy",
		"dsol",
		"dstrok"
	],
	t: [
		"dtdot",
		"dtri",
		"dtrif"
	],
	u: [
		"duarr",
		"duhar"
	],
	w: [
		"dwangle"
	],
	z: [
		"dzcy",
		"dzigrarr"
	]
};
var e$2 = {
	a: [
		"eacute",
		"easter"
	],
	c: [
		"ecaron",
		"ecir",
		"ecirc",
		"ecolon",
		"ecy"
	],
	d: [
		"eddot",
		"edot"
	],
	e: [
		"ee"
	],
	f: [
		"efdot",
		"efr"
	],
	g: [
		"eg",
		"egrave",
		"egs",
		"egsdot"
	],
	l: [
		"el",
		"element",
		"elinters",
		"ell",
		"els",
		"elsdot"
	],
	m: [
		"emacr",
		"empty",
		"emptyset",
		"emptysmallsquare",
		"emptyv",
		"emptyverysmallsquare",
		"emsp",
		"emsp13",
		"emsp14"
	],
	n: [
		"eng",
		"ensp"
	],
	o: [
		"eogon",
		"eopf"
	],
	p: [
		"epar",
		"eparsl",
		"eplus",
		"epsi",
		"epsilon",
		"epsiv"
	],
	q: [
		"eqcirc",
		"eqcolon",
		"eqsim",
		"eqslantgtr",
		"eqslantless",
		"equal",
		"equals",
		"equaltilde",
		"equest",
		"equilibrium",
		"equiv",
		"equivdd",
		"eqvparsl"
	],
	r: [
		"erarr",
		"erdot"
	],
	s: [
		"escr",
		"esdot",
		"esim"
	],
	t: [
		"eta",
		"eth"
	],
	u: [
		"euml",
		"euro"
	],
	x: [
		"excl",
		"exist",
		"exists",
		"expectation",
		"exponentiale"
	]
};
var f$2 = {
	a: [
		"fallingdotseq"
	],
	c: [
		"fcy"
	],
	e: [
		"female"
	],
	f: [
		"ffilig",
		"fflig",
		"ffllig",
		"ffr"
	],
	i: [
		"filig",
		"filledsmallsquare",
		"filledverysmallsquare"
	],
	j: [
		"fjlig"
	],
	l: [
		"flat",
		"fllig",
		"fltns"
	],
	n: [
		"fnof"
	],
	o: [
		"fopf",
		"forall",
		"fork",
		"forkv",
		"fouriertrf"
	],
	p: [
		"fpartint"
	],
	r: [
		"frac12",
		"frac13",
		"frac14",
		"frac15",
		"frac16",
		"frac18",
		"frac23",
		"frac25",
		"frac34",
		"frac35",
		"frac38",
		"frac45",
		"frac56",
		"frac58",
		"frac78",
		"frasl",
		"frown"
	],
	s: [
		"fscr"
	]
};
var g$2 = {
	a: [
		"gacute",
		"gamma",
		"gammad",
		"gap"
	],
	b: [
		"gbreve"
	],
	c: [
		"gcedil",
		"gcirc",
		"gcy"
	],
	d: [
		"gdot"
	],
	e: [
		"ge",
		"gel",
		"geq",
		"geqq",
		"geqslant",
		"ges",
		"gescc",
		"gesdot",
		"gesdoto",
		"gesdotol",
		"gesl",
		"gesles"
	],
	f: [
		"gfr"
	],
	g: [
		"gg",
		"ggg"
	],
	i: [
		"gimel"
	],
	j: [
		"gjcy"
	],
	l: [
		"gl",
		"gla",
		"gle",
		"glj"
	],
	n: [
		"gnap",
		"gnapprox",
		"gne",
		"gneq",
		"gneqq",
		"gnsim"
	],
	o: [
		"gopf"
	],
	r: [
		"grave",
		"greaterequal",
		"greaterequalless",
		"greaterfullequal",
		"greatergreater",
		"greaterless",
		"greaterslantequal",
		"greatertilde"
	],
	s: [
		"gscr",
		"gsim",
		"gsime",
		"gsiml"
	],
	t: [
		"gt",
		"gtcc",
		"gtcir",
		"gtdot",
		"gtlpar",
		"gtquest",
		"gtrapprox",
		"gtrarr",
		"gtrdot",
		"gtreqless",
		"gtreqqless",
		"gtrless",
		"gtrsim"
	],
	v: [
		"gvertneqq",
		"gvne"
	]
};
var h$2 = {
	a: [
		"hacek",
		"hairsp",
		"half",
		"hamilt",
		"hardcy",
		"harr",
		"harrcir",
		"harrw",
		"hat"
	],
	b: [
		"hbar"
	],
	c: [
		"hcirc"
	],
	e: [
		"hearts",
		"heartsuit",
		"hellip",
		"hercon"
	],
	f: [
		"hfr"
	],
	i: [
		"hilbertspace"
	],
	k: [
		"hksearow",
		"hkswarow"
	],
	o: [
		"hoarr",
		"homtht",
		"hookleftarrow",
		"hookrightarrow",
		"hopf",
		"horbar",
		"horizontalline"
	],
	s: [
		"hscr",
		"hslash",
		"hstrok"
	],
	u: [
		"humpdownhump",
		"humpequal"
	],
	y: [
		"hybull",
		"hyphen"
	]
};
var i$2 = {
	a: [
		"iacute"
	],
	c: [
		"ic",
		"icirc",
		"icy"
	],
	d: [
		"idot"
	],
	e: [
		"iecy",
		"iexcl"
	],
	f: [
		"iff",
		"ifr"
	],
	g: [
		"igrave"
	],
	i: [
		"ii",
		"iiiint",
		"iiint",
		"iinfin",
		"iiota"
	],
	j: [
		"ijlig"
	],
	m: [
		"im",
		"imacr",
		"image",
		"imaginaryi",
		"imagline",
		"imagpart",
		"imath",
		"imof",
		"imped",
		"implies"
	],
	n: [
		"in",
		"incare",
		"infin",
		"infintie",
		"inodot",
		"int",
		"intcal",
		"integers",
		"integral",
		"intercal",
		"intersection",
		"intlarhk",
		"intprod",
		"invisiblecomma",
		"invisibletimes"
	],
	o: [
		"iocy",
		"iogon",
		"iopf",
		"iota"
	],
	p: [
		"iprod"
	],
	q: [
		"iquest"
	],
	s: [
		"iscr",
		"isin",
		"isindot",
		"isine",
		"isins",
		"isinsv",
		"isinv"
	],
	t: [
		"it",
		"itilde"
	],
	u: [
		"iukcy",
		"iuml"
	]
};
var j$2 = {
	c: [
		"jcirc",
		"jcy"
	],
	f: [
		"jfr"
	],
	m: [
		"jmath"
	],
	o: [
		"jopf"
	],
	s: [
		"jscr",
		"jsercy"
	],
	u: [
		"jukcy"
	]
};
var k$2 = {
	a: [
		"kappa",
		"kappav"
	],
	c: [
		"kcedil",
		"kcy"
	],
	f: [
		"kfr"
	],
	g: [
		"kgreen"
	],
	h: [
		"khcy"
	],
	j: [
		"kjcy"
	],
	o: [
		"kopf"
	],
	s: [
		"kscr"
	]
};
var l$2 = {
	a: [
		"laarr",
		"lacute",
		"laemptyv",
		"lagran",
		"lambda",
		"lang",
		"langd",
		"langle",
		"lap",
		"laplacetrf",
		"laquo",
		"larr",
		"larrb",
		"larrbfs",
		"larrfs",
		"larrhk",
		"larrlp",
		"larrpl",
		"larrsim",
		"larrtl",
		"lat",
		"latail",
		"late",
		"lates"
	],
	b: [
		"lbarr",
		"lbbrk",
		"lbrace",
		"lbrack",
		"lbrke",
		"lbrksld",
		"lbrkslu"
	],
	c: [
		"lcaron",
		"lcedil",
		"lceil",
		"lcub",
		"lcy"
	],
	d: [
		"ldca",
		"ldquo",
		"ldquor",
		"ldrdhar",
		"ldrushar",
		"ldsh"
	],
	e: [
		"le",
		"leftanglebracket",
		"leftarrow",
		"leftarrowbar",
		"leftarrowrightarrow",
		"leftarrowtail",
		"leftceiling",
		"leftdoublebracket",
		"leftdownteevector",
		"leftdownvector",
		"leftdownvectorbar",
		"leftfloor",
		"leftharpoondown",
		"leftharpoonup",
		"leftleftarrows",
		"leftrightarrow",
		"leftrightarrows",
		"leftrightharpoons",
		"leftrightsquigarrow",
		"leftrightvector",
		"lefttee",
		"leftteearrow",
		"leftteevector",
		"leftthreetimes",
		"lefttriangle",
		"lefttrianglebar",
		"lefttriangleequal",
		"leftupdownvector",
		"leftupteevector",
		"leftupvector",
		"leftupvectorbar",
		"leftvector",
		"leftvectorbar",
		"leg",
		"leq",
		"leqq",
		"leqslant",
		"les",
		"lescc",
		"lesdot",
		"lesdoto",
		"lesdotor",
		"lesg",
		"lesges",
		"lessapprox",
		"lessdot",
		"lesseqgtr",
		"lesseqqgtr",
		"lessequalgreater",
		"lessfullequal",
		"lessgreater",
		"lessgtr",
		"lessless",
		"lesssim",
		"lessslantequal",
		"lesstilde"
	],
	f: [
		"lfisht",
		"lfloor",
		"lfr"
	],
	g: [
		"lg",
		"lge"
	],
	h: [
		"lhar",
		"lhard",
		"lharu",
		"lharul",
		"lhblk"
	],
	j: [
		"ljcy"
	],
	l: [
		"ll",
		"llarr",
		"llcorner",
		"lleftarrow",
		"llhard",
		"lltri"
	],
	m: [
		"lmidot",
		"lmoust",
		"lmoustache"
	],
	n: [
		"lnap",
		"lnapprox",
		"lne",
		"lneq",
		"lneqq",
		"lnsim"
	],
	o: [
		"loang",
		"loarr",
		"lobrk",
		"longleftarrow",
		"longleftrightarrow",
		"longmapsto",
		"longrightarrow",
		"looparrowleft",
		"looparrowright",
		"lopar",
		"lopf",
		"loplus",
		"lotimes",
		"lowast",
		"lowbar",
		"lowerleftarrow",
		"lowerrightarrow",
		"loz",
		"lozenge",
		"lozf"
	],
	p: [
		"lpar",
		"lparlt"
	],
	r: [
		"lrarr",
		"lrcorner",
		"lrhar",
		"lrhard",
		"lrm",
		"lrtri"
	],
	s: [
		"lsaquo",
		"lscr",
		"lsh",
		"lsim",
		"lsime",
		"lsimg",
		"lsqb",
		"lsquo",
		"lsquor",
		"lstrok"
	],
	t: [
		"lt",
		"ltcc",
		"ltcir",
		"ltdot",
		"lthree",
		"ltimes",
		"ltlarr",
		"ltquest",
		"ltri",
		"ltrie",
		"ltrif",
		"ltrpar"
	],
	u: [
		"lurdshar",
		"luruhar"
	],
	v: [
		"lvertneqq",
		"lvne"
	]
};
var m$2 = {
	a: [
		"macr",
		"male",
		"malt",
		"maltese",
		"map",
		"mapsto",
		"mapstodown",
		"mapstoleft",
		"mapstoup",
		"marker"
	],
	c: [
		"mcomma",
		"mcy"
	],
	d: [
		"mdash",
		"mddot"
	],
	e: [
		"measuredangle",
		"mediumspace",
		"mellintrf"
	],
	f: [
		"mfr"
	],
	h: [
		"mho"
	],
	i: [
		"micro",
		"mid",
		"midast",
		"midcir",
		"middot",
		"minus",
		"minusb",
		"minusd",
		"minusdu",
		"minusplus"
	],
	l: [
		"mlcp",
		"mldr"
	],
	n: [
		"mnplus"
	],
	o: [
		"models",
		"mopf"
	],
	p: [
		"mp"
	],
	s: [
		"mscr",
		"mstpos"
	],
	u: [
		"mu",
		"multimap",
		"mumap"
	]
};
var n$2 = {
	a: [
		"nabla",
		"nacute",
		"nang",
		"nap",
		"nape",
		"napid",
		"napos",
		"napprox",
		"natur",
		"natural",
		"naturals"
	],
	b: [
		"nbsp",
		"nbump",
		"nbumpe"
	],
	c: [
		"ncap",
		"ncaron",
		"ncedil",
		"ncong",
		"ncongdot",
		"ncup",
		"ncy"
	],
	d: [
		"ndash"
	],
	e: [
		"ne",
		"nearhk",
		"nearr",
		"nearrow",
		"nedot",
		"negativemediumspace",
		"negativethickspace",
		"negativethinspace",
		"negativeverythinspace",
		"nequiv",
		"nesear",
		"nesim",
		"nestedgreatergreater",
		"nestedlessless",
		"newline",
		"nexist",
		"nexists"
	],
	f: [
		"nfr"
	],
	g: [
		"nge",
		"ngeq",
		"ngeqq",
		"ngeqslant",
		"nges",
		"ngg",
		"ngsim",
		"ngt",
		"ngtr",
		"ngtv"
	],
	h: [
		"nharr",
		"nhpar"
	],
	i: [
		"ni",
		"nis",
		"nisd",
		"niv"
	],
	j: [
		"njcy"
	],
	l: [
		"nlarr",
		"nldr",
		"nle",
		"nleftarrow",
		"nleftrightarrow",
		"nleq",
		"nleqq",
		"nleqslant",
		"nles",
		"nless",
		"nll",
		"nlsim",
		"nlt",
		"nltri",
		"nltrie",
		"nltv"
	],
	m: [
		"nmid"
	],
	o: [
		"nobreak",
		"nonbreakingspace",
		"nopf",
		"not",
		"notcongruent",
		"notcupcap",
		"notdoubleverticalbar",
		"notelement",
		"notequal",
		"notequaltilde",
		"notexists",
		"notgreater",
		"notgreaterequal",
		"notgreaterfullequal",
		"notgreatergreater",
		"notgreaterless",
		"notgreaterslantequal",
		"notgreatertilde",
		"nothumpdownhump",
		"nothumpequal",
		"notin",
		"notindot",
		"notine",
		"notinva",
		"notinvb",
		"notinvc",
		"notlefttriangle",
		"notlefttrianglebar",
		"notlefttriangleequal",
		"notless",
		"notlessequal",
		"notlessgreater",
		"notlessless",
		"notlessslantequal",
		"notlesstilde",
		"notnestedgreatergreater",
		"notnestedlessless",
		"notni",
		"notniva",
		"notnivb",
		"notnivc",
		"notprecedes",
		"notprecedesequal",
		"notprecedesslantequal",
		"notreverseelement",
		"notrighttriangle",
		"notrighttrianglebar",
		"notrighttriangleequal",
		"notsquaresubset",
		"notsquaresubsetequal",
		"notsquaresuperset",
		"notsquaresupersetequal",
		"notsubset",
		"notsubsetequal",
		"notsucceeds",
		"notsucceedsequal",
		"notsucceedsslantequal",
		"notsucceedstilde",
		"notsuperset",
		"notsupersetequal",
		"nottilde",
		"nottildeequal",
		"nottildefullequal",
		"nottildetilde",
		"notverticalbar"
	],
	p: [
		"npar",
		"nparallel",
		"nparsl",
		"npart",
		"npolint",
		"npr",
		"nprcue",
		"npre",
		"nprec",
		"npreceq"
	],
	r: [
		"nrarr",
		"nrarrc",
		"nrarrw",
		"nrightarrow",
		"nrtri",
		"nrtrie"
	],
	s: [
		"nsc",
		"nsccue",
		"nsce",
		"nscr",
		"nshortmid",
		"nshortparallel",
		"nsim",
		"nsime",
		"nsimeq",
		"nsmid",
		"nspar",
		"nsqsube",
		"nsqsupe",
		"nsub",
		"nsube",
		"nsubset",
		"nsubseteq",
		"nsubseteqq",
		"nsucc",
		"nsucceq",
		"nsup",
		"nsupe",
		"nsupset",
		"nsupseteq",
		"nsupseteqq"
	],
	t: [
		"ntgl",
		"ntilde",
		"ntlg",
		"ntriangleleft",
		"ntrianglelefteq",
		"ntriangleright",
		"ntrianglerighteq"
	],
	u: [
		"nu",
		"num",
		"numero",
		"numsp"
	],
	v: [
		"nvap",
		"nvdash",
		"nvge",
		"nvgt",
		"nvharr",
		"nvinfin",
		"nvlarr",
		"nvle",
		"nvlt",
		"nvltrie",
		"nvrarr",
		"nvrtrie",
		"nvsim"
	],
	w: [
		"nwarhk",
		"nwarr",
		"nwarrow",
		"nwnear"
	]
};
var o$2 = {
	a: [
		"oacute",
		"oast"
	],
	c: [
		"ocir",
		"ocirc",
		"ocy"
	],
	d: [
		"odash",
		"odblac",
		"odiv",
		"odot",
		"odsold"
	],
	e: [
		"oelig"
	],
	f: [
		"ofcir",
		"ofr"
	],
	g: [
		"ogon",
		"ograve",
		"ogt"
	],
	h: [
		"ohbar",
		"ohm"
	],
	i: [
		"oint"
	],
	l: [
		"olarr",
		"olcir",
		"olcross",
		"oline",
		"olt"
	],
	m: [
		"omacr",
		"omega",
		"omicron",
		"omid",
		"ominus"
	],
	o: [
		"oopf"
	],
	p: [
		"opar",
		"opencurlydoublequote",
		"opencurlyquote",
		"operp",
		"oplus"
	],
	r: [
		"or",
		"orarr",
		"ord",
		"order",
		"orderof",
		"ordf",
		"ordm",
		"origof",
		"oror",
		"orslope",
		"orv"
	],
	s: [
		"os",
		"oscr",
		"oslash",
		"osol"
	],
	t: [
		"otilde",
		"otimes",
		"otimesas"
	],
	u: [
		"ouml"
	],
	v: [
		"ovbar",
		"overbar",
		"overbrace",
		"overbracket",
		"overparenthesis"
	]
};
var p$2 = {
	a: [
		"par",
		"para",
		"parallel",
		"parsim",
		"parsl",
		"part",
		"partiald"
	],
	c: [
		"pcy"
	],
	e: [
		"percnt",
		"period",
		"permil",
		"perp",
		"pertenk"
	],
	f: [
		"pfr"
	],
	h: [
		"phi",
		"phiv",
		"phmmat",
		"phone"
	],
	i: [
		"pi",
		"pitchfork",
		"piv"
	],
	l: [
		"planck",
		"planckh",
		"plankv",
		"plus",
		"plusacir",
		"plusb",
		"pluscir",
		"plusdo",
		"plusdu",
		"pluse",
		"plusminus",
		"plusmn",
		"plussim",
		"plustwo"
	],
	m: [
		"pm"
	],
	o: [
		"poincareplane",
		"pointint",
		"popf",
		"pound"
	],
	r: [
		"pr",
		"prap",
		"prcue",
		"pre",
		"prec",
		"precapprox",
		"preccurlyeq",
		"precedes",
		"precedesequal",
		"precedesslantequal",
		"precedestilde",
		"preceq",
		"precnapprox",
		"precneqq",
		"precnsim",
		"precsim",
		"prime",
		"primes",
		"prnap",
		"prne",
		"prnsim",
		"prod",
		"product",
		"profalar",
		"profline",
		"profsurf",
		"prop",
		"proportion",
		"proportional",
		"propto",
		"prsim",
		"prurel"
	],
	s: [
		"pscr",
		"psi"
	],
	u: [
		"puncsp"
	]
};
var q$2 = {
	f: [
		"qfr"
	],
	i: [
		"qint"
	],
	o: [
		"qopf"
	],
	p: [
		"qprime"
	],
	s: [
		"qscr"
	],
	u: [
		"quaternions",
		"quatint",
		"quest",
		"questeq",
		"quot"
	]
};
var r$2 = {
	a: [
		"raarr",
		"race",
		"racute",
		"radic",
		"raemptyv",
		"rang",
		"rangd",
		"range",
		"rangle",
		"raquo",
		"rarr",
		"rarrap",
		"rarrb",
		"rarrbfs",
		"rarrc",
		"rarrfs",
		"rarrhk",
		"rarrlp",
		"rarrpl",
		"rarrsim",
		"rarrtl",
		"rarrw",
		"ratail",
		"ratio",
		"rationals"
	],
	b: [
		"rbarr",
		"rbbrk",
		"rbrace",
		"rbrack",
		"rbrke",
		"rbrksld",
		"rbrkslu"
	],
	c: [
		"rcaron",
		"rcedil",
		"rceil",
		"rcub",
		"rcy"
	],
	d: [
		"rdca",
		"rdldhar",
		"rdquo",
		"rdquor",
		"rdsh"
	],
	e: [
		"re",
		"real",
		"realine",
		"realpart",
		"reals",
		"rect",
		"reg",
		"reverseelement",
		"reverseequilibrium",
		"reverseupequilibrium"
	],
	f: [
		"rfisht",
		"rfloor",
		"rfr"
	],
	h: [
		"rhar",
		"rhard",
		"rharu",
		"rharul",
		"rho",
		"rhov"
	],
	i: [
		"rightanglebracket",
		"rightarrow",
		"rightarrowbar",
		"rightarrowleftarrow",
		"rightarrowtail",
		"rightceiling",
		"rightdoublebracket",
		"rightdownteevector",
		"rightdownvector",
		"rightdownvectorbar",
		"rightfloor",
		"rightharpoondown",
		"rightharpoonup",
		"rightleftarrows",
		"rightleftharpoons",
		"rightrightarrows",
		"rightsquigarrow",
		"righttee",
		"rightteearrow",
		"rightteevector",
		"rightthreetimes",
		"righttriangle",
		"righttrianglebar",
		"righttriangleequal",
		"rightupdownvector",
		"rightupteevector",
		"rightupvector",
		"rightupvectorbar",
		"rightvector",
		"rightvectorbar",
		"ring",
		"risingdotseq"
	],
	l: [
		"rlarr",
		"rlhar",
		"rlm"
	],
	m: [
		"rmoust",
		"rmoustache"
	],
	n: [
		"rnmid"
	],
	o: [
		"roang",
		"roarr",
		"robrk",
		"ropar",
		"ropf",
		"roplus",
		"rotimes",
		"roundimplies"
	],
	p: [
		"rpar",
		"rpargt",
		"rppolint"
	],
	r: [
		"rrarr",
		"rrightarrow"
	],
	s: [
		"rsaquo",
		"rscr",
		"rsh",
		"rsqb",
		"rsquo",
		"rsquor"
	],
	t: [
		"rthree",
		"rtimes",
		"rtri",
		"rtrie",
		"rtrif",
		"rtriltri"
	],
	u: [
		"ruledelayed",
		"ruluhar"
	],
	x: [
		"rx"
	]
};
var s$2 = {
	a: [
		"sacute"
	],
	b: [
		"sbquo"
	],
	c: [
		"sc",
		"scap",
		"scaron",
		"sccue",
		"sce",
		"scedil",
		"scirc",
		"scnap",
		"scne",
		"scnsim",
		"scpolint",
		"scsim",
		"scy"
	],
	d: [
		"sdot",
		"sdotb",
		"sdote"
	],
	e: [
		"searhk",
		"searr",
		"searrow",
		"sect",
		"semi",
		"seswar",
		"setminus",
		"setmn",
		"sext"
	],
	f: [
		"sfr",
		"sfrown"
	],
	h: [
		"sharp",
		"shchcy",
		"shcy",
		"shortdownarrow",
		"shortleftarrow",
		"shortmid",
		"shortparallel",
		"shortrightarrow",
		"shortuparrow",
		"shy"
	],
	i: [
		"sigma",
		"sigmaf",
		"sigmav",
		"sim",
		"simdot",
		"sime",
		"simeq",
		"simg",
		"simge",
		"siml",
		"simle",
		"simne",
		"simplus",
		"simrarr"
	],
	l: [
		"slarr"
	],
	m: [
		"smallcircle",
		"smallsetminus",
		"smashp",
		"smeparsl",
		"smid",
		"smile",
		"smt",
		"smte",
		"smtes"
	],
	o: [
		"softcy",
		"sol",
		"solb",
		"solbar",
		"sopf"
	],
	p: [
		"spades",
		"spadesuit",
		"spar"
	],
	q: [
		"sqcap",
		"sqcaps",
		"sqcup",
		"sqcups",
		"sqrt",
		"sqsub",
		"sqsube",
		"sqsubset",
		"sqsubseteq",
		"sqsup",
		"sqsupe",
		"sqsupset",
		"sqsupseteq",
		"squ",
		"square",
		"squareintersection",
		"squaresubset",
		"squaresubsetequal",
		"squaresuperset",
		"squaresupersetequal",
		"squareunion",
		"squarf",
		"squf"
	],
	r: [
		"srarr"
	],
	s: [
		"sscr",
		"ssetmn",
		"ssmile",
		"sstarf"
	],
	t: [
		"star",
		"starf",
		"straightepsilon",
		"straightphi",
		"strns"
	],
	u: [
		"sub",
		"subdot",
		"sube",
		"subedot",
		"submult",
		"subne",
		"subplus",
		"subrarr",
		"subset",
		"subseteq",
		"subseteqq",
		"subsetequal",
		"subsetneq",
		"subsetneqq",
		"subsim",
		"subsub",
		"subsup",
		"succ",
		"succapprox",
		"succcurlyeq",
		"succeeds",
		"succeedsequal",
		"succeedsslantequal",
		"succeedstilde",
		"succeq",
		"succnapprox",
		"succneqq",
		"succnsim",
		"succsim",
		"suchthat",
		"sum",
		"sung",
		"sup",
		"sup1",
		"sup2",
		"sup3",
		"supdot",
		"supdsub",
		"supe",
		"supedot",
		"superset",
		"supersetequal",
		"suphsol",
		"suphsub",
		"suplarr",
		"supmult",
		"supne",
		"supplus",
		"supset",
		"supseteq",
		"supseteqq",
		"supsetneq",
		"supsetneqq",
		"supsim",
		"supsub",
		"supsup"
	],
	w: [
		"swarhk",
		"swarr",
		"swarrow",
		"swnwar"
	],
	z: [
		"szlig"
	]
};
var t$2 = {
	a: [
		"tab",
		"target",
		"tau"
	],
	b: [
		"tbrk"
	],
	c: [
		"tcaron",
		"tcedil",
		"tcy"
	],
	d: [
		"tdot"
	],
	e: [
		"telrec"
	],
	f: [
		"tfr"
	],
	h: [
		"there4",
		"therefore",
		"theta",
		"thetasym",
		"thetav",
		"thickapprox",
		"thicksim",
		"thickspace",
		"thinsp",
		"thinspace",
		"thkap",
		"thksim",
		"thorn"
	],
	i: [
		"tilde",
		"tildeequal",
		"tildefullequal",
		"tildetilde",
		"times",
		"timesb",
		"timesbar",
		"timesd",
		"tint"
	],
	o: [
		"toea",
		"top",
		"topbot",
		"topcir",
		"topf",
		"topfork",
		"tosa"
	],
	p: [
		"tprime"
	],
	r: [
		"trade",
		"triangle",
		"triangledown",
		"triangleleft",
		"trianglelefteq",
		"triangleq",
		"triangleright",
		"trianglerighteq",
		"tridot",
		"trie",
		"triminus",
		"tripledot",
		"triplus",
		"trisb",
		"tritime",
		"trpezium"
	],
	s: [
		"tscr",
		"tscy",
		"tshcy",
		"tstrok"
	],
	w: [
		"twixt",
		"twoheadleftarrow",
		"twoheadrightarrow"
	]
};
var u$2 = {
	a: [
		"uacute",
		"uarr",
		"uarrocir"
	],
	b: [
		"ubrcy",
		"ubreve"
	],
	c: [
		"ucirc",
		"ucy"
	],
	d: [
		"udarr",
		"udblac",
		"udhar"
	],
	f: [
		"ufisht",
		"ufr"
	],
	g: [
		"ugrave"
	],
	h: [
		"uhar",
		"uharl",
		"uharr",
		"uhblk"
	],
	l: [
		"ulcorn",
		"ulcorner",
		"ulcrop",
		"ultri"
	],
	m: [
		"umacr",
		"uml"
	],
	n: [
		"underbar",
		"underbrace",
		"underbracket",
		"underparenthesis",
		"union",
		"unionplus"
	],
	o: [
		"uogon",
		"uopf"
	],
	p: [
		"uparrow",
		"uparrowbar",
		"uparrowdownarrow",
		"updownarrow",
		"upequilibrium",
		"upharpoonleft",
		"upharpoonright",
		"uplus",
		"upperleftarrow",
		"upperrightarrow",
		"upsi",
		"upsih",
		"upsilon",
		"uptee",
		"upteearrow",
		"upuparrows"
	],
	r: [
		"urcorn",
		"urcorner",
		"urcrop",
		"uring",
		"urtri"
	],
	s: [
		"uscr"
	],
	t: [
		"utdot",
		"utilde",
		"utri",
		"utrif"
	],
	u: [
		"uuarr",
		"uuml"
	],
	w: [
		"uwangle"
	]
};
var v$2 = {
	a: [
		"vangrt",
		"varepsilon",
		"varkappa",
		"varnothing",
		"varphi",
		"varpi",
		"varpropto",
		"varr",
		"varrho",
		"varsigma",
		"varsubsetneq",
		"varsubsetneqq",
		"varsupsetneq",
		"varsupsetneqq",
		"vartheta",
		"vartriangleleft",
		"vartriangleright"
	],
	b: [
		"vbar",
		"vbarv"
	],
	c: [
		"vcy"
	],
	d: [
		"vdash",
		"vdashl"
	],
	e: [
		"vee",
		"veebar",
		"veeeq",
		"vellip",
		"verbar",
		"vert",
		"verticalbar",
		"verticalline",
		"verticalseparator",
		"verticaltilde",
		"verythinspace"
	],
	f: [
		"vfr"
	],
	l: [
		"vltri"
	],
	n: [
		"vnsub",
		"vnsup"
	],
	o: [
		"vopf"
	],
	p: [
		"vprop"
	],
	r: [
		"vrtri"
	],
	s: [
		"vscr",
		"vsubne",
		"vsupne"
	],
	v: [
		"vvdash"
	],
	z: [
		"vzigzag"
	]
};
var w$2 = {
	c: [
		"wcirc"
	],
	e: [
		"wedbar",
		"wedge",
		"wedgeq",
		"weierp"
	],
	f: [
		"wfr"
	],
	o: [
		"wopf"
	],
	p: [
		"wp"
	],
	r: [
		"wr",
		"wreath"
	],
	s: [
		"wscr"
	]
};
var x$2 = {
	c: [
		"xcap",
		"xcirc",
		"xcup"
	],
	d: [
		"xdtri"
	],
	f: [
		"xfr"
	],
	h: [
		"xharr"
	],
	i: [
		"xi"
	],
	l: [
		"xlarr"
	],
	m: [
		"xmap"
	],
	n: [
		"xnis"
	],
	o: [
		"xodot",
		"xopf",
		"xoplus",
		"xotime"
	],
	r: [
		"xrarr"
	],
	s: [
		"xscr",
		"xsqcup"
	],
	u: [
		"xuplus",
		"xutri"
	],
	v: [
		"xvee"
	],
	w: [
		"xwedge"
	]
};
var y$2 = {
	a: [
		"yacute",
		"yacy"
	],
	c: [
		"ycirc",
		"ycy"
	],
	e: [
		"yen"
	],
	f: [
		"yfr"
	],
	i: [
		"yicy"
	],
	o: [
		"yopf"
	],
	s: [
		"yscr"
	],
	u: [
		"yucy",
		"yuml"
	]
};
var z$2 = {
	a: [
		"zacute"
	],
	c: [
		"zcaron",
		"zcy"
	],
	d: [
		"zdot"
	],
	e: [
		"zeetrf",
		"zerowidthspace",
		"zeta"
	],
	f: [
		"zfr"
	],
	h: [
		"zhcy"
	],
	i: [
		"zigrarr"
	],
	o: [
		"zopf"
	],
	s: [
		"zscr"
	],
	w: [
		"zwj",
		"zwnj"
	]
};
var startsWithCaseInsensitive = {
	a: a$2,
	b: b$2,
	c: c$2,
	d: d$2,
	e: e$2,
	f: f$2,
	g: g$2,
	h: h$2,
	i: i$2,
	j: j$2,
	k: k$2,
	l: l$2,
	m: m$2,
	n: n$2,
	o: o$2,
	p: p$2,
	q: q$2,
	r: r$2,
	s: s$2,
	t: t$2,
	u: u$2,
	v: v$2,
	w: w$2,
	x: x$2,
	y: y$2,
	z: z$2
};

var e$3 = {
	t: [
		"aacute",
		"acute",
		"cacute",
		"closecurlydoublequote",
		"closecurlyquote",
		"diacriticalacute",
		"diacriticaldoubleacute",
		"eacute",
		"gacute",
		"iacute",
		"lacute",
		"late",
		"nacute",
		"oacute",
		"opencurlydoublequote",
		"opencurlyquote",
		"racute",
		"sacute",
		"sdote",
		"smte",
		"uacute",
		"yacute",
		"zacute"
	],
	v: [
		"abreve",
		"agrave",
		"breve",
		"diacriticalgrave",
		"downbreve",
		"egrave",
		"gbreve",
		"grave",
		"igrave",
		"ograve",
		"ubreve",
		"ugrave"
	],
	c: [
		"ace",
		"hilbertspace",
		"lbrace",
		"mediumspace",
		"negativemediumspace",
		"negativethickspace",
		"negativethinspace",
		"negativeverythinspace",
		"nonbreakingspace",
		"nsce",
		"overbrace",
		"race",
		"rbrace",
		"sce",
		"thickspace",
		"thinspace",
		"underbrace",
		"verythinspace",
		"zerowidthspace"
	],
	p: [
		"andslope",
		"ape",
		"bumpe",
		"csupe",
		"nape",
		"nbumpe",
		"nsqsupe",
		"nsupe",
		"orslope",
		"sqsupe",
		"supe"
	],
	g: [
		"ange",
		"barwedge",
		"bigwedge",
		"blacklozenge",
		"curlywedge",
		"doublebarwedge",
		"ge",
		"image",
		"lge",
		"lozenge",
		"nge",
		"nvge",
		"range",
		"simge",
		"wedge",
		"xwedge"
	],
	l: [
		"angle",
		"blacktriangle",
		"dwangle",
		"exponentiale",
		"female",
		"gle",
		"langle",
		"le",
		"lefttriangle",
		"male",
		"measuredangle",
		"nle",
		"notlefttriangle",
		"notrighttriangle",
		"nvle",
		"rangle",
		"righttriangle",
		"simle",
		"smallcircle",
		"smile",
		"ssmile",
		"triangle",
		"uwangle"
	],
	a: [
		"angmsdae"
	],
	d: [
		"atilde",
		"diacriticaltilde",
		"divide",
		"equaltilde",
		"greatertilde",
		"itilde",
		"lesstilde",
		"notequaltilde",
		"notgreatertilde",
		"notlesstilde",
		"notsucceedstilde",
		"nottilde",
		"nottildetilde",
		"ntilde",
		"otilde",
		"precedestilde",
		"succeedstilde",
		"tilde",
		"tildetilde",
		"trade",
		"utilde",
		"verticaltilde"
	],
	m: [
		"backprime",
		"bprime",
		"bsime",
		"gsime",
		"lsime",
		"nsime",
		"prime",
		"qprime",
		"sime",
		"tprime",
		"tritime",
		"xotime"
	],
	e: [
		"barvee",
		"bigvee",
		"curlyvee",
		"cuvee",
		"doublelefttee",
		"doublerighttee",
		"downtee",
		"ee",
		"lefttee",
		"lthree",
		"righttee",
		"rthree",
		"uptee",
		"vee",
		"xvee"
	],
	s: [
		"because",
		"maltese",
		"pluse"
	],
	r: [
		"blacksquare",
		"cire",
		"dotsquare",
		"emptysmallsquare",
		"emptyverysmallsquare",
		"filledsmallsquare",
		"filledverysmallsquare",
		"incare",
		"npre",
		"pre",
		"re",
		"square",
		"therefore"
	],
	n: [
		"bne",
		"colone",
		"gne",
		"gvne",
		"horizontalline",
		"imagline",
		"isine",
		"lne",
		"lvne",
		"ne",
		"newline",
		"notine",
		"oline",
		"phone",
		"poincareplane",
		"prne",
		"profline",
		"realine",
		"scne",
		"simne",
		"subne",
		"supne",
		"verticalline",
		"vsubne",
		"vsupne"
	],
	i: [
		"bowtie",
		"die",
		"infintie",
		"ltrie",
		"nltrie",
		"nrtrie",
		"nvltrie",
		"nvrtrie",
		"rtrie",
		"trie"
	],
	b: [
		"csube",
		"nsqsube",
		"nsube",
		"sqsube",
		"sube"
	],
	k: [
		"lbrke",
		"rbrke"
	],
	h: [
		"lmoustache",
		"rmoustache"
	],
	u: [
		"nprcue",
		"nsccue",
		"prcue",
		"sccue"
	]
};
var c$3 = {
	a: [
		"ac",
		"angmsdac",
		"dblac",
		"odblac",
		"udblac"
	],
	r: [
		"acirc",
		"bigcirc",
		"ccirc",
		"circ",
		"circledcirc",
		"ecirc",
		"eqcirc",
		"gcirc",
		"hcirc",
		"icirc",
		"jcirc",
		"nrarrc",
		"ocirc",
		"rarrc",
		"scirc",
		"ucirc",
		"wcirc",
		"xcirc",
		"ycirc"
	],
	s: [
		"cuesc",
		"nsc",
		"sc"
	],
	e: [
		"curlyeqprec",
		"nprec",
		"prec",
		"telrec"
	],
	c: [
		"curlyeqsucc",
		"gescc",
		"gtcc",
		"lescc",
		"ltcc",
		"nsucc",
		"succ"
	],
	i: [
		"ic",
		"radic"
	],
	v: [
		"notinvc",
		"notnivc"
	]
};
var d$3 = {
	c: [
		"acd"
	],
	n: [
		"and",
		"andand",
		"capand",
		"diamond",
		"pound"
	],
	d: [
		"andd",
		"dd",
		"equivdd"
	],
	s: [
		"angmsd",
		"minusd",
		"nisd",
		"timesd"
	],
	a: [
		"angmsdad",
		"gammad"
	],
	b: [
		"angrtvbd"
	],
	i: [
		"apid",
		"cirmid",
		"mid",
		"napid",
		"nmid",
		"nshortmid",
		"nsmid",
		"omid",
		"rnmid",
		"shortmid",
		"smid"
	],
	e: [
		"barwed",
		"cuwed",
		"imped",
		"ruledelayed"
	],
	h: [
		"boxhd",
		"ddotrahd"
	],
	l: [
		"capitaldifferentiald",
		"differentiald",
		"lbrksld",
		"odsold",
		"partiald",
		"rbrksld"
	],
	o: [
		"coprod",
		"intprod",
		"iprod",
		"period",
		"prod"
	],
	g: [
		"langd",
		"rangd"
	],
	r: [
		"lhard",
		"llhard",
		"lrhard",
		"ord",
		"rhard"
	]
};
var y$3 = {
	c: [
		"acy",
		"bcy",
		"chcy",
		"dcy",
		"djcy",
		"dscy",
		"dzcy",
		"ecy",
		"fcy",
		"gcy",
		"gjcy",
		"hardcy",
		"icy",
		"iecy",
		"iocy",
		"iukcy",
		"jcy",
		"jsercy",
		"jukcy",
		"kcy",
		"khcy",
		"kjcy",
		"lcy",
		"ljcy",
		"mcy",
		"ncy",
		"njcy",
		"ocy",
		"pcy",
		"rcy",
		"scy",
		"shchcy",
		"shcy",
		"softcy",
		"tcy",
		"tscy",
		"tshcy",
		"ubrcy",
		"ucy",
		"vcy",
		"yacy",
		"ycy",
		"yicy",
		"yucy",
		"zcy",
		"zhcy"
	],
	p: [
		"copy"
	],
	t: [
		"cylcty",
		"empty"
	],
	h: [
		"shy"
	]
};
var g$3 = {
	i: [
		"aelig",
		"ffilig",
		"fflig",
		"ffllig",
		"filig",
		"fjlig",
		"fllig",
		"ijlig",
		"oelig",
		"szlig"
	],
	l: [
		"amalg",
		"lg",
		"ntlg"
	],
	n: [
		"ang",
		"aring",
		"backcong",
		"bcong",
		"cong",
		"eng",
		"lang",
		"leftceiling",
		"loang",
		"nang",
		"ncong",
		"rang",
		"rightceiling",
		"ring",
		"roang",
		"sung",
		"uring",
		"varnothing"
	],
	a: [
		"angmsdag",
		"vzigzag"
	],
	e: [
		"deg",
		"eg",
		"leg",
		"reg"
	],
	g: [
		"gg",
		"ggg",
		"ngg"
	],
	s: [
		"lesg"
	],
	m: [
		"lsimg",
		"simg"
	]
};
var f$3 = {
	a: [
		"af",
		"angmsdaf",
		"sigmaf"
	],
	p: [
		"aopf",
		"bopf",
		"copf",
		"dopf",
		"eopf",
		"fopf",
		"gopf",
		"hopf",
		"iopf",
		"jopf",
		"kopf",
		"lopf",
		"mopf",
		"nopf",
		"oopf",
		"popf",
		"qopf",
		"ropf",
		"sopf",
		"topf",
		"uopf",
		"vopf",
		"wopf",
		"xopf",
		"yopf",
		"zopf"
	],
	i: [
		"dtrif",
		"ltrif",
		"rtrif",
		"utrif"
	],
	o: [
		"fnof",
		"imof",
		"orderof",
		"origof"
	],
	r: [
		"fouriertrf",
		"laplacetrf",
		"mellintrf",
		"profsurf",
		"squarf",
		"sstarf",
		"starf",
		"zeetrf"
	],
	l: [
		"half"
	],
	f: [
		"iff"
	],
	z: [
		"lozf"
	],
	d: [
		"ordf"
	],
	u: [
		"squf"
	]
};
var r$3 = {
	f: [
		"afr",
		"bfr",
		"cfr",
		"dfr",
		"efr",
		"ffr",
		"gfr",
		"hfr",
		"ifr",
		"jfr",
		"kfr",
		"lfr",
		"mfr",
		"nfr",
		"ofr",
		"pfr",
		"qfr",
		"rfr",
		"sfr",
		"tfr",
		"ufr",
		"vfr",
		"wfr",
		"xfr",
		"yfr",
		"zfr"
	],
	c: [
		"amacr",
		"ascr",
		"bscr",
		"cscr",
		"dscr",
		"emacr",
		"escr",
		"fscr",
		"gscr",
		"hscr",
		"imacr",
		"iscr",
		"jscr",
		"kscr",
		"lscr",
		"macr",
		"mscr",
		"nscr",
		"omacr",
		"oscr",
		"pscr",
		"qscr",
		"rscr",
		"sscr",
		"tscr",
		"umacr",
		"uscr",
		"vscr",
		"wscr",
		"xscr",
		"yscr",
		"zscr"
	],
	r: [
		"angzarr",
		"crarr",
		"cudarrr",
		"cularr",
		"curarr",
		"darr",
		"ddarr",
		"dharr",
		"duarr",
		"dzigrarr",
		"erarr",
		"gtrarr",
		"harr",
		"hoarr",
		"laarr",
		"larr",
		"lbarr",
		"llarr",
		"loarr",
		"lrarr",
		"ltlarr",
		"nearr",
		"nharr",
		"nlarr",
		"nrarr",
		"nvharr",
		"nvlarr",
		"nvrarr",
		"nwarr",
		"olarr",
		"orarr",
		"raarr",
		"rarr",
		"rbarr",
		"rlarr",
		"roarr",
		"rrarr",
		"searr",
		"simrarr",
		"slarr",
		"srarr",
		"subrarr",
		"suplarr",
		"swarr",
		"uarr",
		"udarr",
		"uharr",
		"uuarr",
		"varr",
		"xharr",
		"xlarr",
		"xrarr",
		"zigrarr"
	],
	i: [
		"apacir",
		"cir",
		"cirscir",
		"ecir",
		"gtcir",
		"harrcir",
		"ltcir",
		"midcir",
		"ocir",
		"ofcir",
		"olcir",
		"plusacir",
		"pluscir",
		"topcir",
		"uarrocir"
	],
	a: [
		"bigstar",
		"brvbar",
		"dhar",
		"dollar",
		"doubleverticalbar",
		"downarrowbar",
		"downleftvectorbar",
		"downrightvectorbar",
		"duhar",
		"epar",
		"gtlpar",
		"hbar",
		"horbar",
		"ldrdhar",
		"ldrushar",
		"leftarrowbar",
		"leftdownvectorbar",
		"lefttrianglebar",
		"leftupvectorbar",
		"leftvectorbar",
		"lhar",
		"lopar",
		"lowbar",
		"lpar",
		"lrhar",
		"ltrpar",
		"lurdshar",
		"luruhar",
		"nesear",
		"nhpar",
		"notdoubleverticalbar",
		"notlefttrianglebar",
		"notrighttrianglebar",
		"notverticalbar",
		"npar",
		"nspar",
		"nwnear",
		"ohbar",
		"opar",
		"ovbar",
		"overbar",
		"par",
		"profalar",
		"rdldhar",
		"rhar",
		"rightarrowbar",
		"rightdownvectorbar",
		"righttrianglebar",
		"rightupvectorbar",
		"rightvectorbar",
		"rlhar",
		"ropar",
		"rpar",
		"ruluhar",
		"seswar",
		"solbar",
		"spar",
		"star",
		"swnwar",
		"timesbar",
		"udhar",
		"uhar",
		"underbar",
		"uparrowbar",
		"vbar",
		"veebar",
		"verbar",
		"verticalbar",
		"wedbar"
	],
	d: [
		"boxdr",
		"circledr",
		"mldr",
		"nldr"
	],
	u: [
		"boxur",
		"natur"
	],
	v: [
		"boxvr"
	],
	s: [
		"copysr"
	],
	p: [
		"cuepr",
		"npr",
		"pr"
	],
	o: [
		"cupor",
		"downleftrightvector",
		"downleftteevector",
		"downleftvector",
		"downrightteevector",
		"downrightvector",
		"ldquor",
		"leftdownteevector",
		"leftdownvector",
		"leftfloor",
		"leftrightvector",
		"leftteevector",
		"leftupdownvector",
		"leftupteevector",
		"leftupvector",
		"leftvector",
		"lesdotor",
		"lfloor",
		"lsquor",
		"or",
		"oror",
		"rdquor",
		"rfloor",
		"rightdownteevector",
		"rightdownvector",
		"rightfloor",
		"rightteevector",
		"rightupdownvector",
		"rightupteevector",
		"rightupvector",
		"rightvector",
		"rsquor",
		"verticalseparator"
	],
	e: [
		"dagger",
		"ddagger",
		"easter",
		"greatergreater",
		"lessequalgreater",
		"lessgreater",
		"llcorner",
		"lrcorner",
		"marker",
		"nestedgreatergreater",
		"notgreater",
		"notgreatergreater",
		"notlessgreater",
		"notnestedgreatergreater",
		"order",
		"ulcorner",
		"urcorner"
	],
	t: [
		"eqslantgtr",
		"lesseqgtr",
		"lesseqqgtr",
		"lessgtr",
		"ngtr"
	],
	w: [
		"wr"
	]
};
var m$3 = {
	y: [
		"alefsym",
		"thetasym"
	],
	i: [
		"backsim",
		"bsim",
		"eqsim",
		"esim",
		"gnsim",
		"gsim",
		"gtrsim",
		"im",
		"larrsim",
		"lesssim",
		"lnsim",
		"lsim",
		"nesim",
		"ngsim",
		"nlsim",
		"nsim",
		"nvsim",
		"parsim",
		"plussim",
		"precnsim",
		"precsim",
		"prnsim",
		"prsim",
		"rarrsim",
		"scnsim",
		"scsim",
		"sim",
		"subsim",
		"succnsim",
		"succsim",
		"supsim",
		"thicksim",
		"thksim"
	],
	o: [
		"bottom"
	],
	s: [
		"ccupssm"
	],
	r: [
		"curarrm",
		"lrm"
	],
	a: [
		"diam"
	],
	u: [
		"equilibrium",
		"num",
		"reverseequilibrium",
		"reverseupequilibrium",
		"sum",
		"trpezium",
		"upequilibrium"
	],
	h: [
		"ohm"
	],
	d: [
		"ordm"
	],
	p: [
		"pm"
	],
	l: [
		"rlm"
	]
};
var h$3 = {
	p: [
		"aleph",
		"angsph"
	],
	a: [
		"angmsdah"
	],
	s: [
		"backslash",
		"circleddash",
		"dash",
		"hslash",
		"ldsh",
		"lsh",
		"mdash",
		"ndash",
		"nvdash",
		"odash",
		"oslash",
		"rdsh",
		"rsh",
		"vdash",
		"vvdash"
	],
	t: [
		"beth",
		"daleth",
		"eth",
		"imath",
		"jmath",
		"wreath"
	],
	x: [
		"boxh"
	],
	v: [
		"boxvh"
	],
	k: [
		"planckh"
	],
	i: [
		"upsih"
	]
};
var a$3 = {
	h: [
		"alpha"
	],
	a: [
		"angmsdaa"
	],
	t: [
		"beta",
		"delta",
		"eta",
		"iiota",
		"iota",
		"theta",
		"vartheta",
		"zeta"
	],
	l: [
		"cedilla",
		"gla",
		"nabla"
	],
	m: [
		"comma",
		"digamma",
		"gamma",
		"invisiblecomma",
		"mcomma",
		"sigma",
		"varsigma"
	],
	p: [
		"kappa",
		"varkappa"
	],
	d: [
		"lambda"
	],
	c: [
		"ldca",
		"rdca"
	],
	v: [
		"notinva",
		"notniva"
	],
	g: [
		"omega"
	],
	r: [
		"para"
	],
	e: [
		"toea"
	],
	s: [
		"tosa"
	]
};
var p$3 = {
	m: [
		"amp",
		"asymp",
		"bump",
		"comp",
		"humpdownhump",
		"mp",
		"nbump",
		"nothumpdownhump"
	],
	a: [
		"ap",
		"bigcap",
		"cap",
		"capcap",
		"cupbrcap",
		"cupcap",
		"gap",
		"gnap",
		"lap",
		"lnap",
		"map",
		"multimap",
		"mumap",
		"nap",
		"ncap",
		"notcupcap",
		"nvap",
		"prap",
		"prnap",
		"rarrap",
		"scap",
		"scnap",
		"sqcap",
		"thkap",
		"xcap",
		"xmap"
	],
	u: [
		"bigcup",
		"bigsqcup",
		"bigtriangleup",
		"capbrcup",
		"capcup",
		"csup",
		"cup",
		"cupcup",
		"leftharpoonup",
		"mapstoup",
		"ncup",
		"nsup",
		"rightharpoonup",
		"sqcup",
		"sqsup",
		"subsup",
		"sup",
		"supsup",
		"vnsup",
		"xcup",
		"xsqcup"
	],
	r: [
		"cularrp",
		"operp",
		"perp",
		"sharp",
		"weierp"
	],
	o: [
		"dlcrop",
		"drcrop",
		"prop",
		"top",
		"ulcrop",
		"urcrop",
		"vprop"
	],
	s: [
		"emsp",
		"ensp",
		"hairsp",
		"nbsp",
		"numsp",
		"puncsp",
		"thinsp"
	],
	i: [
		"hellip",
		"vellip"
	],
	l: [
		"larrlp",
		"rarrlp"
	],
	c: [
		"mlcp"
	],
	h: [
		"smashp"
	],
	w: [
		"wp"
	]
};
var v$3 = {
	d: [
		"andv"
	],
	r: [
		"barv",
		"orv",
		"vbarv"
	],
	y: [
		"bemptyv",
		"cemptyv",
		"demptyv",
		"emptyv",
		"laemptyv",
		"raemptyv"
	],
	i: [
		"bnequiv",
		"div",
		"epsiv",
		"equiv",
		"nequiv",
		"niv",
		"odiv",
		"phiv",
		"piv"
	],
	x: [
		"boxv"
	],
	h: [
		"dashv"
	],
	k: [
		"forkv",
		"plankv"
	],
	s: [
		"isinsv"
	],
	n: [
		"isinv"
	],
	a: [
		"kappav",
		"sigmav",
		"thetav"
	],
	t: [
		"ngtv",
		"nltv"
	],
	o: [
		"rhov"
	]
};
var b$3 = {
	a: [
		"angmsdab",
		"tab"
	],
	v: [
		"angrtvb",
		"notinvb",
		"notnivb"
	],
	l: [
		"bsolb",
		"solb"
	],
	u: [
		"bsolhsub",
		"csub",
		"lcub",
		"nsub",
		"rcub",
		"sqsub",
		"sub",
		"subsub",
		"supdsub",
		"suphsub",
		"supsub",
		"vnsub"
	],
	r: [
		"larrb",
		"rarrb"
	],
	q: [
		"lsqb",
		"rsqb"
	],
	s: [
		"minusb",
		"plusb",
		"timesb",
		"trisb"
	],
	t: [
		"sdotb"
	]
};
var t$3 = {
	r: [
		"angrt",
		"imagpart",
		"npart",
		"part",
		"realpart",
		"sqrt",
		"vangrt",
		"vert"
	],
	s: [
		"angst",
		"ast",
		"circledast",
		"equest",
		"exist",
		"gtquest",
		"iquest",
		"lmoust",
		"lowast",
		"ltquest",
		"midast",
		"nexist",
		"oast",
		"quest",
		"rmoust"
	],
	n: [
		"awconint",
		"awint",
		"cconint",
		"cent",
		"cirfnint",
		"complement",
		"congruent",
		"conint",
		"cwconint",
		"cwint",
		"element",
		"fpartint",
		"geqslant",
		"iiiint",
		"iiint",
		"int",
		"leqslant",
		"ngeqslant",
		"nleqslant",
		"notcongruent",
		"notelement",
		"notreverseelement",
		"npolint",
		"oint",
		"percnt",
		"pointint",
		"qint",
		"quatint",
		"reverseelement",
		"rppolint",
		"scpolint",
		"tint"
	],
	o: [
		"bigodot",
		"bnot",
		"bot",
		"capdot",
		"cdot",
		"centerdot",
		"circledot",
		"congdot",
		"ctdot",
		"cupdot",
		"diacriticaldot",
		"dot",
		"dotdot",
		"doteqdot",
		"doubledot",
		"dtdot",
		"eddot",
		"edot",
		"efdot",
		"egsdot",
		"elsdot",
		"erdot",
		"esdot",
		"gdot",
		"gesdot",
		"gtdot",
		"gtrdot",
		"idot",
		"inodot",
		"isindot",
		"lesdot",
		"lessdot",
		"lmidot",
		"ltdot",
		"mddot",
		"middot",
		"ncongdot",
		"nedot",
		"not",
		"notindot",
		"odot",
		"quot",
		"sdot",
		"simdot",
		"subdot",
		"subedot",
		"supdot",
		"supedot",
		"tdot",
		"topbot",
		"tridot",
		"tripledot",
		"utdot",
		"xodot",
		"zdot"
	],
	f: [
		"blacktriangleleft",
		"circlearrowleft",
		"curvearrowleft",
		"downharpoonleft",
		"looparrowleft",
		"mapstoleft",
		"ntriangleleft",
		"triangleleft",
		"upharpoonleft",
		"vartriangleleft"
	],
	h: [
		"blacktriangleright",
		"circlearrowright",
		"curvearrowright",
		"dfisht",
		"downharpoonright",
		"homtht",
		"lfisht",
		"looparrowright",
		"ntriangleright",
		"rfisht",
		"triangleright",
		"ufisht",
		"upharpoonright",
		"vartriangleright"
	],
	e: [
		"bullet",
		"caret",
		"emptyset",
		"leftanglebracket",
		"leftdoublebracket",
		"notsquaresubset",
		"notsquaresuperset",
		"notsubset",
		"notsuperset",
		"nsubset",
		"nsupset",
		"overbracket",
		"rightanglebracket",
		"rightdoublebracket",
		"sqsubset",
		"sqsupset",
		"squaresubset",
		"squaresuperset",
		"subset",
		"superset",
		"supset",
		"target",
		"underbracket"
	],
	i: [
		"clubsuit",
		"diamondsuit",
		"heartsuit",
		"it",
		"spadesuit"
	],
	a: [
		"commat",
		"flat",
		"hat",
		"lat",
		"phmmat",
		"suchthat"
	],
	c: [
		"coproduct",
		"product",
		"rect",
		"sect"
	],
	g: [
		"gt",
		"ngt",
		"nvgt",
		"ogt",
		"rpargt"
	],
	l: [
		"hamilt",
		"lparlt",
		"lt",
		"malt",
		"nlt",
		"nvlt",
		"olt",
		"submult",
		"supmult"
	],
	x: [
		"sext",
		"twixt"
	],
	m: [
		"smt"
	]
};
var n$3 = {
	o: [
		"aogon",
		"applyfunction",
		"backepsilon",
		"caron",
		"ccaron",
		"colon",
		"dcaron",
		"ecaron",
		"ecolon",
		"eogon",
		"epsilon",
		"eqcolon",
		"expectation",
		"hercon",
		"intersection",
		"iogon",
		"lcaron",
		"ncaron",
		"ogon",
		"omicron",
		"proportion",
		"rcaron",
		"scaron",
		"squareintersection",
		"squareunion",
		"straightepsilon",
		"tcaron",
		"union",
		"uogon",
		"upsilon",
		"varepsilon",
		"zcaron"
	],
	g: [
		"assign"
	],
	e: [
		"between",
		"curren",
		"hyphen",
		"kgreen",
		"yen"
	],
	w: [
		"bigtriangledown",
		"blacktriangledown",
		"frown",
		"leftharpoondown",
		"mapstodown",
		"rightharpoondown",
		"sfrown",
		"triangledown"
	],
	f: [
		"compfn"
	],
	i: [
		"disin",
		"iinfin",
		"in",
		"infin",
		"isin",
		"notin",
		"nvinfin"
	],
	r: [
		"dlcorn",
		"drcorn",
		"thorn",
		"ulcorn",
		"urcorn"
	],
	a: [
		"lagran"
	],
	m: [
		"plusmn",
		"setmn",
		"ssetmn"
	]
};
var s$3 = {
	o: [
		"apos",
		"mstpos",
		"napos",
		"os"
	],
	u: [
		"becaus",
		"bigoplus",
		"biguplus",
		"boxminus",
		"boxplus",
		"circleminus",
		"circleplus",
		"dotminus",
		"dotplus",
		"eplus",
		"loplus",
		"minus",
		"minusplus",
		"mnplus",
		"ominus",
		"oplus",
		"plus",
		"plusminus",
		"roplus",
		"setminus",
		"simplus",
		"smallsetminus",
		"subplus",
		"supplus",
		"triminus",
		"triplus",
		"unionplus",
		"uplus",
		"xoplus",
		"xuplus"
	],
	i: [
		"bernoullis",
		"nis",
		"overparenthesis",
		"underparenthesis",
		"xnis"
	],
	e: [
		"bigotimes",
		"boxtimes",
		"circletimes",
		"complexes",
		"divideontimes",
		"ges",
		"gesles",
		"implies",
		"invisibletimes",
		"lates",
		"leftthreetimes",
		"les",
		"lesges",
		"lotimes",
		"ltimes",
		"nges",
		"nles",
		"notprecedes",
		"otimes",
		"precedes",
		"primes",
		"rightthreetimes",
		"rotimes",
		"roundimplies",
		"rtimes",
		"smtes",
		"spades",
		"times"
	],
	p: [
		"caps",
		"ccaps",
		"ccups",
		"cups",
		"sqcaps",
		"sqcups"
	],
	y: [
		"cayleys"
	],
	d: [
		"circleds",
		"notsucceeds",
		"succeeds"
	],
	b: [
		"clubs"
	],
	s: [
		"cross",
		"eqslantless",
		"greaterequalless",
		"greaterless",
		"gtreqless",
		"gtreqqless",
		"gtrless",
		"lessless",
		"nestedlessless",
		"nless",
		"notgreaterless",
		"notless",
		"notlessless",
		"notnestedlessless",
		"olcross"
	],
	m: [
		"diams"
	],
	w: [
		"downdownarrows",
		"leftleftarrows",
		"leftrightarrows",
		"rightleftarrows",
		"rightrightarrows",
		"upuparrows"
	],
	g: [
		"egs"
	],
	r: [
		"elinters",
		"integers"
	],
	l: [
		"els",
		"equals",
		"models",
		"naturals",
		"rationals",
		"reals"
	],
	t: [
		"exists",
		"hearts",
		"nexists",
		"notexists"
	],
	n: [
		"fltns",
		"isins",
		"leftrightharpoons",
		"quaternions",
		"rightleftharpoons",
		"strns"
	],
	f: [
		"larrbfs",
		"larrfs",
		"rarrbfs",
		"rarrfs"
	],
	a: [
		"otimesas"
	]
};
var x$3 = {
	o: [
		"approx",
		"boxbox",
		"gnapprox",
		"gtrapprox",
		"lessapprox",
		"lnapprox",
		"napprox",
		"precapprox",
		"precnapprox",
		"succapprox",
		"succnapprox",
		"thickapprox"
	],
	n: [
		"divonx"
	],
	r: [
		"rx"
	]
};
var q$3 = {
	e: [
		"approxeq",
		"asympeq",
		"backsimeq",
		"bumpeq",
		"circeq",
		"coloneq",
		"ddotseq",
		"doteq",
		"fallingdotseq",
		"geq",
		"gneq",
		"leq",
		"lneq",
		"ngeq",
		"nleq",
		"npreceq",
		"nsimeq",
		"nsubseteq",
		"nsucceq",
		"nsupseteq",
		"ntrianglelefteq",
		"ntrianglerighteq",
		"preccurlyeq",
		"preceq",
		"questeq",
		"risingdotseq",
		"simeq",
		"sqsubseteq",
		"sqsupseteq",
		"subseteq",
		"subsetneq",
		"succcurlyeq",
		"succeq",
		"supseteq",
		"supsetneq",
		"trianglelefteq",
		"triangleq",
		"trianglerighteq",
		"varsubsetneq",
		"varsupsetneq",
		"veeeq",
		"wedgeq"
	],
	q: [
		"geqq",
		"gneqq",
		"gvertneqq",
		"leqq",
		"lneqq",
		"lvertneqq",
		"ngeqq",
		"nleqq",
		"nsubseteqq",
		"nsupseteqq",
		"precneqq",
		"subseteqq",
		"subsetneqq",
		"succneqq",
		"supseteqq",
		"supsetneqq",
		"varsubsetneqq",
		"varsupsetneqq"
	]
};
var l$3 = {
	m: [
		"auml",
		"euml",
		"gsiml",
		"iuml",
		"ouml",
		"siml",
		"uml",
		"uuml",
		"yuml"
	],
	d: [
		"boxdl"
	],
	u: [
		"boxul",
		"lharul",
		"rharul"
	],
	v: [
		"boxvl"
	],
	o: [
		"bsol",
		"dsol",
		"gesdotol",
		"osol",
		"sol",
		"suphsol"
	],
	l: [
		"bull",
		"ell",
		"forall",
		"hybull",
		"ll",
		"nll"
	],
	i: [
		"ccedil",
		"cedil",
		"gcedil",
		"kcedil",
		"latail",
		"lcedil",
		"lceil",
		"leftarrowtail",
		"ncedil",
		"permil",
		"ratail",
		"rcedil",
		"rceil",
		"rightarrowtail",
		"scedil",
		"tcedil"
	],
	a: [
		"clockwisecontourintegral",
		"contourintegral",
		"counterclockwisecontourintegral",
		"dotequal",
		"doublecontourintegral",
		"equal",
		"greaterequal",
		"greaterfullequal",
		"greaterslantequal",
		"humpequal",
		"intcal",
		"integral",
		"intercal",
		"lefttriangleequal",
		"lessfullequal",
		"lessslantequal",
		"natural",
		"notequal",
		"notgreaterequal",
		"notgreaterfullequal",
		"notgreaterslantequal",
		"nothumpequal",
		"notlefttriangleequal",
		"notlessequal",
		"notlessslantequal",
		"notprecedesequal",
		"notprecedesslantequal",
		"notrighttriangleequal",
		"notsquaresubsetequal",
		"notsquaresupersetequal",
		"notsubsetequal",
		"notsucceedsequal",
		"notsucceedsslantequal",
		"notsupersetequal",
		"nottildeequal",
		"nottildefullequal",
		"precedesequal",
		"precedesslantequal",
		"proportional",
		"real",
		"righttriangleequal",
		"squaresubsetequal",
		"squaresupersetequal",
		"subsetequal",
		"succeedsequal",
		"succeedsslantequal",
		"supersetequal",
		"tildeequal",
		"tildefullequal"
	],
	r: [
		"cudarrl",
		"dharl",
		"uharl"
	],
	e: [
		"del",
		"el",
		"gel",
		"gimel",
		"nparallel",
		"nshortparallel",
		"parallel",
		"prurel",
		"shortparallel"
	],
	s: [
		"eparsl",
		"eqvparsl",
		"frasl",
		"gesl",
		"nparsl",
		"parsl",
		"smeparsl"
	],
	c: [
		"excl",
		"iexcl"
	],
	g: [
		"gl",
		"ntgl"
	],
	p: [
		"larrpl",
		"rarrpl"
	],
	t: [
		"larrtl",
		"rarrtl"
	],
	h: [
		"vdashl"
	]
};
var k$3 = {
	r: [
		"bbrk",
		"bbrktbrk",
		"checkmark",
		"fork",
		"lbbrk",
		"lobrk",
		"pitchfork",
		"rbbrk",
		"robrk",
		"tbrk",
		"topfork"
	],
	n: [
		"blank",
		"pertenk"
	],
	c: [
		"block",
		"check",
		"lbrack",
		"planck",
		"rbrack"
	],
	o: [
		"dstrok",
		"hstrok",
		"lstrok",
		"tstrok"
	],
	e: [
		"hacek"
	],
	h: [
		"intlarhk",
		"larrhk",
		"nearhk",
		"nwarhk",
		"rarrhk",
		"searhk",
		"swarhk"
	],
	l: [
		"lhblk",
		"uhblk"
	],
	a: [
		"nobreak"
	]
};
var o$3 = {
	u: [
		"bdquo",
		"laquo",
		"ldquo",
		"lsaquo",
		"lsquo",
		"raquo",
		"rdquo",
		"rsaquo",
		"rsquo",
		"sbquo"
	],
	r: [
		"euro",
		"micro",
		"numero"
	],
	t: [
		"gesdoto",
		"lesdoto",
		"longmapsto",
		"mapsto",
		"propto",
		"varpropto"
	],
	h: [
		"mho",
		"rho",
		"varrho"
	],
	d: [
		"plusdo"
	],
	w: [
		"plustwo"
	],
	i: [
		"ratio"
	]
};
var i$3 = {
	s: [
		"bepsi",
		"epsi",
		"psi",
		"upsi"
	],
	m: [
		"bsemi",
		"semi"
	],
	h: [
		"chi",
		"phi",
		"straightphi",
		"varphi"
	],
	r: [
		"dtri",
		"lltri",
		"lrtri",
		"ltri",
		"nltri",
		"nrtri",
		"rtri",
		"rtriltri",
		"ultri",
		"urtri",
		"utri",
		"vltri",
		"vrtri",
		"xdtri",
		"xutri"
	],
	i: [
		"ii"
	],
	y: [
		"imaginaryi"
	],
	n: [
		"ni",
		"notni"
	],
	p: [
		"pi",
		"varpi"
	],
	x: [
		"xi"
	]
};
var u$3 = {
	o: [
		"bernou"
	],
	h: [
		"boxhu"
	],
	l: [
		"lbrkslu",
		"rbrkslu"
	],
	r: [
		"lharu",
		"rharu"
	],
	d: [
		"minusdu",
		"plusdu"
	],
	m: [
		"mu"
	],
	n: [
		"nu"
	],
	q: [
		"squ"
	],
	a: [
		"tau"
	]
};
var w$3 = {
	o: [
		"bkarow",
		"dbkarow",
		"doubledownarrow",
		"doubleleftarrow",
		"doubleleftrightarrow",
		"doublelongleftarrow",
		"doublelongleftrightarrow",
		"doublelongrightarrow",
		"doublerightarrow",
		"doubleuparrow",
		"doubleupdownarrow",
		"downarrow",
		"downarrowuparrow",
		"downteearrow",
		"drbkarow",
		"hksearow",
		"hkswarow",
		"hookleftarrow",
		"hookrightarrow",
		"leftarrow",
		"leftarrowrightarrow",
		"leftrightarrow",
		"leftrightsquigarrow",
		"leftteearrow",
		"lleftarrow",
		"longleftarrow",
		"longleftrightarrow",
		"longrightarrow",
		"lowerleftarrow",
		"lowerrightarrow",
		"nearrow",
		"nleftarrow",
		"nleftrightarrow",
		"nrightarrow",
		"nwarrow",
		"rightarrow",
		"rightarrowleftarrow",
		"rightsquigarrow",
		"rightteearrow",
		"rrightarrow",
		"searrow",
		"shortdownarrow",
		"shortleftarrow",
		"shortrightarrow",
		"shortuparrow",
		"swarrow",
		"twoheadleftarrow",
		"twoheadrightarrow",
		"uparrow",
		"uparrowdownarrow",
		"updownarrow",
		"upperleftarrow",
		"upperrightarrow",
		"upteearrow"
	],
	r: [
		"harrw",
		"nrarrw",
		"rarrw"
	]
};
var j$3 = {
	l: [
		"glj"
	],
	w: [
		"zwj"
	],
	n: [
		"zwnj"
	]
};
var z$3 = {
	o: [
		"loz"
	]
};
var endsWithCaseInsensitive = {
	"1": {
	p: [
		"sup1"
	]
},
	"2": {
	"1": [
		"blk12",
		"frac12"
	],
	p: [
		"sup2"
	]
},
	"3": {
	"1": [
		"emsp13",
		"frac13"
	],
	"2": [
		"frac23"
	],
	p: [
		"sup3"
	]
},
	"4": {
	"1": [
		"blk14",
		"emsp14",
		"frac14"
	],
	"3": [
		"blk34",
		"frac34"
	],
	e: [
		"there4"
	]
},
	"5": {
	"1": [
		"frac15"
	],
	"2": [
		"frac25"
	],
	"3": [
		"frac35"
	],
	"4": [
		"frac45"
	]
},
	"6": {
	"1": [
		"frac16"
	],
	"5": [
		"frac56"
	]
},
	"8": {
	"1": [
		"frac18"
	],
	"3": [
		"frac38"
	],
	"5": [
		"frac58"
	],
	"7": [
		"frac78"
	]
},
	e: e$3,
	c: c$3,
	d: d$3,
	y: y$3,
	g: g$3,
	f: f$3,
	r: r$3,
	m: m$3,
	h: h$3,
	a: a$3,
	p: p$3,
	v: v$3,
	b: b$3,
	t: t$3,
	n: n$3,
	s: s$3,
	x: x$3,
	q: q$3,
	l: l$3,
	k: k$3,
	o: o$3,
	i: i$3,
	u: u$3,
	w: w$3,
	j: j$3,
	z: z$3
};

var AMP$1 = "#x26";
var Abreve$1 = "#x102";
var Acy$1 = "#x410";
var Afr$1 = "#x1D504";
var Amacr$1 = "#x100";
var And$1 = "#x2A53";
var Aogon$1 = "#x104";
var Aopf$1 = "#x1D538";
var ApplyFunction$1 = "#x2061";
var Ascr$1 = "#x1D49C";
var Assign$1 = "#x2254";
var Backslash$1 = "#x2216";
var Barv$1 = "#x2AE7";
var Barwed$1 = "#x2306";
var Bcy$1 = "#x411";
var Because$1 = "#x2235";
var Bernoullis$1 = "#x212C";
var Bfr$1 = "#x1D505";
var Bopf$1 = "#x1D539";
var Breve$1 = "#x2D8";
var Bscr$1 = "#x212C";
var Bumpeq$1 = "#x224E";
var CHcy$1 = "#x427";
var COPY$1 = "#xA9";
var Cacute$1 = "#x106";
var Cap$1 = "#x22D2";
var CapitalDifferentialD$1 = "#x2145";
var Cayleys$1 = "#x212D";
var Ccaron$1 = "#x10C";
var Ccirc$1 = "#x108";
var Cconint$1 = "#x2230";
var Cdot$1 = "#x10A";
var Cedilla$1 = "#xB8";
var CenterDot$1 = "#xB7";
var Cfr$1 = "#x212D";
var CircleDot$1 = "#x2299";
var CircleMinus$1 = "#x2296";
var CirclePlus$1 = "#x2295";
var CircleTimes$1 = "#x2297";
var ClockwiseContourIntegral$1 = "#x2232";
var CloseCurlyDoubleQuote$1 = "#x201D";
var CloseCurlyQuote$1 = "#x2019";
var Colon$1 = "#x2237";
var Colone$1 = "#x2A74";
var Congruent$1 = "#x2261";
var Conint$1 = "#x222F";
var ContourIntegral$1 = "#x222E";
var Copf$1 = "#x2102";
var Coproduct$1 = "#x2210";
var CounterClockwiseContourIntegral$1 = "#x2233";
var Cross$1 = "#x2A2F";
var Cscr$1 = "#x1D49E";
var Cup$1 = "#x22D3";
var CupCap$1 = "#x224D";
var DD$1 = "#x2145";
var DDotrahd$1 = "#x2911";
var DJcy$1 = "#x402";
var DScy$1 = "#x405";
var DZcy$1 = "#x40F";
var Darr$1 = "#x21A1";
var Dashv$1 = "#x2AE4";
var Dcaron$1 = "#x10E";
var Dcy$1 = "#x414";
var Del$1 = "#x2207";
var Dfr$1 = "#x1D507";
var DiacriticalAcute$1 = "#xB4";
var DiacriticalDot$1 = "#x2D9";
var DiacriticalDoubleAcute$1 = "#x2DD";
var DiacriticalGrave$1 = "#x60";
var DiacriticalTilde$1 = "#x2DC";
var Diamond$1 = "#x22C4";
var DifferentialD$1 = "#x2146";
var Dopf$1 = "#x1D53B";
var Dot$1 = "#xA8";
var DotDot$1 = "#x20DC";
var DotEqual$1 = "#x2250";
var DoubleContourIntegral$1 = "#x222F";
var DoubleDot$1 = "#xA8";
var DoubleDownArrow$1 = "#x21D3";
var DoubleLeftArrow$1 = "#x21D0";
var DoubleLeftRightArrow$1 = "#x21D4";
var DoubleLeftTee$1 = "#x2AE4";
var DoubleLongLeftArrow$1 = "#x27F8";
var DoubleLongLeftRightArrow$1 = "#x27FA";
var DoubleLongRightArrow$1 = "#x27F9";
var DoubleRightArrow$1 = "#x21D2";
var DoubleRightTee$1 = "#x22A8";
var DoubleUpArrow$1 = "#x21D1";
var DoubleUpDownArrow$1 = "#x21D5";
var DoubleVerticalBar$1 = "#x2225";
var DownArrow$1 = "#x2193";
var DownArrowBar$1 = "#x2913";
var DownArrowUpArrow$1 = "#x21F5";
var DownBreve$1 = "#x311";
var DownLeftRightVector$1 = "#x2950";
var DownLeftTeeVector$1 = "#x295E";
var DownLeftVector$1 = "#x21BD";
var DownLeftVectorBar$1 = "#x2956";
var DownRightTeeVector$1 = "#x295F";
var DownRightVector$1 = "#x21C1";
var DownRightVectorBar$1 = "#x2957";
var DownTee$1 = "#x22A4";
var DownTeeArrow$1 = "#x21A7";
var Downarrow$1 = "#x21D3";
var Dscr$1 = "#x1D49F";
var Dstrok$1 = "#x110";
var ENG$1 = "#x14A";
var Ecaron$1 = "#x11A";
var Ecy$1 = "#x42D";
var Edot$1 = "#x116";
var Efr$1 = "#x1D508";
var Element$1 = "#x2208";
var Emacr$1 = "#x112";
var EmptySmallSquare$1 = "#x25FB";
var EmptyVerySmallSquare$1 = "#x25AB";
var Eogon$1 = "#x118";
var Eopf$1 = "#x1D53C";
var Equal$1 = "#x2A75";
var EqualTilde$1 = "#x2242";
var Equilibrium$1 = "#x21CC";
var Escr$1 = "#x2130";
var Esim$1 = "#x2A73";
var Exists$1 = "#x2203";
var ExponentialE$1 = "#x2147";
var Fcy$1 = "#x424";
var Ffr$1 = "#x1D509";
var FilledSmallSquare$1 = "#x25FC";
var FilledVerySmallSquare$1 = "#x25AA";
var Fopf$1 = "#x1D53D";
var ForAll$1 = "#x2200";
var Fouriertrf$1 = "#x2131";
var Fscr$1 = "#x2131";
var GJcy$1 = "#x403";
var GT$1 = "#x3E";
var Gammad$1 = "#x3DC";
var Gbreve$1 = "#x11E";
var Gcedil$1 = "#x122";
var Gcirc$1 = "#x11C";
var Gcy$1 = "#x413";
var Gdot$1 = "#x120";
var Gfr$1 = "#x1D50A";
var Gg$1 = "#x22D9";
var Gopf$1 = "#x1D53E";
var GreaterEqual$1 = "#x2265";
var GreaterEqualLess$1 = "#x22DB";
var GreaterFullEqual$1 = "#x2267";
var GreaterGreater$1 = "#x2AA2";
var GreaterLess$1 = "#x2277";
var GreaterSlantEqual$1 = "#x2A7E";
var GreaterTilde$1 = "#x2273";
var Gscr$1 = "#x1D4A2";
var Gt$1 = "#x226B";
var HARDcy$1 = "#x42A";
var Hacek$1 = "#x2C7";
var Hcirc$1 = "#x124";
var Hfr$1 = "#x210C";
var HilbertSpace$1 = "#x210B";
var Hopf$1 = "#x210D";
var HorizontalLine$1 = "#x2500";
var Hscr$1 = "#x210B";
var Hstrok$1 = "#x126";
var HumpDownHump$1 = "#x224E";
var HumpEqual$1 = "#x224F";
var IEcy$1 = "#x415";
var IJlig$1 = "#x132";
var IOcy$1 = "#x401";
var Icy$1 = "#x418";
var Idot$1 = "#x130";
var Ifr$1 = "#x2111";
var Im$1 = "#x2111";
var Imacr$1 = "#x12A";
var ImaginaryI$1 = "#x2148";
var Implies$1 = "#x21D2";
var Int$1 = "#x222C";
var Integral$1 = "#x222B";
var Intersection$1 = "#x22C2";
var InvisibleComma$1 = "#x2063";
var InvisibleTimes$1 = "#x2062";
var Iogon$1 = "#x12E";
var Iopf$1 = "#x1D540";
var Iscr$1 = "#x2110";
var Itilde$1 = "#x128";
var Iukcy$1 = "#x406";
var Jcirc$1 = "#x134";
var Jcy$1 = "#x419";
var Jfr$1 = "#x1D50D";
var Jopf$1 = "#x1D541";
var Jscr$1 = "#x1D4A5";
var Jsercy$1 = "#x408";
var Jukcy$1 = "#x404";
var KHcy$1 = "#x425";
var KJcy$1 = "#x40C";
var Kcedil$1 = "#x136";
var Kcy$1 = "#x41A";
var Kfr$1 = "#x1D50E";
var Kopf$1 = "#x1D542";
var Kscr$1 = "#x1D4A6";
var LJcy$1 = "#x409";
var LT$1 = "#x3C";
var Lacute$1 = "#x139";
var Lang$1 = "#x27EA";
var Laplacetrf$1 = "#x2112";
var Larr$1 = "#x219E";
var Lcaron$1 = "#x13D";
var Lcedil$1 = "#x13B";
var Lcy$1 = "#x41B";
var LeftAngleBracket$1 = "#x27E8";
var LeftArrow$1 = "#x2190";
var LeftArrowBar$1 = "#x21E4";
var LeftArrowRightArrow$1 = "#x21C6";
var LeftCeiling$1 = "#x2308";
var LeftDoubleBracket$1 = "#x27E6";
var LeftDownTeeVector$1 = "#x2961";
var LeftDownVector$1 = "#x21C3";
var LeftDownVectorBar$1 = "#x2959";
var LeftFloor$1 = "#x230A";
var LeftRightArrow$1 = "#x2194";
var LeftRightVector$1 = "#x294E";
var LeftTee$1 = "#x22A3";
var LeftTeeArrow$1 = "#x21A4";
var LeftTeeVector$1 = "#x295A";
var LeftTriangle$1 = "#x22B2";
var LeftTriangleBar$1 = "#x29CF";
var LeftTriangleEqual$1 = "#x22B4";
var LeftUpDownVector$1 = "#x2951";
var LeftUpTeeVector$1 = "#x2960";
var LeftUpVector$1 = "#x21BF";
var LeftUpVectorBar$1 = "#x2958";
var LeftVector$1 = "#x21BC";
var LeftVectorBar$1 = "#x2952";
var Leftarrow$1 = "#x21D0";
var Leftrightarrow$1 = "#x21D4";
var LessEqualGreater$1 = "#x22DA";
var LessFullEqual$1 = "#x2266";
var LessGreater$1 = "#x2276";
var LessLess$1 = "#x2AA1";
var LessSlantEqual$1 = "#x2A7D";
var LessTilde$1 = "#x2272";
var Lfr$1 = "#x1D50F";
var Ll$1 = "#x22D8";
var Lleftarrow$1 = "#x21DA";
var Lmidot$1 = "#x13F";
var LongLeftArrow$1 = "#x27F5";
var LongLeftRightArrow$1 = "#x27F7";
var LongRightArrow$1 = "#x27F6";
var Longleftarrow$1 = "#x27F8";
var Longleftrightarrow$1 = "#x27FA";
var Longrightarrow$1 = "#x27F9";
var Lopf$1 = "#x1D543";
var LowerLeftArrow$1 = "#x2199";
var LowerRightArrow$1 = "#x2198";
var Lscr$1 = "#x2112";
var Lsh$1 = "#x21B0";
var Lstrok$1 = "#x141";
var Lt$1 = "#x226A";
var Mcy$1 = "#x41C";
var MediumSpace$1 = "#x205F";
var Mellintrf$1 = "#x2133";
var Mfr$1 = "#x1D510";
var MinusPlus$1 = "#x2213";
var Mopf$1 = "#x1D544";
var Mscr$1 = "#x2133";
var NJcy$1 = "#x40A";
var Nacute$1 = "#x143";
var Ncaron$1 = "#x147";
var Ncedil$1 = "#x145";
var Ncy$1 = "#x41D";
var NegativeMediumSpace$1 = "#x200B";
var NegativeThickSpace$1 = "#x200B";
var NegativeThinSpace$1 = "#x200B";
var NegativeVeryThinSpace$1 = "#x200B";
var NestedGreaterGreater$1 = "#x226B";
var NestedLessLess$1 = "#x226A";
var Nfr$1 = "#x1D511";
var NoBreak$1 = "#x2060";
var NonBreakingSpace$1 = "#xA0";
var Nopf$1 = "#x2115";
var Not$1 = "#x2AEC";
var NotCongruent$1 = "#x2262";
var NotCupCap$1 = "#x226D";
var NotDoubleVerticalBar$1 = "#x2226";
var NotElement$1 = "#x2209";
var NotEqual$1 = "#x2260";
var NotEqualTilde$1 = "#x2242;&#x338";
var NotExists$1 = "#x2204";
var NotGreater$1 = "#x226F";
var NotGreaterEqual$1 = "#x2271";
var NotGreaterFullEqual$1 = "#x2267;&#x338";
var NotGreaterGreater$1 = "#x226B;&#x338";
var NotGreaterLess$1 = "#x2279";
var NotGreaterSlantEqual$1 = "#x2A7E;&#x338";
var NotGreaterTilde$1 = "#x2275";
var NotHumpDownHump$1 = "#x224E;&#x338";
var NotHumpEqual$1 = "#x224F;&#x338";
var NotLeftTriangle$1 = "#x22EA";
var NotLeftTriangleBar$1 = "#x29CF;&#x338";
var NotLeftTriangleEqual$1 = "#x22EC";
var NotLess$1 = "#x226E";
var NotLessEqual$1 = "#x2270";
var NotLessGreater$1 = "#x2278";
var NotLessLess$1 = "#x226A;&#x338";
var NotLessSlantEqual$1 = "#x2A7D;&#x338";
var NotLessTilde$1 = "#x2274";
var NotNestedGreaterGreater$1 = "#x2AA2;&#x338";
var NotNestedLessLess$1 = "#x2AA1;&#x338";
var NotPrecedes$1 = "#x2280";
var NotPrecedesEqual$1 = "#x2AAF;&#x338";
var NotPrecedesSlantEqual$1 = "#x22E0";
var NotReverseElement$1 = "#x220C";
var NotRightTriangle$1 = "#x22EB";
var NotRightTriangleBar$1 = "#x29D0;&#x338";
var NotRightTriangleEqual$1 = "#x22ED";
var NotSquareSubset$1 = "#x228F;&#x338";
var NotSquareSubsetEqual$1 = "#x22E2";
var NotSquareSuperset$1 = "#x2290;&#x338";
var NotSquareSupersetEqual$1 = "#x22E3";
var NotSubset$1 = "#x2282;&#x20D2";
var NotSubsetEqual$1 = "#x2288";
var NotSucceeds$1 = "#x2281";
var NotSucceedsEqual$1 = "#x2AB0;&#x338";
var NotSucceedsSlantEqual$1 = "#x22E1";
var NotSucceedsTilde$1 = "#x227F;&#x338";
var NotSuperset$1 = "#x2283;&#x20D2";
var NotSupersetEqual$1 = "#x2289";
var NotTilde$1 = "#x2241";
var NotTildeEqual$1 = "#x2244";
var NotTildeFullEqual$1 = "#x2247";
var NotTildeTilde$1 = "#x2249";
var NotVerticalBar$1 = "#x2224";
var Nscr$1 = "#x1D4A9";
var Ocy$1 = "#x41E";
var Odblac$1 = "#x150";
var Ofr$1 = "#x1D512";
var Omacr$1 = "#x14C";
var Oopf$1 = "#x1D546";
var OpenCurlyDoubleQuote$1 = "#x201C";
var OpenCurlyQuote$1 = "#x2018";
var Or$1 = "#x2A54";
var Oscr$1 = "#x1D4AA";
var Otimes$1 = "#x2A37";
var OverBar$1 = "#x203E";
var OverBrace$1 = "#x23DE";
var OverBracket$1 = "#x23B4";
var OverParenthesis$1 = "#x23DC";
var PartialD$1 = "#x2202";
var Pcy$1 = "#x41F";
var Pfr$1 = "#x1D513";
var PlusMinus$1 = "#xB1";
var Poincareplane$1 = "#x210C";
var Popf$1 = "#x2119";
var Pr$1 = "#x2ABB";
var Precedes$1 = "#x227A";
var PrecedesEqual$1 = "#x2AAF";
var PrecedesSlantEqual$1 = "#x227C";
var PrecedesTilde$1 = "#x227E";
var Product$1 = "#x220F";
var Proportion$1 = "#x2237";
var Proportional$1 = "#x221D";
var Pscr$1 = "#x1D4AB";
var QUOT$1 = "#x22";
var Qfr$1 = "#x1D514";
var Qopf$1 = "#x211A";
var Qscr$1 = "#x1D4AC";
var RBarr$1 = "#x2910";
var REG$1 = "#xAE";
var Racute$1 = "#x154";
var Rang$1 = "#x27EB";
var Rarr$1 = "#x21A0";
var Rarrtl$1 = "#x2916";
var Rcaron$1 = "#x158";
var Rcedil$1 = "#x156";
var Rcy$1 = "#x420";
var Re$1 = "#x211C";
var ReverseElement$1 = "#x220B";
var ReverseEquilibrium$1 = "#x21CB";
var ReverseUpEquilibrium$1 = "#x296F";
var Rfr$1 = "#x211C";
var RightAngleBracket$1 = "#x27E9";
var RightArrow$1 = "#x2192";
var RightArrowBar$1 = "#x21E5";
var RightArrowLeftArrow$1 = "#x21C4";
var RightCeiling$1 = "#x2309";
var RightDoubleBracket$1 = "#x27E7";
var RightDownTeeVector$1 = "#x295D";
var RightDownVector$1 = "#x21C2";
var RightDownVectorBar$1 = "#x2955";
var RightFloor$1 = "#x230B";
var RightTee$1 = "#x22A2";
var RightTeeArrow$1 = "#x21A6";
var RightTeeVector$1 = "#x295B";
var RightTriangle$1 = "#x22B3";
var RightTriangleBar$1 = "#x29D0";
var RightTriangleEqual$1 = "#x22B5";
var RightUpDownVector$1 = "#x294F";
var RightUpTeeVector$1 = "#x295C";
var RightUpVector$1 = "#x21BE";
var RightUpVectorBar$1 = "#x2954";
var RightVector$1 = "#x21C0";
var RightVectorBar$1 = "#x2953";
var Rightarrow$1 = "#x21D2";
var Ropf$1 = "#x211D";
var RoundImplies$1 = "#x2970";
var Rrightarrow$1 = "#x21DB";
var Rscr$1 = "#x211B";
var Rsh$1 = "#x21B1";
var RuleDelayed$1 = "#x29F4";
var SHCHcy$1 = "#x429";
var SHcy$1 = "#x428";
var SOFTcy$1 = "#x42C";
var Sacute$1 = "#x15A";
var Sc$1 = "#x2ABC";
var Scedil$1 = "#x15E";
var Scirc$1 = "#x15C";
var Scy$1 = "#x421";
var Sfr$1 = "#x1D516";
var ShortDownArrow$1 = "#x2193";
var ShortLeftArrow$1 = "#x2190";
var ShortRightArrow$1 = "#x2192";
var ShortUpArrow$1 = "#x2191";
var SmallCircle$1 = "#x2218";
var Sopf$1 = "#x1D54A";
var Sqrt$1 = "#x221A";
var Square$1 = "#x25A1";
var SquareIntersection$1 = "#x2293";
var SquareSubset$1 = "#x228F";
var SquareSubsetEqual$1 = "#x2291";
var SquareSuperset$1 = "#x2290";
var SquareSupersetEqual$1 = "#x2292";
var SquareUnion$1 = "#x2294";
var Sscr$1 = "#x1D4AE";
var Star$1 = "#x22C6";
var Sub$1 = "#x22D0";
var Subset$1 = "#x22D0";
var SubsetEqual$1 = "#x2286";
var Succeeds$1 = "#x227B";
var SucceedsEqual$1 = "#x2AB0";
var SucceedsSlantEqual$1 = "#x227D";
var SucceedsTilde$1 = "#x227F";
var SuchThat$1 = "#x220B";
var Sum$1 = "#x2211";
var Sup$1 = "#x22D1";
var Superset$1 = "#x2283";
var SupersetEqual$1 = "#x2287";
var Supset$1 = "#x22D1";
var TRADE$1 = "#x2122";
var TSHcy$1 = "#x40B";
var TScy$1 = "#x426";
var Tab$1 = "#x9";
var Tcaron$1 = "#x164";
var Tcedil$1 = "#x162";
var Tcy$1 = "#x422";
var Tfr$1 = "#x1D517";
var Therefore$1 = "#x2234";
var ThickSpace$1 = "#x205F;&#x200A";
var ThinSpace$1 = "#x2009";
var Tilde$1 = "#x223C";
var TildeEqual$1 = "#x2243";
var TildeFullEqual$1 = "#x2245";
var TildeTilde$1 = "#x2248";
var Topf$1 = "#x1D54B";
var TripleDot$1 = "#x20DB";
var Tscr$1 = "#x1D4AF";
var Tstrok$1 = "#x166";
var Uarr$1 = "#x219F";
var Uarrocir$1 = "#x2949";
var Ubrcy$1 = "#x40E";
var Ubreve$1 = "#x16C";
var Ucy$1 = "#x423";
var Udblac$1 = "#x170";
var Ufr$1 = "#x1D518";
var Umacr$1 = "#x16A";
var UnderBrace$1 = "#x23DF";
var UnderBracket$1 = "#x23B5";
var UnderParenthesis$1 = "#x23DD";
var Union$1 = "#x22C3";
var UnionPlus$1 = "#x228E";
var Uogon$1 = "#x172";
var Uopf$1 = "#x1D54C";
var UpArrow$1 = "#x2191";
var UpArrowBar$1 = "#x2912";
var UpArrowDownArrow$1 = "#x21C5";
var UpDownArrow$1 = "#x2195";
var UpEquilibrium$1 = "#x296E";
var UpTee$1 = "#x22A5";
var UpTeeArrow$1 = "#x21A5";
var Uparrow$1 = "#x21D1";
var Updownarrow$1 = "#x21D5";
var UpperLeftArrow$1 = "#x2196";
var UpperRightArrow$1 = "#x2197";
var Upsi$1 = "#x3D2";
var Uring$1 = "#x16E";
var Uscr$1 = "#x1D4B0";
var Utilde$1 = "#x168";
var VDash$1 = "#x22AB";
var Vbar$1 = "#x2AEB";
var Vcy$1 = "#x412";
var Vdash$1 = "#x22A9";
var Vdashl$1 = "#x2AE6";
var Vee$1 = "#x22C1";
var Verbar$1 = "#x2016";
var Vert$1 = "#x2016";
var VerticalBar$1 = "#x2223";
var VerticalSeparator$1 = "#x2758";
var VerticalTilde$1 = "#x2240";
var VeryThinSpace$1 = "#x200A";
var Vfr$1 = "#x1D519";
var Vopf$1 = "#x1D54D";
var Vscr$1 = "#x1D4B1";
var Vvdash$1 = "#x22AA";
var Wcirc$1 = "#x174";
var Wedge$1 = "#x22C0";
var Wfr$1 = "#x1D51A";
var Wopf$1 = "#x1D54E";
var Wscr$1 = "#x1D4B2";
var Xfr$1 = "#x1D51B";
var Xopf$1 = "#x1D54F";
var Xscr$1 = "#x1D4B3";
var YAcy$1 = "#x42F";
var YIcy$1 = "#x407";
var YUcy$1 = "#x42E";
var Ycirc$1 = "#x176";
var Ycy$1 = "#x42B";
var Yfr$1 = "#x1D51C";
var Yopf$1 = "#x1D550";
var Yscr$1 = "#x1D4B4";
var ZHcy$1 = "#x416";
var Zacute$1 = "#x179";
var Zcaron$1 = "#x17D";
var Zcy$1 = "#x417";
var Zdot$1 = "#x17B";
var ZeroWidthSpace$1 = "#x200B";
var Zfr$1 = "#x2128";
var Zopf$1 = "#x2124";
var Zscr$1 = "#x1D4B5";
var abreve$1 = "#x103";
var ac$1 = "#x223E";
var acE$1 = "#x223E;&#x333";
var acd$1 = "#x223F";
var acy$1 = "#x430";
var af$1 = "#x2061";
var afr$1 = "#x1D51E";
var aleph$1 = "#x2135";
var amacr$1 = "#x101";
var amalg$1 = "#x2A3F";
var andand$1 = "#x2A55";
var andd$1 = "#x2A5C";
var andslope$1 = "#x2A58";
var andv$1 = "#x2A5A";
var ange$1 = "#x29A4";
var angle$1 = "#x2220";
var angmsd$1 = "#x2221";
var angmsdaa$1 = "#x29A8";
var angmsdab$1 = "#x29A9";
var angmsdac$1 = "#x29AA";
var angmsdad$1 = "#x29AB";
var angmsdae$1 = "#x29AC";
var angmsdaf$1 = "#x29AD";
var angmsdag$1 = "#x29AE";
var angmsdah$1 = "#x29AF";
var angrt$1 = "#x221F";
var angrtvb$1 = "#x22BE";
var angrtvbd$1 = "#x299D";
var angsph$1 = "#x2222";
var angst$1 = "#xC5";
var angzarr$1 = "#x237C";
var aogon$1 = "#x105";
var aopf$1 = "#x1D552";
var ap$1 = "#x2248";
var apE$1 = "#x2A70";
var apacir$1 = "#x2A6F";
var ape$1 = "#x224A";
var apid$1 = "#x224B";
var approx$1 = "#x2248";
var approxeq$1 = "#x224A";
var ascr$1 = "#x1D4B6";
var asympeq$1 = "#x224D";
var awconint$1 = "#x2233";
var awint$1 = "#x2A11";
var bNot$1 = "#x2AED";
var backcong$1 = "#x224C";
var backepsilon$1 = "#x3F6";
var backprime$1 = "#x2035";
var backsim$1 = "#x223D";
var backsimeq$1 = "#x22CD";
var barvee$1 = "#x22BD";
var barwed$1 = "#x2305";
var barwedge$1 = "#x2305";
var bbrk$1 = "#x23B5";
var bbrktbrk$1 = "#x23B6";
var bcong$1 = "#x224C";
var bcy$1 = "#x431";
var becaus$1 = "#x2235";
var because$1 = "#x2235";
var bemptyv$1 = "#x29B0";
var bepsi$1 = "#x3F6";
var bernou$1 = "#x212C";
var beth$1 = "#x2136";
var between$1 = "#x226C";
var bfr$1 = "#x1D51F";
var bigcap$1 = "#x22C2";
var bigcirc$1 = "#x25EF";
var bigcup$1 = "#x22C3";
var bigodot$1 = "#x2A00";
var bigoplus$1 = "#x2A01";
var bigotimes$1 = "#x2A02";
var bigsqcup$1 = "#x2A06";
var bigstar$1 = "#x2605";
var bigtriangledown$1 = "#x25BD";
var bigtriangleup$1 = "#x25B3";
var biguplus$1 = "#x2A04";
var bigvee$1 = "#x22C1";
var bigwedge$1 = "#x22C0";
var bkarow$1 = "#x290D";
var blacklozenge$1 = "#x29EB";
var blacksquare$1 = "#x25AA";
var blacktriangle$1 = "#x25B4";
var blacktriangledown$1 = "#x25BE";
var blacktriangleleft$1 = "#x25C2";
var blacktriangleright$1 = "#x25B8";
var blank$1 = "#x2423";
var blk12$1 = "#x2592";
var blk14$1 = "#x2591";
var blk34$1 = "#x2593";
var block$1 = "#x2588";
var bne$1 = "=&#x20E5";
var bnequiv$1 = "#x2261;&#x20E5";
var bnot$1 = "#x2310";
var bopf$1 = "#x1D553";
var bot$1 = "#x22A5";
var bottom$1 = "#x22A5";
var bowtie$1 = "#x22C8";
var boxDL$1 = "#x2557";
var boxDR$1 = "#x2554";
var boxDl$1 = "#x2556";
var boxDr$1 = "#x2553";
var boxH$1 = "#x2550";
var boxHD$1 = "#x2566";
var boxHU$1 = "#x2569";
var boxHd$1 = "#x2564";
var boxHu$1 = "#x2567";
var boxUL$1 = "#x255D";
var boxUR$1 = "#x255A";
var boxUl$1 = "#x255C";
var boxUr$1 = "#x2559";
var boxV$1 = "#x2551";
var boxVH$1 = "#x256C";
var boxVL$1 = "#x2563";
var boxVR$1 = "#x2560";
var boxVh$1 = "#x256B";
var boxVl$1 = "#x2562";
var boxVr$1 = "#x255F";
var boxbox$1 = "#x29C9";
var boxdL$1 = "#x2555";
var boxdR$1 = "#x2552";
var boxdl$1 = "#x2510";
var boxdr$1 = "#x250C";
var boxh$1 = "#x2500";
var boxhD$1 = "#x2565";
var boxhU$1 = "#x2568";
var boxhd$1 = "#x252C";
var boxhu$1 = "#x2534";
var boxminus$1 = "#x229F";
var boxplus$1 = "#x229E";
var boxtimes$1 = "#x22A0";
var boxuL$1 = "#x255B";
var boxuR$1 = "#x2558";
var boxul$1 = "#x2518";
var boxur$1 = "#x2514";
var boxv$1 = "#x2502";
var boxvH$1 = "#x256A";
var boxvL$1 = "#x2561";
var boxvR$1 = "#x255E";
var boxvh$1 = "#x253C";
var boxvl$1 = "#x2524";
var boxvr$1 = "#x251C";
var bprime$1 = "#x2035";
var breve$1 = "#x2D8";
var bscr$1 = "#x1D4B7";
var bsemi$1 = "#x204F";
var bsim$1 = "#x223D";
var bsime$1 = "#x22CD";
var bsolb$1 = "#x29C5";
var bsolhsub$1 = "#x27C8";
var bullet$1 = "#x2022";
var bump$1 = "#x224E";
var bumpE$1 = "#x2AAE";
var bumpe$1 = "#x224F";
var bumpeq$1 = "#x224F";
var cacute$1 = "#x107";
var capand$1 = "#x2A44";
var capbrcup$1 = "#x2A49";
var capcap$1 = "#x2A4B";
var capcup$1 = "#x2A47";
var capdot$1 = "#x2A40";
var caps$1 = "#x2229;&#xFE00";
var caret$1 = "#x2041";
var caron$1 = "#x2C7";
var ccaps$1 = "#x2A4D";
var ccaron$1 = "#x10D";
var ccirc$1 = "#x109";
var ccups$1 = "#x2A4C";
var ccupssm$1 = "#x2A50";
var cdot$1 = "#x10B";
var cemptyv$1 = "#x29B2";
var centerdot$1 = "#xB7";
var cfr$1 = "#x1D520";
var chcy$1 = "#x447";
var check$1 = "#x2713";
var checkmark$1 = "#x2713";
var cir$1 = "#x25CB";
var cirE$1 = "#x29C3";
var circeq$1 = "#x2257";
var circlearrowleft$1 = "#x21BA";
var circlearrowright$1 = "#x21BB";
var circledR$1 = "#xAE";
var circledS$1 = "#x24C8";
var circledast$1 = "#x229B";
var circledcirc$1 = "#x229A";
var circleddash$1 = "#x229D";
var cire$1 = "#x2257";
var cirfnint$1 = "#x2A10";
var cirmid$1 = "#x2AEF";
var cirscir$1 = "#x29C2";
var clubsuit$1 = "#x2663";
var colone$1 = "#x2254";
var coloneq$1 = "#x2254";
var comp$1 = "#x2201";
var compfn$1 = "#x2218";
var complement$1 = "#x2201";
var complexes$1 = "#x2102";
var congdot$1 = "#x2A6D";
var conint$1 = "#x222E";
var copf$1 = "#x1D554";
var coprod$1 = "#x2210";
var copysr$1 = "#x2117";
var cross$1 = "#x2717";
var cscr$1 = "#x1D4B8";
var csub$1 = "#x2ACF";
var csube$1 = "#x2AD1";
var csup$1 = "#x2AD0";
var csupe$1 = "#x2AD2";
var ctdot$1 = "#x22EF";
var cudarrl$1 = "#x2938";
var cudarrr$1 = "#x2935";
var cuepr$1 = "#x22DE";
var cuesc$1 = "#x22DF";
var cularr$1 = "#x21B6";
var cularrp$1 = "#x293D";
var cupbrcap$1 = "#x2A48";
var cupcap$1 = "#x2A46";
var cupcup$1 = "#x2A4A";
var cupdot$1 = "#x228D";
var cupor$1 = "#x2A45";
var cups$1 = "#x222A;&#xFE00";
var curarr$1 = "#x21B7";
var curarrm$1 = "#x293C";
var curlyeqprec$1 = "#x22DE";
var curlyeqsucc$1 = "#x22DF";
var curlyvee$1 = "#x22CE";
var curlywedge$1 = "#x22CF";
var curvearrowleft$1 = "#x21B6";
var curvearrowright$1 = "#x21B7";
var cuvee$1 = "#x22CE";
var cuwed$1 = "#x22CF";
var cwconint$1 = "#x2232";
var cwint$1 = "#x2231";
var cylcty$1 = "#x232D";
var dHar$1 = "#x2965";
var daleth$1 = "#x2138";
var dash$1 = "#x2010";
var dashv$1 = "#x22A3";
var dbkarow$1 = "#x290F";
var dblac$1 = "#x2DD";
var dcaron$1 = "#x10F";
var dcy$1 = "#x434";
var dd$1 = "#x2146";
var ddagger$1 = "#x2021";
var ddarr$1 = "#x21CA";
var ddotseq$1 = "#x2A77";
var demptyv$1 = "#x29B1";
var dfisht$1 = "#x297F";
var dfr$1 = "#x1D521";
var dharl$1 = "#x21C3";
var dharr$1 = "#x21C2";
var diam$1 = "#x22C4";
var diamond$1 = "#x22C4";
var diamondsuit$1 = "#x2666";
var die$1 = "#xA8";
var digamma$1 = "#x3DD";
var disin$1 = "#x22F2";
var div$1 = "#xF7";
var divideontimes$1 = "#x22C7";
var divonx$1 = "#x22C7";
var djcy$1 = "#x452";
var dlcorn$1 = "#x231E";
var dlcrop$1 = "#x230D";
var dopf$1 = "#x1D555";
var dot$1 = "#x2D9";
var doteq$1 = "#x2250";
var doteqdot$1 = "#x2251";
var dotminus$1 = "#x2238";
var dotplus$1 = "#x2214";
var dotsquare$1 = "#x22A1";
var doublebarwedge$1 = "#x2306";
var downarrow$1 = "#x2193";
var downdownarrows$1 = "#x21CA";
var downharpoonleft$1 = "#x21C3";
var downharpoonright$1 = "#x21C2";
var drbkarow$1 = "#x2910";
var drcorn$1 = "#x231F";
var drcrop$1 = "#x230C";
var dscr$1 = "#x1D4B9";
var dscy$1 = "#x455";
var dsol$1 = "#x29F6";
var dstrok$1 = "#x111";
var dtdot$1 = "#x22F1";
var dtri$1 = "#x25BF";
var dtrif$1 = "#x25BE";
var duarr$1 = "#x21F5";
var duhar$1 = "#x296F";
var dwangle$1 = "#x29A6";
var dzcy$1 = "#x45F";
var dzigrarr$1 = "#x27FF";
var eDDot$1 = "#x2A77";
var eDot$1 = "#x2251";
var easter$1 = "#x2A6E";
var ecaron$1 = "#x11B";
var ecir$1 = "#x2256";
var ecolon$1 = "#x2255";
var ecy$1 = "#x44D";
var edot$1 = "#x117";
var ee$1 = "#x2147";
var efDot$1 = "#x2252";
var efr$1 = "#x1D522";
var eg$1 = "#x2A9A";
var egs$1 = "#x2A96";
var egsdot$1 = "#x2A98";
var el$1 = "#x2A99";
var elinters$1 = "#x23E7";
var ell$1 = "#x2113";
var els$1 = "#x2A95";
var elsdot$1 = "#x2A97";
var emacr$1 = "#x113";
var emptyset$1 = "#x2205";
var emptyv$1 = "#x2205";
var emsp13$1 = "#x2004";
var emsp14$1 = "#x2005";
var eng$1 = "#x14B";
var eogon$1 = "#x119";
var eopf$1 = "#x1D556";
var epar$1 = "#x22D5";
var eparsl$1 = "#x29E3";
var eplus$1 = "#x2A71";
var epsi$1 = "#x3B5";
var epsiv$1 = "#x3F5";
var eqcirc$1 = "#x2256";
var eqcolon$1 = "#x2255";
var eqsim$1 = "#x2242";
var eqslantgtr$1 = "#x2A96";
var eqslantless$1 = "#x2A95";
var equest$1 = "#x225F";
var equivDD$1 = "#x2A78";
var eqvparsl$1 = "#x29E5";
var erDot$1 = "#x2253";
var erarr$1 = "#x2971";
var escr$1 = "#x212F";
var esdot$1 = "#x2250";
var esim$1 = "#x2242";
var expectation$1 = "#x2130";
var exponentiale$1 = "#x2147";
var fallingdotseq$1 = "#x2252";
var fcy$1 = "#x444";
var female$1 = "#x2640";
var ffilig$1 = "#xFB03";
var fflig$1 = "#xFB00";
var ffllig$1 = "#xFB04";
var ffr$1 = "#x1D523";
var filig$1 = "#xFB01";
var flat$1 = "#x266D";
var fllig$1 = "#xFB02";
var fltns$1 = "#x25B1";
var fopf$1 = "#x1D557";
var fork$1 = "#x22D4";
var forkv$1 = "#x2AD9";
var fpartint$1 = "#x2A0D";
var frac13$1 = "#x2153";
var frac15$1 = "#x2155";
var frac16$1 = "#x2159";
var frac18$1 = "#x215B";
var frac23$1 = "#x2154";
var frac25$1 = "#x2156";
var frac35$1 = "#x2157";
var frac38$1 = "#x215C";
var frac45$1 = "#x2158";
var frac56$1 = "#x215A";
var frac58$1 = "#x215D";
var frac78$1 = "#x215E";
var frown$1 = "#x2322";
var fscr$1 = "#x1D4BB";
var gE$1 = "#x2267";
var gEl$1 = "#x2A8C";
var gacute$1 = "#x1F5";
var gammad$1 = "#x3DD";
var gap$1 = "#x2A86";
var gbreve$1 = "#x11F";
var gcirc$1 = "#x11D";
var gcy$1 = "#x433";
var gdot$1 = "#x121";
var gel$1 = "#x22DB";
var geq$1 = "#x2265";
var geqq$1 = "#x2267";
var geqslant$1 = "#x2A7E";
var ges$1 = "#x2A7E";
var gescc$1 = "#x2AA9";
var gesdot$1 = "#x2A80";
var gesdoto$1 = "#x2A82";
var gesdotol$1 = "#x2A84";
var gesl$1 = "#x22DB;&#xFE00";
var gesles$1 = "#x2A94";
var gfr$1 = "#x1D524";
var gg$1 = "#x226B";
var ggg$1 = "#x22D9";
var gimel$1 = "#x2137";
var gjcy$1 = "#x453";
var gl$1 = "#x2277";
var glE$1 = "#x2A92";
var gla$1 = "#x2AA5";
var glj$1 = "#x2AA4";
var gnE$1 = "#x2269";
var gnap$1 = "#x2A8A";
var gnapprox$1 = "#x2A8A";
var gne$1 = "#x2A88";
var gneq$1 = "#x2A88";
var gneqq$1 = "#x2269";
var gnsim$1 = "#x22E7";
var gopf$1 = "#x1D558";
var grave$1 = "#x60";
var gscr$1 = "#x210A";
var gsim$1 = "#x2273";
var gsime$1 = "#x2A8E";
var gsiml$1 = "#x2A90";
var gtcc$1 = "#x2AA7";
var gtcir$1 = "#x2A7A";
var gtdot$1 = "#x22D7";
var gtlPar$1 = "#x2995";
var gtquest$1 = "#x2A7C";
var gtrapprox$1 = "#x2A86";
var gtrarr$1 = "#x2978";
var gtrdot$1 = "#x22D7";
var gtreqless$1 = "#x22DB";
var gtreqqless$1 = "#x2A8C";
var gtrless$1 = "#x2277";
var gtrsim$1 = "#x2273";
var gvertneqq$1 = "#x2269;&#xFE00";
var gvnE$1 = "#x2269;&#xFE00";
var hairsp$1 = "#x200A";
var half$1 = "#xBD";
var hamilt$1 = "#x210B";
var hardcy$1 = "#x44A";
var harrcir$1 = "#x2948";
var harrw$1 = "#x21AD";
var hbar$1 = "#x210F";
var hcirc$1 = "#x125";
var heartsuit$1 = "#x2665";
var hercon$1 = "#x22B9";
var hfr$1 = "#x1D525";
var hksearow$1 = "#x2925";
var hkswarow$1 = "#x2926";
var hoarr$1 = "#x21FF";
var homtht$1 = "#x223B";
var hookleftarrow$1 = "#x21A9";
var hookrightarrow$1 = "#x21AA";
var hopf$1 = "#x1D559";
var horbar$1 = "#x2015";
var hscr$1 = "#x1D4BD";
var hslash$1 = "#x210F";
var hstrok$1 = "#x127";
var hybull$1 = "#x2043";
var hyphen$1 = "#x2010";
var ic$1 = "#x2063";
var icy$1 = "#x438";
var iecy$1 = "#x435";
var iff$1 = "#x21D4";
var ifr$1 = "#x1D526";
var ii$1 = "#x2148";
var iiiint$1 = "#x2A0C";
var iiint$1 = "#x222D";
var iinfin$1 = "#x29DC";
var iiota$1 = "#x2129";
var ijlig$1 = "#x133";
var imacr$1 = "#x12B";
var imagline$1 = "#x2110";
var imagpart$1 = "#x2111";
var imath$1 = "#x131";
var imof$1 = "#x22B7";
var imped$1 = "#x1B5";
var incare$1 = "#x2105";
var infintie$1 = "#x29DD";
var inodot$1 = "#x131";
var intcal$1 = "#x22BA";
var integers$1 = "#x2124";
var intercal$1 = "#x22BA";
var intlarhk$1 = "#x2A17";
var intprod$1 = "#x2A3C";
var iocy$1 = "#x451";
var iogon$1 = "#x12F";
var iopf$1 = "#x1D55A";
var iprod$1 = "#x2A3C";
var iscr$1 = "#x1D4BE";
var isinE$1 = "#x22F9";
var isindot$1 = "#x22F5";
var isins$1 = "#x22F4";
var isinsv$1 = "#x22F3";
var isinv$1 = "#x2208";
var it$1 = "#x2062";
var itilde$1 = "#x129";
var iukcy$1 = "#x456";
var jcirc$1 = "#x135";
var jcy$1 = "#x439";
var jfr$1 = "#x1D527";
var jmath$1 = "#x237";
var jopf$1 = "#x1D55B";
var jscr$1 = "#x1D4BF";
var jsercy$1 = "#x458";
var jukcy$1 = "#x454";
var kappav$1 = "#x3F0";
var kcedil$1 = "#x137";
var kcy$1 = "#x43A";
var kfr$1 = "#x1D528";
var kgreen$1 = "#x138";
var khcy$1 = "#x445";
var kjcy$1 = "#x45C";
var kopf$1 = "#x1D55C";
var kscr$1 = "#x1D4C0";
var lAarr$1 = "#x21DA";
var lAtail$1 = "#x291B";
var lBarr$1 = "#x290E";
var lE$1 = "#x2266";
var lEg$1 = "#x2A8B";
var lHar$1 = "#x2962";
var lacute$1 = "#x13A";
var laemptyv$1 = "#x29B4";
var lagran$1 = "#x2112";
var langd$1 = "#x2991";
var langle$1 = "#x27E8";
var lap$1 = "#x2A85";
var larrb$1 = "#x21E4";
var larrbfs$1 = "#x291F";
var larrfs$1 = "#x291D";
var larrhk$1 = "#x21A9";
var larrlp$1 = "#x21AB";
var larrpl$1 = "#x2939";
var larrsim$1 = "#x2973";
var larrtl$1 = "#x21A2";
var lat$1 = "#x2AAB";
var latail$1 = "#x2919";
var late$1 = "#x2AAD";
var lates$1 = "#x2AAD;&#xFE00";
var lbarr$1 = "#x290C";
var lbbrk$1 = "#x2772";
var lbrace$1 = "{";
var lbrack$1 = "[";
var lbrke$1 = "#x298B";
var lbrksld$1 = "#x298F";
var lbrkslu$1 = "#x298D";
var lcaron$1 = "#x13E";
var lcedil$1 = "#x13C";
var lcub$1 = "{";
var lcy$1 = "#x43B";
var ldca$1 = "#x2936";
var ldquor$1 = "#x201E";
var ldrdhar$1 = "#x2967";
var ldrushar$1 = "#x294B";
var ldsh$1 = "#x21B2";
var leftarrow$1 = "#x2190";
var leftarrowtail$1 = "#x21A2";
var leftharpoondown$1 = "#x21BD";
var leftharpoonup$1 = "#x21BC";
var leftleftarrows$1 = "#x21C7";
var leftrightarrow$1 = "#x2194";
var leftrightarrows$1 = "#x21C6";
var leftrightharpoons$1 = "#x21CB";
var leftrightsquigarrow$1 = "#x21AD";
var leftthreetimes$1 = "#x22CB";
var leg$1 = "#x22DA";
var leq$1 = "#x2264";
var leqq$1 = "#x2266";
var leqslant$1 = "#x2A7D";
var les$1 = "#x2A7D";
var lescc$1 = "#x2AA8";
var lesdot$1 = "#x2A7F";
var lesdoto$1 = "#x2A81";
var lesdotor$1 = "#x2A83";
var lesg$1 = "#x22DA;&#xFE00";
var lesges$1 = "#x2A93";
var lessapprox$1 = "#x2A85";
var lessdot$1 = "#x22D6";
var lesseqgtr$1 = "#x22DA";
var lesseqqgtr$1 = "#x2A8B";
var lessgtr$1 = "#x2276";
var lesssim$1 = "#x2272";
var lfisht$1 = "#x297C";
var lfr$1 = "#x1D529";
var lg$1 = "#x2276";
var lgE$1 = "#x2A91";
var lhard$1 = "#x21BD";
var lharu$1 = "#x21BC";
var lharul$1 = "#x296A";
var lhblk$1 = "#x2584";
var ljcy$1 = "#x459";
var ll$1 = "#x226A";
var llarr$1 = "#x21C7";
var llcorner$1 = "#x231E";
var llhard$1 = "#x296B";
var lltri$1 = "#x25FA";
var lmidot$1 = "#x140";
var lmoust$1 = "#x23B0";
var lmoustache$1 = "#x23B0";
var lnE$1 = "#x2268";
var lnap$1 = "#x2A89";
var lnapprox$1 = "#x2A89";
var lne$1 = "#x2A87";
var lneq$1 = "#x2A87";
var lneqq$1 = "#x2268";
var lnsim$1 = "#x22E6";
var loang$1 = "#x27EC";
var loarr$1 = "#x21FD";
var lobrk$1 = "#x27E6";
var longleftarrow$1 = "#x27F5";
var longleftrightarrow$1 = "#x27F7";
var longmapsto$1 = "#x27FC";
var longrightarrow$1 = "#x27F6";
var looparrowleft$1 = "#x21AB";
var looparrowright$1 = "#x21AC";
var lopar$1 = "#x2985";
var lopf$1 = "#x1D55D";
var loplus$1 = "#x2A2D";
var lotimes$1 = "#x2A34";
var lozenge$1 = "#x25CA";
var lozf$1 = "#x29EB";
var lparlt$1 = "#x2993";
var lrarr$1 = "#x21C6";
var lrcorner$1 = "#x231F";
var lrhar$1 = "#x21CB";
var lrhard$1 = "#x296D";
var lrtri$1 = "#x22BF";
var lscr$1 = "#x1D4C1";
var lsh$1 = "#x21B0";
var lsim$1 = "#x2272";
var lsime$1 = "#x2A8D";
var lsimg$1 = "#x2A8F";
var lsquor$1 = "#x201A";
var lstrok$1 = "#x142";
var ltcc$1 = "#x2AA6";
var ltcir$1 = "#x2A79";
var ltdot$1 = "#x22D6";
var lthree$1 = "#x22CB";
var ltimes$1 = "#x22C9";
var ltlarr$1 = "#x2976";
var ltquest$1 = "#x2A7B";
var ltrPar$1 = "#x2996";
var ltri$1 = "#x25C3";
var ltrie$1 = "#x22B4";
var ltrif$1 = "#x25C2";
var lurdshar$1 = "#x294A";
var luruhar$1 = "#x2966";
var lvertneqq$1 = "#x2268;&#xFE00";
var lvnE$1 = "#x2268;&#xFE00";
var mDDot$1 = "#x223A";
var male$1 = "#x2642";
var malt$1 = "#x2720";
var maltese$1 = "#x2720";
var map$1 = "#x21A6";
var mapsto$1 = "#x21A6";
var mapstodown$1 = "#x21A7";
var mapstoleft$1 = "#x21A4";
var mapstoup$1 = "#x21A5";
var marker$1 = "#x25AE";
var mcomma$1 = "#x2A29";
var mcy$1 = "#x43C";
var measuredangle$1 = "#x2221";
var mfr$1 = "#x1D52A";
var mho$1 = "#x2127";
var mid$1 = "#x2223";
var midcir$1 = "#x2AF0";
var minusb$1 = "#x229F";
var minusd$1 = "#x2238";
var minusdu$1 = "#x2A2A";
var mlcp$1 = "#x2ADB";
var mldr$1 = "hellip";
var mnplus$1 = "#x2213";
var models$1 = "#x22A7";
var mopf$1 = "#x1D55E";
var mp$1 = "#x2213";
var mscr$1 = "#x1D4C2";
var mstpos$1 = "#x223E";
var multimap$1 = "#x22B8";
var mumap$1 = "#x22B8";
var nGg$1 = "#x22D9;&#x338";
var nGt$1 = "#x226B;&#x20D2";
var nGtv$1 = "#x226B;&#x338";
var nLeftarrow$1 = "#x21CD";
var nLeftrightarrow$1 = "#x21CE";
var nLl$1 = "#x22D8;&#x338";
var nLt$1 = "#x226A;&#x20D2";
var nLtv$1 = "#x226A;&#x338";
var nRightarrow$1 = "#x21CF";
var nVDash$1 = "#x22AF";
var nVdash$1 = "#x22AE";
var nacute$1 = "#x144";
var nang$1 = "#x2220;&#x20D2";
var nap$1 = "#x2249";
var napE$1 = "#x2A70;&#x338";
var napid$1 = "#x224B;&#x338";
var napos$1 = "#x149";
var napprox$1 = "#x2249";
var natur$1 = "#x266E";
var natural$1 = "#x266E";
var naturals$1 = "#x2115";
var nbump$1 = "#x224E;&#x338";
var nbumpe$1 = "#x224F;&#x338";
var ncap$1 = "#x2A43";
var ncaron$1 = "#x148";
var ncedil$1 = "#x146";
var ncong$1 = "#x2247";
var ncongdot$1 = "#x2A6D;&#x338";
var ncup$1 = "#x2A42";
var ncy$1 = "#x43D";
var neArr$1 = "#x21D7";
var nearhk$1 = "#x2924";
var nearr$1 = "#x2197";
var nearrow$1 = "#x2197";
var nedot$1 = "#x2250;&#x338";
var nequiv$1 = "#x2262";
var nesear$1 = "#x2928";
var nesim$1 = "#x2242;&#x338";
var nexist$1 = "#x2204";
var nexists$1 = "#x2204";
var nfr$1 = "#x1D52B";
var ngE$1 = "#x2267;&#x338";
var nge$1 = "#x2271";
var ngeq$1 = "#x2271";
var ngeqq$1 = "#x2267;&#x338";
var ngeqslant$1 = "#x2A7E;&#x338";
var nges$1 = "#x2A7E;&#x338";
var ngsim$1 = "#x2275";
var ngt$1 = "#x226F";
var ngtr$1 = "#x226F";
var nhArr$1 = "#x21CE";
var nharr$1 = "#x21AE";
var nhpar$1 = "#x2AF2";
var nis$1 = "#x22FC";
var nisd$1 = "#x22FA";
var niv$1 = "#x220B";
var njcy$1 = "#x45A";
var nlArr$1 = "#x21CD";
var nlE$1 = "#x2266;&#x338";
var nlarr$1 = "#x219A";
var nldr$1 = "#x2025";
var nle$1 = "#x2270";
var nleftarrow$1 = "#x219A";
var nleftrightarrow$1 = "#x21AE";
var nleq$1 = "#x2270";
var nleqq$1 = "#x2266;&#x338";
var nleqslant$1 = "#x2A7D;&#x338";
var nles$1 = "#x2A7D;&#x338";
var nless$1 = "#x226E";
var nlsim$1 = "#x2274";
var nlt$1 = "#x226E";
var nltri$1 = "#x22EA";
var nltrie$1 = "#x22EC";
var nmid$1 = "#x2224";
var nopf$1 = "#x1D55F";
var notinE$1 = "#x22F9;&#x338";
var notindot$1 = "#x22F5;&#x338";
var notinva$1 = "#x2209";
var notinvb$1 = "#x22F7";
var notinvc$1 = "#x22F6";
var notni$1 = "#x220C";
var notniva$1 = "#x220C";
var notnivb$1 = "#x22FE";
var notnivc$1 = "#x22FD";
var npar$1 = "#x2226";
var nparallel$1 = "#x2226";
var nparsl$1 = "#x2AFD;&#x20E5";
var npart$1 = "#x2202;&#x338";
var npolint$1 = "#x2A14";
var npr$1 = "#x2280";
var nprcue$1 = "#x22E0";
var npre$1 = "#x2AAF;&#x338";
var nprec$1 = "#x2280";
var npreceq$1 = "#x2AAF;&#x338";
var nrArr$1 = "#x21CF";
var nrarr$1 = "#x219B";
var nrarrc$1 = "#x2933;&#x338";
var nrarrw$1 = "#x219D;&#x338";
var nrightarrow$1 = "#x219B";
var nrtri$1 = "#x22EB";
var nrtrie$1 = "#x22ED";
var nsc$1 = "#x2281";
var nsccue$1 = "#x22E1";
var nsce$1 = "#x2AB0;&#x338";
var nscr$1 = "#x1D4C3";
var nshortmid$1 = "#x2224";
var nshortparallel$1 = "#x2226";
var nsim$1 = "#x2241";
var nsime$1 = "#x2244";
var nsimeq$1 = "#x2244";
var nsmid$1 = "#x2224";
var nspar$1 = "#x2226";
var nsqsube$1 = "#x22E2";
var nsqsupe$1 = "#x22E3";
var nsubE$1 = "#x2AC5;&#x338";
var nsube$1 = "#x2288";
var nsubset$1 = "#x2282;&#x20D2";
var nsubseteq$1 = "#x2288";
var nsubseteqq$1 = "#x2AC5;&#x338";
var nsucc$1 = "#x2281";
var nsucceq$1 = "#x2AB0;&#x338";
var nsup$1 = "#x2285";
var nsupE$1 = "#x2AC6;&#x338";
var nsupe$1 = "#x2289";
var nsupset$1 = "#x2283;&#x20D2";
var nsupseteq$1 = "#x2289";
var nsupseteqq$1 = "#x2AC6;&#x338";
var ntgl$1 = "#x2279";
var ntlg$1 = "#x2278";
var ntriangleleft$1 = "#x22EA";
var ntrianglelefteq$1 = "#x22EC";
var ntriangleright$1 = "#x22EB";
var ntrianglerighteq$1 = "#x22ED";
var numero$1 = "#x2116";
var numsp$1 = "#x2007";
var nvDash$1 = "#x22AD";
var nvHarr$1 = "#x2904";
var nvap$1 = "#x224D;&#x20D2";
var nvdash$1 = "#x22AC";
var nvge$1 = "#x2265;&#x20D2";
var nvgt$1 = "#x3E;&#x20D2";
var nvinfin$1 = "#x29DE";
var nvlArr$1 = "#x2902";
var nvle$1 = "#x2264;&#x20D2";
var nvlt$1 = "#x3C;&#x20D2";
var nvltrie$1 = "#x22B4;&#x20D2";
var nvrArr$1 = "#x2903";
var nvrtrie$1 = "#x22B5;&#x20D2";
var nvsim$1 = "#x223C;&#x20D2";
var nwArr$1 = "#x21D6";
var nwarhk$1 = "#x2923";
var nwarr$1 = "#x2196";
var nwarrow$1 = "#x2196";
var nwnear$1 = "#x2927";
var oS$1 = "#x24C8";
var oast$1 = "#x229B";
var ocir$1 = "#x229A";
var ocy$1 = "#x43E";
var odash$1 = "#x229D";
var odblac$1 = "#x151";
var odiv$1 = "#x2A38";
var odot$1 = "#x2299";
var odsold$1 = "#x29BC";
var ofcir$1 = "#x29BF";
var ofr$1 = "#x1D52C";
var ogon$1 = "#x2DB";
var ogt$1 = "#x29C1";
var ohbar$1 = "#x29B5";
var ohm$1 = "#x3A9";
var oint$1 = "#x222E";
var olarr$1 = "#x21BA";
var olcir$1 = "#x29BE";
var olcross$1 = "#x29BB";
var olt$1 = "#x29C0";
var omacr$1 = "#x14D";
var omid$1 = "#x29B6";
var ominus$1 = "#x2296";
var oopf$1 = "#x1D560";
var opar$1 = "#x29B7";
var operp$1 = "#x29B9";
var orarr$1 = "#x21BB";
var ord$1 = "#x2A5D";
var order$1 = "#x2134";
var orderof$1 = "#x2134";
var origof$1 = "#x22B6";
var oror$1 = "#x2A56";
var orslope$1 = "#x2A57";
var orv$1 = "#x2A5B";
var oscr$1 = "#x2134";
var osol$1 = "#x2298";
var otimesas$1 = "#x2A36";
var ovbar$1 = "#x233D";
var par$1 = "#x2225";
var parallel$1 = "#x2225";
var parsim$1 = "#x2AF3";
var parsl$1 = "#x2AFD";
var pcy$1 = "#x43F";
var pertenk$1 = "#x2031";
var pfr$1 = "#x1D52D";
var phiv$1 = "#x3D5";
var phmmat$1 = "#x2133";
var phone$1 = "#x260E";
var pitchfork$1 = "#x22D4";
var planck$1 = "#x210F";
var planckh$1 = "#x210E";
var plankv$1 = "#x210F";
var plusacir$1 = "#x2A23";
var plusb$1 = "#x229E";
var pluscir$1 = "#x2A22";
var plusdo$1 = "#x2214";
var plusdu$1 = "#x2A25";
var pluse$1 = "#x2A72";
var plussim$1 = "#x2A26";
var plustwo$1 = "#x2A27";
var pm$1 = "#xB1";
var pointint$1 = "#x2A15";
var popf$1 = "#x1D561";
var pr$1 = "#x227A";
var prE$1 = "#x2AB3";
var prap$1 = "#x2AB7";
var prcue$1 = "#x227C";
var pre$1 = "#x2AAF";
var prec$1 = "#x227A";
var precapprox$1 = "#x2AB7";
var preccurlyeq$1 = "#x227C";
var preceq$1 = "#x2AAF";
var precnapprox$1 = "#x2AB9";
var precneqq$1 = "#x2AB5";
var precnsim$1 = "#x22E8";
var precsim$1 = "#x227E";
var primes$1 = "#x2119";
var prnE$1 = "#x2AB5";
var prnap$1 = "#x2AB9";
var prnsim$1 = "#x22E8";
var profalar$1 = "#x232E";
var profline$1 = "#x2312";
var profsurf$1 = "#x2313";
var propto$1 = "#x221D";
var prsim$1 = "#x227E";
var prurel$1 = "#x22B0";
var pscr$1 = "#x1D4C5";
var puncsp$1 = "#x2008";
var qfr$1 = "#x1D52E";
var qint$1 = "#x2A0C";
var qopf$1 = "#x1D562";
var qprime$1 = "#x2057";
var qscr$1 = "#x1D4C6";
var quaternions$1 = "#x210D";
var quatint$1 = "#x2A16";
var questeq$1 = "#x225F";
var rAarr$1 = "#x21DB";
var rAtail$1 = "#x291C";
var rBarr$1 = "#x290F";
var rHar$1 = "#x2964";
var race$1 = "#x223D;&#x331";
var racute$1 = "#x155";
var raemptyv$1 = "#x29B3";
var rangd$1 = "#x2992";
var range$1 = "#x29A5";
var rangle$1 = "#x27E9";
var rarrap$1 = "#x2975";
var rarrb$1 = "#x21E5";
var rarrbfs$1 = "#x2920";
var rarrc$1 = "#x2933";
var rarrfs$1 = "#x291E";
var rarrhk$1 = "#x21AA";
var rarrlp$1 = "#x21AC";
var rarrpl$1 = "#x2945";
var rarrsim$1 = "#x2974";
var rarrtl$1 = "#x21A3";
var rarrw$1 = "#x219D";
var ratail$1 = "#x291A";
var ratio$1 = "#x2236";
var rationals$1 = "#x211A";
var rbarr$1 = "#x290D";
var rbbrk$1 = "#x2773";
var rbrke$1 = "#x298C";
var rbrksld$1 = "#x298E";
var rbrkslu$1 = "#x2990";
var rcaron$1 = "#x159";
var rcedil$1 = "#x157";
var rcy$1 = "#x440";
var rdca$1 = "#x2937";
var rdldhar$1 = "#x2969";
var rdquor$1 = "#x201D";
var rdsh$1 = "#x21B3";
var realine$1 = "#x211B";
var realpart$1 = "#x211C";
var reals$1 = "#x211D";
var rect$1 = "#x25AD";
var rfisht$1 = "#x297D";
var rfr$1 = "#x1D52F";
var rhard$1 = "#x21C1";
var rharu$1 = "#x21C0";
var rharul$1 = "#x296C";
var rhov$1 = "#x3F1";
var rightarrow$1 = "#x2192";
var rightarrowtail$1 = "#x21A3";
var rightharpoondown$1 = "#x21C1";
var rightharpoonup$1 = "#x21C0";
var rightleftarrows$1 = "#x21C4";
var rightleftharpoons$1 = "#x21CC";
var rightrightarrows$1 = "#x21C9";
var rightsquigarrow$1 = "#x219D";
var rightthreetimes$1 = "#x22CC";
var ring$1 = "#x2DA";
var risingdotseq$1 = "#x2253";
var rlarr$1 = "#x21C4";
var rlhar$1 = "#x21CC";
var rmoust$1 = "#x23B1";
var rmoustache$1 = "#x23B1";
var rnmid$1 = "#x2AEE";
var roang$1 = "#x27ED";
var roarr$1 = "#x21FE";
var robrk$1 = "#x27E7";
var ropar$1 = "#x2986";
var ropf$1 = "#x1D563";
var roplus$1 = "#x2A2E";
var rotimes$1 = "#x2A35";
var rpargt$1 = "#x2994";
var rppolint$1 = "#x2A12";
var rrarr$1 = "#x21C9";
var rscr$1 = "#x1D4C7";
var rsh$1 = "#x21B1";
var rsquor$1 = "#x2019";
var rthree$1 = "#x22CC";
var rtimes$1 = "#x22CA";
var rtri$1 = "#x25B9";
var rtrie$1 = "#x22B5";
var rtrif$1 = "#x25B8";
var rtriltri$1 = "#x29CE";
var ruluhar$1 = "#x2968";
var rx$1 = "#x211E";
var sacute$1 = "#x15B";
var sc$1 = "#x227B";
var scE$1 = "#x2AB4";
var scap$1 = "#x2AB8";
var sccue$1 = "#x227D";
var sce$1 = "#x2AB0";
var scedil$1 = "#x15F";
var scirc$1 = "#x15D";
var scnE$1 = "#x2AB6";
var scnap$1 = "#x2ABA";
var scnsim$1 = "#x22E9";
var scpolint$1 = "#x2A13";
var scsim$1 = "#x227F";
var scy$1 = "#x441";
var sdotb$1 = "#x22A1";
var sdote$1 = "#x2A66";
var seArr$1 = "#x21D8";
var searhk$1 = "#x2925";
var searr$1 = "#x2198";
var searrow$1 = "#x2198";
var seswar$1 = "#x2929";
var setminus$1 = "#x2216";
var setmn$1 = "#x2216";
var sext$1 = "#x2736";
var sfr$1 = "#x1D530";
var sfrown$1 = "#x2322";
var sharp$1 = "#x266F";
var shchcy$1 = "#x449";
var shcy$1 = "#x448";
var shortmid$1 = "#x2223";
var shortparallel$1 = "#x2225";
var sigmav$1 = "#x3C2";
var simdot$1 = "#x2A6A";
var sime$1 = "#x2243";
var simeq$1 = "#x2243";
var simg$1 = "#x2A9E";
var simgE$1 = "#x2AA0";
var siml$1 = "#x2A9D";
var simlE$1 = "#x2A9F";
var simne$1 = "#x2246";
var simplus$1 = "#x2A24";
var simrarr$1 = "#x2972";
var slarr$1 = "#x2190";
var smallsetminus$1 = "#x2216";
var smashp$1 = "#x2A33";
var smeparsl$1 = "#x29E4";
var smid$1 = "#x2223";
var smile$1 = "#x2323";
var smt$1 = "#x2AAA";
var smte$1 = "#x2AAC";
var smtes$1 = "#x2AAC;&#xFE00";
var softcy$1 = "#x44C";
var solb$1 = "#x29C4";
var solbar$1 = "#x233F";
var sopf$1 = "#x1D564";
var spadesuit$1 = "#x2660";
var spar$1 = "#x2225";
var sqcap$1 = "#x2293";
var sqcaps$1 = "#x2293;&#xFE00";
var sqcup$1 = "#x2294";
var sqcups$1 = "#x2294;&#xFE00";
var sqsub$1 = "#x228F";
var sqsube$1 = "#x2291";
var sqsubset$1 = "#x228F";
var sqsubseteq$1 = "#x2291";
var sqsup$1 = "#x2290";
var sqsupe$1 = "#x2292";
var sqsupset$1 = "#x2290";
var sqsupseteq$1 = "#x2292";
var squ$1 = "#x25A1";
var square$1 = "#x25A1";
var squarf$1 = "#x25AA";
var squf$1 = "#x25AA";
var srarr$1 = "#x2192";
var sscr$1 = "#x1D4C8";
var ssetmn$1 = "#x2216";
var ssmile$1 = "#x2323";
var sstarf$1 = "#x22C6";
var star$1 = "#x2606";
var starf$1 = "#x2605";
var straightepsilon$1 = "#x3F5";
var straightphi$1 = "#x3D5";
var strns$1 = "#xAF";
var subE$1 = "#x2AC5";
var subdot$1 = "#x2ABD";
var subedot$1 = "#x2AC3";
var submult$1 = "#x2AC1";
var subnE$1 = "#x2ACB";
var subne$1 = "#x228A";
var subplus$1 = "#x2ABF";
var subrarr$1 = "#x2979";
var subset$1 = "#x2282";
var subseteq$1 = "#x2286";
var subseteqq$1 = "#x2AC5";
var subsetneq$1 = "#x228A";
var subsetneqq$1 = "#x2ACB";
var subsim$1 = "#x2AC7";
var subsub$1 = "#x2AD5";
var subsup$1 = "#x2AD3";
var succ$1 = "#x227B";
var succapprox$1 = "#x2AB8";
var succcurlyeq$1 = "#x227D";
var succeq$1 = "#x2AB0";
var succnapprox$1 = "#x2ABA";
var succneqq$1 = "#x2AB6";
var succnsim$1 = "#x22E9";
var succsim$1 = "#x227F";
var sung$1 = "#x266A";
var supE$1 = "#x2AC6";
var supdot$1 = "#x2ABE";
var supdsub$1 = "#x2AD8";
var supedot$1 = "#x2AC4";
var suphsol$1 = "#x27C9";
var suphsub$1 = "#x2AD7";
var suplarr$1 = "#x297B";
var supmult$1 = "#x2AC2";
var supnE$1 = "#x2ACC";
var supne$1 = "#x228B";
var supplus$1 = "#x2AC0";
var supset$1 = "#x2283";
var supseteq$1 = "#x2287";
var supseteqq$1 = "#x2AC6";
var supsetneq$1 = "#x228B";
var supsetneqq$1 = "#x2ACC";
var supsim$1 = "#x2AC8";
var supsub$1 = "#x2AD4";
var supsup$1 = "#x2AD6";
var swArr$1 = "#x21D9";
var swarhk$1 = "#x2926";
var swarr$1 = "#x2199";
var swarrow$1 = "#x2199";
var swnwar$1 = "#x292A";
var target$1 = "#x2316";
var tbrk$1 = "#x23B4";
var tcaron$1 = "#x165";
var tcedil$1 = "#x163";
var tcy$1 = "#x442";
var tdot$1 = "#x20DB";
var telrec$1 = "#x2315";
var tfr$1 = "#x1D531";
var therefore$1 = "#x2234";
var thetav$1 = "#x3D1";
var thickapprox$1 = "#x2248";
var thicksim$1 = "#x223C";
var thkap$1 = "#x2248";
var thksim$1 = "#x223C";
var timesb$1 = "#x22A0";
var timesbar$1 = "#x2A31";
var timesd$1 = "#x2A30";
var tint$1 = "#x222D";
var toea$1 = "#x2928";
var top$1 = "#x22A4";
var topbot$1 = "#x2336";
var topcir$1 = "#x2AF1";
var topf$1 = "#x1D565";
var topfork$1 = "#x2ADA";
var tosa$1 = "#x2929";
var tprime$1 = "#x2034";
var triangle$1 = "#x25B5";
var triangledown$1 = "#x25BF";
var triangleleft$1 = "#x25C3";
var trianglelefteq$1 = "#x22B4";
var triangleq$1 = "#x225C";
var triangleright$1 = "#x25B9";
var trianglerighteq$1 = "#x22B5";
var tridot$1 = "#x25EC";
var trie$1 = "#x225C";
var triminus$1 = "#x2A3A";
var triplus$1 = "#x2A39";
var trisb$1 = "#x29CD";
var tritime$1 = "#x2A3B";
var trpezium$1 = "#x23E2";
var tscr$1 = "#x1D4C9";
var tscy$1 = "#x446";
var tshcy$1 = "#x45B";
var tstrok$1 = "#x167";
var twixt$1 = "#x226C";
var twoheadleftarrow$1 = "#x219E";
var twoheadrightarrow$1 = "#x21A0";
var uHar$1 = "#x2963";
var ubrcy$1 = "#x45E";
var ubreve$1 = "#x16D";
var ucy$1 = "#x443";
var udarr$1 = "#x21C5";
var udblac$1 = "#x171";
var udhar$1 = "#x296E";
var ufisht$1 = "#x297E";
var ufr$1 = "#x1D532";
var uharl$1 = "#x21BF";
var uharr$1 = "#x21BE";
var uhblk$1 = "#x2580";
var ulcorn$1 = "#x231C";
var ulcorner$1 = "#x231C";
var ulcrop$1 = "#x230F";
var ultri$1 = "#x25F8";
var umacr$1 = "#x16B";
var uogon$1 = "#x173";
var uopf$1 = "#x1D566";
var uparrow$1 = "#x2191";
var updownarrow$1 = "#x2195";
var upharpoonleft$1 = "#x21BF";
var upharpoonright$1 = "#x21BE";
var uplus$1 = "#x228E";
var upsi$1 = "#x3C5";
var upuparrows$1 = "#x21C8";
var urcorn$1 = "#x231D";
var urcorner$1 = "#x231D";
var urcrop$1 = "#x230E";
var uring$1 = "#x16F";
var urtri$1 = "#x25F9";
var uscr$1 = "#x1D4CA";
var utdot$1 = "#x22F0";
var utilde$1 = "#x169";
var utri$1 = "#x25B5";
var utrif$1 = "#x25B4";
var uuarr$1 = "#x21C8";
var uwangle$1 = "#x29A7";
var vArr$1 = "#x21D5";
var vBar$1 = "#x2AE8";
var vBarv$1 = "#x2AE9";
var vDash$1 = "#x22A8";
var vangrt$1 = "#x299C";
var varepsilon$1 = "#x3F5";
var varkappa$1 = "#x3F0";
var varnothing$1 = "#x2205";
var varphi$1 = "#x3D5";
var varpi$1 = "#x3D6";
var varpropto$1 = "#x221D";
var varr$1 = "#x2195";
var varrho$1 = "#x3F1";
var varsigma$1 = "#x3C2";
var varsubsetneq$1 = "#x228A;&#xFE00";
var varsubsetneqq$1 = "#x2ACB;&#xFE00";
var varsupsetneq$1 = "#x228B;&#xFE00";
var varsupsetneqq$1 = "#x2ACC;&#xFE00";
var vartheta$1 = "#x3D1";
var vartriangleleft$1 = "#x22B2";
var vartriangleright$1 = "#x22B3";
var vcy$1 = "#x432";
var vdash$1 = "#x22A2";
var vee$1 = "#x2228";
var veebar$1 = "#x22BB";
var veeeq$1 = "#x225A";
var vellip$1 = "#x22EE";
var vfr$1 = "#x1D533";
var vltri$1 = "#x22B2";
var vnsub$1 = "#x2282;&#x20D2";
var vnsup$1 = "#x2283;&#x20D2";
var vopf$1 = "#x1D567";
var vprop$1 = "#x221D";
var vrtri$1 = "#x22B3";
var vscr$1 = "#x1D4CB";
var vsubnE$1 = "#x2ACB;&#xFE00";
var vsubne$1 = "#x228A;&#xFE00";
var vsupnE$1 = "#x2ACC;&#xFE00";
var vsupne$1 = "#x228B;&#xFE00";
var vzigzag$1 = "#x299A";
var wcirc$1 = "#x175";
var wedbar$1 = "#x2A5F";
var wedge$1 = "#x2227";
var wedgeq$1 = "#x2259";
var wfr$1 = "#x1D534";
var wopf$1 = "#x1D568";
var wp$1 = "#x2118";
var wr$1 = "#x2240";
var wreath$1 = "#x2240";
var wscr$1 = "#x1D4CC";
var xcap$1 = "#x22C2";
var xcirc$1 = "#x25EF";
var xcup$1 = "#x22C3";
var xdtri$1 = "#x25BD";
var xfr$1 = "#x1D535";
var xhArr$1 = "#x27FA";
var xharr$1 = "#x27F7";
var xlArr$1 = "#x27F8";
var xlarr$1 = "#x27F5";
var xmap$1 = "#x27FC";
var xnis$1 = "#x22FB";
var xodot$1 = "#x2A00";
var xopf$1 = "#x1D569";
var xoplus$1 = "#x2A01";
var xotime$1 = "#x2A02";
var xrArr$1 = "#x27F9";
var xrarr$1 = "#x27F6";
var xscr$1 = "#x1D4CD";
var xsqcup$1 = "#x2A06";
var xuplus$1 = "#x2A04";
var xutri$1 = "#x25B3";
var xvee$1 = "#x22C1";
var xwedge$1 = "#x22C0";
var yacy$1 = "#x44F";
var ycirc$1 = "#x177";
var ycy$1 = "#x44B";
var yfr$1 = "#x1D536";
var yicy$1 = "#x457";
var yopf$1 = "#x1D56A";
var yscr$1 = "#x1D4CE";
var yucy$1 = "#x44E";
var zacute$1 = "#x17A";
var zcaron$1 = "#x17E";
var zcy$1 = "#x437";
var zdot$1 = "#x17C";
var zeetrf$1 = "#x2128";
var zfr$1 = "#x1D537";
var zhcy$1 = "#x436";
var zigrarr$1 = "#x21DD";
var zopf$1 = "#x1D56B";
var zscr$1 = "#x1D4CF";
var notEmailFriendly = {
	AMP: AMP$1,
	Abreve: Abreve$1,
	Acy: Acy$1,
	Afr: Afr$1,
	Amacr: Amacr$1,
	And: And$1,
	Aogon: Aogon$1,
	Aopf: Aopf$1,
	ApplyFunction: ApplyFunction$1,
	Ascr: Ascr$1,
	Assign: Assign$1,
	Backslash: Backslash$1,
	Barv: Barv$1,
	Barwed: Barwed$1,
	Bcy: Bcy$1,
	Because: Because$1,
	Bernoullis: Bernoullis$1,
	Bfr: Bfr$1,
	Bopf: Bopf$1,
	Breve: Breve$1,
	Bscr: Bscr$1,
	Bumpeq: Bumpeq$1,
	CHcy: CHcy$1,
	COPY: COPY$1,
	Cacute: Cacute$1,
	Cap: Cap$1,
	CapitalDifferentialD: CapitalDifferentialD$1,
	Cayleys: Cayleys$1,
	Ccaron: Ccaron$1,
	Ccirc: Ccirc$1,
	Cconint: Cconint$1,
	Cdot: Cdot$1,
	Cedilla: Cedilla$1,
	CenterDot: CenterDot$1,
	Cfr: Cfr$1,
	CircleDot: CircleDot$1,
	CircleMinus: CircleMinus$1,
	CirclePlus: CirclePlus$1,
	CircleTimes: CircleTimes$1,
	ClockwiseContourIntegral: ClockwiseContourIntegral$1,
	CloseCurlyDoubleQuote: CloseCurlyDoubleQuote$1,
	CloseCurlyQuote: CloseCurlyQuote$1,
	Colon: Colon$1,
	Colone: Colone$1,
	Congruent: Congruent$1,
	Conint: Conint$1,
	ContourIntegral: ContourIntegral$1,
	Copf: Copf$1,
	Coproduct: Coproduct$1,
	CounterClockwiseContourIntegral: CounterClockwiseContourIntegral$1,
	Cross: Cross$1,
	Cscr: Cscr$1,
	Cup: Cup$1,
	CupCap: CupCap$1,
	DD: DD$1,
	DDotrahd: DDotrahd$1,
	DJcy: DJcy$1,
	DScy: DScy$1,
	DZcy: DZcy$1,
	Darr: Darr$1,
	Dashv: Dashv$1,
	Dcaron: Dcaron$1,
	Dcy: Dcy$1,
	Del: Del$1,
	Dfr: Dfr$1,
	DiacriticalAcute: DiacriticalAcute$1,
	DiacriticalDot: DiacriticalDot$1,
	DiacriticalDoubleAcute: DiacriticalDoubleAcute$1,
	DiacriticalGrave: DiacriticalGrave$1,
	DiacriticalTilde: DiacriticalTilde$1,
	Diamond: Diamond$1,
	DifferentialD: DifferentialD$1,
	Dopf: Dopf$1,
	Dot: Dot$1,
	DotDot: DotDot$1,
	DotEqual: DotEqual$1,
	DoubleContourIntegral: DoubleContourIntegral$1,
	DoubleDot: DoubleDot$1,
	DoubleDownArrow: DoubleDownArrow$1,
	DoubleLeftArrow: DoubleLeftArrow$1,
	DoubleLeftRightArrow: DoubleLeftRightArrow$1,
	DoubleLeftTee: DoubleLeftTee$1,
	DoubleLongLeftArrow: DoubleLongLeftArrow$1,
	DoubleLongLeftRightArrow: DoubleLongLeftRightArrow$1,
	DoubleLongRightArrow: DoubleLongRightArrow$1,
	DoubleRightArrow: DoubleRightArrow$1,
	DoubleRightTee: DoubleRightTee$1,
	DoubleUpArrow: DoubleUpArrow$1,
	DoubleUpDownArrow: DoubleUpDownArrow$1,
	DoubleVerticalBar: DoubleVerticalBar$1,
	DownArrow: DownArrow$1,
	DownArrowBar: DownArrowBar$1,
	DownArrowUpArrow: DownArrowUpArrow$1,
	DownBreve: DownBreve$1,
	DownLeftRightVector: DownLeftRightVector$1,
	DownLeftTeeVector: DownLeftTeeVector$1,
	DownLeftVector: DownLeftVector$1,
	DownLeftVectorBar: DownLeftVectorBar$1,
	DownRightTeeVector: DownRightTeeVector$1,
	DownRightVector: DownRightVector$1,
	DownRightVectorBar: DownRightVectorBar$1,
	DownTee: DownTee$1,
	DownTeeArrow: DownTeeArrow$1,
	Downarrow: Downarrow$1,
	Dscr: Dscr$1,
	Dstrok: Dstrok$1,
	ENG: ENG$1,
	Ecaron: Ecaron$1,
	Ecy: Ecy$1,
	Edot: Edot$1,
	Efr: Efr$1,
	Element: Element$1,
	Emacr: Emacr$1,
	EmptySmallSquare: EmptySmallSquare$1,
	EmptyVerySmallSquare: EmptyVerySmallSquare$1,
	Eogon: Eogon$1,
	Eopf: Eopf$1,
	Equal: Equal$1,
	EqualTilde: EqualTilde$1,
	Equilibrium: Equilibrium$1,
	Escr: Escr$1,
	Esim: Esim$1,
	Exists: Exists$1,
	ExponentialE: ExponentialE$1,
	Fcy: Fcy$1,
	Ffr: Ffr$1,
	FilledSmallSquare: FilledSmallSquare$1,
	FilledVerySmallSquare: FilledVerySmallSquare$1,
	Fopf: Fopf$1,
	ForAll: ForAll$1,
	Fouriertrf: Fouriertrf$1,
	Fscr: Fscr$1,
	GJcy: GJcy$1,
	GT: GT$1,
	Gammad: Gammad$1,
	Gbreve: Gbreve$1,
	Gcedil: Gcedil$1,
	Gcirc: Gcirc$1,
	Gcy: Gcy$1,
	Gdot: Gdot$1,
	Gfr: Gfr$1,
	Gg: Gg$1,
	Gopf: Gopf$1,
	GreaterEqual: GreaterEqual$1,
	GreaterEqualLess: GreaterEqualLess$1,
	GreaterFullEqual: GreaterFullEqual$1,
	GreaterGreater: GreaterGreater$1,
	GreaterLess: GreaterLess$1,
	GreaterSlantEqual: GreaterSlantEqual$1,
	GreaterTilde: GreaterTilde$1,
	Gscr: Gscr$1,
	Gt: Gt$1,
	HARDcy: HARDcy$1,
	Hacek: Hacek$1,
	Hcirc: Hcirc$1,
	Hfr: Hfr$1,
	HilbertSpace: HilbertSpace$1,
	Hopf: Hopf$1,
	HorizontalLine: HorizontalLine$1,
	Hscr: Hscr$1,
	Hstrok: Hstrok$1,
	HumpDownHump: HumpDownHump$1,
	HumpEqual: HumpEqual$1,
	IEcy: IEcy$1,
	IJlig: IJlig$1,
	IOcy: IOcy$1,
	Icy: Icy$1,
	Idot: Idot$1,
	Ifr: Ifr$1,
	Im: Im$1,
	Imacr: Imacr$1,
	ImaginaryI: ImaginaryI$1,
	Implies: Implies$1,
	Int: Int$1,
	Integral: Integral$1,
	Intersection: Intersection$1,
	InvisibleComma: InvisibleComma$1,
	InvisibleTimes: InvisibleTimes$1,
	Iogon: Iogon$1,
	Iopf: Iopf$1,
	Iscr: Iscr$1,
	Itilde: Itilde$1,
	Iukcy: Iukcy$1,
	Jcirc: Jcirc$1,
	Jcy: Jcy$1,
	Jfr: Jfr$1,
	Jopf: Jopf$1,
	Jscr: Jscr$1,
	Jsercy: Jsercy$1,
	Jukcy: Jukcy$1,
	KHcy: KHcy$1,
	KJcy: KJcy$1,
	Kcedil: Kcedil$1,
	Kcy: Kcy$1,
	Kfr: Kfr$1,
	Kopf: Kopf$1,
	Kscr: Kscr$1,
	LJcy: LJcy$1,
	LT: LT$1,
	Lacute: Lacute$1,
	Lang: Lang$1,
	Laplacetrf: Laplacetrf$1,
	Larr: Larr$1,
	Lcaron: Lcaron$1,
	Lcedil: Lcedil$1,
	Lcy: Lcy$1,
	LeftAngleBracket: LeftAngleBracket$1,
	LeftArrow: LeftArrow$1,
	LeftArrowBar: LeftArrowBar$1,
	LeftArrowRightArrow: LeftArrowRightArrow$1,
	LeftCeiling: LeftCeiling$1,
	LeftDoubleBracket: LeftDoubleBracket$1,
	LeftDownTeeVector: LeftDownTeeVector$1,
	LeftDownVector: LeftDownVector$1,
	LeftDownVectorBar: LeftDownVectorBar$1,
	LeftFloor: LeftFloor$1,
	LeftRightArrow: LeftRightArrow$1,
	LeftRightVector: LeftRightVector$1,
	LeftTee: LeftTee$1,
	LeftTeeArrow: LeftTeeArrow$1,
	LeftTeeVector: LeftTeeVector$1,
	LeftTriangle: LeftTriangle$1,
	LeftTriangleBar: LeftTriangleBar$1,
	LeftTriangleEqual: LeftTriangleEqual$1,
	LeftUpDownVector: LeftUpDownVector$1,
	LeftUpTeeVector: LeftUpTeeVector$1,
	LeftUpVector: LeftUpVector$1,
	LeftUpVectorBar: LeftUpVectorBar$1,
	LeftVector: LeftVector$1,
	LeftVectorBar: LeftVectorBar$1,
	Leftarrow: Leftarrow$1,
	Leftrightarrow: Leftrightarrow$1,
	LessEqualGreater: LessEqualGreater$1,
	LessFullEqual: LessFullEqual$1,
	LessGreater: LessGreater$1,
	LessLess: LessLess$1,
	LessSlantEqual: LessSlantEqual$1,
	LessTilde: LessTilde$1,
	Lfr: Lfr$1,
	Ll: Ll$1,
	Lleftarrow: Lleftarrow$1,
	Lmidot: Lmidot$1,
	LongLeftArrow: LongLeftArrow$1,
	LongLeftRightArrow: LongLeftRightArrow$1,
	LongRightArrow: LongRightArrow$1,
	Longleftarrow: Longleftarrow$1,
	Longleftrightarrow: Longleftrightarrow$1,
	Longrightarrow: Longrightarrow$1,
	Lopf: Lopf$1,
	LowerLeftArrow: LowerLeftArrow$1,
	LowerRightArrow: LowerRightArrow$1,
	Lscr: Lscr$1,
	Lsh: Lsh$1,
	Lstrok: Lstrok$1,
	Lt: Lt$1,
	"Map": "#x2905",
	Mcy: Mcy$1,
	MediumSpace: MediumSpace$1,
	Mellintrf: Mellintrf$1,
	Mfr: Mfr$1,
	MinusPlus: MinusPlus$1,
	Mopf: Mopf$1,
	Mscr: Mscr$1,
	NJcy: NJcy$1,
	Nacute: Nacute$1,
	Ncaron: Ncaron$1,
	Ncedil: Ncedil$1,
	Ncy: Ncy$1,
	NegativeMediumSpace: NegativeMediumSpace$1,
	NegativeThickSpace: NegativeThickSpace$1,
	NegativeThinSpace: NegativeThinSpace$1,
	NegativeVeryThinSpace: NegativeVeryThinSpace$1,
	NestedGreaterGreater: NestedGreaterGreater$1,
	NestedLessLess: NestedLessLess$1,
	Nfr: Nfr$1,
	NoBreak: NoBreak$1,
	NonBreakingSpace: NonBreakingSpace$1,
	Nopf: Nopf$1,
	Not: Not$1,
	NotCongruent: NotCongruent$1,
	NotCupCap: NotCupCap$1,
	NotDoubleVerticalBar: NotDoubleVerticalBar$1,
	NotElement: NotElement$1,
	NotEqual: NotEqual$1,
	NotEqualTilde: NotEqualTilde$1,
	NotExists: NotExists$1,
	NotGreater: NotGreater$1,
	NotGreaterEqual: NotGreaterEqual$1,
	NotGreaterFullEqual: NotGreaterFullEqual$1,
	NotGreaterGreater: NotGreaterGreater$1,
	NotGreaterLess: NotGreaterLess$1,
	NotGreaterSlantEqual: NotGreaterSlantEqual$1,
	NotGreaterTilde: NotGreaterTilde$1,
	NotHumpDownHump: NotHumpDownHump$1,
	NotHumpEqual: NotHumpEqual$1,
	NotLeftTriangle: NotLeftTriangle$1,
	NotLeftTriangleBar: NotLeftTriangleBar$1,
	NotLeftTriangleEqual: NotLeftTriangleEqual$1,
	NotLess: NotLess$1,
	NotLessEqual: NotLessEqual$1,
	NotLessGreater: NotLessGreater$1,
	NotLessLess: NotLessLess$1,
	NotLessSlantEqual: NotLessSlantEqual$1,
	NotLessTilde: NotLessTilde$1,
	NotNestedGreaterGreater: NotNestedGreaterGreater$1,
	NotNestedLessLess: NotNestedLessLess$1,
	NotPrecedes: NotPrecedes$1,
	NotPrecedesEqual: NotPrecedesEqual$1,
	NotPrecedesSlantEqual: NotPrecedesSlantEqual$1,
	NotReverseElement: NotReverseElement$1,
	NotRightTriangle: NotRightTriangle$1,
	NotRightTriangleBar: NotRightTriangleBar$1,
	NotRightTriangleEqual: NotRightTriangleEqual$1,
	NotSquareSubset: NotSquareSubset$1,
	NotSquareSubsetEqual: NotSquareSubsetEqual$1,
	NotSquareSuperset: NotSquareSuperset$1,
	NotSquareSupersetEqual: NotSquareSupersetEqual$1,
	NotSubset: NotSubset$1,
	NotSubsetEqual: NotSubsetEqual$1,
	NotSucceeds: NotSucceeds$1,
	NotSucceedsEqual: NotSucceedsEqual$1,
	NotSucceedsSlantEqual: NotSucceedsSlantEqual$1,
	NotSucceedsTilde: NotSucceedsTilde$1,
	NotSuperset: NotSuperset$1,
	NotSupersetEqual: NotSupersetEqual$1,
	NotTilde: NotTilde$1,
	NotTildeEqual: NotTildeEqual$1,
	NotTildeFullEqual: NotTildeFullEqual$1,
	NotTildeTilde: NotTildeTilde$1,
	NotVerticalBar: NotVerticalBar$1,
	Nscr: Nscr$1,
	Ocy: Ocy$1,
	Odblac: Odblac$1,
	Ofr: Ofr$1,
	Omacr: Omacr$1,
	Oopf: Oopf$1,
	OpenCurlyDoubleQuote: OpenCurlyDoubleQuote$1,
	OpenCurlyQuote: OpenCurlyQuote$1,
	Or: Or$1,
	Oscr: Oscr$1,
	Otimes: Otimes$1,
	OverBar: OverBar$1,
	OverBrace: OverBrace$1,
	OverBracket: OverBracket$1,
	OverParenthesis: OverParenthesis$1,
	PartialD: PartialD$1,
	Pcy: Pcy$1,
	Pfr: Pfr$1,
	PlusMinus: PlusMinus$1,
	Poincareplane: Poincareplane$1,
	Popf: Popf$1,
	Pr: Pr$1,
	Precedes: Precedes$1,
	PrecedesEqual: PrecedesEqual$1,
	PrecedesSlantEqual: PrecedesSlantEqual$1,
	PrecedesTilde: PrecedesTilde$1,
	Product: Product$1,
	Proportion: Proportion$1,
	Proportional: Proportional$1,
	Pscr: Pscr$1,
	QUOT: QUOT$1,
	Qfr: Qfr$1,
	Qopf: Qopf$1,
	Qscr: Qscr$1,
	RBarr: RBarr$1,
	REG: REG$1,
	Racute: Racute$1,
	Rang: Rang$1,
	Rarr: Rarr$1,
	Rarrtl: Rarrtl$1,
	Rcaron: Rcaron$1,
	Rcedil: Rcedil$1,
	Rcy: Rcy$1,
	Re: Re$1,
	ReverseElement: ReverseElement$1,
	ReverseEquilibrium: ReverseEquilibrium$1,
	ReverseUpEquilibrium: ReverseUpEquilibrium$1,
	Rfr: Rfr$1,
	RightAngleBracket: RightAngleBracket$1,
	RightArrow: RightArrow$1,
	RightArrowBar: RightArrowBar$1,
	RightArrowLeftArrow: RightArrowLeftArrow$1,
	RightCeiling: RightCeiling$1,
	RightDoubleBracket: RightDoubleBracket$1,
	RightDownTeeVector: RightDownTeeVector$1,
	RightDownVector: RightDownVector$1,
	RightDownVectorBar: RightDownVectorBar$1,
	RightFloor: RightFloor$1,
	RightTee: RightTee$1,
	RightTeeArrow: RightTeeArrow$1,
	RightTeeVector: RightTeeVector$1,
	RightTriangle: RightTriangle$1,
	RightTriangleBar: RightTriangleBar$1,
	RightTriangleEqual: RightTriangleEqual$1,
	RightUpDownVector: RightUpDownVector$1,
	RightUpTeeVector: RightUpTeeVector$1,
	RightUpVector: RightUpVector$1,
	RightUpVectorBar: RightUpVectorBar$1,
	RightVector: RightVector$1,
	RightVectorBar: RightVectorBar$1,
	Rightarrow: Rightarrow$1,
	Ropf: Ropf$1,
	RoundImplies: RoundImplies$1,
	Rrightarrow: Rrightarrow$1,
	Rscr: Rscr$1,
	Rsh: Rsh$1,
	RuleDelayed: RuleDelayed$1,
	SHCHcy: SHCHcy$1,
	SHcy: SHcy$1,
	SOFTcy: SOFTcy$1,
	Sacute: Sacute$1,
	Sc: Sc$1,
	Scedil: Scedil$1,
	Scirc: Scirc$1,
	Scy: Scy$1,
	Sfr: Sfr$1,
	ShortDownArrow: ShortDownArrow$1,
	ShortLeftArrow: ShortLeftArrow$1,
	ShortRightArrow: ShortRightArrow$1,
	ShortUpArrow: ShortUpArrow$1,
	SmallCircle: SmallCircle$1,
	Sopf: Sopf$1,
	Sqrt: Sqrt$1,
	Square: Square$1,
	SquareIntersection: SquareIntersection$1,
	SquareSubset: SquareSubset$1,
	SquareSubsetEqual: SquareSubsetEqual$1,
	SquareSuperset: SquareSuperset$1,
	SquareSupersetEqual: SquareSupersetEqual$1,
	SquareUnion: SquareUnion$1,
	Sscr: Sscr$1,
	Star: Star$1,
	Sub: Sub$1,
	Subset: Subset$1,
	SubsetEqual: SubsetEqual$1,
	Succeeds: Succeeds$1,
	SucceedsEqual: SucceedsEqual$1,
	SucceedsSlantEqual: SucceedsSlantEqual$1,
	SucceedsTilde: SucceedsTilde$1,
	SuchThat: SuchThat$1,
	Sum: Sum$1,
	Sup: Sup$1,
	Superset: Superset$1,
	SupersetEqual: SupersetEqual$1,
	Supset: Supset$1,
	TRADE: TRADE$1,
	TSHcy: TSHcy$1,
	TScy: TScy$1,
	Tab: Tab$1,
	Tcaron: Tcaron$1,
	Tcedil: Tcedil$1,
	Tcy: Tcy$1,
	Tfr: Tfr$1,
	Therefore: Therefore$1,
	ThickSpace: ThickSpace$1,
	ThinSpace: ThinSpace$1,
	Tilde: Tilde$1,
	TildeEqual: TildeEqual$1,
	TildeFullEqual: TildeFullEqual$1,
	TildeTilde: TildeTilde$1,
	Topf: Topf$1,
	TripleDot: TripleDot$1,
	Tscr: Tscr$1,
	Tstrok: Tstrok$1,
	Uarr: Uarr$1,
	Uarrocir: Uarrocir$1,
	Ubrcy: Ubrcy$1,
	Ubreve: Ubreve$1,
	Ucy: Ucy$1,
	Udblac: Udblac$1,
	Ufr: Ufr$1,
	Umacr: Umacr$1,
	UnderBrace: UnderBrace$1,
	UnderBracket: UnderBracket$1,
	UnderParenthesis: UnderParenthesis$1,
	Union: Union$1,
	UnionPlus: UnionPlus$1,
	Uogon: Uogon$1,
	Uopf: Uopf$1,
	UpArrow: UpArrow$1,
	UpArrowBar: UpArrowBar$1,
	UpArrowDownArrow: UpArrowDownArrow$1,
	UpDownArrow: UpDownArrow$1,
	UpEquilibrium: UpEquilibrium$1,
	UpTee: UpTee$1,
	UpTeeArrow: UpTeeArrow$1,
	Uparrow: Uparrow$1,
	Updownarrow: Updownarrow$1,
	UpperLeftArrow: UpperLeftArrow$1,
	UpperRightArrow: UpperRightArrow$1,
	Upsi: Upsi$1,
	Uring: Uring$1,
	Uscr: Uscr$1,
	Utilde: Utilde$1,
	VDash: VDash$1,
	Vbar: Vbar$1,
	Vcy: Vcy$1,
	Vdash: Vdash$1,
	Vdashl: Vdashl$1,
	Vee: Vee$1,
	Verbar: Verbar$1,
	Vert: Vert$1,
	VerticalBar: VerticalBar$1,
	VerticalSeparator: VerticalSeparator$1,
	VerticalTilde: VerticalTilde$1,
	VeryThinSpace: VeryThinSpace$1,
	Vfr: Vfr$1,
	Vopf: Vopf$1,
	Vscr: Vscr$1,
	Vvdash: Vvdash$1,
	Wcirc: Wcirc$1,
	Wedge: Wedge$1,
	Wfr: Wfr$1,
	Wopf: Wopf$1,
	Wscr: Wscr$1,
	Xfr: Xfr$1,
	Xopf: Xopf$1,
	Xscr: Xscr$1,
	YAcy: YAcy$1,
	YIcy: YIcy$1,
	YUcy: YUcy$1,
	Ycirc: Ycirc$1,
	Ycy: Ycy$1,
	Yfr: Yfr$1,
	Yopf: Yopf$1,
	Yscr: Yscr$1,
	ZHcy: ZHcy$1,
	Zacute: Zacute$1,
	Zcaron: Zcaron$1,
	Zcy: Zcy$1,
	Zdot: Zdot$1,
	ZeroWidthSpace: ZeroWidthSpace$1,
	Zfr: Zfr$1,
	Zopf: Zopf$1,
	Zscr: Zscr$1,
	abreve: abreve$1,
	ac: ac$1,
	acE: acE$1,
	acd: acd$1,
	acy: acy$1,
	af: af$1,
	afr: afr$1,
	aleph: aleph$1,
	amacr: amacr$1,
	amalg: amalg$1,
	andand: andand$1,
	andd: andd$1,
	andslope: andslope$1,
	andv: andv$1,
	ange: ange$1,
	angle: angle$1,
	angmsd: angmsd$1,
	angmsdaa: angmsdaa$1,
	angmsdab: angmsdab$1,
	angmsdac: angmsdac$1,
	angmsdad: angmsdad$1,
	angmsdae: angmsdae$1,
	angmsdaf: angmsdaf$1,
	angmsdag: angmsdag$1,
	angmsdah: angmsdah$1,
	angrt: angrt$1,
	angrtvb: angrtvb$1,
	angrtvbd: angrtvbd$1,
	angsph: angsph$1,
	angst: angst$1,
	angzarr: angzarr$1,
	aogon: aogon$1,
	aopf: aopf$1,
	ap: ap$1,
	apE: apE$1,
	apacir: apacir$1,
	ape: ape$1,
	apid: apid$1,
	approx: approx$1,
	approxeq: approxeq$1,
	ascr: ascr$1,
	asympeq: asympeq$1,
	awconint: awconint$1,
	awint: awint$1,
	bNot: bNot$1,
	backcong: backcong$1,
	backepsilon: backepsilon$1,
	backprime: backprime$1,
	backsim: backsim$1,
	backsimeq: backsimeq$1,
	barvee: barvee$1,
	barwed: barwed$1,
	barwedge: barwedge$1,
	bbrk: bbrk$1,
	bbrktbrk: bbrktbrk$1,
	bcong: bcong$1,
	bcy: bcy$1,
	becaus: becaus$1,
	because: because$1,
	bemptyv: bemptyv$1,
	bepsi: bepsi$1,
	bernou: bernou$1,
	beth: beth$1,
	between: between$1,
	bfr: bfr$1,
	bigcap: bigcap$1,
	bigcirc: bigcirc$1,
	bigcup: bigcup$1,
	bigodot: bigodot$1,
	bigoplus: bigoplus$1,
	bigotimes: bigotimes$1,
	bigsqcup: bigsqcup$1,
	bigstar: bigstar$1,
	bigtriangledown: bigtriangledown$1,
	bigtriangleup: bigtriangleup$1,
	biguplus: biguplus$1,
	bigvee: bigvee$1,
	bigwedge: bigwedge$1,
	bkarow: bkarow$1,
	blacklozenge: blacklozenge$1,
	blacksquare: blacksquare$1,
	blacktriangle: blacktriangle$1,
	blacktriangledown: blacktriangledown$1,
	blacktriangleleft: blacktriangleleft$1,
	blacktriangleright: blacktriangleright$1,
	blank: blank$1,
	blk12: blk12$1,
	blk14: blk14$1,
	blk34: blk34$1,
	block: block$1,
	bne: bne$1,
	bnequiv: bnequiv$1,
	bnot: bnot$1,
	bopf: bopf$1,
	bot: bot$1,
	bottom: bottom$1,
	bowtie: bowtie$1,
	boxDL: boxDL$1,
	boxDR: boxDR$1,
	boxDl: boxDl$1,
	boxDr: boxDr$1,
	boxH: boxH$1,
	boxHD: boxHD$1,
	boxHU: boxHU$1,
	boxHd: boxHd$1,
	boxHu: boxHu$1,
	boxUL: boxUL$1,
	boxUR: boxUR$1,
	boxUl: boxUl$1,
	boxUr: boxUr$1,
	boxV: boxV$1,
	boxVH: boxVH$1,
	boxVL: boxVL$1,
	boxVR: boxVR$1,
	boxVh: boxVh$1,
	boxVl: boxVl$1,
	boxVr: boxVr$1,
	boxbox: boxbox$1,
	boxdL: boxdL$1,
	boxdR: boxdR$1,
	boxdl: boxdl$1,
	boxdr: boxdr$1,
	boxh: boxh$1,
	boxhD: boxhD$1,
	boxhU: boxhU$1,
	boxhd: boxhd$1,
	boxhu: boxhu$1,
	boxminus: boxminus$1,
	boxplus: boxplus$1,
	boxtimes: boxtimes$1,
	boxuL: boxuL$1,
	boxuR: boxuR$1,
	boxul: boxul$1,
	boxur: boxur$1,
	boxv: boxv$1,
	boxvH: boxvH$1,
	boxvL: boxvL$1,
	boxvR: boxvR$1,
	boxvh: boxvh$1,
	boxvl: boxvl$1,
	boxvr: boxvr$1,
	bprime: bprime$1,
	breve: breve$1,
	bscr: bscr$1,
	bsemi: bsemi$1,
	bsim: bsim$1,
	bsime: bsime$1,
	bsolb: bsolb$1,
	bsolhsub: bsolhsub$1,
	bullet: bullet$1,
	bump: bump$1,
	bumpE: bumpE$1,
	bumpe: bumpe$1,
	bumpeq: bumpeq$1,
	cacute: cacute$1,
	capand: capand$1,
	capbrcup: capbrcup$1,
	capcap: capcap$1,
	capcup: capcup$1,
	capdot: capdot$1,
	caps: caps$1,
	caret: caret$1,
	caron: caron$1,
	ccaps: ccaps$1,
	ccaron: ccaron$1,
	ccirc: ccirc$1,
	ccups: ccups$1,
	ccupssm: ccupssm$1,
	cdot: cdot$1,
	cemptyv: cemptyv$1,
	centerdot: centerdot$1,
	cfr: cfr$1,
	chcy: chcy$1,
	check: check$1,
	checkmark: checkmark$1,
	cir: cir$1,
	cirE: cirE$1,
	circeq: circeq$1,
	circlearrowleft: circlearrowleft$1,
	circlearrowright: circlearrowright$1,
	circledR: circledR$1,
	circledS: circledS$1,
	circledast: circledast$1,
	circledcirc: circledcirc$1,
	circleddash: circleddash$1,
	cire: cire$1,
	cirfnint: cirfnint$1,
	cirmid: cirmid$1,
	cirscir: cirscir$1,
	clubsuit: clubsuit$1,
	colone: colone$1,
	coloneq: coloneq$1,
	comp: comp$1,
	compfn: compfn$1,
	complement: complement$1,
	complexes: complexes$1,
	congdot: congdot$1,
	conint: conint$1,
	copf: copf$1,
	coprod: coprod$1,
	copysr: copysr$1,
	cross: cross$1,
	cscr: cscr$1,
	csub: csub$1,
	csube: csube$1,
	csup: csup$1,
	csupe: csupe$1,
	ctdot: ctdot$1,
	cudarrl: cudarrl$1,
	cudarrr: cudarrr$1,
	cuepr: cuepr$1,
	cuesc: cuesc$1,
	cularr: cularr$1,
	cularrp: cularrp$1,
	cupbrcap: cupbrcap$1,
	cupcap: cupcap$1,
	cupcup: cupcup$1,
	cupdot: cupdot$1,
	cupor: cupor$1,
	cups: cups$1,
	curarr: curarr$1,
	curarrm: curarrm$1,
	curlyeqprec: curlyeqprec$1,
	curlyeqsucc: curlyeqsucc$1,
	curlyvee: curlyvee$1,
	curlywedge: curlywedge$1,
	curvearrowleft: curvearrowleft$1,
	curvearrowright: curvearrowright$1,
	cuvee: cuvee$1,
	cuwed: cuwed$1,
	cwconint: cwconint$1,
	cwint: cwint$1,
	cylcty: cylcty$1,
	dHar: dHar$1,
	daleth: daleth$1,
	dash: dash$1,
	dashv: dashv$1,
	dbkarow: dbkarow$1,
	dblac: dblac$1,
	dcaron: dcaron$1,
	dcy: dcy$1,
	dd: dd$1,
	ddagger: ddagger$1,
	ddarr: ddarr$1,
	ddotseq: ddotseq$1,
	demptyv: demptyv$1,
	dfisht: dfisht$1,
	dfr: dfr$1,
	dharl: dharl$1,
	dharr: dharr$1,
	diam: diam$1,
	diamond: diamond$1,
	diamondsuit: diamondsuit$1,
	die: die$1,
	digamma: digamma$1,
	disin: disin$1,
	div: div$1,
	divideontimes: divideontimes$1,
	divonx: divonx$1,
	djcy: djcy$1,
	dlcorn: dlcorn$1,
	dlcrop: dlcrop$1,
	dopf: dopf$1,
	dot: dot$1,
	doteq: doteq$1,
	doteqdot: doteqdot$1,
	dotminus: dotminus$1,
	dotplus: dotplus$1,
	dotsquare: dotsquare$1,
	doublebarwedge: doublebarwedge$1,
	downarrow: downarrow$1,
	downdownarrows: downdownarrows$1,
	downharpoonleft: downharpoonleft$1,
	downharpoonright: downharpoonright$1,
	drbkarow: drbkarow$1,
	drcorn: drcorn$1,
	drcrop: drcrop$1,
	dscr: dscr$1,
	dscy: dscy$1,
	dsol: dsol$1,
	dstrok: dstrok$1,
	dtdot: dtdot$1,
	dtri: dtri$1,
	dtrif: dtrif$1,
	duarr: duarr$1,
	duhar: duhar$1,
	dwangle: dwangle$1,
	dzcy: dzcy$1,
	dzigrarr: dzigrarr$1,
	eDDot: eDDot$1,
	eDot: eDot$1,
	easter: easter$1,
	ecaron: ecaron$1,
	ecir: ecir$1,
	ecolon: ecolon$1,
	ecy: ecy$1,
	edot: edot$1,
	ee: ee$1,
	efDot: efDot$1,
	efr: efr$1,
	eg: eg$1,
	egs: egs$1,
	egsdot: egsdot$1,
	el: el$1,
	elinters: elinters$1,
	ell: ell$1,
	els: els$1,
	elsdot: elsdot$1,
	emacr: emacr$1,
	emptyset: emptyset$1,
	emptyv: emptyv$1,
	emsp13: emsp13$1,
	emsp14: emsp14$1,
	eng: eng$1,
	eogon: eogon$1,
	eopf: eopf$1,
	epar: epar$1,
	eparsl: eparsl$1,
	eplus: eplus$1,
	epsi: epsi$1,
	epsiv: epsiv$1,
	eqcirc: eqcirc$1,
	eqcolon: eqcolon$1,
	eqsim: eqsim$1,
	eqslantgtr: eqslantgtr$1,
	eqslantless: eqslantless$1,
	equest: equest$1,
	equivDD: equivDD$1,
	eqvparsl: eqvparsl$1,
	erDot: erDot$1,
	erarr: erarr$1,
	escr: escr$1,
	esdot: esdot$1,
	esim: esim$1,
	expectation: expectation$1,
	exponentiale: exponentiale$1,
	fallingdotseq: fallingdotseq$1,
	fcy: fcy$1,
	female: female$1,
	ffilig: ffilig$1,
	fflig: fflig$1,
	ffllig: ffllig$1,
	ffr: ffr$1,
	filig: filig$1,
	flat: flat$1,
	fllig: fllig$1,
	fltns: fltns$1,
	fopf: fopf$1,
	fork: fork$1,
	forkv: forkv$1,
	fpartint: fpartint$1,
	frac13: frac13$1,
	frac15: frac15$1,
	frac16: frac16$1,
	frac18: frac18$1,
	frac23: frac23$1,
	frac25: frac25$1,
	frac35: frac35$1,
	frac38: frac38$1,
	frac45: frac45$1,
	frac56: frac56$1,
	frac58: frac58$1,
	frac78: frac78$1,
	frown: frown$1,
	fscr: fscr$1,
	gE: gE$1,
	gEl: gEl$1,
	gacute: gacute$1,
	gammad: gammad$1,
	gap: gap$1,
	gbreve: gbreve$1,
	gcirc: gcirc$1,
	gcy: gcy$1,
	gdot: gdot$1,
	gel: gel$1,
	geq: geq$1,
	geqq: geqq$1,
	geqslant: geqslant$1,
	ges: ges$1,
	gescc: gescc$1,
	gesdot: gesdot$1,
	gesdoto: gesdoto$1,
	gesdotol: gesdotol$1,
	gesl: gesl$1,
	gesles: gesles$1,
	gfr: gfr$1,
	gg: gg$1,
	ggg: ggg$1,
	gimel: gimel$1,
	gjcy: gjcy$1,
	gl: gl$1,
	glE: glE$1,
	gla: gla$1,
	glj: glj$1,
	gnE: gnE$1,
	gnap: gnap$1,
	gnapprox: gnapprox$1,
	gne: gne$1,
	gneq: gneq$1,
	gneqq: gneqq$1,
	gnsim: gnsim$1,
	gopf: gopf$1,
	grave: grave$1,
	gscr: gscr$1,
	gsim: gsim$1,
	gsime: gsime$1,
	gsiml: gsiml$1,
	gtcc: gtcc$1,
	gtcir: gtcir$1,
	gtdot: gtdot$1,
	gtlPar: gtlPar$1,
	gtquest: gtquest$1,
	gtrapprox: gtrapprox$1,
	gtrarr: gtrarr$1,
	gtrdot: gtrdot$1,
	gtreqless: gtreqless$1,
	gtreqqless: gtreqqless$1,
	gtrless: gtrless$1,
	gtrsim: gtrsim$1,
	gvertneqq: gvertneqq$1,
	gvnE: gvnE$1,
	hairsp: hairsp$1,
	half: half$1,
	hamilt: hamilt$1,
	hardcy: hardcy$1,
	harrcir: harrcir$1,
	harrw: harrw$1,
	hbar: hbar$1,
	hcirc: hcirc$1,
	heartsuit: heartsuit$1,
	hercon: hercon$1,
	hfr: hfr$1,
	hksearow: hksearow$1,
	hkswarow: hkswarow$1,
	hoarr: hoarr$1,
	homtht: homtht$1,
	hookleftarrow: hookleftarrow$1,
	hookrightarrow: hookrightarrow$1,
	hopf: hopf$1,
	horbar: horbar$1,
	hscr: hscr$1,
	hslash: hslash$1,
	hstrok: hstrok$1,
	hybull: hybull$1,
	hyphen: hyphen$1,
	ic: ic$1,
	icy: icy$1,
	iecy: iecy$1,
	iff: iff$1,
	ifr: ifr$1,
	ii: ii$1,
	iiiint: iiiint$1,
	iiint: iiint$1,
	iinfin: iinfin$1,
	iiota: iiota$1,
	ijlig: ijlig$1,
	imacr: imacr$1,
	imagline: imagline$1,
	imagpart: imagpart$1,
	imath: imath$1,
	imof: imof$1,
	imped: imped$1,
	"in": "#x2208",
	incare: incare$1,
	infintie: infintie$1,
	inodot: inodot$1,
	intcal: intcal$1,
	integers: integers$1,
	intercal: intercal$1,
	intlarhk: intlarhk$1,
	intprod: intprod$1,
	iocy: iocy$1,
	iogon: iogon$1,
	iopf: iopf$1,
	iprod: iprod$1,
	iscr: iscr$1,
	isinE: isinE$1,
	isindot: isindot$1,
	isins: isins$1,
	isinsv: isinsv$1,
	isinv: isinv$1,
	it: it$1,
	itilde: itilde$1,
	iukcy: iukcy$1,
	jcirc: jcirc$1,
	jcy: jcy$1,
	jfr: jfr$1,
	jmath: jmath$1,
	jopf: jopf$1,
	jscr: jscr$1,
	jsercy: jsercy$1,
	jukcy: jukcy$1,
	kappav: kappav$1,
	kcedil: kcedil$1,
	kcy: kcy$1,
	kfr: kfr$1,
	kgreen: kgreen$1,
	khcy: khcy$1,
	kjcy: kjcy$1,
	kopf: kopf$1,
	kscr: kscr$1,
	lAarr: lAarr$1,
	lAtail: lAtail$1,
	lBarr: lBarr$1,
	lE: lE$1,
	lEg: lEg$1,
	lHar: lHar$1,
	lacute: lacute$1,
	laemptyv: laemptyv$1,
	lagran: lagran$1,
	langd: langd$1,
	langle: langle$1,
	lap: lap$1,
	larrb: larrb$1,
	larrbfs: larrbfs$1,
	larrfs: larrfs$1,
	larrhk: larrhk$1,
	larrlp: larrlp$1,
	larrpl: larrpl$1,
	larrsim: larrsim$1,
	larrtl: larrtl$1,
	lat: lat$1,
	latail: latail$1,
	late: late$1,
	lates: lates$1,
	lbarr: lbarr$1,
	lbbrk: lbbrk$1,
	lbrace: lbrace$1,
	lbrack: lbrack$1,
	lbrke: lbrke$1,
	lbrksld: lbrksld$1,
	lbrkslu: lbrkslu$1,
	lcaron: lcaron$1,
	lcedil: lcedil$1,
	lcub: lcub$1,
	lcy: lcy$1,
	ldca: ldca$1,
	ldquor: ldquor$1,
	ldrdhar: ldrdhar$1,
	ldrushar: ldrushar$1,
	ldsh: ldsh$1,
	leftarrow: leftarrow$1,
	leftarrowtail: leftarrowtail$1,
	leftharpoondown: leftharpoondown$1,
	leftharpoonup: leftharpoonup$1,
	leftleftarrows: leftleftarrows$1,
	leftrightarrow: leftrightarrow$1,
	leftrightarrows: leftrightarrows$1,
	leftrightharpoons: leftrightharpoons$1,
	leftrightsquigarrow: leftrightsquigarrow$1,
	leftthreetimes: leftthreetimes$1,
	leg: leg$1,
	leq: leq$1,
	leqq: leqq$1,
	leqslant: leqslant$1,
	les: les$1,
	lescc: lescc$1,
	lesdot: lesdot$1,
	lesdoto: lesdoto$1,
	lesdotor: lesdotor$1,
	lesg: lesg$1,
	lesges: lesges$1,
	lessapprox: lessapprox$1,
	lessdot: lessdot$1,
	lesseqgtr: lesseqgtr$1,
	lesseqqgtr: lesseqqgtr$1,
	lessgtr: lessgtr$1,
	lesssim: lesssim$1,
	lfisht: lfisht$1,
	lfr: lfr$1,
	lg: lg$1,
	lgE: lgE$1,
	lhard: lhard$1,
	lharu: lharu$1,
	lharul: lharul$1,
	lhblk: lhblk$1,
	ljcy: ljcy$1,
	ll: ll$1,
	llarr: llarr$1,
	llcorner: llcorner$1,
	llhard: llhard$1,
	lltri: lltri$1,
	lmidot: lmidot$1,
	lmoust: lmoust$1,
	lmoustache: lmoustache$1,
	lnE: lnE$1,
	lnap: lnap$1,
	lnapprox: lnapprox$1,
	lne: lne$1,
	lneq: lneq$1,
	lneqq: lneqq$1,
	lnsim: lnsim$1,
	loang: loang$1,
	loarr: loarr$1,
	lobrk: lobrk$1,
	longleftarrow: longleftarrow$1,
	longleftrightarrow: longleftrightarrow$1,
	longmapsto: longmapsto$1,
	longrightarrow: longrightarrow$1,
	looparrowleft: looparrowleft$1,
	looparrowright: looparrowright$1,
	lopar: lopar$1,
	lopf: lopf$1,
	loplus: loplus$1,
	lotimes: lotimes$1,
	lozenge: lozenge$1,
	lozf: lozf$1,
	lparlt: lparlt$1,
	lrarr: lrarr$1,
	lrcorner: lrcorner$1,
	lrhar: lrhar$1,
	lrhard: lrhard$1,
	lrtri: lrtri$1,
	lscr: lscr$1,
	lsh: lsh$1,
	lsim: lsim$1,
	lsime: lsime$1,
	lsimg: lsimg$1,
	lsquor: lsquor$1,
	lstrok: lstrok$1,
	ltcc: ltcc$1,
	ltcir: ltcir$1,
	ltdot: ltdot$1,
	lthree: lthree$1,
	ltimes: ltimes$1,
	ltlarr: ltlarr$1,
	ltquest: ltquest$1,
	ltrPar: ltrPar$1,
	ltri: ltri$1,
	ltrie: ltrie$1,
	ltrif: ltrif$1,
	lurdshar: lurdshar$1,
	luruhar: luruhar$1,
	lvertneqq: lvertneqq$1,
	lvnE: lvnE$1,
	mDDot: mDDot$1,
	male: male$1,
	malt: malt$1,
	maltese: maltese$1,
	map: map$1,
	mapsto: mapsto$1,
	mapstodown: mapstodown$1,
	mapstoleft: mapstoleft$1,
	mapstoup: mapstoup$1,
	marker: marker$1,
	mcomma: mcomma$1,
	mcy: mcy$1,
	measuredangle: measuredangle$1,
	mfr: mfr$1,
	mho: mho$1,
	mid: mid$1,
	midcir: midcir$1,
	minusb: minusb$1,
	minusd: minusd$1,
	minusdu: minusdu$1,
	mlcp: mlcp$1,
	mldr: mldr$1,
	mnplus: mnplus$1,
	models: models$1,
	mopf: mopf$1,
	mp: mp$1,
	mscr: mscr$1,
	mstpos: mstpos$1,
	multimap: multimap$1,
	mumap: mumap$1,
	nGg: nGg$1,
	nGt: nGt$1,
	nGtv: nGtv$1,
	nLeftarrow: nLeftarrow$1,
	nLeftrightarrow: nLeftrightarrow$1,
	nLl: nLl$1,
	nLt: nLt$1,
	nLtv: nLtv$1,
	nRightarrow: nRightarrow$1,
	nVDash: nVDash$1,
	nVdash: nVdash$1,
	nacute: nacute$1,
	nang: nang$1,
	nap: nap$1,
	napE: napE$1,
	napid: napid$1,
	napos: napos$1,
	napprox: napprox$1,
	natur: natur$1,
	natural: natural$1,
	naturals: naturals$1,
	nbump: nbump$1,
	nbumpe: nbumpe$1,
	ncap: ncap$1,
	ncaron: ncaron$1,
	ncedil: ncedil$1,
	ncong: ncong$1,
	ncongdot: ncongdot$1,
	ncup: ncup$1,
	ncy: ncy$1,
	neArr: neArr$1,
	nearhk: nearhk$1,
	nearr: nearr$1,
	nearrow: nearrow$1,
	nedot: nedot$1,
	nequiv: nequiv$1,
	nesear: nesear$1,
	nesim: nesim$1,
	nexist: nexist$1,
	nexists: nexists$1,
	nfr: nfr$1,
	ngE: ngE$1,
	nge: nge$1,
	ngeq: ngeq$1,
	ngeqq: ngeqq$1,
	ngeqslant: ngeqslant$1,
	nges: nges$1,
	ngsim: ngsim$1,
	ngt: ngt$1,
	ngtr: ngtr$1,
	nhArr: nhArr$1,
	nharr: nharr$1,
	nhpar: nhpar$1,
	nis: nis$1,
	nisd: nisd$1,
	niv: niv$1,
	njcy: njcy$1,
	nlArr: nlArr$1,
	nlE: nlE$1,
	nlarr: nlarr$1,
	nldr: nldr$1,
	nle: nle$1,
	nleftarrow: nleftarrow$1,
	nleftrightarrow: nleftrightarrow$1,
	nleq: nleq$1,
	nleqq: nleqq$1,
	nleqslant: nleqslant$1,
	nles: nles$1,
	nless: nless$1,
	nlsim: nlsim$1,
	nlt: nlt$1,
	nltri: nltri$1,
	nltrie: nltrie$1,
	nmid: nmid$1,
	nopf: nopf$1,
	notinE: notinE$1,
	notindot: notindot$1,
	notinva: notinva$1,
	notinvb: notinvb$1,
	notinvc: notinvc$1,
	notni: notni$1,
	notniva: notniva$1,
	notnivb: notnivb$1,
	notnivc: notnivc$1,
	npar: npar$1,
	nparallel: nparallel$1,
	nparsl: nparsl$1,
	npart: npart$1,
	npolint: npolint$1,
	npr: npr$1,
	nprcue: nprcue$1,
	npre: npre$1,
	nprec: nprec$1,
	npreceq: npreceq$1,
	nrArr: nrArr$1,
	nrarr: nrarr$1,
	nrarrc: nrarrc$1,
	nrarrw: nrarrw$1,
	nrightarrow: nrightarrow$1,
	nrtri: nrtri$1,
	nrtrie: nrtrie$1,
	nsc: nsc$1,
	nsccue: nsccue$1,
	nsce: nsce$1,
	nscr: nscr$1,
	nshortmid: nshortmid$1,
	nshortparallel: nshortparallel$1,
	nsim: nsim$1,
	nsime: nsime$1,
	nsimeq: nsimeq$1,
	nsmid: nsmid$1,
	nspar: nspar$1,
	nsqsube: nsqsube$1,
	nsqsupe: nsqsupe$1,
	nsubE: nsubE$1,
	nsube: nsube$1,
	nsubset: nsubset$1,
	nsubseteq: nsubseteq$1,
	nsubseteqq: nsubseteqq$1,
	nsucc: nsucc$1,
	nsucceq: nsucceq$1,
	nsup: nsup$1,
	nsupE: nsupE$1,
	nsupe: nsupe$1,
	nsupset: nsupset$1,
	nsupseteq: nsupseteq$1,
	nsupseteqq: nsupseteqq$1,
	ntgl: ntgl$1,
	ntlg: ntlg$1,
	ntriangleleft: ntriangleleft$1,
	ntrianglelefteq: ntrianglelefteq$1,
	ntriangleright: ntriangleright$1,
	ntrianglerighteq: ntrianglerighteq$1,
	numero: numero$1,
	numsp: numsp$1,
	nvDash: nvDash$1,
	nvHarr: nvHarr$1,
	nvap: nvap$1,
	nvdash: nvdash$1,
	nvge: nvge$1,
	nvgt: nvgt$1,
	nvinfin: nvinfin$1,
	nvlArr: nvlArr$1,
	nvle: nvle$1,
	nvlt: nvlt$1,
	nvltrie: nvltrie$1,
	nvrArr: nvrArr$1,
	nvrtrie: nvrtrie$1,
	nvsim: nvsim$1,
	nwArr: nwArr$1,
	nwarhk: nwarhk$1,
	nwarr: nwarr$1,
	nwarrow: nwarrow$1,
	nwnear: nwnear$1,
	oS: oS$1,
	oast: oast$1,
	ocir: ocir$1,
	ocy: ocy$1,
	odash: odash$1,
	odblac: odblac$1,
	odiv: odiv$1,
	odot: odot$1,
	odsold: odsold$1,
	ofcir: ofcir$1,
	ofr: ofr$1,
	ogon: ogon$1,
	ogt: ogt$1,
	ohbar: ohbar$1,
	ohm: ohm$1,
	oint: oint$1,
	olarr: olarr$1,
	olcir: olcir$1,
	olcross: olcross$1,
	olt: olt$1,
	omacr: omacr$1,
	omid: omid$1,
	ominus: ominus$1,
	oopf: oopf$1,
	opar: opar$1,
	operp: operp$1,
	orarr: orarr$1,
	ord: ord$1,
	order: order$1,
	orderof: orderof$1,
	origof: origof$1,
	oror: oror$1,
	orslope: orslope$1,
	orv: orv$1,
	oscr: oscr$1,
	osol: osol$1,
	otimesas: otimesas$1,
	ovbar: ovbar$1,
	par: par$1,
	parallel: parallel$1,
	parsim: parsim$1,
	parsl: parsl$1,
	pcy: pcy$1,
	pertenk: pertenk$1,
	pfr: pfr$1,
	phiv: phiv$1,
	phmmat: phmmat$1,
	phone: phone$1,
	pitchfork: pitchfork$1,
	planck: planck$1,
	planckh: planckh$1,
	plankv: plankv$1,
	plusacir: plusacir$1,
	plusb: plusb$1,
	pluscir: pluscir$1,
	plusdo: plusdo$1,
	plusdu: plusdu$1,
	pluse: pluse$1,
	plussim: plussim$1,
	plustwo: plustwo$1,
	pm: pm$1,
	pointint: pointint$1,
	popf: popf$1,
	pr: pr$1,
	prE: prE$1,
	prap: prap$1,
	prcue: prcue$1,
	pre: pre$1,
	prec: prec$1,
	precapprox: precapprox$1,
	preccurlyeq: preccurlyeq$1,
	preceq: preceq$1,
	precnapprox: precnapprox$1,
	precneqq: precneqq$1,
	precnsim: precnsim$1,
	precsim: precsim$1,
	primes: primes$1,
	prnE: prnE$1,
	prnap: prnap$1,
	prnsim: prnsim$1,
	profalar: profalar$1,
	profline: profline$1,
	profsurf: profsurf$1,
	propto: propto$1,
	prsim: prsim$1,
	prurel: prurel$1,
	pscr: pscr$1,
	puncsp: puncsp$1,
	qfr: qfr$1,
	qint: qint$1,
	qopf: qopf$1,
	qprime: qprime$1,
	qscr: qscr$1,
	quaternions: quaternions$1,
	quatint: quatint$1,
	questeq: questeq$1,
	rAarr: rAarr$1,
	rAtail: rAtail$1,
	rBarr: rBarr$1,
	rHar: rHar$1,
	race: race$1,
	racute: racute$1,
	raemptyv: raemptyv$1,
	rangd: rangd$1,
	range: range$1,
	rangle: rangle$1,
	rarrap: rarrap$1,
	rarrb: rarrb$1,
	rarrbfs: rarrbfs$1,
	rarrc: rarrc$1,
	rarrfs: rarrfs$1,
	rarrhk: rarrhk$1,
	rarrlp: rarrlp$1,
	rarrpl: rarrpl$1,
	rarrsim: rarrsim$1,
	rarrtl: rarrtl$1,
	rarrw: rarrw$1,
	ratail: ratail$1,
	ratio: ratio$1,
	rationals: rationals$1,
	rbarr: rbarr$1,
	rbbrk: rbbrk$1,
	rbrke: rbrke$1,
	rbrksld: rbrksld$1,
	rbrkslu: rbrkslu$1,
	rcaron: rcaron$1,
	rcedil: rcedil$1,
	rcy: rcy$1,
	rdca: rdca$1,
	rdldhar: rdldhar$1,
	rdquor: rdquor$1,
	rdsh: rdsh$1,
	realine: realine$1,
	realpart: realpart$1,
	reals: reals$1,
	rect: rect$1,
	rfisht: rfisht$1,
	rfr: rfr$1,
	rhard: rhard$1,
	rharu: rharu$1,
	rharul: rharul$1,
	rhov: rhov$1,
	rightarrow: rightarrow$1,
	rightarrowtail: rightarrowtail$1,
	rightharpoondown: rightharpoondown$1,
	rightharpoonup: rightharpoonup$1,
	rightleftarrows: rightleftarrows$1,
	rightleftharpoons: rightleftharpoons$1,
	rightrightarrows: rightrightarrows$1,
	rightsquigarrow: rightsquigarrow$1,
	rightthreetimes: rightthreetimes$1,
	ring: ring$1,
	risingdotseq: risingdotseq$1,
	rlarr: rlarr$1,
	rlhar: rlhar$1,
	rmoust: rmoust$1,
	rmoustache: rmoustache$1,
	rnmid: rnmid$1,
	roang: roang$1,
	roarr: roarr$1,
	robrk: robrk$1,
	ropar: ropar$1,
	ropf: ropf$1,
	roplus: roplus$1,
	rotimes: rotimes$1,
	rpargt: rpargt$1,
	rppolint: rppolint$1,
	rrarr: rrarr$1,
	rscr: rscr$1,
	rsh: rsh$1,
	rsquor: rsquor$1,
	rthree: rthree$1,
	rtimes: rtimes$1,
	rtri: rtri$1,
	rtrie: rtrie$1,
	rtrif: rtrif$1,
	rtriltri: rtriltri$1,
	ruluhar: ruluhar$1,
	rx: rx$1,
	sacute: sacute$1,
	sc: sc$1,
	scE: scE$1,
	scap: scap$1,
	sccue: sccue$1,
	sce: sce$1,
	scedil: scedil$1,
	scirc: scirc$1,
	scnE: scnE$1,
	scnap: scnap$1,
	scnsim: scnsim$1,
	scpolint: scpolint$1,
	scsim: scsim$1,
	scy: scy$1,
	sdotb: sdotb$1,
	sdote: sdote$1,
	seArr: seArr$1,
	searhk: searhk$1,
	searr: searr$1,
	searrow: searrow$1,
	seswar: seswar$1,
	setminus: setminus$1,
	setmn: setmn$1,
	sext: sext$1,
	sfr: sfr$1,
	sfrown: sfrown$1,
	sharp: sharp$1,
	shchcy: shchcy$1,
	shcy: shcy$1,
	shortmid: shortmid$1,
	shortparallel: shortparallel$1,
	sigmav: sigmav$1,
	simdot: simdot$1,
	sime: sime$1,
	simeq: simeq$1,
	simg: simg$1,
	simgE: simgE$1,
	siml: siml$1,
	simlE: simlE$1,
	simne: simne$1,
	simplus: simplus$1,
	simrarr: simrarr$1,
	slarr: slarr$1,
	smallsetminus: smallsetminus$1,
	smashp: smashp$1,
	smeparsl: smeparsl$1,
	smid: smid$1,
	smile: smile$1,
	smt: smt$1,
	smte: smte$1,
	smtes: smtes$1,
	softcy: softcy$1,
	solb: solb$1,
	solbar: solbar$1,
	sopf: sopf$1,
	spadesuit: spadesuit$1,
	spar: spar$1,
	sqcap: sqcap$1,
	sqcaps: sqcaps$1,
	sqcup: sqcup$1,
	sqcups: sqcups$1,
	sqsub: sqsub$1,
	sqsube: sqsube$1,
	sqsubset: sqsubset$1,
	sqsubseteq: sqsubseteq$1,
	sqsup: sqsup$1,
	sqsupe: sqsupe$1,
	sqsupset: sqsupset$1,
	sqsupseteq: sqsupseteq$1,
	squ: squ$1,
	square: square$1,
	squarf: squarf$1,
	squf: squf$1,
	srarr: srarr$1,
	sscr: sscr$1,
	ssetmn: ssetmn$1,
	ssmile: ssmile$1,
	sstarf: sstarf$1,
	star: star$1,
	starf: starf$1,
	straightepsilon: straightepsilon$1,
	straightphi: straightphi$1,
	strns: strns$1,
	subE: subE$1,
	subdot: subdot$1,
	subedot: subedot$1,
	submult: submult$1,
	subnE: subnE$1,
	subne: subne$1,
	subplus: subplus$1,
	subrarr: subrarr$1,
	subset: subset$1,
	subseteq: subseteq$1,
	subseteqq: subseteqq$1,
	subsetneq: subsetneq$1,
	subsetneqq: subsetneqq$1,
	subsim: subsim$1,
	subsub: subsub$1,
	subsup: subsup$1,
	succ: succ$1,
	succapprox: succapprox$1,
	succcurlyeq: succcurlyeq$1,
	succeq: succeq$1,
	succnapprox: succnapprox$1,
	succneqq: succneqq$1,
	succnsim: succnsim$1,
	succsim: succsim$1,
	sung: sung$1,
	supE: supE$1,
	supdot: supdot$1,
	supdsub: supdsub$1,
	supedot: supedot$1,
	suphsol: suphsol$1,
	suphsub: suphsub$1,
	suplarr: suplarr$1,
	supmult: supmult$1,
	supnE: supnE$1,
	supne: supne$1,
	supplus: supplus$1,
	supset: supset$1,
	supseteq: supseteq$1,
	supseteqq: supseteqq$1,
	supsetneq: supsetneq$1,
	supsetneqq: supsetneqq$1,
	supsim: supsim$1,
	supsub: supsub$1,
	supsup: supsup$1,
	swArr: swArr$1,
	swarhk: swarhk$1,
	swarr: swarr$1,
	swarrow: swarrow$1,
	swnwar: swnwar$1,
	target: target$1,
	tbrk: tbrk$1,
	tcaron: tcaron$1,
	tcedil: tcedil$1,
	tcy: tcy$1,
	tdot: tdot$1,
	telrec: telrec$1,
	tfr: tfr$1,
	therefore: therefore$1,
	thetav: thetav$1,
	thickapprox: thickapprox$1,
	thicksim: thicksim$1,
	thkap: thkap$1,
	thksim: thksim$1,
	timesb: timesb$1,
	timesbar: timesbar$1,
	timesd: timesd$1,
	tint: tint$1,
	toea: toea$1,
	top: top$1,
	topbot: topbot$1,
	topcir: topcir$1,
	topf: topf$1,
	topfork: topfork$1,
	tosa: tosa$1,
	tprime: tprime$1,
	triangle: triangle$1,
	triangledown: triangledown$1,
	triangleleft: triangleleft$1,
	trianglelefteq: trianglelefteq$1,
	triangleq: triangleq$1,
	triangleright: triangleright$1,
	trianglerighteq: trianglerighteq$1,
	tridot: tridot$1,
	trie: trie$1,
	triminus: triminus$1,
	triplus: triplus$1,
	trisb: trisb$1,
	tritime: tritime$1,
	trpezium: trpezium$1,
	tscr: tscr$1,
	tscy: tscy$1,
	tshcy: tshcy$1,
	tstrok: tstrok$1,
	twixt: twixt$1,
	twoheadleftarrow: twoheadleftarrow$1,
	twoheadrightarrow: twoheadrightarrow$1,
	uHar: uHar$1,
	ubrcy: ubrcy$1,
	ubreve: ubreve$1,
	ucy: ucy$1,
	udarr: udarr$1,
	udblac: udblac$1,
	udhar: udhar$1,
	ufisht: ufisht$1,
	ufr: ufr$1,
	uharl: uharl$1,
	uharr: uharr$1,
	uhblk: uhblk$1,
	ulcorn: ulcorn$1,
	ulcorner: ulcorner$1,
	ulcrop: ulcrop$1,
	ultri: ultri$1,
	umacr: umacr$1,
	uogon: uogon$1,
	uopf: uopf$1,
	uparrow: uparrow$1,
	updownarrow: updownarrow$1,
	upharpoonleft: upharpoonleft$1,
	upharpoonright: upharpoonright$1,
	uplus: uplus$1,
	upsi: upsi$1,
	upuparrows: upuparrows$1,
	urcorn: urcorn$1,
	urcorner: urcorner$1,
	urcrop: urcrop$1,
	uring: uring$1,
	urtri: urtri$1,
	uscr: uscr$1,
	utdot: utdot$1,
	utilde: utilde$1,
	utri: utri$1,
	utrif: utrif$1,
	uuarr: uuarr$1,
	uwangle: uwangle$1,
	vArr: vArr$1,
	vBar: vBar$1,
	vBarv: vBarv$1,
	vDash: vDash$1,
	vangrt: vangrt$1,
	varepsilon: varepsilon$1,
	varkappa: varkappa$1,
	varnothing: varnothing$1,
	varphi: varphi$1,
	varpi: varpi$1,
	varpropto: varpropto$1,
	varr: varr$1,
	varrho: varrho$1,
	varsigma: varsigma$1,
	varsubsetneq: varsubsetneq$1,
	varsubsetneqq: varsubsetneqq$1,
	varsupsetneq: varsupsetneq$1,
	varsupsetneqq: varsupsetneqq$1,
	vartheta: vartheta$1,
	vartriangleleft: vartriangleleft$1,
	vartriangleright: vartriangleright$1,
	vcy: vcy$1,
	vdash: vdash$1,
	vee: vee$1,
	veebar: veebar$1,
	veeeq: veeeq$1,
	vellip: vellip$1,
	vfr: vfr$1,
	vltri: vltri$1,
	vnsub: vnsub$1,
	vnsup: vnsup$1,
	vopf: vopf$1,
	vprop: vprop$1,
	vrtri: vrtri$1,
	vscr: vscr$1,
	vsubnE: vsubnE$1,
	vsubne: vsubne$1,
	vsupnE: vsupnE$1,
	vsupne: vsupne$1,
	vzigzag: vzigzag$1,
	wcirc: wcirc$1,
	wedbar: wedbar$1,
	wedge: wedge$1,
	wedgeq: wedgeq$1,
	wfr: wfr$1,
	wopf: wopf$1,
	wp: wp$1,
	wr: wr$1,
	wreath: wreath$1,
	wscr: wscr$1,
	xcap: xcap$1,
	xcirc: xcirc$1,
	xcup: xcup$1,
	xdtri: xdtri$1,
	xfr: xfr$1,
	xhArr: xhArr$1,
	xharr: xharr$1,
	xlArr: xlArr$1,
	xlarr: xlarr$1,
	xmap: xmap$1,
	xnis: xnis$1,
	xodot: xodot$1,
	xopf: xopf$1,
	xoplus: xoplus$1,
	xotime: xotime$1,
	xrArr: xrArr$1,
	xrarr: xrarr$1,
	xscr: xscr$1,
	xsqcup: xsqcup$1,
	xuplus: xuplus$1,
	xutri: xutri$1,
	xvee: xvee$1,
	xwedge: xwedge$1,
	yacy: yacy$1,
	ycirc: ycirc$1,
	ycy: ycy$1,
	yfr: yfr$1,
	yicy: yicy$1,
	yopf: yopf$1,
	yscr: yscr$1,
	yucy: yucy$1,
	zacute: zacute$1,
	zcaron: zcaron$1,
	zcy: zcy$1,
	zdot: zdot$1,
	zeetrf: zeetrf$1,
	zfr: zfr$1,
	zhcy: zhcy$1,
	zigrarr: zigrarr$1,
	zopf: zopf$1,
	zscr: zscr$1
};

var ac$2 = {
	addAmpIfSemiPresent: "edge only",
	addSemiIfAmpPresent: false
};
var acute$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Alpha$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var alpha$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var amp$1 = {
	addAmpIfSemiPresent: "edge only",
	addSemiIfAmpPresent: true
};
var And$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: true
};
var and$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: true
};
var ange$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: true
};
var angle$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var angst$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ap$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ape$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var approx$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Aring$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var aring$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Ascr$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ascr$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Assign$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ast$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var atilde$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: true
};
var Backslash$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var barwedge$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var becaus$2 = {
	addAmpIfSemiPresent: true,
	addSemiIfAmpPresent: "edge only"
};
var Because$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var because$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var bepsi$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Bernoullis$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Beta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var beta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var beth$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var between$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var blank$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var block$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var bot$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var bottom$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var bowtie$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var breve$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var bull$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var bullet$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var bump$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var cacute$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Cap$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var cap$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var capand$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var caps$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var caret$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var caron$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var cedil$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Cedilla$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var cent$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: true
};
var check$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var checkmark$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Chi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var chi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var cir$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var circ$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var clubs$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var clubsuit$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Colon$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var colon$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Colone$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var colone$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var comma$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var commat$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var comp$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var complement$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var complexes$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var cong$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Congruent$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var conint$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var copf$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var coprod$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var COPY$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var copy$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Cross$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var cross$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Cup$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var cup$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var cups$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Dagger$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var dagger$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var daleth$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var darr$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var dash$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var DD$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var dd$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var deg$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Del$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Delta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var delta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var dharr$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var diam$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Diamond$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var diamond$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var diams$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var die$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var digamma$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var disin$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var div$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var divide$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var dollar$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var dopf$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Dot$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var dot$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var dsol$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var dtri$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var easter$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var ecir$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ecolon$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ecy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var edot$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ee$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var efr$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var eg$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var egrave$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var egs$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var el$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ell$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var els$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var empty$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var ENG$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var eng$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var epsi$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Epsilon$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var epsilon$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Equal$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var equals$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var equest$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Equilibrium$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var equiv$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var escr$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var esim$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Eta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var eta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ETH$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var eth$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var euro$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var excl$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var exist$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Exists$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var expectation$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var female$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var flat$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var fork$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var frown$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Gamma$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var gamma$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var gap$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var gcy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ge$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var gel$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var geq$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ges$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var gesl$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var gg$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var gl$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var gla$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var gne$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var grave$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var GT$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var gt$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var half$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Hat$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var hearts$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var hopf$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var hyphen$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var ic$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var icy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var iff$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ii$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var image$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var imped$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var int$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var integers$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var iocy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var iogon$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var iota$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var isin$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var it$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Kappa$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var kappa$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var kopf$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Lambda$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lambda$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lang$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lap$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lat$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var late$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lates$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var le$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var leg$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var leq$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var les$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lg$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ll$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var lne$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var lozenge$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lsh$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var LT$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var lt$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ltimes$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: true
};
var male$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var malt$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var map$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var marker$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var mid$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var minus$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var models$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var mp$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var mu$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var nang$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nap$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var natural$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var naturals$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var ncy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ne$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nge$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ngt$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ni$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nis$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nle$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nles$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nless$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nlt$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nopf$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Not$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var not$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var nsc$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nsce$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var nu$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var num$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var ogt$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ohm$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var oline$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var olt$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Omega$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var omega$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Omicron$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var omicron$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var oopf$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var opar$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var or$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var order$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var oror$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var orv$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var osol$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var par$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var para$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var parallel$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var part$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var phi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var phone$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Pi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var pi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var pitchfork$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var plus$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var pm$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var popf$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var pound$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var pr$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var prime$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var primes$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var prod$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Product$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var prop$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Proportion$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Proportional$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var psi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var quest$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var QUOT$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var quot$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var race$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var rang$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var range$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var ratio$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Re$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var real$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var reals$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var rect$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var REG$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: true
};
var reg$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ring$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var rsh$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var sc$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var scap$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var sce$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var scy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var sdot$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var sect$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var semi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var sharp$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var shy$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Sigma$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var sigma$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var sim$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var sol$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var spades$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var square$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Star$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var star$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Sub$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var sub$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var sube$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Sum$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var sum$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Tab$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var target$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Tau$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var tau$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var therefore$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Theta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var theta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var THORN$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var thorn$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Tilde$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var tilde$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var times$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var tint$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var top$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var tosa$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var TRADE$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var trade$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var triangle$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var trie$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ucy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var uml$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Union$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var uplus$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Upsi$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var upsi$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var uring$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var vee$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Vert$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var vert$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var wedge$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Wedge$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var wreath$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Xi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var xi$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Ycirc$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ycirc$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var ycy$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var yen$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var Zacute$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var zacute$2 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: "edge only"
};
var Zeta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var zeta$1 = {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
};
var uncertain = {
	ac: ac$2,
	acute: acute$1,
	Alpha: Alpha$1,
	alpha: alpha$1,
	amp: amp$1,
	And: And$2,
	and: and$1,
	ange: ange$2,
	angle: angle$2,
	angst: angst$2,
	ap: ap$2,
	ape: ape$2,
	approx: approx$2,
	Aring: Aring$1,
	aring: aring$1,
	Ascr: Ascr$2,
	ascr: ascr$2,
	Assign: Assign$2,
	ast: ast$1,
	atilde: atilde$1,
	Backslash: Backslash$2,
	barwedge: barwedge$2,
	becaus: becaus$2,
	Because: Because$2,
	because: because$2,
	bepsi: bepsi$2,
	Bernoullis: Bernoullis$2,
	Beta: Beta$1,
	beta: beta$1,
	beth: beth$2,
	between: between$2,
	blank: blank$2,
	block: block$2,
	bot: bot$2,
	bottom: bottom$2,
	bowtie: bowtie$2,
	breve: breve$2,
	bull: bull$1,
	bullet: bullet$2,
	bump: bump$2,
	cacute: cacute$2,
	Cap: Cap$2,
	cap: cap$1,
	capand: capand$2,
	caps: caps$2,
	caret: caret$2,
	caron: caron$2,
	cedil: cedil$1,
	Cedilla: Cedilla$2,
	cent: cent$1,
	check: check$2,
	checkmark: checkmark$2,
	Chi: Chi$1,
	chi: chi$1,
	cir: cir$2,
	circ: circ$1,
	clubs: clubs$1,
	clubsuit: clubsuit$2,
	Colon: Colon$2,
	colon: colon$1,
	Colone: Colone$2,
	colone: colone$2,
	comma: comma$1,
	commat: commat$1,
	comp: comp$2,
	complement: complement$2,
	complexes: complexes$2,
	cong: cong$1,
	Congruent: Congruent$2,
	conint: conint$2,
	copf: copf$2,
	coprod: coprod$2,
	COPY: COPY$2,
	copy: copy$1,
	Cross: Cross$2,
	cross: cross$2,
	Cup: Cup$2,
	cup: cup$1,
	cups: cups$2,
	Dagger: Dagger$1,
	dagger: dagger$1,
	daleth: daleth$2,
	darr: darr$1,
	dash: dash$2,
	DD: DD$2,
	dd: dd$2,
	deg: deg$1,
	Del: Del$2,
	Delta: Delta$1,
	delta: delta$1,
	dharr: dharr$2,
	diam: diam$2,
	Diamond: Diamond$2,
	diamond: diamond$2,
	diams: diams$1,
	die: die$2,
	digamma: digamma$2,
	disin: disin$2,
	div: div$2,
	divide: divide$1,
	dollar: dollar$1,
	dopf: dopf$2,
	Dot: Dot$2,
	dot: dot$2,
	dsol: dsol$2,
	dtri: dtri$2,
	easter: easter$2,
	ecir: ecir$2,
	ecolon: ecolon$2,
	ecy: ecy$2,
	edot: edot$2,
	ee: ee$2,
	efr: efr$2,
	eg: eg$2,
	egrave: egrave$1,
	egs: egs$2,
	el: el$2,
	ell: ell$2,
	els: els$2,
	empty: empty$1,
	ENG: ENG$2,
	eng: eng$2,
	epsi: epsi$2,
	Epsilon: Epsilon$1,
	epsilon: epsilon$1,
	Equal: Equal$2,
	equals: equals$1,
	equest: equest$2,
	Equilibrium: Equilibrium$2,
	equiv: equiv$1,
	escr: escr$2,
	esim: esim$2,
	Eta: Eta$1,
	eta: eta$1,
	ETH: ETH$1,
	eth: eth$1,
	euro: euro$1,
	excl: excl$1,
	exist: exist$1,
	Exists: Exists$2,
	expectation: expectation$2,
	female: female$2,
	flat: flat$2,
	fork: fork$2,
	frown: frown$2,
	Gamma: Gamma$1,
	gamma: gamma$1,
	gap: gap$2,
	gcy: gcy$2,
	ge: ge$1,
	gel: gel$2,
	geq: geq$2,
	ges: ges$2,
	gesl: gesl$2,
	gg: gg$2,
	gl: gl$2,
	gla: gla$2,
	gne: gne$2,
	grave: grave$2,
	GT: GT$2,
	gt: gt$1,
	half: half$2,
	Hat: Hat$1,
	hearts: hearts$1,
	hopf: hopf$2,
	hyphen: hyphen$2,
	ic: ic$2,
	icy: icy$2,
	iff: iff$2,
	ii: ii$2,
	image: image$1,
	imped: imped$2,
	"in": {
	addAmpIfSemiPresent: false,
	addSemiIfAmpPresent: false
},
	int: int$1,
	integers: integers$2,
	iocy: iocy$2,
	iogon: iogon$2,
	iota: iota$1,
	isin: isin$1,
	it: it$2,
	Kappa: Kappa$1,
	kappa: kappa$1,
	kopf: kopf$2,
	Lambda: Lambda$1,
	lambda: lambda$1,
	lang: lang$1,
	lap: lap$2,
	lat: lat$2,
	late: late$2,
	lates: lates$2,
	le: le$1,
	leg: leg$2,
	leq: leq$2,
	les: les$2,
	lg: lg$2,
	ll: ll$2,
	lne: lne$2,
	lozenge: lozenge$2,
	lsh: lsh$2,
	LT: LT$2,
	lt: lt$1,
	ltimes: ltimes$2,
	male: male$2,
	malt: malt$2,
	map: map$2,
	marker: marker$2,
	mid: mid$2,
	minus: minus$1,
	models: models$2,
	mp: mp$2,
	mu: mu$1,
	nang: nang$2,
	nap: nap$2,
	natural: natural$2,
	naturals: naturals$2,
	ncy: ncy$2,
	ne: ne$1,
	nge: nge$2,
	ngt: ngt$2,
	ni: ni$1,
	nis: nis$2,
	nle: nle$2,
	nles: nles$2,
	nless: nless$2,
	nlt: nlt$2,
	nopf: nopf$2,
	Not: Not$2,
	not: not$1,
	nsc: nsc$2,
	nsce: nsce$2,
	nu: nu$1,
	num: num$1,
	ogt: ogt$2,
	ohm: ohm$2,
	oline: oline$1,
	olt: olt$2,
	Omega: Omega$1,
	omega: omega$1,
	Omicron: Omicron$1,
	omicron: omicron$1,
	oopf: oopf$2,
	opar: opar$2,
	or: or$1,
	order: order$2,
	oror: oror$2,
	orv: orv$2,
	osol: osol$2,
	par: par$2,
	para: para$1,
	parallel: parallel$2,
	part: part$1,
	phi: phi$1,
	phone: phone$2,
	Pi: Pi$1,
	pi: pi$1,
	pitchfork: pitchfork$2,
	plus: plus$1,
	pm: pm$2,
	popf: popf$2,
	pound: pound$1,
	pr: pr$2,
	prime: prime$1,
	primes: primes$2,
	prod: prod$1,
	Product: Product$2,
	prop: prop$1,
	Proportion: Proportion$2,
	Proportional: Proportional$2,
	psi: psi$1,
	quest: quest$1,
	QUOT: QUOT$2,
	quot: quot$1,
	race: race$2,
	rang: rang$1,
	range: range$2,
	ratio: ratio$2,
	Re: Re$2,
	real: real$1,
	reals: reals$2,
	rect: rect$2,
	REG: REG$2,
	reg: reg$1,
	ring: ring$2,
	rsh: rsh$2,
	sc: sc$2,
	scap: scap$2,
	sce: sce$2,
	scy: scy$2,
	sdot: sdot$1,
	sect: sect$1,
	semi: semi$1,
	sharp: sharp$2,
	shy: shy$1,
	Sigma: Sigma$1,
	sigma: sigma$1,
	sim: sim$1,
	sol: sol$1,
	spades: spades$1,
	square: square$2,
	Star: Star$2,
	star: star$2,
	Sub: Sub$2,
	sub: sub$1,
	sube: sube$1,
	Sum: Sum$2,
	sum: sum$1,
	Tab: Tab$2,
	target: target$2,
	Tau: Tau$1,
	tau: tau$1,
	therefore: therefore$2,
	Theta: Theta$1,
	theta: theta$1,
	THORN: THORN$1,
	thorn: thorn$1,
	Tilde: Tilde$2,
	tilde: tilde$1,
	times: times$1,
	tint: tint$2,
	top: top$2,
	tosa: tosa$2,
	TRADE: TRADE$2,
	trade: trade$1,
	triangle: triangle$2,
	trie: trie$2,
	ucy: ucy$2,
	uml: uml$1,
	Union: Union$2,
	uplus: uplus$2,
	Upsi: Upsi$2,
	upsi: upsi$2,
	uring: uring$2,
	vee: vee$2,
	Vert: Vert$2,
	vert: vert$1,
	wedge: wedge$2,
	Wedge: Wedge$2,
	wreath: wreath$2,
	Xi: Xi$1,
	xi: xi$1,
	Ycirc: Ycirc$2,
	ycirc: ycirc$2,
	ycy: ycy$2,
	yen: yen$1,
	Zacute: Zacute$2,
	zacute: zacute$2,
	Zeta: Zeta$1,
	zeta: zeta$1
};

function decode(ent) {
  if (
    typeof ent !== "string" ||
    !ent.length ||
    !ent.startsWith("&") ||
    !ent.endsWith(";")
  ) {
    throw new Error(
      `all-named-html-entities/decode(): [THROW_ID_01] Input must be an HTML entity with leading ampersand and trailing semicolon, but "${ent}" was given`
    );
  }
  const val = ent.slice(1, ent.length - 1);
  return allNamedEntities[val] ? allNamedEntities[val] : null;
}
const minLength = 2;
const maxLength = 31;

export { allNamedEntities, brokenNamedEntities, decode, endsWith as entEndsWith, endsWithCaseInsensitive as entEndsWithCaseInsensitive, startsWith as entStartsWith, startsWithCaseInsensitive as entStartsWithCaseInsensitive, maxLength, minLength, notEmailFriendly, uncertain };
